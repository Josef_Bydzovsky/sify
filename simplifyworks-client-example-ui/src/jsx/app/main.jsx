'use strict';
var SifyComponents = require('./component/index.jsx');
//window.AbstractFormComponent = SifyComponents.AbstractFormComponent;
window.BasicForm = SifyComponents.BasicForm;
window.FilterForm = SifyComponents.FilterForm;
window.InputText = SifyComponents.InputText;
window.TextArea = SifyComponents.TextArea;
window.CodeList = SifyComponents.CodeList;
window.FormTable = SifyComponents.FormTable;
window.MultiSelect = SifyComponents.MultiSelect;
window.Checkbox = SifyComponents.Checkbox;
window.Datepicker = SifyComponents.Datepicker;
window.Enumbox = SifyComponents.Enumbox;
window.FlashMessages = SifyComponents.FlashMessages;
window.BasicTable = SifyComponents.BasicTable;
window.ModalDialog = SifyComponents.ModalDialog.ModalDialog;
window.ModalDialogHeader = SifyComponents.ModalDialogHeader;
window.ModalDialogBody = SifyComponents.ModalDialogBody;
window.ModalDialogFooter = SifyComponents.ModalDialogFooter;
window.ModalDialogManager = SifyComponents.ModalDialogManager;
window.Attachment = SifyComponents.Attachment;
window.Dropzone = SifyComponents.Dropzone;
window.DragAndDrop = SifyComponents.DragAndDrop;
window.WorkflowForm = SifyComponents.WorkflowForm;
window.AdvancedTable = SifyComponents.AdvancedTable;

var SimplifyworksCore = require('simplifyworks-core');

/* Initialize Locales */
var packageDescriptor = require('../../../package.json');
var enabledModules = packageDescriptor.simplifyworks.modules;
enabledModules.push('app'); // last one wins - good for overriding some localisations in client app
l20n.initializeLocales(
    enabledModules,
    {
        'locales': ['en', 'cs'],
        'default': 'en'
    }
);

/* Initializing touch events */
React.initializeTouchEvents(true);

require('./preloader.jsx');

// configuring default options for Messenger
// http://github.hubspot.com/messenger/docs/welcome/
Messenger.options = {
    theme: 'flat'
};
//Auth.init();

var routes = require('./routes.jsx');
var Fluxxor = require('fluxxor');
/*
 *fluxxor
 */
var stores = {
    AuthStore: new SimplifyworksCore.CoreStores.AuthStore(),
    FlashStore: new SimplifyworksCore.CoreStores.FlashStore()
};
var actions = {
    auth: SimplifyworksCore.CoreActions.AuthActions,
    flash: SimplifyworksCore.CoreActions.FlashActions
};
var flux = new Fluxxor.Flux(stores, actions);

flux.on('dispatch', function(type, payload) {
    if (console && console.log) {
        console.log('[Dispatch client]', type, payload);
    }
});

Pace.once('hide', function () {
    $('#pace-loader').removeClass('pace-big').addClass('pace-small');
});


var InitializeRouter = function (View) {
    // cleanup
    if (window.Rubix) window.Rubix.Cleanup();
    Pace.restart();
    if (window.hasOwnProperty('ga') && typeof window.ga === 'function') {
        window.ga('send', 'pageview', {
            'page': window.location.pathname + window.location.search + window.location.hash
        });
    }

    React.render(<View flux={flux} />, document.getElementById('app-container'), function () {
        // l20n initialized only after everything is rendered/updated
        l20n.ready();
        setTimeout(function () {
            $('body').removeClass('fade-out');
        }, 500);
    });
};

// RT: entry point
if (Modernizr.history)
    ReactRouter.run(routes, ReactRouter.HistoryLocation, InitializeRouter);
else
    ReactRouter.run(routes, InitializeRouter);
