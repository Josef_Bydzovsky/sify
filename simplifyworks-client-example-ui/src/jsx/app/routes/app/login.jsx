var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var LoginForm = React.createClass({

    mixins: [React.addons.LinkedStateMixin, ReactRouter.State, ReactRouter.Navigation, FluxMixin, Fluxxor.StoreWatchMixin("AuthStore")],

    getInitialState() {
        return {
            username: '',
            password: '',
            placeholder: []
        };
    },

    getStateFromFlux() {
        return this.getFlux().store("AuthStore").getState();
    },

    componentWillMount() {
        ReactBootstrap.Dispatcher.on('ctx:ready', this.l20nContextReady);
        // TODO: https://github.com/gaearon/react-document-title ?
        document.title = 'Login::Simplifyworks';
        this.getFlux().store("FlashStore").clear('loginErrors');
    },

    componentWillUnmount() {
        ReactBootstrap.Dispatcher.off('ctx:ready', this.l20nContextReady);
    },

    componentDidMount() {

    },

    l20nContextReady() {
        // TODO: State is bad used - we need to register localization ready listener
        if(this.isMounted()) {
            this.setState({
                placeholder: {
                    username: l20n.ctx.getSync('user_username'),
                    password: l20n.ctx.getSync('user_password')
                }
            });
        }
    },

    login(e) {
        e.preventDefault();
        this.getFlux().actions.auth.login(
            this.state.username,
            this.state.password,
            function(userInfo) {
                this.getFlux().actions.flash.clear();
                this.getFlux().actions.flash.toast({
                    messageId: 'login-success',
                    type: 'success',
                    message: l20n.ctx.getSync('login_success', { user: userInfo.username })
                });
                this.transitionTo('/');
            }.bind(this),
            function(xhr, status, err) {
                switch(xhr.status) {
                    case 400: {
                        this.getFlux().actions.flash.add({
                            container: 'loginErrors',
                            messageId: 'error',
                            type: 'warning',
                            message: (<span><strong>{l20n.ctx.getSync('login_failed')}: </strong> {l20n.ctx.getSync('login_bad_credentials')}</span>)
                        });
                        break;
                    }
                    default: {
                        this.getFlux().actions.flash.ajaxError(xhr, status, err);
                    }
                }
            }.bind(this)
        );
    },

    render() {
        return (
            <Form>
                <PanelContainer noControls bordered>
                    <Panel>

                        <PanelHeader className='bg-primary fg-white'>
                            <Grid>
                                <Row>
                                    <Col xs={12}>
                                        <h3>
                                            <Entity entity='page_login' data={{message: 'panel_title'}}/>
                                        </h3>
                                    </Col>
                                </Row>
                            </Grid>
                        </PanelHeader>

                        <PanelBody>
                            <Grid>
                                <Row>
                                    <Col xs={12}>
                                        <FlashMessages id="loginErrors"/>
                                        <FormGroup>
                                            <InputGroup lg>
                                                <InputGroupAddon>
                                                    <Icon glyph='icon-fontello-user'/>
                                                </InputGroupAddon>
                                                <Input autoFocus
                                                       type='text'
                                                       id='username'
                                                       valueLink={this.linkState('username')}
                                                       placeholder={this.state.placeholder.username}/>
                                            </InputGroup>
                                        </FormGroup>
                                        <FormGroup>
                                            <InputGroup lg>
                                                <InputGroupAddon>
                                                    <Icon glyph='icon-fontello-key'/>
                                                </InputGroupAddon>
                                                <Input type='password'
                                                       id='password'
                                                       valueLink={this.linkState('password')}
                                                       placeholder={this.state.placeholder.password}/>
                                            </InputGroup>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </Grid>
                        </PanelBody>

                        <PanelFooter>
                            <Grid>
                                <Row>
                                    <Col xs={12} className='text-right'>
                                        <Button lg type='submit' bsStyle='primary' onClick={this.login}>
                                            <Entity entity='page_login' data={{message: 'button_login'}}/>
                                        </Button>
                                    </Col>
                                </Row>
                            </Grid>
                        </PanelFooter>

                    </Panel>
                </PanelContainer>
            </Form>
        );
    }
});


var Body = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    back(e) {
        e.preventDefault();
        e.stopPropagation();
        this.transitionTo('/');
    },

    componentDidMount() {
        $('html').addClass('authentication');
    },

    componentWillUnmount() {
        $('html').removeClass('authentication');
    },

    render() {
        return (
            <Container id='auth-container' className='login'>
                <Container id='auth-row'>
                    <Container id='auth-cell'>
                        <Grid>
                            <Row>
                                <Col sm={12}>
                                    <LoginForm/>
                                </Col>
                            </Row>
                        </Grid>
                    </Container>
                </Container>
            </Container>
        );
    }
});

var classSet = React.addons.classSet;
var LoginPage = React.createClass({

    mixins: [SidebarMixin, FluxMixin],

    render() {
        var classes = classSet({
            'container-open': this.state.open
        });
        return (
            <Container id='container' className={classes}>
                <Body />
            </Container>
        );
    }
});

module.exports = LoginPage;
