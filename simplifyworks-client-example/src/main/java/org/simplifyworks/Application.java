/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

/**
 * Vstupni bod aplikace
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		application.showBanner(false);
		return application.sources(Application.class);
	}

	/**
	 * TODO: Move to core
	 * @return 
	 */
	@Bean
	public Mapper mapper() {
		return new DozerBeanMapper();
	}
}
