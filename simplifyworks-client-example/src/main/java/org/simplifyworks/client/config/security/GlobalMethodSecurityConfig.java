package org.simplifyworks.client.config.security;

import org.simplifyworks.uam.service.PermissionEvaluator;
import org.simplifyworks.uam.service.impl.CorePermissionEvaluatorImpl;
import org.simplifyworks.uam.service.impl.CoreRoleHierarchyImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.vote.RoleHierarchyVoter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

/**
 *
 * @author Svanda
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class GlobalMethodSecurityConfig extends GlobalMethodSecurityConfiguration {

	@Override
	protected MethodSecurityExpressionHandler createExpressionHandler() {
		DefaultMethodSecurityExpressionHandler methodSecurityExpressionHandler = new DefaultMethodSecurityExpressionHandler();
		methodSecurityExpressionHandler.setRoleHierarchy(roleHierarchy());
		methodSecurityExpressionHandler.setPermissionEvaluator(permissionEvaluator());
		return methodSecurityExpressionHandler;
	}

	@Bean
	public PermissionEvaluator permissionEvaluator() {
		return new CorePermissionEvaluatorImpl();
	}

	@Bean
	public RoleHierarchyVoter roleVoter() {
		RoleHierarchyVoter roleHierarchyVoter = new RoleHierarchyVoter(roleHierarchy());
		return roleHierarchyVoter;
	}

	@Bean
	/**
	 * Load composition roles from database
	 */
	public RoleHierarchy roleHierarchy() {
		return new CoreRoleHierarchyImpl();
	}

}
