/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.client.config.security;

import org.simplifyworks.uam.service.impl.RoleOrganizationAuthenticationConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * TODO: V budoucnu je mozne rozdelit na dva servery resource / authorization
 *
 * Doku:
 * http://spring.io/guides/tutorials/bookmarks/
 * http://projects.spring.io/spring-security-oauth/docs/oauth2.html
 * http://jaxenter.com/rest-api-spring-java-8-112289.html
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Configuration
@EnableAuthorizationServer
public class OAuth2Configuration extends AuthorizationServerConfigurerAdapter {

    private String applicationName = "sify";
    // This is required for password grants, which we specify below as one of the
    // {@literal authorizedGrantTypes()}.
    @Autowired
    AuthenticationManagerBuilder authenticationManager;

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        // Workaround for https://github.com/spring-projects/spring-boot/issues/1801
        endpoints
                .tokenEnhancer(tokenEnhancer())
                .tokenStore(tokenStore())
                .authenticationManager((Authentication authentication) -> {
                    return authenticationManager.getOrBuild().authenticate(authentication);
                });
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients
                .inMemory()
                .withClient(applicationName)
                .authorizedGrantTypes("password", "authorization_code", "refresh_token")
                .authorities("admin", "user") // is it needed here?
                .scopes("read", "write")
                .resourceIds("oauth2-resource")
                .secret("password");
    }

    @Bean
    public JwtAccessTokenConverter tokenEnhancer() {
        final JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        ((DefaultAccessTokenConverter) jwtAccessTokenConverter.getAccessTokenConverter()).setUserTokenConverter(new RoleOrganizationAuthenticationConverter());
        // TODO: keystore - https://github.com/spring-projects/spring-security-oauth/issues/186
        jwtAccessTokenConverter.setSigningKey("sify-jwt-pass");
        return jwtAccessTokenConverter;
    }

    @Bean
    public JwtTokenStore tokenStore() {
        return new JwtTokenStore(tokenEnhancer());
    }

    @Bean
    public DefaultTokenServices defaultTokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenEnhancer(tokenEnhancer());
        defaultTokenServices.setTokenStore(tokenStore());
        return defaultTokenServices;
    }
}
