/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.client.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * Rest api security
 * TODO: http://projects.spring.io/spring-security-oauth/docs/oauth2.html
 * 
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Configuration
@EnableResourceServer
public class ResourceServer extends ResourceServerConfigurerAdapter {
	
	@Autowired
	AuthenticationManagerBuilder authenticationManager;
    @Autowired
    TokenStore tokenStore;

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.requestMatchers().antMatchers("/api/**")
						.and()
						.authorizeRequests()
						.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
						.antMatchers(HttpMethod.GET, "/**").access("#oauth2.hasScope('read')")
						.antMatchers(HttpMethod.PATCH, "/**").access("#oauth2.hasScope('write')")
						.antMatchers(HttpMethod.POST, "/**").access("#oauth2.hasScope('write')")
						.antMatchers(HttpMethod.PUT, "/**").access("#oauth2.hasScope('write')")
						.antMatchers(HttpMethod.DELETE, "/**").access("#oauth2.hasScope('write')");
	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.resourceId("oauth2-resource");
        resources.tokenStore(tokenStore);
	}

}
