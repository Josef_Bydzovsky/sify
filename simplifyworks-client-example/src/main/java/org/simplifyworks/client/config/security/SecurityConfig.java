package org.simplifyworks.client.config.security;

import org.simplifyworks.security.service.impl.DefaultUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Nastaveni zabezpeceni
 * <p>
 * TODO: konfigurace pristupu k zdrojum v http TODO:
 * http://stackoverflow.com/questions/28197941/spring-oauth2-not-redirecting-back-to-client-on-form-login
 * TODO:
 * http://cscarioni.blogspot.cz/2013/04/pro-spring-security-and-oauth-2.html
 * TODO:
 * http://porterhead.blogspot.cz/2014/05/securing-rest-services-with-spring.html
 * TODO"
 * https://auth0.com/blog/2014/01/07/angularjs-authentication-with-cookies-vs-token/
 * <p>
 * Client TODO:
 * https://auth0.com/blog/2015/04/09/adding-authentication-to-your-react-flux-app/
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        // @formatter:off
        http
            .httpBasic()
            .and()
                .authorizeRequests()
                .antMatchers("/", "/j_spring_security_check", "/error", "/oauth/**", "/api/**").permitAll()
                .antMatchers("/view/**").fullyAuthenticated()
                .antMatchers("/admin/**").access("hasRole('ADMIN')")
                .anyRequest().authenticated()
            .and()
                .formLogin()
                .failureUrl("/login-error")
                .loginPage("/login").permitAll()
            .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET"))
                .logoutSuccessUrl("/logout-success")
                .deleteCookies("remember-me")
                .permitAll()
            .and()
                .rememberMe();
        // @formatter:on
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authenticationService()).passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
            .ignoring()
            .antMatchers("/js/**", "/css/**", "/img/**", "/libs/**", "/favicons/**"); // TODO: remove api/event
    }

    @Bean
    public UserDetailsService authenticationService() {
        return new DefaultUserDetailsService();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityContextRepository securityContextRepository() {
        return new HttpSessionSecurityContextRepository();
    }
}
