
package org.simplifyworks.client.config.workflow;

import com.google.common.collect.Lists;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.impl.bpmn.parser.factory.ActivityBehaviorFactory;
import org.activiti.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.simplifyworks.workflow.domain.CustomActivityBehaviorFactory;
import org.simplifyworks.workflow.domain.WorkflowListener;
import org.simplifyworks.workflow.service.WorkflowService;
import org.simplifyworks.workflow.service.impl.WorkflowServiceImpl;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Workflow
 */
@Configuration
public class WorkflowConfig {

    @Autowired
    ProcessEngineConfiguration processEngineConfiguration;

    @Bean
    public ActivityBehaviorFactory activityBehaviorFactory() {
        CustomActivityBehaviorFactory customActivityBehaviorFactory = new CustomActivityBehaviorFactory();
        //We have to set expressionManager for evaluate expression in workflow
        customActivityBehaviorFactory.setExpressionManager(((SpringProcessEngineConfiguration)processEngineConfiguration).getExpressionManager());
        //Set custom activity behavior ... for catch email
        ((SpringProcessEngineConfiguration)processEngineConfiguration).getBpmnParser().setActivityBehaviorFactory(customActivityBehaviorFactory);
        return customActivityBehaviorFactory;
    }



    @Bean
    public WorkflowListener workflowListener() {
        return new WorkflowListener();
    }

}
