/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.client.web.controller;

import org.simplifyworks.core.service.ElasticsearchAdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Project create and config test
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
@Controller
public class LoginController extends BaseController {

	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private ConsumerTokenServices defaultTokenServices;
	
	@Autowired
	private ElasticsearchAdminService elasticsearchAdminService;

	@ModelAttribute("page")
	public String module() {
		return "login";
	}

	@RequestMapping("/login")
	public String login() {
		return module();
	}

	@RequestMapping("/logout-success")
	public String logout(RedirectAttributes redirectAttributes) {
		return "redirect:/" + module();
	}

	@RequestMapping("/login-error")
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		return module();
	}

	/**
	 * Remove active token from oauth
	 * @param token
	 */
	@ResponseBody
	@RequestMapping(value = "/oauth/token/revoke", method = RequestMethod.POST)
	public void revoke(@RequestParam(value = "token") String token) {
		defaultTokenServices.revokeToken(token);
	}
	
	// TODO move
	@ResponseBody
	@RequestMapping(value = "/oauth/reindex", method = RequestMethod.GET)
	public void reindex() throws Exception {
		elasticsearchAdminService.reindexAll(); 
	}
}
