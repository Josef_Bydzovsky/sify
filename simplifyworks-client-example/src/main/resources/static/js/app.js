$(function () {
	
	$('.message .close').on('click', function () {
		$(this).closest('.message').fadeOut();
	});

	$('.ui.dropdown').dropdown();

	$('.ui.checkbox').checkbox();

	$('.menu .item').tab();

	$('.waiting-show').click(function () {
		$('.main-loading').addClass('active');
	});

	$('.not-implemented').click(function () {
		$('.basic.modal').modal('show');
		return false;
	});

	$('.give-show').click(function () {
		$('.give.modal')
						.modal({
							onApprove: function () {
								return false;
							}
						})
						.modal('show');
		return false;
	});


//	var client;
//	var stomp;
//	var initiator = false; // ui = true
//
//	function notify(message) {
//		if (!initiator) {
//			$.ajax({
//				url: window.location.href,
//				context: document.body
//			}).done(function (html) {
//				$('#main-content').html($(html).find('#main-content'));
//			});
//		}
//		initiator = false;
//	}
//	;
//
//	function reconnect() {
//		setTimeout(initSockets, 10000);
//	}
//
//	function initSockets() {
//		client = new SockJS('/simplifyworks/notify');
//		stomp = Stomp.over(client);
//		stomp.connect({}, function () {
//			stomp.subscribe("/topic/notify", notify);
//		});
//		client.onclose = reconnect;
//	}
//	;
//
//	initSockets();

});