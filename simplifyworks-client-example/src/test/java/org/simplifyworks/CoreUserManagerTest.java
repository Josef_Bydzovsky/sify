/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Locale;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonDto;
import org.simplifyworks.uam.service.CoreUserWithPersonManager;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html
 * http://www.jayway.com/2014/07/04/integration-testing-a-spring-boot-application/
 * http://g00glen00b.be/spring-boot-rest-assured/
 * http://www.vogella.com/tutorials/Mockito/article.html
 *
 * TODO: Zatim navazano na konfiguraci aplikace - prehodit na test context
 *
 * TODO: hierarchie testu - testy manageru prenest do jednotlivych balicku
 *
 * TODO: Prenest unit test defaultnich manageru z jenero
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class})
@WebAppConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles("test")
@TestExecutionListeners(inheritListeners = false, listeners = {
	DependencyInjectionTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class})
public class CoreUserManagerTest extends AbstractTestNGSpringContextTests {

	private static final Logger LOG = LoggerFactory.getLogger(CoreUserManagerTest.class);
	@Autowired
	private CoreUserWithPersonManager coreUserManager;
	@Autowired
	protected SecurityService securityService;
	//
	private CoreUserWithPersonDto tomiska;
	private CoreUserWithPersonDto svanda;
	private CoreUserWithPersonDto siroky;

	@BeforeClass
	public void setUp() {
		LOG.info("Running coreUserManager test");
		tomiska = new CoreUserWithPersonDto("tomiskangtestikxxx", "heslo".toCharArray());
		svanda = new CoreUserWithPersonDto("svandangtestikxxx", "heslo".toCharArray());
		siroky = new CoreUserWithPersonDto("sirokyngtestikxxx", "heslo".toCharArray());

		List<GrantedAuthority> roles = Lists.newArrayList();
		roles.add(new RoleOrganizationGrantedAuthority("admin", null));
		User user = new User("test", "password", roles);
		SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(user, "password", roles));

		coreUserManager.reindexAll();
		tomiska = prepareUser(tomiska);
		svanda = prepareUser(svanda);
		siroky = prepareUser(siroky);
	}

	private CoreUserWithPersonDto prepareUser(CoreUserWithPersonDto user) {
		CoreUserWithPersonDto u = coreUserManager.findByUsername(user.getUsername());
		if (u != null) {
			coreUserManager.remove(u.getId());
		}
		return coreUserManager.create(user);
	}

	@AfterClass
	public void cleanUp() {
		LOG.info("Finishing coreUserManager test");
		coreUserManager.remove(tomiska.getId());
		coreUserManager.remove(svanda.getId());
		coreUserManager.remove(siroky.getId());
	}

	@Test
	public void coreUserManagerImplTest() {
		LOG.info("Test [" + coreUserManager + "]");
		SearchParameters sp = new SearchParameters(Locale.getDefault());
		sp.clearFilters();
		sp.addFilter(new FilterValue("username", FilterOperator.EQUALS, Lists.newArrayList(tomiska.getUsername(), svanda.getUsername(), siroky.getUsername())));
		Assert.assertEquals(3, coreUserManager.search(sp).size());
	}
}
