package org.simplifyworks;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class})
@WebAppConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles("test")
@TestExecutionListeners(inheritListeners = false, listeners = {
       DependencyInjectionTestExecutionListener.class,
       DirtiesContextTestExecutionListener.class })
public class ManagerTest extends AbstractTestNGSpringContextTests {

	@Autowired
	private ApplicationContext context;

	@Test
	public void testManager() {
		StringBuilder result = new StringBuilder();
		// TODO fix test
		/*
		try {
			for (AnyTypeManager manager : context.getBeansOfType(AnyTypeManager.class).values()) {
				String managerName = manager.toString();
				managerName = managerName.substring(0, managerName.indexOf("@"));

				result.append("Manager: ").append(managerName);

				SearchParameters searchParameters = new SearchParameters();
//				searchParameters.addManualFilter(FilterValue.single(AnyTypeDao.FILTER_CURRENT_USERNAME, ""));
				searchParameters.setRange(new RecordRange(0, 50));
				long count = manager.count(searchParameters);

				try {
					List data = manager.search(searchParameters);

					result.append(", returned ").append(data.size()).append(" entities (").append(count).append(" total).\n");
				} catch (Exception ex) {
					result.append(ExceptionUtils.getStackTrace(ex));
				}
			}

		} finally {
			System.out.println(result.toString());
		}*/
	}
}
