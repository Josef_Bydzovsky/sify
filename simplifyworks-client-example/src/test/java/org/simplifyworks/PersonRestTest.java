/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks;

import com.google.common.collect.Lists;
import com.jayway.restassured.RestAssured;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.http.ContentType;
import java.util.List;
import org.hamcrest.Matchers;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.simplifyworks.uam.service.CorePersonManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Rest integration test
 * 
 * http://www.mkyong.com/unittest/junit-4-vs-testng-comparison/
 *
 * TODO: uni test na table resty
 *
 * http://g00glen00b.be/spring-boot-rest-assured/
 * http://www.jayway.com/2014/07/04/integration-testing-a-spring-boot-application/
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@SpringApplicationConfiguration(classes = {Application.class})
@WebAppConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles("test")
@TestExecutionListeners(inheritListeners = false, listeners = {
       DependencyInjectionTestExecutionListener.class,
       DirtiesContextTestExecutionListener.class })
public class PersonRestTest extends AbstractTestNGSpringContextTests {

	private static final Logger LOG = LoggerFactory.getLogger(PersonRestTest.class);
	@Autowired
	private CorePersonManager corePersonManager;
	//
	private CorePersonDto testPerson;
	@Value("${local.server.port}")
	int port;
	//
	private String token;

	@BeforeClass
	public void setUp() {
		LOG.info("Running personRestTest test [" + corePersonManager  + "]");
		testPerson = new CorePersonDto();
		testPerson.setSurname("test_user_1456");
		
		List<GrantedAuthority> roles = Lists.newArrayList();
		roles.add(new RoleOrganizationGrantedAuthority("admin", null));
		User user = new User("test", "password", roles);
		SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(user, "password", roles));
		
		// test po implementaci manageru
		testPerson = corePersonManager.create(testPerson);
		//
		RestAssured.port = port;
		// oauth login
		// TODO: config
		token =  given()
						.parameters("username", "tomiska", "password", "heslo", "scope", "write read", "grant_type", "password")
						.header("Authorization", "Basic c2lmeTpwYXNzd29yZA==")
						.post("/oauth/token")
						.then()
						.statusCode(HttpStatus.OK.value())
						.extract().jsonPath().getString("access_token");
	}

	@AfterClass
	public void cleanUp() {
		LOG.info("Finishing personRestTest test");
		corePersonManager.remove(testPerson.getId());
	}

	@Test
	public void canFetchById() {
		given()
						.header("Authorization", "Bearer " + token)
						.get("/api/core/persons/{id}", testPerson.getId())
						.then()
						.statusCode(HttpStatus.OK.value())
						.body("resource.surname", Matchers.is("test_user_1456"))
						.body("resource.id", Matchers.is(testPerson.getId().intValue()));
	}
	
	@Test(invocationCount = 1, threadPoolSize = 1)
	public void loadCreate() {
		CorePersonDto testPerson7 = new CorePersonDto();
		String surname = "test_person_" + System.currentTimeMillis();	
		testPerson7.setSurname(surname);
		given()
						.body(testPerson7)
						.contentType(ContentType.JSON)
						.when()
						.header("Authorization", "Bearer " + token)
						.post("/api/core/persons")
						.then()
						.statusCode(HttpStatus.OK.value())
						.body("resource.surname", Matchers.is(surname));
		LOG.info("Create person [" + surname + "]");
	}
}
