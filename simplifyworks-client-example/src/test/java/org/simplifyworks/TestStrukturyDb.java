package org.simplifyworks;

import com.google.common.collect.Lists;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import org.junit.runner.RunWith;
import org.simplifyworks.test.utils.TestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class})
@WebAppConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles("test")
@TestExecutionListeners(inheritListeners = false, listeners = {
       DependencyInjectionTestExecutionListener.class,
       DirtiesContextTestExecutionListener.class })
public class TestStrukturyDb extends AbstractTestNGSpringContextTests {

	@Autowired
	private EntityManagerFactory entityManagerFactory;
	//
	private final TestUtils testUtils;

	public TestStrukturyDb() {
		testUtils = new TestUtils();
		testUtils.setForeignIndexCheck(false);
		testUtils.setForeignKeyNameCheck(false);
		testUtils.setTriggersCheck(false);
		testUtils.setSequencesCheck(false);

		List<String> packagesToSkip = Lists.newArrayList();

		testUtils.setPackagesToSkip(packagesToSkip);
	}

	@Test
	public void testStruktury() {
		testUtils.setEntityManager(entityManagerFactory.createEntityManager());

		String errors = testUtils.testStruktury();

		if (!errors.isEmpty()) {
			System.out.println(errors);
			Assert.fail(errors);
		}
	}

	@Test
	public void dbEntityDtoTest() {
		testUtils.setEntityManager(entityManagerFactory.createEntityManager());

		String errors = testUtils.dbEntityDtoTest();

		if (!errors.isEmpty()) {
			System.out.println(errors);
			Assert.fail(errors);
		}
	}

	@Test
	public void testGetInstances() {
		testUtils.setEntityManager(entityManagerFactory.createEntityManager());

		String errors = testUtils.testGetInstances(100);

		if (!errors.isEmpty()) {
			System.out.println(errors);
			Assert.fail(errors);
		}
	}
}
