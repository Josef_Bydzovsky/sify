<!-- Commons -->
<patternDate 'mm/dd/yyyy'>
<selectedFlag 'United-Kingdom'>
<locale 'en'>
<languageMenuHeading 'Choose a language'>
<languageMenu[$lang] {
  en: 'English',
  cs: 'Czech'
}>


<!-- Navigation -->
<core_navigation[$item] {
  dashboard: 'Dashboard',
  system: 'System',
  person: '{{person}} detail',
  persons: '{{person.persons}}',
  roles: '{{role.roles}}',
  user: '{{user}} detail',
  users: '{{user.users}}',
  organization: '{{organization}} detail',
  organizations: 'Organizations',
  setting: 'Parameters',
  attachment: 'Attachments',
  workflow: 'Workflow',
  template:"Templates"
}>


<!-- Buttons -->
<btn_cancel 'Cancel'>
<btn_save 'Save'>
<btn_create 'Create'>
<btn_filter 'Filter'>
<btn_clear 'Clear'>
<btn_copy 'Copy'>


<!-- Drag and drop -->
<dnd[$property] {
	dropHere: 'Drop here',
	deleteTooltip: 'Remove selected item'
}>
<dnd_drop_here '{{dnd.dropHere}}'>
<dnd_delete_tooltip '{{dnd.deleteTooltip}}'>


<!-- Messages -->
<success 'The operation was successful'>
<error 'The operation wasn't successful'>
<validation_error 'Form contains validation errors!'>
<validation_dnd_error 'There are no fields to save!'>


<!-- Basic table -->
<basicTable_column_boolean_yes 'Yes'>
<basicTable_column_boolean_no 'No'>
<basicTable_button_new 'Create new'>
<basicTable_button_delete 'Delete item'>
<basicTable_button_delete_confirm 'Do you want to delete selected item?'>
<basicTable_disable_delete_tooltip 'This record cannot be deleted'>


<!-- Validations -->
<validation[$item] {
  isLowercase: 'Lowercase is required!',
  isLength:'Length must be between {{$arg1}} and {{$arg2}}!',
  isLengthMin: 'Length must be min {{$arg1}}!',
  required: 'Item is mandatory!',
  isEmail: 'Email is required!'
}>


<!-- Authentization -->
<login_success[$user] 'Welcome {{$user}}'>
<logout_success 'You are signed out'>
<login_failed 'Login failed'>
<login_bad_credentials '{{error.invalid_grant}}'>
<page_login[$message] {
  title: 'Sign in',
  panel_title: 'Sign in to Simplifyworks',
  button_login: 'Login'
}>


<!-- Entities -->
<id 'Id'>
<creator 'Creator'>
<created 'Created'>
<modifier 'Modifier'>
<modfied 'Modified'>
<version 'Version'>


<!--  Person -->
<person[$property] {
	*person: 'Person',
	persons: 'Persons'
}>
<person_surname 'Surname'>
<person_firstname 'Firstname'>
<person_email 'Email'>
<person_phone 'Phone'>
<person_titleBefore 'Title before'>
<person_titleAfter 'Title after'>
<person_note 'Note'>
<person_users 'Users'>
<person_detail 'Person detail'>
<person_personOrganizations 'Obligations'>
<person_personalNumber 'Personal number'>


<!--  User -->
<user[$property] {
	*user: 'User',
	users: 'Users',
}>
<user_username 'Username'>
<user_password 'Password'>
<user_detail 'User datail'>
<user_person 'Person'>
<user_roles 'Roles'>
<user_lastLogin 'Last login'>


<!--  Attachment -->
<attachment[$property] {
	*attachment: 'Attachment'
}>
<attachment_detail 'Attachment detail'>
<attachment_name 'Name'>
<attachment_contentGuid 'GUID'>
<attachment_objectType 'Object type'>
<attachment_objectIdentifier 'Object identifier'>
<attachment_dropfile 'Drop files here to upload, or browse.'>
<attachment_description 'Description'>
<attachment_mimetype 'Mimetype'>
<attachment_attachments 'Attachments /just for test purpose/'>


<!--  Workflow -->
<workflow[$property] {
	*workflow: 'Workflow'
}>
<workflow_key 'Key'>
<workflow_upload 'Drop workflow definiton (*.bpmn20.xml), or browse.'>
<workflow_ext_only_bpmn 'Workflow definition must be of type *.bpmn20.xml'>
<workflow_deployed 'Workflow was success deployed.'>
<workflow_not_deployed 'Workflow was not deployed.'>
<workflow_name 'Name'>
<workflow_category 'Category'>
<workflow_version 'Version'>
<workflow_resourceName 'Resource name'>
<workflow_success_remove_def 'Removing the workflow definition was success'>
<workflow_error_remove_def 'Removing the workflow definition end with error'>
<workflow_error_load_def 'Loading workflow definitions ended with error'>
<workflow_description 'Description'>
<workflow_detail 'Workflow detail'>


<!--  Organization -->
<organization[$property] {
	*organization: 'Organization',
	organizations: 'Organizations'
}>
<organization_name 'Name'>
<organization_code 'Code'>
<organization_detail 'Organization detail'>
<organization_abbreviation 'Abbreviation'>
<organization_organization '{{organization}}'>
<organization_organizations '{{organization.organizations}}'>
<organization_sub_organizations 'Sub-organizations'>
<organization_parent_organization 'Parent organization'>
<organization_personOrganizations 'Obligations'>


<!--  PersonOrganization -->
<corePersonOrganization[$property] {
	*corePersonOrganization: 'Obligation',
	corePersonOrganizations: 'Obligations'
}>
<corePersonOrganization_corePersonOrganizations '{{corePersonOrganization.corePersonOrganizations}}'>
<corePersonOrganization_detail 'Obligation detail'>
<corePersonOrganization_person 'Person'>
<corePersonOrganization_organization 'Organization'>
<corePersonOrganization_validFrom 'Valid from'>
<corePersonOrganization_validTill 'Valid till'>
<corePersonOrganization_category 'Category'>
<corePersonOrganization_obligation 'Obligation'>


<!--  Role -->
<role[$property] {
	*role: 'Role',
	roles:'Roles',
	superior_roles: 'Nadřazené role',
	sub_roles: 'Podřízené role'
}>
<role_name 'Name'>
<role_description 'Description'>
<role_detail 'Role detail'>
<role_users 'Users'>


<!--  UserRole -->
<user_role_user 'User'>
<user_role_detail 'User role'>
<user_role_role 'Role'>
<user_role_validTill 'Valid till'>
<user_role_validFrom 'Valid from'>

<role_composition_detail 'Role composition'>
<role_composition_superior_role 'Superior role'>
<role_composition_sub_role 'Sub role'>

<!--  Setting -->
<setting[$property] {
	*setting: 'Application setting',
	settings: 'Application settings',
}>
<setting_detail 'Setting detail'>
<setting_settingKey 'Setting key'>
<setting_value 'Value'>
<setting_system 'System property'>

<!--  Templates -->
<template_code 'Code'>
<template_description 'Description'>
<template_body 'Template'>
<template_detail 'Template detail'>

<!-- Error messages -->
<!-- TODO: message dynamic parameters - combined with exception handling -->
<error[$message,$status] {
	uncaught: 'Ops! Error {{$status}} occured',
	invalid_grant: 'Bad credentials',
	invalid_token: 'Session timeout (invalid authentization token). Please logout and login again.',
	entity_not_found: 'Record [{{$entity_id}}] not found',
	timeout: 'Operation was too long and timeouted. Please refresh page and try it again.'
}>
