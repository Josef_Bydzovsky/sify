var constants = require('./auth-constants.jsx');

var AuthActions = {
    login: function(username, password, successCallback, failedCallback) {
        this.dispatch(constants.AUTH_LOGIN,
            {
                username: username,
                password: password,
                successCallback: successCallback,
                failedCallback: failedCallback
            });
    },

    logout: function(successCallback) {
        this.dispatch(constants.AUTH_LOGOUT, {successCallback: successCallback});
    },

    changeRoles: function() {
        this.dispatch(constants.AUTH_CHANGE_ROLES);
    }
};

module.exports = AuthActions;
