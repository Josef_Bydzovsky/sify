var AuthConstants = {
    AUTH_LOGIN: "AUTH_LOGIN",
    AUTH_LOGOUT: "AUTH_LOGOUT",
    AUTH_CHANGE_ROLES: "AUTH_CHANGE_ROLES"
};

module.exports = AuthConstants;
