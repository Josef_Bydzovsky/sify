var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var Sidebar = React.createClass({

    mixins: [ReactRouter.State, FluxMixin, Fluxxor.StoreWatchMixin("AuthStore")],

    getStateFromFlux: function () {
        return this.getFlux().store("AuthStore").getState();
    },

    render: function () {
        return (
            this.getFlux().store("AuthStore").hasRole('admin') ?
                (
                    <SidebarNavItem glyph='icon-simple-line-icons-settings'
                                    name={
                                <span><Entity entity='core_navigation' data={{item: 'system'}} /></span>
                            }>

                        <SidebarNav>
                            <SidebarNavItem glyph='icon-simple-line-icons-user'
                                            name={<Entity entity='core_navigation' data={{item: 'users'}}/>}
                                            href='/system/user-list'/>
                            <SidebarNavItem glyph='icon-dripicons-user'
                                            name={<Entity entity='core_navigation' data={{item: 'persons'}}/>}
                                            href='/system/person-list'/>
                            <SidebarNavItem glyph='icon-simple-line-icons-users'
                                            name={<Entity entity='core_navigation' data={{item: 'roles'}}/>}
                                            href='/system/role-list'/>

                            {this.getFlux().store("AuthStore").hasRole('admin') ?
                                <SidebarNavItem glyph='icon-dripicons-home'
                                                name={<Entity entity='core_navigation' data={{item: 'organizations'}}/>}
                                                href='/system/organization-list'/>
                                :
                                null
                            }

                            <SidebarNavItem glyph='icon-simple-line-icons-settings'
                                            name={<Entity entity='core_navigation' data={{item: 'setting'}}/>}
                                            href='/system/setting-list'/>
                            <SidebarNavItem glyph='icon-dripicons-attachment'
                                            name={<Entity entity='core_navigation' data={{item: 'attachment'}}/>}
                                            href='/system/attachment-list'/>
                            <SidebarNavItem glyph='icon-fontello-flow-branch'
                                            name={<Entity entity='core_navigation' data={{item: 'workflow'}}/>}
                                            href='/system/workflow-list'/>
                            <SidebarNavItem glyph='icon-simple-line-icons-grid'
                                            name={<Entity entity='core_navigation' data={{item: 'template'}}/>}
                                            href='/system/template-list'/>

                        </SidebarNav>
                    </SidebarNavItem>

                )
                : null
        );
    }
});

module.exports = Sidebar;
