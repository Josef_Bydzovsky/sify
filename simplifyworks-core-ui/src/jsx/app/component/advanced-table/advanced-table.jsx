var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var AdvancedTableHead = require('./head/head.jsx');
var Body = require('./body/body.jsx');
var Foot = require('./foot/foot.jsx')

var PageSettings = require('./foot/page-settings.jsx');
var ColumnFilter = require('./head/column/column-filter.jsx');

/******************************************************************************************************************************************************
 * 
 * AdvancedTable is complex React component for quick and easy table building.
 * 
 ******************************************************************************************************************************************************/
var AdvancedTable = React.createClass({
    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    statics: {
    	// returns property value for specified object (supports 'dot convention')
    	getPropertyValue: function(object, property) {
    		if(property) {
    			var value = object;
    			var propertyParts = property.split('.');
    			
    			// iterate through property name parts and updates value
    			for(var index = 0; index < propertyParts.length; index++) {
    				if(value) {
    					value = value[propertyParts[index]];
    				}
    			}		
    			
    			return value;
    		} else {
    			return null;
    		}
    	},
    	
    	// default action for common deletion 
    	defaultDeleteAction: {    		
    		render: function() {
    			return (
    				<Icon glyph='icon-fontello-trash' />
    			);
    		},
    		
    		execute: function(rest, records, searchCallback) {
    			vex.dialog.confirm({
    				message: 'Do you really want to delete ' + records.length + ' record(s)?',
    				callback: function(confirm) {
    					if(confirm) {
    						var counter = 0;
    		    			
    		    			for(var index = 0; index < records.length; index++) {
    			    			$.ajax({
    			        			headers: {
    			         	    		'Accept': 'application/json',
    			         	            'Content-Type': 'application/json'
    			         	        },
    			         	        method: 'DELETE',
    			         	        url: rest + '/' + records[index].id,
    			         	        dataType: 'text',
    			         	        success: function (response) {
    			         	        	counter++;
    			         	        	
    			         	        	if(counter === records.length) {
    			         	        		searchCallback(true);
    			         	        	}
    			         	        }.bind(this),
    			         	        error: function (xhr, status, error) {
    			         	        	console.log(xhr + ' ' + status + ' ' + error);	
    			         	        }.bind(this)
    			         	    });	    			
    		    			}
    					}
    				}
    			});
    		},
    		
    		isEnabled: function(rest, record) {
    			return true;
    		}
    	},
    },
    
    propTypes: {
        id: React.PropTypes.string.isRequired, // id
        
        rest: React.PropTypes.string.isRequired, // REST URL for getting data
        filter: React.PropTypes.string, // default filter for data
        customFilterFunc: React.PropTypes.func, // function responsible for rendering of custom filter
        customNamedFilters: React.PropTypes.arrayOf(React.PropTypes.object), // custom named filters (object with attributes name and filter)
        
        createNewDetail: React.PropTypes.string, // URL of detail of new object (primarily used if defined instead of following)        
        createNewDetailModalFunc: React.PropTypes.func, // function responsible for creating modal detail of new object
        
        actionsUniqueProperty: React.PropTypes.string, // unique property for actions purposes (it is used for selection state evaluation)
        actions: React.PropTypes.arrayOf(React.PropTypes.object) // actions (object with functions 'render()', 'execute(rest, records, searchCallback)' and 'isEnabled(rest, record)')
    },
    
    // returns initial state for table
    getInitialState: function() {
        return {        
        	// component property
        	processing: true, // we start in processing state
        	
        	// request properties
        	pageNumber: 1,
        	pageSize: PageSettings.pageSizes[0], // use default value        	
        	ordering: [],
        	defaultFilterGroup: {},
        	columnFilterGroup: {
        		name: 'column_group',
        		groupOperator: 'AND',
        		filters: []
        	},
        	customFilterGroup: {},
        	customFilterHidden: false,
        	
        	// selected records (can be from different pages)
        	selectedRecords: [],
        	
        	// response properties
        	recordsFiltered: 0,
    		recordsReturned: 0,
    		recordsTotal: 0,
    		records: []
        };
    },
    
    // builds default filter and runs search
    componentWillMount: function() {
    	if(this.props.filter) {
    		$.ajax({
    			headers: {
     	    		'Accept': 'application/json',
     	            'Content-Type': 'application/json'
     	        },
     	        method: 'GET',
     	        url: this.props.rest + '/buildfilter?filter=' + this.props.filter,
     	        success: function (response) {
     	        	this.setState({
     	        		defaultFilterGroup: response.filterGroups.length === 1 ? response.filterGroups[0] : null
     	        	}, this.search);
     	        }.bind(this),
     	        error: function (xhr, status, error) {
     	        	this.processError(xhr, status, error);	
     	        }.bind(this)
     	    });	
    	} else {
    		this.search(false);
    	}   	
    },
    
    // renders whole component
    render: function () {
    	return (   		  		
    		<Table id={this.props.id} className='table table-striped table-bordered table-condensed' cellSpacing='0' width='100%'>
    			<AdvancedTableHead
    				key='head'
    				tableId={this.props.id}
    				searchFunc={this.search}
    				searchWithCustomFilterFunc={this.searchWithCustomFilter}
    				columnsCount={this.getColumnsCount()}
    				rest={this.props.rest}
    				createNewDetail={this.props.createNewDetail}
    				createNewDetailModalFunc={this.props.createNewDetailModalFunc}
    				customFilterFunc={this.props.customFilterFunc}
    				customNamedFilters={this.props.customNamedFilters}
    				columns={this.getColumns()}
    				columnFilterGroupConjunction={this.state.columnFilterGroup.groupOperator}
		     		changeColumnFilterGroupConjunctionFunc={this.changeColumnFilterGroupConjunction} 
    				ordering={this.state.ordering}
    				changeOrderingFunc={this.changeOrdering} 
    				updateFilteringFunc={this.updateFiltering}
    				showActionsHeaders={this.visibleActionHeaders()}
    				selectPageFunc={this.selectPage}
    				deselectAllFunc={this.deselectAll} />
    			
    			<AdvancedTableBody
    				key='body'
    				tableId={this.props.id}
    				rest={this.props.rest}
    				columns={this.getColumns()}
    				records={this.state.records}
    				searchFunc={this.search}
    	    		actions={this.props.actions}
    				isRecordSelectedFunc={this.isRecordSelected}
    				selectRecordFunc={this.selectRecord}
    				deselectRecordFunc={this.deselectRecord} />
    			
    			<AdvancedTableFoot
    				key='foot'
    				rest={this.props.rest}
    				columnsCount={this.getColumnsCount()}
    				columns={this.getColumns()}
    				recordsReturned={this.state.recordsReturned}
    				recordsTotal={this.state.recordsTotal}
    				recordsFiltered={this.state.recordsFiltered}
    				pageNumber={this.state.pageNumber}
    				pageSize={this.state.pageSize}
    				changePageNumberFunc={this.changePageNumber}
    				changePageSizeFunc={this.changePageSize}
    				searchFunc={this.search}
    				selectedRecords={this.state.selectedRecords}
    				actions={this.props.actions} />
    		</Table>
        );
    },
    
    visibleActionHeaders: function() {
    	return this.props.actions != undefined
    },
    
    getColumnsCount: function() {
    	var columnsCount = 0;
    	
    	if(this.visibleActionHeaders()) {
    		columnsCount += 2;
    	}
    	
    	React.Children.forEach(this.props.children, function(child, column) {
    		if(child.props.visible) {
    			columnsCount++;;
    		}
    	});
    	
    	return columnsCount;
    },
    
    // returns columns for further processing (to render column headers etc.)
    getColumns: function() {
    	var columns = [];
    	
    	React.Children.forEach(this.props.children, function(child, column) {
    		if(child.props.visible) {
    			columns.push(child.props);
    		}
    	});
    	
    	return columns;
    },
    
    // changes page number and invokes search function
    changePageNumber: function(newPageNumber) {    	
    	var pageCount = Math.trunc(this.state.recordsTotal / this.state.pageSize) + (this.state.recordsTotal % this.state.pageSize > 0 ? 1 : 0);
    	
    	if(!isNaN(newPageNumber) && newPageNumber >= 1 && newPageNumber <= pageCount) {    	
		    this.setState({
		    	processing: true,
		    	pageNumber: parseInt(newPageNumber)
		    }, this.search);
    	} else {
    		// TODO number validation failed
    		console.log(newPageNumber);
    	}
    },
    
    // changes page size and invokes search function
    changePageSize: function(newPageSize) {
    	if(!isNaN(newPageSize) && newPageSize > 0) {
	    	this.setState({
	    		processing: true,
	    		pageSize: parseInt(newPageSize),
	    		pageNumber: 1
	    	}, this.search);
    	} else {
    		// TODO number validation failed
    		console.log(newPageNumber);
    	}
    },
    
    // changes ordering and invokes search function
    changeOrdering: function(property, order, nullsOrder) {
    	// TODO validate property
    	var newOrdering = [];
    	
    	// conditionally add property for ordering
    	if(order) {
    		newOrdering.push({
	    		property: property,
	    		order: order,
	    		nullsOrder: nullsOrder
	    	});
    	}
    	
    	// add previous ordering except current ordering (property)
    	for(var index = 0; index < this.state.ordering.length; index++) {
    		if(this.state.ordering[index].property !== property) {
    			newOrdering.push({
    				property: this.state.ordering[index].property,
    				order: this.state.ordering[index].order,
    				nullsOrder: this.state.ordering[index].nullsOrder
    			});
    		}
    	}
    	
    	this.setState({
    		processing: true,
    		ordering: newOrdering
    	}, this.search);
    },
    
    updateFiltering: function(property, filterType, filterValues) {    	
    	var filterGroup = this.getFilterGroup(property);    	
    	
    	if(filterType === 'null' || filterType === 'notnull' || !this.isFilterValuesArrayEmpty(filterValues)) {
	    	filterGroup.filters.push({
	    		propertyName: property,
	    		operator: {'@type': AdvancedTableColumnFilter.getFilterOperatorType(filterType)},
	    		values: filterValues 
	    	});
    	}
    	
    	this.setState({
    		columnFilterGroup: filterGroup
    	}, this.search);
    },
    
    isFilterValuesArrayEmpty: function(filterValues) {
    	if(filterValues != null) {    	
	    	for(var index = 0; index < filterValues.length; index++) {
	    		if(filterValues[index] != undefined && filterValues[index] != null && filterValues[index] != '') {
	    			return false;
	    		}
	    	}
    	}
	    	
    	return true;
    },
    
    getFilterGroup: function(ignoredProperty) { 
    	var filterGroup = {
    		name: 'column_group',
    		groupOperator: this.state.columnFilterGroup.groupOperator,
    		filters: this.getFilters(this.state.columnFilterGroup.filters, ignoredProperty)
    	}
    			    			
    	return filterGroup;
    },
    
    getFilters: function(filters, ignoredProperty) {
    	var newFilters = [];
    	
    	for(var index = 0; index < filters.length; index++) {
    		if(filters[index].propertyName !== ignoredProperty) {
    			newFilters.push(filters[index]);
    		}
    	}
    	
    	return newFilters;
    },
    
    changeColumnFilterGroupConjunction: function(conjunction) {
    	var columnFilterGroup = this.getFilterGroup(null, null);
    	
    	columnFilterGroup.groupOperator = conjunction;
    	
    	this.setState({
    		columnFilterGroup: columnFilterGroup
    	}, this.search);
    },
    
    // returns record selection flag (uses actionUniqueProperty)
    isRecordSelected: function(record) {
    	return this.state.selectedRecords.map(this.getActionUniquePropertyValue).indexOf(this.getActionUniquePropertyValue(record)) != -1;
    },
    
    // returns action unique property value for specified record
    getActionUniquePropertyValue: function(record) {
    	return AdvancedTable.getPropertyValue(record, this.props.actionUniqueProperty);
    },
    
    // adds record to current selection
    selectRecord: function(record) {
    	var newSelectedRecords = [];
    	
    	newSelectedRecords.push(record);
    	
    	for(var index = 0; index < this.state.selectedRecords.length; index++) {
    		newSelectedRecords.push(this.state.selectedRecords[index]);
    	}
    	
    	this.setState({
    		selectedRecords: newSelectedRecords
    	});
    },
    
    // removes record from current selection
    deselectRecord: function(record) {
    	var newSelectedRecords = [];    	
    	
    	for(var index = 0; index < this.state.selectedRecords.length; index++) {
    		if(this.getActionUniquePropertyValue(this.state.selectedRecords[index]) !== this.getActionUniquePropertyValue(record)) {
    			newSelectedRecords.push(this.state.selectedRecords[index]);
    		}
    	}
    	
    	this.setState({
    		selectedRecords: newSelectedRecords
    	});
    },
    
    // adds all records from current page to selection
    selectPage: function() {
    	var newSelectedRecords = [];
    	    	
    	for(var index = 0; index < this.state.selectedRecords.length; index++) {
    		newSelectedRecords.push(this.state.selectedRecords[index]);
    	}
    	
    	for(var index = 0; index < this.state.records.length; index++) {
    		var record = this.state.records[index];
    		
    		if(this.state.selectedRecords.map(function(selectedRecord) {return this.getActionUniquePropertyValue(selectedRecord)}.bind(this)).indexOf(this.getActionUniquePropertyValue(record)) === -1) {
    			newSelectedRecords.push(record);
    		}
    	}
    	
    	this.setState({
    		selectedRecords: newSelectedRecords
    	});
    },
    
    // removes all records from current selection
    deselectAll: function() {
    	this.setState({
    		selectedRecords: []
    	});
    },
    
    // searches for data
    search: function(clearSelectedRecords) {
	    $.ajax({
	    	headers: {
	    		'Accept': 'application/json',
	            'Content-Type': 'application/json'
	        },
	        method: 'POST',
	        url: this.props.rest + '/filter',
	        data: JSON.stringify(this.createSearchParameters()),            
	        success: function (response) {
	        	this.processResponse(response, clearSelectedRecords);
	        }.bind(this),
	        error: function (xhr, status, error) {
	        	this.processError(xhr, status, error);	
	        }.bind(this)
	    });
    },
    
    searchWithCustomFilter: function(filter) {
    	$.ajax({
			headers: {
 	    		'Accept': 'application/json',
 	            'Content-Type': 'application/json'
 	        },
 	        method: 'GET',
 	        url: this.props.rest + '/buildfilter?filter=' + filter,
 	        success: function (response) {
 	        	this.setState({
 	        		customFilterGroup: response.filterGroups.length === 1
 	        							? {
 	        								name: 'custom_group',
 	        								groupOperator: response.filterGroups[0].groupOperator,
 	        								filters: response.filterGroups[0].filters
 	        							}
 	        							: null
 	        	}, this.search);
 	        }.bind(this),
 	        error: function (xhr, status, error) {
 	        	this.processError(xhr, status, error);	
 	        }.bind(this)
 	    });	
    },
    
    // creates search parameters using component state
    createSearchParameters: function() {    	
    	return {
    		'range': this.createSearchParametersRange(),
    		'sorts': this.createSearchParametersSorts(),
    		'filterGroups' : this.createSearchParametersFilterGroups()
    	};
    },
    
    // creates range attribute for search parameters using component state
    createSearchParametersRange: function() {
    	var firstRow = (this.state.pageNumber - 1) * this.state.pageSize;
    	var rows = this.state.pageSize;
    	
    	return {
    		'firstRow': firstRow,
			'rows': rows
    	};
    },
    
    // creates sorts attribute for search parameters using component state
    createSearchParametersSorts: function() {
    	var sorts = [];
    	
    	for(var index = 0; index < this.state.ordering.length; index++) {    		
    		sorts.push({
        		propertyName: this.state.ordering[index].property,
    	    	sortOrder: this.state.ordering[index].order
        	});
    	}
    	
    	return sorts;
    },
    
    // creates filter groups attribute for search parameters using component properties and state
    createSearchParametersFilterGroups: function() {
    	var filterGroups = [];
    	
    	if(this.state.defaultFilterGroup) {
    		filterGroups.push(this.state.defaultFilterGroup);
    	}
    	
    	if(this.state.columnFilterGroup) {
    		filterGroups.push(this.state.columnFilterGroup);
    	}
    	
    	if(this.state.customFilterGroup) {
    		filterGroups.push(this.state.customFilterGroup);
    	}
    	
    	return filterGroups;
    },
    
    // processes response (refreshes state of component)
    processResponse: function(response, clearSelectedRecords) {
    	var records = [];
    	
    	if(response._embedded && response._embedded.resources) {
    		var resources = response._embedded.resources;
    		
    		for(var index = 0; index < resources.length; index++) {
    			records.push(resources[index].resource);
    		}
    	}
    	
    	this.setState({
    		processing: false,
    		
    		selectedRecords: clearSelectedRecords === true ? [] : this.state.selectedRecords,
    		
    		recordsReturned: response.recordsReturned,
    		recordsTotal: response.recordsTotal,
    		recordsFiltered: response.recordsFiltered,
    		records: records
    	});
    },
    
    // processes error (shows message to user)
    processError: function(xhr, status, error) {
    	//TODO process errors
    	console.log(xhr);
    	console.log(error);
    }
});

/******************************************************************************************************************************************************
 * 
 * Represents column definition for advanced table.
 * 
 ******************************************************************************************************************************************************/
AdvancedTable.Column = React.createClass({
	
    propTypes: {
    	id: React.PropTypes.string, // this component id
    	
    	property: React.PropTypes.string, // property name
    	tooltip: React.PropTypes.string, // tool tip string
    	
    	visible: React.PropTypes.bool, // visibility flag
    	orderable: React.PropTypes.bool, // ordering flag
    	
    	searchable: React.PropTypes.bool, // filtering flag
    	filterType: React.PropTypes.string, // default filter type
    	
    	face: React.PropTypes.oneOf(['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string']), // face of this component
    	renderFunc: React.PropTypes.func, // own cell render function (instead of face)
    	summaryRenderFunc: React.PropTypes.func, // summary cell render function
    	
    	detail: React.PropTypes.string, // detail URL
    	detailProperty: React.PropTypes.string, // property used for detail URL creation
    	
    	detailRest: React.PropTypes.string, // REST URL in case of modal usage
    	detailModalFunc: React.PropTypes.func // function responsible for creating modal detail of selected object
    },
    
    getDefaultProps() {    	
        return {
        	visible: true,
            orderable: true,
            
            searchable: true,
            filterType: null,
            
            face: 'string'
        };
    },
    
    // we do not use this render function because we use this component only as column descriptor
    render: function() {    	
    	return null;
    }
});

module.exports = AdvancedTable;