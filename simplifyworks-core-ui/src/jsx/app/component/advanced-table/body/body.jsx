var AdvancedTableRecord = require('./record.jsx');

/******************************************************************************************************************************************************
 * 
 * Represents body of table.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableBody = React.createClass({
	
	propTypes: {
		tableId: React.PropTypes.string, // id of table
		rest: React.PropTypes.string.isRequired, // REST URL for getting data
		
		columns: React.PropTypes.array, // columns definition
		records: React.PropTypes.arrayOf(React.PropTypes.object), // array of records
		
		searchFunc: React.PropTypes.func, // function responsible for searching
		
		actions: React.PropTypes.arrayOf(React.PropTypes.object), // actions (object with functions 'render()', 'execute(rest, records, searchCallback)' and 'isEnabled(rest, record)')
        
        isRecordSelectedFunc: React.PropTypes.func, // record selection status function
        selectRecordFunc: React.PropTypes.func, // function responsible for adding of record to current selection
        deselectRecordFunc: React.PropTypes.func // function responsible for removing of record from current selection
	},
	
	// renders table body using set of record components (alternatively renders plain string for no records)
	render: function() {
		var columnsCount = this.props.columns.length + (this.props.actions ? 2 : 0);
		var records = [];
    	
		// create rows using properties
    	for(var index = 0; index < this.props.records.length; index++) {    		
    		records.push(
    			<AdvancedTableRecord
    				key={index}
    				tableId={this.props.tableId}
    				rest={this.props.rest}
    				columns={this.props.columns}
    				record={this.props.records[index]}
    				searchFunc={this.props.searchFunc}
    				actions={this.props.actions}
    				isRecordSelectedFunc={this.props.isRecordSelectedFunc}    				
    				selectRecordFunc={this.props.selectRecordFunc}
					deselectRecordFunc={this.props.deselectRecordFunc} />  			    			
    		);
    	}
		
		return (				
			<tbody>
				{records.length === 0
					?	<tr>
							<td colSpan={columnsCount}>
								No data found
							</td>
						</tr>
					:	records
				}
			</tbody>
		)
	}
});

module.exports = AdvancedTableBody;