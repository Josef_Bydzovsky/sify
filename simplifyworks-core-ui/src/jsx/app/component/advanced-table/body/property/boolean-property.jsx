/******************************************************************************************************************************************************
 * 
 * Component which renders boolean property of some record.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableBooleanProperty = React.createClass({
	
	propTypes: {
		value: React.PropTypes.bool // value for rendering (boolean)
	},
	
	// renders value using icon with glyph
	// TODO render nulls (currently empty strings)
	render: function() {
		var glyph = this.props.value === true ? 'icon-fontello-check-1' : (this.props.value === false ? 'icon-fontello-check-empty' : '');
		
		return (
			<Icon glyph={glyph} />
		);
	}
});

module.exports = AdvancedTableBooleanProperty;
