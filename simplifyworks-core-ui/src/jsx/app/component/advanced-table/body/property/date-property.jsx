/******************************************************************************************************************************************************
 * 
 * Component which renders date property of some record.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableDateProperty = React.createClass({
	
	propTypes: {
		value: React.PropTypes.number // value for rendering (date)
	},
	
	// renders date using specified format
	// TODO move format to some central place
	render: function() {
		return (
			<div>
				{this.props.value ? new Date(this.props.value).format("yyyy-mm-dd") : ''}
			</div>
		)
	}
});

module.exports = AdvancedTableDateProperty;