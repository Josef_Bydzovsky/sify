/******************************************************************************************************************************************************
 * 
 * Component which renders datetime property of some record.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableDateTimeProperty = React.createClass({
	
	propTypes: {
		value: React.PropTypes.number // value for rendering (datetime)
	},
	
	// renders date and time using specified format
	// TODO move format to some central place
	render: function() {		
		return (
			<div>
				{this.props.value ? new Date(this.props.value).format("yyyy-mm-dd HH:MM:ss") : ''}
			</div>
		)
	}
});

module.exports = AdvancedTableDateTimeProperty;