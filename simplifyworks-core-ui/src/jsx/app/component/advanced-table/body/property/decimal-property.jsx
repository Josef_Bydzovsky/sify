/******************************************************************************************************************************************************
 * 
 * Component which renders decimal property of some record.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableDecimalProperty = React.createClass({
	
	propTypes: {
		value: React.PropTypes.string // value for rendering (decimal)
	},
	
	// renders decimal using format
	// TODO format of decimals
	render: function() {
		return (
			<div>
				{this.props.value}
			</div>
		)
	}
});

module.exports = AdvancedTableDecimalProperty;