/******************************************************************************************************************************************************
 * 
 * Component which renders integer property of some record.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableIntegerProperty = React.createClass({
	
	propTypes: {
		value: React.PropTypes.number // value for rendering (integer)
	},
	
	// renders integer using format
	// TODO format of integers
	render: function() {
		return (
			<div>
				{this.props.value}
			</div>
		)
	}
});

module.exports = AdvancedTableIntegerProperty;