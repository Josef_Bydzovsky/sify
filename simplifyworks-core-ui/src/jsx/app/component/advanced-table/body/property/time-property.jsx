/******************************************************************************************************************************************************
 * 
 * Component which renders time property of some record.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableTimeProperty = React.createClass({
	
	propTypes: {
		value: React.PropTypes.number // value for rendering (time)
	},
	
	// renders time using specified format
	// TODO move format to some central place
	render: function() {		
		return (
			<div>
				{this.props.value ? new Date(this.props.value).format("HH:MM:ss") : ''}
			</div>
		)
	}
});

module.exports = AdvancedTableTimeProperty;