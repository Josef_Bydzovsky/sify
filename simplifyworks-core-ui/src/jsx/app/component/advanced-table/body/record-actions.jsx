/******************************************************************************************************************************************************
 * 
 * Component which renders action buttons for single record. 
 * 
 ******************************************************************************************************************************************************/
AdvancedTableRecordActions = React.createClass({
	
	propTypes: {
		rest: React.PropTypes.string.isRequired, // REST URL for getting data
		record: React.PropTypes.object.isRequired, // record		
		searchFunc: React.PropTypes.func.isRequired, // function responsible for searching
		actions: React.PropTypes.arrayOf(React.PropTypes.object) // actions (object with functions 'render()', 'execute(rest, records, searchCallback)' and 'isEnabled(rest, record)')
	},
	
	// renders action buttons
	render: function() {
		var actionButtons = [];
		
		for(var index = 0; index < this.props.actions.length; index++) {
			var action = this.props.actions[index];
			
			if(action.isEnabled(this.props.rest, this.props.record)) {
				actionButtons.push(
					<Button sm key={index} onClick={action.execute.bind(this, this.props.rest, [this.props.record], this.props.searchFunc)}>
						{action.render()}
					</Button>
				);
			}
		}
		
		return (
			<td>
				{actionButtons}
			</td>
		);
	}
});

module.exports = AdvancedTableRecordActions;