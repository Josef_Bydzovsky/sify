var AdvancedTableRecordSelection = require('./record-selection.jsx');
var AdvancedTableRecordActions = require('./record-actions.jsx');
var AdvancedTableRecordProperty = require('./property/record-property.jsx');

/******************************************************************************************************************************************************
 * 
 * Component which renders single record and conditionally selection and actions.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableRecord = React.createClass({
	
	propTypes: {
		tableId: React.PropTypes.string.isRequired, // id of table
		rest: React.PropTypes.string.isRequired, // REST URL for getting data
		
		columns: React.PropTypes.array.isRequired, // columns definition
		record: React.PropTypes.object.isRequired, // row value (i.e. values for all columns)
		
		searchFunc: React.PropTypes.func.isRequired, // function responsible for searching
		
        actions: React.PropTypes.arrayOf(React.PropTypes.object), // actions
        
        isRecordSelectedFunc: React.PropTypes.func, // record selection status function
        selectRecordFunc: React.PropTypes.func, // function responsible for adding of record to current selection
        deselectRecordFunc: React.PropTypes.func // function responsible for removing of record from current selection
	},
	
	// renders single row
	render: function() {
		var record = this.props.record;
		var cells = [];
	
		if(this.props.actions) {
			cells.push(
				<AdvancedTableRecordSelection
					key={'record-selection'}
					record={record}
					recordSelected={this.props.isRecordSelectedFunc(record)} 				
					selectRecordFunc={this.props.selectRecordFunc}
					deselectRecordFunc={this.props.deselectRecordFunc} />
			);
		}
		
		// creates cells using properties
		for(var index = 0; index < this.props.columns.length; index++) {
			var column = this.props.columns[index];
			
			var value = AdvancedTable.getPropertyValue(record, column.property);
			var detailValue = AdvancedTable.getPropertyValue(record, column.detailProperty);
			
			cells.push(
				<AdvancedTableRecordProperty
					key={'record-property-' + index}
					tableId={this.props.tableId}
					column={column}
					value={value}
					detailValue={detailValue ? detailValue : value}
					searchFunc={this.props.searchFunc}/>
			);
		}
		
		if(this.props.actions) {
			cells.push(
				<AdvancedTableRecordActions
					key={'record-actions'}
					rest={this.props.rest}
					record={record}
					searchFunc={this.props.searchFunc}
					actions={this.props.actions} />
			);
		}
		
		return (
			<tr>
				{cells}
			</tr>
		);
	}
});

module.exports = AdvancedTableRecord;