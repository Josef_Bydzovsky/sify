var AdvancedTableRecordsSelectionSummary = require('./records-selection-summary.jsx');
var AdvancedTableRecordsActions = require('./records-actions.jsx');

var AdvancedTablePageStatistics = require('./page-statistics.jsx');
var AdvancedTablePageScroller = require('./page-scroller.jsx');
var AdvancedTablePageSettings = require('./page-settings.jsx');

/******************************************************************************************************************************************************
 * 
 * Represents foot of the table (shows PageScroller, PageStatistics and PageSettings).
 * 
 ******************************************************************************************************************************************************/
AdvancedTableFoot = React.createClass({
	
	propTypes: {
		rest: React.PropTypes.string.isRequired, // REST URL for getting data
		searchFunc: React.PropTypes.func, // function responsible for searching
		
		selectedRecords: React.PropTypes.arrayOf(React.PropTypes.node), // selected records
		actions: React.PropTypes.arrayOf(React.PropTypes.object), // actions (object with functions 'render()', 'execute(rest, records, searchCallback)' and 'isEnabled(rest, record)')
		
		columns: React.PropTypes.array, // columns definition
		columnsCount: React.PropTypes.number.isRequired, // count of columns
		
		recordsReturned: React.PropTypes.number.isRequired, // number of records returned in last response
		recordsTotal: React.PropTypes.number.isRequired, // total number of records
		recordsFiltered: React.PropTypes.number.isRequired, // number of filtered records in last response
		
		pageNumber: React.PropTypes.number.isRequired, // page number
		pageSize: React.PropTypes.number.isRequired, // page size (i.e. number of entries per page)
		
		changePageNumberFunc: React.PropTypes.func.isRequired, // function for changing page number
		changePageSizeFunc: React.PropTypes.func.isRequired // function for changing page size
	},
	
	// renders foot with paging components
	render: function() {
		// TODO conditionally render summaries and actions etc.
	
		return (
			<tfoot>
				<tr>
					<td>
						<AdvancedTableRecordsSelectionSummary
							selectedRecords={this.props.selectedRecords} />
					</td>
				
					{this.createColumnSummaries()}
					
					<td>
						<AdvancedTableRecordsActions
							rest={this.props.rest}
							searchFunc={this.props.searchFunc}
							selectedRecords={this.props.selectedRecords}
							actions={this.props.actions} />
					</td>
				</tr>
			
				<tr>
					<td colSpan={this.props.columnsCount}>
						<Grid>						
							<Row>
								<Col sm={3} md={3} lg={3}>
									<AdvancedTablePageStatistics
										recordsReturned={this.props.recordsReturned}
										recordsTotal={this.props.recordsTotal}
										recordsFiltered={this.props.recordsFiltered}
										pageNumber={this.props.pageNumber}
										pageSize={this.props.pageSize} />
								</Col>
								
								<Col sm={6} md={6} lg={6}>
									<AdvancedTablePageScroller
										recordsFiltered={this.props.recordsFiltered}
										pageNumber={this.props.pageNumber}
										pageSize={this.props.pageSize}
										changePageNumberFunc={this.props.changePageNumberFunc} />
								</Col>
								
								<Col sm={3} md={3} lg={3}>
									<AdvancedTablePageSettings
										pageSize={this.props.pageSize}
										changePageSizeFunc={this.props.changePageSizeFunc} />
								</Col>
							</Row>
						</Grid>						
					</td>
				</tr>
			</tfoot>
		);
	},
	
	createColumnSummaries: function() {
		var columnSummaries = [];
		
		for(var index = 0; index < this.props.columns.length; index++) {
			if(this.props.columns[index].summaryRenderFunc) {
				columnSummaries.push(
					<td key={'column-summary-' + index}>
						{this.props.columns[index].summaryRenderFunc(this.props.selectedRecords)}
					</td>
				);
			} else {
				columnSummaries.push(
					<td key={'column-summary-' + index}/>
				);
			}
		}
		
		return columnSummaries;
	}
});

module.exports = AdvancedTableFoot;