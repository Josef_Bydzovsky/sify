/******************************************************************************************************************************************************
 * 
 * Represents component which is responsible for changing pages.
 * 
 ******************************************************************************************************************************************************/
PageScroller = React.createClass({
	
	propTypes: {
		recordsFiltered: React.PropTypes.number.isRequired, // number of filtered records in last response
		
		pageNumber: React.PropTypes.number.isRequired, // page number (starts with 1)
		pageSize: React.PropTypes.number.isRequired, // page size (i.e. number of entries per page)
		
		changePageNumberFunc: React.PropTypes.func.isRequired // function for changing page number
	},
	
	// returns initial state for input
	getInitialState: function() {
		return {
			pageNumber: null
		}
	},
	
	// renders change page buttons and input for direct page selection
	render: function() {		
		return (
			<Grid collapse>
				<Row>
					<Col sm={10} md={10} lg={10} xsOnlyGutterBottom smCollapseRight>
						<ButtonGroup sm>
							{this.createChangePageButtons()}
						</ButtonGroup>
					</Col>
						
					<Col sm={2} md={2} lg={2}>				
						<Input type='text' placeholder='Go to page' value={this.state.pageNumber} onChange={this.onChange} />
					</Col>
				</Row>
			</Grid>
		);
	},
	
	// creates change page buttons using properties
	createChangePageButtons: function() {		
		var changePageButtons = [];
		var pageCount = Math.trunc(this.props.recordsFiltered / this.props.pageSize) + (this.props.recordsFiltered % this.props.pageSize > 0 ? 1 : 0);
		
		changePageButtons.push(this.createChangePageButton('<<', 1, false, true));
		changePageButtons.push(this.createChangePageButton('<', this.props.pageNumber - 1, false, this.props.pageNumber > 1));
		
		for(var pageNumber = 1; pageNumber <= pageCount; pageNumber++) {
			changePageButtons.push(this.createChangePageButton(pageNumber, pageNumber, pageNumber === this.props.pageNumber, true));
		}
		
		changePageButtons.push(this.createChangePageButton('>', this.props.pageNumber + 1, false, this.props.pageNumber < pageCount));
		changePageButtons.push(this.createChangePageButton('>>', pageCount, false, true));
		
		return changePageButtons;
	},	
	
	// creates single change page button
	createChangePageButton: function(key, pageNumber, active, enabled) {		
		return (
			<Button sm key={key} onClick={this.props.changePageNumberFunc.bind(null, pageNumber)} bsStyle={active ? 'darkblue' : 'grey'} disabled={!enabled}>
				{key}
			</Button>
		);
	},
	
	// listener for input
	onChange: function(event) {
		this.setState({
			pageNumber: event.target.value
		}, this.props.changePageNumberFunc.bind(null, event.target.value));
	}
});

module.exports = PageScroller;