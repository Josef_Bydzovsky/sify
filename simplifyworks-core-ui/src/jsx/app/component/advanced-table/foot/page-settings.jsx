/******************************************************************************************************************************************************
 * 
 * Represents component which is responsible for page settings.
 * 
 ******************************************************************************************************************************************************/
AdvancedTablePageSettings = React.createClass({
	
	statics: {
		pageSizes: [10, 25, 50, 100]
	},
	
	propTypes: {
		pageSize: React.PropTypes.number.isRequired, // page size (i.e. number of entries per page)
		changePageSizeFunc: React.PropTypes.func.isRequired // function for changing page size
	},
	
	// renders label and select for page size
	render: function() {
		var options = [];
		
		for(var index = 0; index < AdvancedTablePageSettings.pageSizes.length; index++) {
			options.push(
				<option key={'page-size' + index} value={AdvancedTablePageSettings.pageSizes[index]}>{AdvancedTablePageSettings.pageSizes[index]}</option>
			);
		}
		
		return (
			<Grid>
				<Row>
					<Col sm={6} md={6} lg={6} xsOnlyGutterBottom smCollapseRight>
						<label>Entries per page</label>
					</Col>
					
					<Col sm={6} md={6} lg={6}>				
						<Select value={this.props.pageSize} onChange={this.onChange}>
							{options}
						</Select>
					</Col>
				</Row>
			</Grid>
		);
	},
	
	// listener for select
	onChange: function(event) {
		this.props.changePageSizeFunc(event.currentTarget.value);
	}
});

module.exports = AdvancedTablePageSettings;