/******************************************************************************************************************************************************
 * 
 * Represents component which is responsible for displaying page statistics.
 * 
 ******************************************************************************************************************************************************/
AdvancedTablePageStatistics = React.createClass({
	
	propTypes: {
		recordsFiltered: React.PropTypes.number.isRequired, // count of filtered entries
		recordsReturned: React.PropTypes.number.isRequired, // count of returned entries
		recordsTotal: React.PropTypes.number.isRequired, // total count of entries
		
		pageNumber: React.PropTypes.number.isRequired, // page number (starts with 1)
		pageSize: React.PropTypes.number.isRequired, // page size (i.e. number of entries per page)
	},

	// renders statistics as plain text
	render: function() {
		var statistics = 'Showing ' + this.getFirstVisibleRecordOrder() + ' to ' + this.getLastVisibleRecordOrder() + ' of ' + this.props.recordsTotal + ' entries';
		
		if(this.props.recordsFiltered < this.props.recordsTotal) {
			statistics += ' (' + this.props.recordsFiltered + ' filtered)';
		}
		
		return (
			<Grid>
				<Row>
					<Col sm={12} md={12} lg={12}>
						{statistics}
					</Col>
				</Row>
			</Grid>
		);		
	},
	
	// returns number of first visible record
	getFirstVisibleRecordOrder: function() {
		return this.props.recordsFiltered > 0 ? ((this.props.pageNumber - 1) * this.props.pageSize + 1) : 0;
	},
	
	// returns number of last visible record
	getLastVisibleRecordOrder: function() {
		return this.props.recordsFiltered > 0 ? (this.getFirstVisibleRecordOrder() + this.props.recordsReturned - 1) : 0;
	}	
});

module.exports = AdvancedTablePageStatistics;