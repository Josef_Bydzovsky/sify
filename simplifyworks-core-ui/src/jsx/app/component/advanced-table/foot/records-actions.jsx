/******************************************************************************************************************************************************
 * 
 * Component which renders action buttons for multiple selected records.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableRecordsActions = React.createClass({
	
	propTypes: {
		rest: React.PropTypes.string.isRequired, // REST URL for getting data
		searchFunc: React.PropTypes.func.isRequired, // function responsible for searching
		selectedRecords: React.PropTypes.arrayOf(React.PropTypes.object).isRequired, // selected records		
		actions: React.PropTypes.arrayOf(React.PropTypes.object) // actions (object with functions 'render()', 'execute(rest, records, searchCallback)' and 'isEnabled(rest, record)')
	},
	
	// renders action buttons or some text when no actions are available for current record selection
	render: function() {
		var actionButtons = this.createActionButtons();
		
		return (
			<span>
				{actionButtons.length === 0
					?	'No actions available for current selection'
					:	actionButtons
				}
			</span>
		);
	},
	
	// creates action buttons
	createActionButtons: function() {
		var buttons = [];
		
		for(var index = 0; index < this.props.actions.length; index++) {
			var action = this.props.actions[index];
			
			if(this.isActionEnabledForAllValues(action)) {
				buttons.push(
						<Button sm onClick={action.execute.bind(this, this.props.rest, this.props.selectedRecords, this.props.searchFunc)}>
							{action.render()}
						</Button>
					);
			}
		}
		
		return buttons;
	},
	
	// returns true if action is enabled for current record selection
	isActionEnabledForAllValues: function(action) {
		var selectedRecords = this.props.selectedRecords;
		
		for(var index = 0; index < selectedRecords.length; index++) {
			if(!action.isEnabled(this.props.rest, selectedRecords[index])) {
				return false;
			}
		}
		
		return selectedRecords.length > 0;
	}
});

module.exports = AdvancedTableRecordsActions;