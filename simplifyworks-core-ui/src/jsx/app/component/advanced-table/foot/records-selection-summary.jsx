/******************************************************************************************************************************************************
 * 
 * Component which renders simple text containing number of selected records.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableRecordsSelectionSummary = React.createClass({
	
	propTypes: {
		selectedRecords: React.PropTypes.arrayOf(React.PropTypes.node).isRequired, // selected records
	},
	
	// renders text
	render: function() {
		var text = this.props.selectedRecords.length === 0 ? 'No rows selected' : (this.props.selectedRecords.length + ' row(s) selected');
		
		return (
			<span>
				{text}
			</span>
		);
	}
});

module.exports = AdvancedTableRecordsSelectionSummary;