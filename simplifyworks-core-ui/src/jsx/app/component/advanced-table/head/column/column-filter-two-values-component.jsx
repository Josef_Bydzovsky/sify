/******************************************************************************************************************************************************
 * 
 * Represents filter component with two values in state.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableColumnFilterTwoValuesComponent = React.createClass({
	
	propTypes: {
		column: React.PropTypes.object, // column definition
		
		filterType: React.PropTypes.string, // current column filter type
		updateFilteringFunc: React.PropTypes.func // function responsible for update of filter
	},
	
	// returns initial state
	getInitialState: function() {
		return {
			valueFrom: null,
			valueTo: null
		}
	},
	
	// renders couple of components according to column face
	render: function() {
		if(this.props.filterType == 'range') {
			switch(this.props.column.face) {
				case 'integer':
				case 'decimal':
				case 'string':
					return (
						<Grid>
							<Row>
								<Col sm={6} md={6} lg={6} xsOnlyGutterBottom smCollapseRight>						
									<Input sm
										placeholder={AdvancedTableColumnFilter.getDescription('ge')}
										value={this.state.valueFrom}
										onChange={this.onInputFromChange} />
								</Col>
									
								<Col sm={6} md={6} lg={6}>
									<Input sm
										placeholder={AdvancedTableColumnFilter.getDescription('le')}
										value={this.state.valueTo}
										onChange={this.onInputToChange} />
								</Col>
							</Row>
						</Grid>
					);
				case 'date':
				case 'datetime':
				case 'time':
					return (
						<Grid>
							<Row>
								<Col sm={6} md={6} lg={6} xsOnlyGutterBottom smCollapseRight>	
									<Datepicker sm
										labelKey=''
										name={'dateFilterRangeGe-' + this.props.column.property}
										placeholder={AdvancedTableColumnFilter.getDescription('ge')}
										showButtons={false}
										timeEnabled={this.props.column.face != 'date'}
										onChange={this.onDatepickerFromChange} />							
								</Col>
									
								<Col sm={6} md={6} lg={6}>
									<Datepicker sm
										labelKey=''
										name={'dateFilterRangeLe-' + this.props.column.property}
										placeholder={AdvancedTableColumnFilter.getDescription('le')}
										showButtons={false}
										timeEnabled={this.props.column.face != 'date'}
										onChange={this.onDatepickerToChange} />
								</Col>
							</Row>
						</Grid>
					);
				default:
					return null;
			}
		} else {
			return null;
		}		
	},
	
	// listener for lower bound input
	onInputFromChange: function(event) {
		this.setState({
			valueFrom : event.target.value
		}, this.props.updateFilteringFunc.bind(this, this.props.column.property, this.props.filterType, [event.target.value, this.state.valueTo]));
	},
	
	// listener for upper bound input
	onInputToChange: function(event) {
		this.setState({
			valueTo : event.target.value
		}, this.props.updateFilteringFunc.bind(this, this.props.column.property, this.props.filterType, [this.state.valueFrom, event.target.value]));
	},
	
	// listener for lower bound datepicker
	onDatepickerFromChange: function(newValue) {
		this.setState({
			valueFrom : newValue
		}, this.props.updateFilteringFunc.bind(this, this.props.column.property, this.props.filterType, [newValue, this.state.valueTo]));		
	},
	
	// listener for upper bound datepicker
	onDatepickerToChange: function(newValue) {
		this.setState({
			valueTo : newValue
		}, this.props.updateFilteringFunc.bind(this, this.props.column.property, this.props.filterType, [this.state.valueFrom, newValue]));		
	}
});

module.exports = AdvancedTableColumnFilterTwoValuesComponent;