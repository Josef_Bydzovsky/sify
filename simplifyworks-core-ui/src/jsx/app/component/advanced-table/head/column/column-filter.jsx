var AdvancedTableColumnFilterEmptyValueComponent = require('./column-filter-empty-value-component.jsx');
var AdvancedTableColumnFilterOneValueComponent = require('./column-filter-one-value-component.jsx');
var AdvancedTableColumnFilterTwoValuesComponent = require('./column-filter-two-values-component.jsx');

/******************************************************************************************************************************************************
 * 
 * Represents single filter cell which is responsible for filtering per column.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableColumnFilter = React.createClass({
	
	statics: {
		// common options for column filters
		options: [
		 {filterType: null, description: 'no filter', faces: ['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string'], filterOperator: null},
		 {filterType: 'like', description: '\u2245 like', faces: ['string'], filterOperator: 'LIKE'},
		 {filterType: 'mask', description: '\u2248 like with mask', faces: ['string'], filterOperator: 'LIKE_MASK'},
		 
		 {filterType: 'eq', description: '= equal', faces: ['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string'], filterOperator: 'EQUALS'},
		 {filterType: 'ne', description: '\u2260 not equal', faces: ['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string'], filterOperator: 'NOT_EQUALS'},
		 
		 {filterType: 'lt', description: '< less than', faces: ['date', 'datetime', 'time', 'integer', 'decimal', 'string'], filterOperator: 'LESS'},
		 {filterType: 'le', description: '\u2264 less than or equal', faces: ['date', 'datetime', 'time', 'integer', 'decimal', 'string'], filterOperator: 'LESS_EQUALS'},
		 
		 {filterType: 'gt', description: '> greater than', faces: ['date', 'datetime', 'time', 'integer', 'decimal', 'string'], filterOperator: 'GREATER'},
		 {filterType: 'ge', description: '\u2265 greater than or equal', faces: ['date', 'datetime', 'time', 'integer', 'decimal', 'string'], filterOperator: 'GREATER_EQUALS'},
		 
		 {filterType: 'range', description: '\u2194 range', faces: ['date', 'datetime', 'time', 'integer', 'decimal', 'string'], filterOperator: 'INTERVAL'},
		 
		 {filterType: 'null', description: 'null', faces: ['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string'], filterOperator: 'IS_NULL'},
		 {filterType: 'notnull', description: 'not null', faces: ['date', 'datetime', 'time', 'enum', 'integer', 'decimal', 'boolean', 'string'], filterOperator: 'IS_NOT_NULL'},
		],
		
		// returns description for specified filterType
		getDescription: function(filterType) {
			for(var index = 0; index < AdvancedTableColumnFilter.options.length; index++) {
				if(AdvancedTableColumnFilter.options[index].filterType === filterType) {
					return AdvancedTableColumnFilter.options[index].description;
				}
			}
			
			return null;
		},
		
		// returns all options which are available for specified column face
		getOptions: function(face) {
			var options = [];
			
			for(var index = 0; index < AdvancedTableColumnFilter.options.length; index++) {
				if(AdvancedTableColumnFilter.options[index].faces.indexOf(face) != -1) {
					options.push(AdvancedTableColumnFilter.options[index]);
				}
			}
			
			return options;
		},
		
		// returns operator type for specified filter type (translation to FilterGroup)
		getFilterOperatorType: function(filterType) {
	    	for(var index = 0; index < AdvancedTableColumnFilter.options.length; index++) {
	    		if(AdvancedTableColumnFilter.options[index].filterType === filterType) {
	    			return AdvancedTableColumnFilter.options[index].filterOperator;
	    		}
	    	}
	    	
	    	return null;
	    }
	},
	
	propTypes: {
		column: React.PropTypes.object, // column definition
		
		filterType: React.PropTypes.string, // current column filter type
		updateFilteringFunc: React.PropTypes.func // function responsible for update of filter
	},
		
	// renders filter component according to the filter type
	render: function() {
		switch(this.props.filterType) {
			case 'null':
			case 'notnull':
				return (
					<th>
						<AdvancedTableColumnFilterEmptyValueComponent
							column={this.props.column}
							filterType={this.props.filterType}
							updateFilteringFunc={this.props.updateFilteringFunc} />
					</th>
				);
			case 'like':
			case 'mask':
			case 'eq':
			case 'ne':
			case 'lt':
			case 'le':
			case 'gt':
			case 'ge':
				return (
					<th>
						<AdvancedTableColumnFilterOneValueComponent
							column={this.props.column}
							filterType={this.props.filterType}
							updateFilteringFunc={this.props.updateFilteringFunc} />
					</th>
				);
			case 'range':
				return (
					<th>
						<AdvancedTableColumnFilterTwoValuesComponent
							column={this.props.column}
							filterType={this.props.filterType}
							updateFilteringFunc={this.props.updateFilteringFunc} />
					</th>
				);
			default:
				return (
					<th/>
				);
		}
	}
});

module.exports = AdvancedTableColumnFilter;