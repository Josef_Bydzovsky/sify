/******************************************************************************************************************************************************
 * 
 * Represents column head cell which is responsible for displaying column name, ordering and conditionally for filter type selection.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableColumnHead = React.createClass({	
	
	propTypes: {
		column: React.PropTypes.object, // column definition
		
		order: React.PropTypes.string, // order
		nullsOrder: React.PropTypes.string, // nulls order (currently not used - not supported by server)
		changeOrderingFunc: React.PropTypes.func, // function for changing ordering
		changeFilterTypeFunc: React.PropTypes.func // function responsible for changing type of filter
	},
	
	// renders button with column name and ordering support and conditionally also filter type dropdown
	render: function() {
		return (
			<th>
				<div>
					<Grid collapse>
						<Row>						
							<Col sm={12} md={12} lg={12}>
								<ButtonGroup>
									<Button sm bsStyle='primary'
										title={this.props.column.tooltip}
										onClick={this.props.changeOrderingFunc.bind(null, this.props.column.property, this.getNextOrder(), this.getNextNullsOrder())}>
	
										<Icon glyph={this.getGlyph()} />
										{' '}{this.props.column.property}
									</Button>
	
									{this.props.column.searchable === true && this.props.column.face !== 'actions'
										?	<ButtonGroup>
												<Dropdown>
													<DropdownButton sm container={this} menu='optionsMenu'>
														<Icon glyph='icon-fontello-menu' />
													</DropdownButton>
		
													<Menu ref='optionsMenu' onItemSelect={this.onItemSelect}>
														{this.createFilterTypeMenuItems()}				
													</Menu>
												</Dropdown>
											</ButtonGroup>
										: 	null
									}
								</ButtonGroup>
							</Col>
						</Row>
					</Grid>
				</div>
			</th>
		);
	},	
	
	// returns glyph according to the current order and nulls order
	getGlyph: function() {
		switch(this.props.order) {
			case 'ASC':
				return 'icon-fontello-sort-up';
			case 'DESC':
				return 'icon-fontello-sort-down';
			default:
				return 'icon-fontello-sort';
		}
	},

	// returns filter type menu items according to column face
	createFilterTypeMenuItems: function() {
		var filterTypeMenuItems = [];		
		var options = AdvancedTableColumnFilter.getOptions(this.props.column.face);
		
		for(var index = 0; index < options.length; index++) {
			filterTypeMenuItems.push(
				<MenuItem
					key={'filter-type' + options[index].filterType}
					keyaction={options[index].filterType}
					href='#'
					active={options[index].filterType === this.props.filterType}>
					
					{options[index].description}
				</MenuItem>
			);
		}
		
		return filterTypeMenuItems;
	},
	
	// returns next order according to the current order
	getNextOrder: function() {		
		switch(this.props.order) {
			case 'ASC':
				return 'DESC';
			case 'DESC':
				return null;
			default:
				return 'ASC';
		}
	},
	
	// returns next null order according to the current nulls order
	getNextNullsOrder: function() {
		return null;
	},
	
	// listener for filter type selection
	onItemSelect: function(event) {
		this.props.changeFilterTypeFunc(this.props.column.property, event.keyaction);
	}
});

module.exports = AdvancedTableColumnHead;