var AdvancedTableOptions = require('./options.jsx');
var AdvancedTableColumnHead = require('./column/column-head.jsx');
var AdvancedTableRecordSelectionOptions = require('./record-selection-options.jsx');

/******************************************************************************************************************************************************
 * 
 * Component which represents head of table.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableHead = React.createClass({
	
	propTypes: {
		tableId: React.PropTypes.string, // table id
		searchFunc: React.PropTypes.func, // function responsible for searching
		searchWithCustomFilterFunc: React.PropTypes.func, // function responsible for searching with custom filter
		columnsCount: React.PropTypes.number, // total count of columns (may be different from columns.length)
	        
		rest: React.PropTypes.string, // REST URL for getting data
	        
	    createNewDetail: React.PropTypes.string, // URL of detail of new object (primarily used if defined instead of following)        
	    createNewDetailModalFunc: React.PropTypes.func, // function responsible for creating modal detail of new object
	     
	    customFilterFunc: React.PropTypes.func, // function responsible for rendering of custom filter
	    customNamedFilters: React.PropTypes.arrayOf(React.PropTypes.object), // custom named filters (object with attributes name and filter)
		columns: React.PropTypes.array, // columns definition
		
		columnFilterGroupConjunction: React.PropTypes.string, // conjunction in column filter group
	    changeColumnFilterGroupConjunctionFunc: React.PropTypes.func, // function responsible for changing conjunction
		
		ordering: React.PropTypes.array, // ordering state
		changeOrderingFunc: React.PropTypes.func, // function for changing ordering
		
		updateFilteringFunc: React.PropTypes.func, // function responsible for update of filter
		showActionsHeaders: React.PropTypes.bool, // show actions related headers (selection and action column)
		
		selectPageFunc: React.PropTypes.func, // function responsible for adding whole page of records to current selection
        deselectAllFunc: React.PropTypes.func // function responsible for removing all records from current selection
	},
	
	// returns initial state
	getInitialState: function() {
		var columnFilterTypes = [];
		
		for(var index = 0; index < this.props.columns.length; index++) {
			columnFilterTypes.push({
				property: this.props.columns[index].property,
				filterType: this.props.columns[index].filterType == null ? this.getDefaultFilterType(this.props.columns[index].face) : this.props.columns[index].filterType,
			});
		}
		
		return {
			columnFilterTypes: columnFilterTypes
		};
	},

	// returns default filter type if not defined by column
    getDefaultFilterType: function(face) {
    	switch(face) {
    		case 'date':
    		case 'datetime':
    		case 'time':
    		case 'decimal':
    			return 'range';
    		case 'enum':
    		case 'integer':
    			return 'eq';
    		case 'string':
    			return 'like';
    		default:
    			return null;
    	}
    },
	
	// renders table options and column heads&filters
	render: function() {
		var columnHeads = this.createColumnHeads();
		var columnFilters = this.createColumnFilters();
		
    	return (
    		<thead>
    			<tr>
    				<th colSpan={this.props.columnsCount}>
	    				<AdvancedTableOptions 
	    				 	tableId={this.props.tableId}
	    				 	searchFunc={this.props.searchFunc}
	    					searchWithCustomFilterFunc={this.props.searchWithCustomFilterFunc}
	    				 	rest={this.props.rest}
	    			        createNewDetail={this.props.createNewDetail}        
	    			     	createNewDetailModalFunc={this.props.createNewDetailModalFunc}
	    					customFilterFunc={this.props.customFilterFunc}
	    					customNamedFilters={this.props.customNamedFilters}
	    					columnFilterGroupConjunction={this.props.columnFilterGroupConjunction}
	    			     	changeColumnFilterGroupConjunctionFunc={this.props.changeColumnFilterGroupConjunctionFunc} />
    				</th>
    			</tr>
    			
    			<tr>
    				{columnHeads}
    			</tr>
    			
    			<tr>
    				{columnFilters}
    			</tr>
    		</thead>
    	);
    },
    
    // creates heads for all visible columns
    createColumnHeads: function() {    	
    	var columnHeads = [];
    	
    	if(this.props.showActionsHeaders) {
    		columnHeads.push(<th key='empty-selection-head'/>);
    	}
    	
    	for(var index = 0; index < this.props.columns.length; index++) {
    		var ordering = this.getOrdering(index);
    		
    		columnHeads.push(
    			<AdvancedTableColumnHead
    				key={'head-' + index}
    				column={this.props.columns[index]}
    				order={ordering ? ordering.order : null}
    				nullsOrder={ordering ? ordering.nullsOrder : null}
    				changeOrderingFunc={this.props.changeOrderingFunc}
    				changeFilterTypeFunc={this.changeFilterType}
    				updateFilteringFunc={this.props.updateFilteringFunc} />
    		);
    	}
    	
    	if(this.props.showActionsHeaders) {
    		columnHeads.push(<th key='empty-actions-head' />);
    	}
    	
    	return columnHeads;
    },
    
    // creates filters for all searchable columns
    createColumnFilters: function() {
    	var columnFilters = [];
    	
    	if(this.props.showActionsHeaders) {
    		columnFilters.push(
    			<AdvancedTableRecordSelectionOptions
    				key='record-selection-options'
    				selectPageFunc={this.props.selectPageFunc}
    	        	deselectAllFunc={this.props.deselectAllFunc} />
    		);
    	}
    	
    	for(var index = 0; index < this.props.columns.length; index++) {   		
    		if(this.props.columns[index].searchable) {
    			columnFilters.push(
	    			<AdvancedTableColumnFilter
	    				key={'filter-' + index}
						column={this.props.columns[index]}
						filterType={this.state.columnFilterTypes[index].filterType}
						updateFilteringFunc={this.props.updateFilteringFunc} />
    			)
    		} else {
    			columnFilters.push(
    				<th key={'filter-' + index} />
    			);    			
    		}
    	}
    	
    	if(this.props.showActionsHeaders) {
    		columnFilters.push(<th key='empty-actions-filter'/>);
    	}
    	
    	return columnFilters;
    },
    
    // returns ordering for specified column
    getOrdering: function(column) {
    	for(var index = 0; index < this.props.ordering.length; index++) {
    		if(this.props.ordering[index].property === this.props.columns[column].property) {
    			return this.props.ordering[index];
    		}
    	}
    	
    	return null;
    },
    
    // changes type of filter for specified property
    changeFilterType: function(property, filterType) {
    	var newColumnFilterTypes = [];
    	
    	for(var index = 0; index < this.state.columnFilterTypes.length; index++) {
    		if(this.state.columnFilterTypes[index].property === property) {
    			newColumnFilterTypes.push({
    				property: property,
    				filterType: filterType
    			});
    		} else {
    			newColumnFilterTypes.push({
    				property: this.state.columnFilterTypes[index].property,
    				filterType: this.state.columnFilterTypes[index].filterType
    			});
    		}
    	}
    	
    	this.setState({
    		columnFilterTypes: newColumnFilterTypes
    	}, this.props.updateFilteringFunc.bind(this, property, filterType, []));
    }
});

module.exports = AdvancedTableHead;