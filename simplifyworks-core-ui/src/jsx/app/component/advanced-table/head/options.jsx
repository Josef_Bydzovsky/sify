var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

/******************************************************************************************************************************************************
 * 
 * Component responsible for rendering common table buttons (create new, toggle on/off custom filter and custom filters).
 * 
 ******************************************************************************************************************************************************/
AdvancedTableOptions = React.createClass({
	mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],
	
	statics: {
		conjunctions: ['AND', 'OR']
	},
	
	propTypes: {
		 tableId: React.PropTypes.string, // table id
		 searchFunc: React.PropTypes.func, // function responsible for searching
		 searchWithCustomFilterFunc: React.PropTypes.func, // function responsible for searching with custom filter
	        
		 rest: React.PropTypes.string, // REST URL for getting data
	        
	     createNewDetail: React.PropTypes.string, // URL of detail of new object (primarily used if defined instead of following)        
	     createNewDetailModalFunc: React.PropTypes.func, // function responsible for creating modal detail of new object
	     
	     customFilterFunc: React.PropTypes.func, // function responsible for rendering of custom filter
	     customNamedFilters: React.PropTypes.arrayOf(React.PropTypes.object), // custom named filters (object with attributes name and filter)
	     
	     columnFilterGroupConjunction: React.PropTypes.string, // conjunction in column filter group
	     changeColumnFilterGroupConjunctionFunc: React.PropTypes.func // function responsible for changing conjunction
	},
	
	// returns initial state
	getInitialState: function() {
		return {
			customFilterHidden: this.props.customFilterFunc ? false : null // custom filter visibility flag
		}
	},
	
	// renders buttons and conditionally custom filter
	render: function() {
		var primaryOptions = this.createPrimaryOptions();
		var secondaryOptions = this.createSecondaryOptions();		
	
		return (
			<Grid collapse>
				<Row>
					<Col sm={6} md={6} lg={6}>
						<ButtonToolbar>
							{primaryOptions}
						</ButtonToolbar>
					</Col>
					
					<Col sm={6} md={6} lg={6}>
						<ButtonToolbar style={{float: 'right'}}>
							{secondaryOptions}
						</ButtonToolbar>
					</Col>
				</Row>
				
				{this.state.customFilterHidden
					?	<Row>
							{this.props.customFilterFunc()}
						</Row>
					: 	null
				}
			</Grid>
		);
	},
	
	createPrimaryOptions: function() {
		var primaryOptions = [];
		
		primaryOptions.push(
			<ButtonGroup key='create-new-button-group'>
				<Button sm key='create-new' bsStyle='primary' onClick={this.props.createNewDetail ? this.goToCreateNewDetail : this.showCreateNewDetailModal}>
					<Icon glyph='icon-simple-line-icons-plus'/>{' '}
					Create new
				</Button>
			</ButtonGroup>
		);
			
		// toggle on/off custom filter button is visible conditionally
		if(this.state.customFilterHidden != null) {
			var customFilterButtons = [];
		
			customFilterButtons.push(
				<Button sm key ='toggle-custom-filter' onClick={this.toggleCustomFilter} title='Toggle custom filter'>
					<Icon glyph={this.state.customFilterHidden ? 'icon-fontello-down-open-3' : 'icon-fontello-up-open-3'}/>
					{' '}Filter
				</Button>
			);
			
			for(var index = 0; index < this.props.customNamedFilters.length; index++) {
				var customNamedFilter = this.props.customNamedFilters[index];
				
				customFilterButtons.push(
					<Button sm key={'custom-named-filter-' + index} onClick={this.props.searchWithCustomFilterFunc.bind(null, customNamedFilter.createFilter())}>
						{customNamedFilter.render()}
					</Button>
				);
			}
			
			if(this.props.customNamedFilters.length > 0) {
				customFilterButtons.push(
					<Button sm key={'clear-custom-named-filter'} title='Clear named filter' onClick={this.props.searchWithCustomFilterFunc.bind(null, '')}>
						<Icon glyph='icon-fontello-cancel' />
					</Button>
				);				
			}
			
			primaryOptions.push(
				<ButtonGroup key='custom-filter-button-group'>
					{customFilterButtons}
				</ButtonGroup>
			);
		}
		
		return primaryOptions;
	},
	
	createSecondaryOptions: function() {
		var secondaryOptions = [];
		
		secondaryOptions.push(
			<Dropdown key='conjunction-dropdown'>
				<DropdownButton sm container={this} menu='conjunctionsMenu'>
					<span>
						<Icon glyph='icon-fontello-cog-alt' />
						{' '}Conjunction
					</span>
				</DropdownButton>
		
				<Menu ref='conjunctionsMenu' onItemSelect={this.onItemSelect} alignRight>
					{this.createConjunctionMenuItems()}				
				</Menu>
			</Dropdown>
		);
		
		return secondaryOptions;
	},
	
	// goes to create new detail
    goToCreateNewDetail: function() {
    	this.transitionTo(this.props.createNewDetail, {id: 'new'})
    },
    
    // shows create new detail modal
    showCreateNewDetailModal: function() {    	
    	ModalDialogManager.create(this.createNewDetailModal, this.props.tableId);
    },
    
    // returns modal dialog component with content specified by createNewDetailModalFunc
    createNewDetailModal: function() {
    	return (
    		<ModalDialog lg modalId={this.props.tableId}>
    	    	<ModalDialogBody>
    	        	{this.props.createNewDetailModalFunc(this.props.rest + '/new', true, 'new', this.closeCreateNewDetailModal)}
    	        </ModalDialogBody>
    	    </ModalDialog>
    	)
    },
    
    // closes modal dialog and runs search to refresh data
    closeCreateNewDetailModal: function() {
    	ModalDialogManager.remove(this.props.tableId);
    	
    	this.props.searchFunc(false);
    },
	
    // toggles custom filter
	toggleCustomFilter: function(flag) {
		this.setState({
			customFilterHidden: !this.state.customFilterHidden
		});
	},
	
	// returns conjunction menu items
	createConjunctionMenuItems: function() {
		var conjunctionMenuItems = [];
		
		for(var index = 0; index < AdvancedTableOptions.conjunctions.length; index++) {
			var conjunction = AdvancedTableOptions.conjunctions[index];
			
			conjunctionMenuItems.push(
					<MenuItem
						key={conjunction}
						keyaction={conjunction}
						href='#'
							active={conjunction === this.props.columnFilterGroupConjunction}>
					
					{conjunction}
					</MenuItem>
			);
		}
		
		return conjunctionMenuItems;
	},
	
	// listener for conjunction selection
	onItemSelect: function(event) {
		this.props.changeColumnFilterGroupConjunctionFunc(event.keyaction);
	}
});

module.exports = AdvancedTableOptions;