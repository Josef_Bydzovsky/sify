/******************************************************************************************************************************************************
 * 
 * Represents selection column head cell.
 * 
 ******************************************************************************************************************************************************/
AdvancedTableRecordSelectionOptions = React.createClass({	
	
	propTypes: {
		selectPageFunc: React.PropTypes.func.isRequired, // function responsible for adding whole page of records to current selection
        deselectAllFunc: React.PropTypes.func.isRequired // function responsible for removing all records from current selection
	},
	
	// renders buttons for selectPage and deselectAll
	render: function() {
		var buttons = [];
		
		buttons.push(
			<Button sm key='select-page' title='Select page' onClick={this.props.selectPageFunc}>
				<Icon glyph='icon-fontello-ok-squared' />
			</Button>
		);
		
		buttons.push(
			<Button sm key='deselect-all' title='Deselect all' onClick={this.props.deselectAllFunc}>
				<Icon glyph='icon-fontello-minus-squared' />
			</Button>
		);
		
		return (
			<th>
				<ButtonGroup>
					{buttons}
				</ButtonGroup>
			</th>
		);
	}
});

module.exports = AdvancedTableRecordSelectionOptions;