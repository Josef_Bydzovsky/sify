var restEndpoint = '/api';

var AbstractFormComponent = require('./abstract-form-component.jsx');
var Fluxxor = require('fluxxor');
var Dropzone = require('./dropzone.jsx');
var FluxMixin = Fluxxor.FluxMixin(React);

var Attachment = React.createClass({
    mixins: [AbstractFormComponent, FluxMixin, Fluxxor.StoreWatchMixin("FlashStore")],

    propTypes: {
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.array,
        validations: React.PropTypes.string,
        validationsMessage: React.PropTypes.string,
        required: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        labelKey: React.PropTypes.string.isRequired,
        objectType: React.PropTypes.string.isRequired, //Attachment is attached to Object whit this object type in String
        objectIdentifier: React.PropTypes.string, //Attachment is attached to Object whit this object identifier
        attachmentChangeFunc: React.PropTypes.func, // call when changed any component in attachment row (overwrite func attachmentComponentChange ) - for custom detail attachment
        attachmentRowFunc: React.PropTypes.func, // return view for attachment row (overwrite func getAttachmentRow) - for custom detail attachment
        attachmentModalFunc: React.PropTypes.func,    // return view for attachment modal window after file chosen
        attachmentHandleSubmit: React.PropTypes.func,  // override function for handle submit
        rest: React.PropTypes.string,
        filter: React.PropTypes.string
    },

    handleSubmit: function(attachment,component){
      attachment.description = component.model.description;
      var postUrl = restEndpoint + '/attachments';
      var dataToPut = JSON.stringify(attachment);

      //Universal method for save form
      BasicForm.saveMethod(this, component, postUrl, dataToPut, null, this.closeModal.bind(this, this.getModalId(attachment)));
    },

    getStateFromFlux: function () {
        return this.getFlux().store("FlashStore").getState();
    },

    // Whenever the input changes we update the value state
    // of this component
    valueChange: function (value) {
        this.showValidations();
        this.setState({
            value: value
            // When the value changes, wait for it to propagate and
            // then validate the input
        }, function () {
            if (this.props.validate) {
                this.props.validate(this);
            }
            if (this.props.onChange) {
                this.props.onChange(this.state.value);
            }
        }.bind(this));
    },

    componentWillMount: function () {
        this.setStateValue();
        this.props.validations = this.props.validations ? this.props.validations : '';
        if (this.props.attachToForm) {
            this.props.attachToForm(this); // Attaching the component to the form
        }

    },

    componentWillReceiveProps: function(){
        this.setStateValue();
    },

    componentWillUnmount: function () {
        console.log('componentWillUnmount: ' + this.props.name);
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    setStateValue: function(){
        var value = this.getDataValues();
         this.setState({
             value: value
         });
    },

    //Load data via rest api
    getDataValues: function () {
        var parent = this;
        var values = new Array();
        $.ajax({
            async: false,
            url: parent.props.filter ? parent.props.rest+'?filter='+parent.props.filter : parent.props.rest,
            dataType: 'json',
            success: function (data) {
                if(data._embedded){
                  var items = data._embedded.resources;
                  for (var i = 0; i < items.length; i++) {
                      values.push(items[i].resource);
                  }
                }
            }.bind(this)
        });
        return values;
    },

    uploadFile: function (file) {
        var fd = new FormData();
        fd.append('file', file);
        fd.append('name', file.name);
        fd.append('objectType', this.props.objectType);
        fd.append('objectIdentifier', this.props.objectIdentifier);
        fd.append('mimetype', file.type ? file.type : 'none');

        var parent = this;
        $.ajax({
            url: '/api/attachments/upload-temp',
            data: fd,
            dataType: 'json',
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            type: 'POST',
            error: function (xhr, status, err) {
                parent.getFlux().actions.flash.add({
                    container: 'formErrors',
                    messageId: 'formErrors',
                    type: 'error',
                    message: l20n.ctx.getSync(err)
                });

            },
            success: function (success) {
                console.log('upload success', status, JSON.stringify(success));
                var value = parent.state.value;
                if (!value) {
                    value = [];
                }
                value.push(success);
                parent.valueChange(value);
                parent.showModalDetail(success);
            }
        });
    },

    downloadFile: function (item) {
        var url = '/api/attachments/' + item.id + '/download';
        var parent = this;
        $.ajax({
            url: url,
            dataType: 'text',
            enctype: item.mimetype,
            processData: false,
            async: false,
            contentType: false,
            type: 'GET',
            error: function (xhr, status, err) {
                parent.getFlux().actions.flash.add({
                    messageId: 'formErrors',
                    type: 'error',
                    message: l20n.ctx.getSync(err)
                });

            },
            success: function (success) {
                console.log('upload success', success);
                window.open(success);

            }
        });
    },

    onDrop: function (files) {
        console.log('Received files: ', files);
        this.uploadFile(files[0]);
    },

    onOpenClick: function () {
        this.refs.dropzone.open();
    },


    removeItem: function (item, e) {
        console.log('remove item ', item);
        var i = this.state.value.indexOf(item);
        delete this.state.value.splice(i, 1);
        this.valueChange(this.state.value);

        //asynchronously remove attachment
        if(item.id)
          this.asyncRemoveItem(item.id);
    },

    asyncRemoveItem: function (id){
        var postUrl = this.props.rest + '/' + id;
        $.ajax({
            url: postUrl,
            dataType: 'text',
            type: 'DELETE',
            success: function () {
                this.getFlux().actions.flash.add({
                    messageId: 'formErrors',
                    type: 'success',
                    message: l20n.ctx.getSync('success')
                });
            }.bind(this)
        });
    },

    // call when changed any component in attachment row
    attachmentComponentChange: function (attachment, item, type, value) {
        if (type == "description") {
            attachment.description = value;
            return attachment;
        }
    },


    itemChange: function (item, type, event) {
        var value = this.state.value;
        var i = value.indexOf(item);
        var attachment = value[i];

        value[i] = this.props.attachmentChangeFunc ? this.props.attachmentChangeFunc(attachment, item, type, event) : this.attachmentComponentChange(attachment, item, type, event);
        this.valueChange(value);

    },

    editItem: function(item, e){
       this.showModalDetail(item);
    },

   showModalDetail: function(attachment){
      var idModal = attachment.id || "new";
      ModalDialogManager.create(this.getModalDetail.bind(this, attachment),idModal);
   },

   getModalDetail: function(attachment) {
        return (
            <ModalDialog lg modalId={attachment}>
                <ModalDialogBody>
                    {this.getAttachmentModal(attachment)}
                </ModalDialogBody>
            </ModalDialog>
        );
    },

    //return view for attachment row
    getAttachmentRow: function (attachment, changeFunc, editFunc, removeFunc) {
        var currentToken =  this.getFlux().store("AuthStore").getCurrentToken();
        var downloadUrl = "/api/attachments/" + attachment.id + "/download/?access_token="+currentToken;

        return (
            <Row>
                <Col sm={4}>
                    <Row>
                        <Col sm={2}>
                            <a className='dropzone-download' href={downloadUrl}><Icon bundle='icon-ikons-download'
                                                                                      glyph='download'/></a>
                        </Col>
                        <Col sm={10} className='dropzone-name'>
                            {attachment.name}
                        </Col>
                    </Row>
                </Col>
                <Col sm={6}>
                  <FormGroup>
                    <Input type={'text'}
                                     name={attachment.contentGuid}
                                     id={this.props.name}
                                     disabled={true}
                                     value={attachment.description}/>
                  </FormGroup>
                </Col>
                <Button
                    name={"edit"}
                    outlined
                    className='dropzone-remove-btn'
                    bsStyle='blue'
                    onClick={editFunc.bind(this,attachment)}>
                      <Icon glyph={'icon-fontello-pencil'}/>
                </Button>
                {' '}
                <Button
                    name={"delete"}
                    outlined
                    className='dropzone-remove-btn'
                    bsStyle='red'
                    onClick={removeFunc.bind(this,attachment)}>
                    X
                </Button>

            </Row>
        );

    },

    cancelForm: function(attachment,removeFunc, modalId){
      if(!modalId)
         removeFunc(attachment);

      this.closeModal(modalId);
    },

    closeModal: function(idModal){
      if(!idModal)
        idModal = "new";

      ModalDialogManager.remove(idModal);
      this.setStateValue();
    },

    getModalId: function(attachment){
      return (attachment.id ? attachment.id : "new");
    },

    //return view for attachment modal window
    getAttachmentModal: function (attachment) {
          return (
              <Grid>
                  <Row>
                      <Col xs={12}>
                        <BasicForm name={'form-attachment-detail-'+attachment.id}
                                   nameKey={attachment.name}
                                   onSubmit={this.props.attachmentHandleSubmit ? this.props.attachmentHandleSubmit.bind(this,attachment) : this.handleSubmit.bind(this,attachment)}
                                   formColor='blue'
                                   modalClose={this.cancelForm.bind(this,attachment,this.removeItem,attachment.id)}
                                   flux={this.getFlux()}>

                                 {this.props.attachmentModalFunc ?
                                   this.props.attachmentModalFunc(attachment,this.itemChange)
                                 :
                                   <InputText
                                     value={attachment.description}
                                     placeholder={l20n.ctx.getSync('attachment_description')}
                                     labelKey={'attachment_description'}
                                     name='description'
                                     />
                                    }
                        </BasicForm>
                      </Col>
                  </Row>
              </Grid>
          );
    },

    makeFile: function (item) {
        // do some calculations
        return {
            mime: item.mimetype,
            filename: item.name,
            contents: this.downloadFile(item)
        }
    },

    render: function () {
        var value = this.state.value;
        var className = '';

        if (this.state.isValid === false && !this.state.firstValidation && !this.state.disabled) {
            className = 'has-error';
        }
        var attachments = [];
        if (value) {
            for (var i = 0; i < value.length; i++) {
                 if(value[i].id){
                    attachments.push(
                        this.props.attachmentRowFunc ? this.props.attachmentRowFunc(value[i], this.itemChange, this.editItem, this.removeItem) : this.getAttachmentRow(value[i], this.itemChange, this.editItem, this.removeItem)
                    )
                 }
            }
        }

        var component =
            <div>
                <div>
                    <Dropzone ref="dropzone" multiple={false} onDrop={this.onDrop}>
                    </Dropzone>
                </div>
                <Grid>
                    {attachments}
                </Grid>
            </div>

        return this.formGroup(component, className);
    }

});

module.exports = Attachment;
