/**
 * Created by tomiska on 29.4.2015.
 */
var AbstractForm = require('./abstract-form.jsx');
var validator = require('validator');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var BasicForm = React.createClass({

    mixins: [AbstractForm, SidebarMixin, ReactRouter.Navigation, ReactRouter.History, FluxMixin, Fluxxor.StoreWatchMixin("FlashStore")],

    propTypes: {
        name: React.PropTypes.string.isRequired,
        onSubmit: React.PropTypes.func.isRequired,
        routeOnCancel: React.PropTypes.string,
        formColor: React.PropTypes.string,
        isFormChild: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        visibleButtons: React.PropTypes.bool,
        visibleSaveButton: React.PropTypes.bool,
        nameKey: React.PropTypes.string,
        resetForm: React.PropTypes.func,
        glyph: React.PropTypes.string,
        modalClose: React.PropTypes.func
    },


    statics: {
        //Universal method for save form (data tu save are prepare in implement template "componentMain")
        saveMethod: function (componentMain, basicFormComponent, postUrl, dataToPut, detailRoute, modalClose) {
            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                url: postUrl,
                dataType: 'json',
                type: componentMain.state.isNew === true ? 'POST' : 'PUT',
                data: dataToPut,
                success: function (data) {
                    this.getFlux().actions.flash.add({
                        messageId: 'formErrors',
                        type: 'success',
                        message: l20n.ctx.getSync('success')
                    });
                    console.log("Results: " + JSON.stringify(data));
                    basicFormComponent.setState({
                        isSubmitting: false
                    });

                    if (modalClose) {
                        //in case of modal window, only close them
                        modalClose();
                    } else {
                        if (componentMain.state.isNew === true) {
                            //If is this new form, we update state and do redirect to new perssited item
                            componentMain.setState({
                                data: data.resource,
                                id: data.resource.id,
                                loaded: true,
                                isNew: false
                            })
                            console.log('Redirect to: ' + detailRoute + ' id: ' + data.resource.id);
                            componentMain.transitionTo(detailRoute, {id: data.resource.id});
                        } else {
                            componentMain.setState({data: data.resource, loaded: true})
                        }
                    }

                }.bind(componentMain),
                error: function (xhr, status, err) {
                    console.error(componentMain.props.url, status, err.toString());
                    basicFormComponent.setState({
                        isSubmitting: false
                    });
                }
            });
        },

        loadMethod: function (component, url, loadOtherFunc) {
            // configuring default options for Messenger
            Messenger.options = {
                theme: 'flat'
            };
            var includeMetadata = false;
            if (component.props.includeMetadata === true) {
                includeMetadata = true;
            }
            if (component.state.isNew !== true) {
                $.ajax({
                    url: url,
                    data: {includeMetadata : includeMetadata},
                    dataType: 'json',
                    success: function (data) {
                        var otherData;
                        if (loadOtherFunc) {
                            otherData = loadOtherFunc();
                        }
                        this.setState({data: data.resource, loaded: true, otherData: otherData, metadata: data.metadata});
                    }.bind(component)
                });
            } else {
                //I dont know why, byt when i used only "this.setState({data: [], loaded: true})" than thrown "Context not ready"
                $.ajax({
                    url: '',
                    dataType: '',
                    success: function (data) {
                        var data = {};
                        //when is entity new we generate uiid as @id
                        data['@id'] = BasicForm.generateUUID();
                        this.setState({data: data, loaded: true})
                    }.bind(component)
                });
            }
            component.getFlux().actions.flash.clear('formErrors');
        },

        isNew: function (id) {
            return id == 'new' ? true : false;
        },

        generateUUID() {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        }


    },

    render: function () {

        //TODO: to css
        var detail_footer_style = {
            textAlign: 'right'
        };

        var btnSaveLabel = l20n.ctx.getSync(this.state.isNew ? 'btn_create' : 'btn_save')
        var btnCancelLabel = l20n.ctx.getSync('btn_cancel')

        var componentForm =
            <Form id={'form-'+this.props.name} onChange={this.onChange}
                  onSubmit={this.handleSubmit}>
                {this.props.children}
            </Form>;

        var componentFooterButton = '';

        if (this.props.visibleButtons && !this.props.isFormChild) {
            componentFooterButton =
                <ButtonToolbar style={{display: 'inline-block'}}>
                    <Button outlined
                            onlyOnHover
                            bsStyle='red'
                            disabled={this.state.isSubmitting}
                            onClick={this.cancelForm}>
                        {btnCancelLabel}
                    </Button>
                    {this.props.visibleSaveButton === false ? "" : (
                        <Button
                            bsStyle='primary'
                            onClick={this.handleSubmit}
                            disabled={this.state.isSubmitting}>
                            {btnSaveLabel}
                        </Button >)}
                </ButtonToolbar>;
        }


        if (this.props.isFormChild === true) {
            componentForm = this.props.children;
        }

        var formClassName = '';
        if (this.props.formColor != null) {
            formClassName = 'bg-dark' + this.props.formColor + ' fg-white';
        }

        var component =
            <PanelContainer noOverflow bordered>
                <Panel>
                    <PanelHeader className={formClassName}>
                        <Grid>
                            <Row>
                                <Col xs={12}>
                                    <h3>
                                        {this.props.glyph != null ?
                                            <span><Icon glyph={this.props.glyph}/>{' '}</span>
                                            :
                                            null
                                        }
                                        {l20n.ctx.getSync(this.props.nameKey)}
                                    </h3>
                                </Col>
                            </Row>
                        </Grid>
                    </PanelHeader>
                    <PanelBody className={this.props.isFormChild ?'basic-formChild' : null}>
                        <Grid>
                            <Row>
                                <Col xs={12}>
                                    <FlashMessages id="formErrors" ref="formErrors"/>
                                    {componentForm}
                                </Col>
                            </Row>
                        </Grid>
                    </PanelBody>
                    {componentFooterButton ?
                        <PanelFooter>
                            <Grid>
                                <Row>
                                    <Col xs={12} className='text-right'>
                                        {componentFooterButton}
                                    </Col>
                                </Row>
                            </Grid>
                        </PanelFooter>
                        :
                        null
                    }

                </Panel>
            </PanelContainer>


        return (
            <div>{component}</div>
        )

    }
});


module.exports = BasicForm;