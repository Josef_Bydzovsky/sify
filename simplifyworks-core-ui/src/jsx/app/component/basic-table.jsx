var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var BasicTable = React.createClass({
    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    propTypes: {
        id: React.PropTypes.string, // this component id
        entity: React.PropTypes.string, // entity name from localisation - is used for automatic key resolution ie. user_username
        parentId: React.PropTypes.string,
        rest: React.PropTypes.string, // url for load data
        filter: React.PropTypes.string, // additional filter container
        order: React.PropTypes.string, // default order (columns)
        searching: React.PropTypes.bool,
        ordering: React.PropTypes.bool,
        paging: React.PropTypes.bool,
        info: React.PropTypes.bool,
        detail: React.PropTypes.string,
        showNewButton: React.PropTypes.bool,
        showFulltext: React.PropTypes.bool, // shows input for fulltext serch
        filterForm: React.PropTypes.func,
        value: React.PropTypes.array,
        modalDetail: React.PropTypes.func,
        removeItemFunc: React.PropTypes.func //For custom remove
    },

    getDefaultProps() {
        return {
            id: "basic-table-1", // TODO: dynamic client id
            searching: true,
            ordering: true,
            paging: true,
            info: true,
            showNewButton: true,
            showFulltext: false
        };
    },

    getInitialState() {
        return {
            filterHidden: true,
            value: this.props.value,
            resolvedHeaders: []
        };
    },

    componentWillMount() {
        ReactBootstrap.Dispatcher.on('ctx:ready', this.l20nContextReady);
        if(l20n.isReady()) {
            this.l20nContextReady();
        }
        this.setState({filter: this.props.filter});
    },

    componentWillUnmount() {
        ReactBootstrap.Dispatcher.off('ctx:ready', this.l20nContextReady);
    },

    l20nContextReady() {
        var entityPrefix = this._resolveEntityName();
        resolvedHeaders = [];
        React.Children.map(this.props.children, function (child, index) {
            var headerText;
            // action or detail column doesnt have title
            if (child.props.face == 'actions' || child.props.face == 'detail') {
                return "";
            }
            if (child.props.labelKey != null) {
                headerText = l20n.ctx.getSync(child.props.labelKey);
            } else {
                if(entityPrefix != null) {
                    var entityKey = entityPrefix + "_" + child.props.property.replace('.', '_');
                    headerText = l20n.ctx.getSync(entityKey);
                    if(headerText == entityKey) {
                        headerText = null;
                    }
                }
                if(headerText == null) {
                    headerText = l20n.ctx.getSync(child.props.property.replace('.', '_'));
                }
            }
            resolvedHeaders[index] = headerText;
        }.bind(this));

        this.setState({
            resolvedHeaders: resolvedHeaders
        });
    },

    _resolveEntityName() {
        if(this.props.entity != null) {
            return this.props.entity;
        }
        if(!this.props.rest || this.props.rest.lastIndexOf("/") < 0) {
            return null;
        }
        var result = this.props.rest.substr(this.props.rest.lastIndexOf("/") + 1);
        return result.substr(0, result.length - 1);
    },

    componentWillReceiveProps(next) {
        if (next.filter !== this.props.filter) {
            console.log('componentWillReceiveProps new filter: ' + next.filter);
            this.setState({filter: next.filter}, function () {
                $("#" + this.props.id).dataTable().fnDraw();
            });
        }
        if (next.value !== this.state.value) {
            console.log('componentWillReceiveProps new value: ' + next.value);
            this.setState({value: next.value}, function () {
                var data = [];
                var metadata = [];
                this.evalStaticData(data, metadata);
                this.metadata = metadata;

                $("#" + this.props.id).dataTable().fnClearTable();
                $("#" + this.props.id).dataTable().fnAddData(data);
                $("#" + this.props.id).dataTable().fnDraw();
            });
        }
    },

    evalStaticData(data, metadata) {
        for (var resource in this.state.value) {
            var id = this.state.value[resource].resource.id;
            //console.log('Value::'+this.state.value[resource].resource.id);
            data.push(this.state.value[resource].resource);
            metadata[id] = this.state.value[resource].metadata;
        }
    },

    componentDidMount() {
        var columns = [];
        var columnDefs = [];
        var viedId = this.getFlux().store("AuthStore").hasRole('admin');
        React.Children.map(this.props.children, function (child, index) {
            columns.push({"data": child.props.property});
            // column rendering
            columnDefs.push({
                targets: index,
                data: null,
                defaultContent: "",
                visible: true,
                className: (child.props.visible ? '' : 'hidden') + (child.props.face == 'actions' || child.props.face == 'detail' ? ' actions' : ''),
                searchable: child.props.searchable && child.props.face != 'actions' && child.props.face != 'detail',
                width: child.props.width,
                orderable: child.props.orderable && child.props.face != 'actions' && child.props.face != 'detail',
                render: function (data, type, row) {
                    // TODO: we would append react child to table
                    switch (child.props.face) {
                        case 'detail':
                            if (this.props.selectItem) {
                                return "<a href='#' onclick='$(\"#selectItem-button" + this.props.id + "\").attr(\"entityId\", \"" + data + "\").click(); return false;' title='Select record" + (viedId ? " [id:" + data + "]" : "") + "' class='rubix-icon icon-flatline-ok detail-link'></a>";
                            } else {
                                return "<a href='#' onclick='$(\"#detail-button" + this.props.id + "\").attr(\"entityId\", \"" + data + "\").click(); return false;' title='View detail" + (viedId ? " [id:" + data + "]" : "") + "' class='rubix-icon icon-flatline-search detail-link'></a>";
                            }
                        case 'actions':
                            if (!this.props.disabled && !this.props.selectItem && (!this.metadata[data] || (this.metadata[data] && this.metadata[data].canDelete))) {
                                return "<a href='#' onclick='$(\"#delete-button" + this.props.id + "\").attr(\"entityId\", \"" + data + "\").click(); return false;' title='Delete record" + (viedId ? " [id:" + data + "]" : "") + "' class='rubix-icon icon-ikons-square-delete delete-action'></a>";
                            } else {
                                var title = 'basicTable_disable_delete_tooltip'; //defaultni tooltip
                                if (typeof child.props.title != 'undefined' && child.props.title != '') {
                                    title = child.props.title; //vlastni tooltip
                                }
                                return "<span title='" + l20n.ctx.getSync(title) + "' class='rubix-icon icon-ikons-square-delete delete-disable'></span>";
                            }
                        case 'boolean':
                            if (!data) {
                                return '<i class="rubix-icon icon-fontello-check-empty" title="' + l20n.ctx.getSync('basicTable_column_boolean_no') + '"></i>';
                            }
                            return '<i class="rubix-icon icon-fontello-check-1" title="' + l20n.ctx.getSync('basicTable_column_boolean_yes') + '"></i>';
                        case 'date':
                            if (data == null) {
                                return "";
                            }
                            // TODO: Date pattern from localization
                            return new Date(data).format("yyyy-mm-dd HH:MM:ss");
                        case 'enum':
                            return l20n.ctx.getSync(child.props.localizationKeyPrefix + data);
                        case 'codelist':
                            return child.props.niceLabelFunc(data);
                        default:
                            return data;
                    }
                }.bind(this)
            });
        }.bind(this));

        var filter = this.props.filter;
        var order = this.props.order;
        var tableId = this.props.id;

        var data = [];
        var metadata = [];
        if (this.state.value) {
            this.evalStaticData(data, metadata);
            this.metadata = metadata;
        }

        var table = $('#' + this.props.id)
            .DataTable({
                responsive: true,
                processing: true,
                serverSide: this.props.rest ? true : false,
                searching: this.props.rest ? true : false,
                ordering: this.props.rest ? this.props.ordering : false,
                paging: this.props.rest ? this.props.paging : false,
                info: this.props.info,
                stateSave: false,
                dom: '<"top"' + (this.props.showFulltext ? 'f' : '') + '>rt<"bottom"lpi><"clear">',
                aaSorting: [],
                data: data ? data : undefined,
                ajax: this.props.rest !== undefined ? {
                    url: this.props.rest,
                    data: function (data) {
                        data.filter = (this.state.filter ? this.state.filter : '') + ',' + (this.state.userFilter ? this.state.userFilter : '');
                        data.simpleOrder = order;
                        data.includeMetadata = true;
                    }.bind(this),
                    dataSrc: function (json) {
                        // we need extract source data from hateoas response
                        var data = [];
                        var metadata = [];
                        if (json._embedded !== undefined) {
                            for (var resource in json._embedded.resources) {
                                var id = json._embedded.resources[resource].resource.id;
                                data.push(json._embedded.resources[resource].resource);
                                metadata[id] = json._embedded.resources[resource].metadata;
                            }
                            this.metadata = metadata;
                        }
                        return data;
                    }.bind(this),
                    error: function (xhr, status, error) {
                        this.getFlux().actions.flash.ajaxError(xhr, status, error);
                    }.bind(this)
                } : undefined,
                columns: columns,
                columnDefs: columnDefs,
                lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).on('dblclick', function () {
                        $(nRow).find('.detail-link').click();
                    });
                }
            });

        this.setState({table: table});
    },

    resolveHeaderTooltip(child){
        var headerTooltip;
        if (child.props.labelKey != null) {
            var tooltipKey = child.props.labelKey+'_tooltip';
            headerTooltip = l20n.ctx.getSync(tooltipKey);
            if (headerTooltip == tooltipKey)
                headerTooltip = null;
        }
        return headerTooltip;
    },

    reloadTable(userFilter, callback) {
        this.setState({userFilter: userFilter}, function () {
            $("#" + this.props.id).dataTable().fnDraw();
            callback();
        });
    },

    renderFilter() {
        var headers = [];
        var table = this;
        React.Children.map(this.props.children, function (child, index) {
            headers.push(
                <td className={'col'+index}>
                    {child.props.searchable
                        ?
                        <BasicTable.FilterComponent name={table.props.id+'-filter-'+index} face={child.props.face}
                                                    table={table.state.table}
                                                    colIdx={index}
                                                    placeholder={'Search'}
                                                    enumValues={child.props.enumValues}
                                                    localizationKeyPrefix={child.props.localizationKeyPrefix}
                            />
                        :
                        <span></span>
                    }
                </td>
            );
        });
        return (
            <tr className='basic-table-filter'>{headers}</tr>
        );
    },

    renderHeader() {
        var headers = [];
        React.Children.map(this.props.children, function (child, index) {
            headers.push(
                <th title={this.resolveHeaderTooltip(child)}>
                    {this.state.resolvedHeaders[index]}
                </th>
            );
        }.bind(this));
        return (
            <tr>{headers}</tr>
        );
    },

    closeModal() {
        ModalDialogManager.remove(this.props.id);
        $("#" + this.props.id).dataTable().fnDraw();
    },

    getModalDetail(idElement) {
        if (!idElement) {
            idElement = $('#detail-button' + this.props.id).attr('entityId');
        }
        console.log("url: " + this.props.rest + '/' + idElement);
        return (
            <ModalDialog lg modalId={this.props.id}>
                <ModalDialogBody>
                    {this.props.modalDetail ? this.props.modalDetail(this.props.rest + '/' + idElement, BasicForm.isNew(idElement), idElement, this.closeModal) : null}
                </ModalDialogBody>
            </ModalDialog>
        );
    },

    goToDetail(event) {
        this.transitionTo(this.props.detail, {
            id: $('#' + event.target.id).attr('entityId'),
            parentId: this.props.parentId
        });
    },

    selectItem(event) {
        this.props.selectItem($('#' + event.target.id).attr('entityId'));
    },

    deleteItem(event) {
        var id = $('#' + event.target.id).attr('entityId');
        var table = this;
        vex.dialog.confirm(
            {
                message: 'TODO Chcete odstranit vybraný záznam?', //TODO l20n.ctx.getSync('basicTable_button_delete_confirm'),
                callback: function (value) {
                    if (!value) {
                        return;
                    }

                    if (table.props.removeItemFunc) {
                        table.props.removeItemFunc(id, table);
                    } else {

                        $.ajax({
                            headers: {
                                'Accept': 'plain/text',
                                'Content-Type': 'application/json'
                            },
                            url: table.props.rest + '/' + id,
                            type: 'DELETE',
                            dataType: 'text',
                            success: function (data) {
                                table.getFlux().actions.flash.add({
                                    messageId: 'success',
                                    type: 'success',
                                    message: l20n.ctx.getSync('success')
                                });

                                table.state.table.ajax.reload(null, false);
                            }
                        });
                    }
                }
            });
    },

    goToNewDetail(event) {
        this.transitionTo(this.props.detail, {id: 'new', parentId: this.props.parentId});
    },

    collapseFilter() {
        this.setState({
            filterHidden: true
        });
        var container = $(this.refs['filter-container'].getDOMNode());
        this.height = container.height();
        $(container).css('overflow', 'hidden');
        $(container).animate({
            height: 0
        }, 250, 'swing');
    },

    expandFilter() {
        this.setState({
            filterHidden: false
        });
        var container = $(this.refs['filter-container'].getDOMNode());
        $(container).animate({
            height: this.height
        }, 250, 'swing', function () {
            $(container).css({
                height: '',
                overflow: ''
            });
        });
    },

    toogleFilter(e) {
        e.preventDefault();
        e.stopPropagation();
        if (this.state.filterHidden) {
            this.expandFilter();
        } else {
            this.collapseFilter();
        }
    },

    render() {
        var createNewButton =
            (this.props.modalDetail ? (
                <Button sm
                        outlined
                        bsStyle='primary'
                        disabled={this.props.disabled}
                        onClick={ModalDialogManager.create.bind(this, this.getModalDetail.bind(this, "new"), this.props.id)}>
                    <Icon glyph='icon-simple-line-icons-plus'/>{' '}
                    <Entity entity='basicTable_button_new'/>
                </Button>
            )
                :
                (
                    <Button sm
                            outlined
                            bsStyle='primary'
                            disabled={this.props.disabled}
                            onClick={this.goToNewDetail}>
                        <Icon glyph='icon-simple-line-icons-plus'/>{' '}
                        <Entity entity='basicTable_button_new'/>
                    </Button>
                )
            );

        var toogleFilterButton =
            <Button sm
                    outlined
                    onClick={this.toogleFilter}
                    id='filter-button'
                    className='active'>
                <Icon glyph={this.state.filterHidden ? 'icon-fontello-down-open-3' : 'icon-fontello-up-open-3'}/>{' '}
                Filter
            </Button>;

        var filterClasses = React.addons.classSet({
            'basic-table-filter': true,
            'collapsed': this.state.filterHidden
        });

        return (
            <div className='basic-table-container'>
                <div className='hidden'>
                    <Button bsStyle='primary' onClick={this.reload}>reload</Button>                    
                    {this.props.modalDetail && !this.props.detail ?
                        (
                            <Button onClick={ModalDialogManager.create.bind(this, this.getModalDetail, this.props.id)}
                                    id={'detail-button'+this.props.id}>Detail</Button>
                        )
                        :
                        (
                            <Button onClick={this.goToDetail} id={'detail-button'+this.props.id}>Detail</Button>
                        )
                    }
                    <Button onClick={this.selectItem} id={'selectItem-button'+this.props.id}>Select item</Button>
                    <Button onClick={this.deleteItem} id={'delete-button'+this.props.id}>Delete item</Button>
                </div>

                {this.props.showNewButton ?
                    <ButtonToolbar>
                        {createNewButton}
                        {this.props.filterForm ? toogleFilterButton : null}
                    </ButtonToolbar>
                    :
                    null
                }

                {this.props.filterForm ?
                    <div ref="filter-container" className={filterClasses}>
                        {this.props.filterForm()}
                    </div>
                    :
                    null
                }

                <Table id={this.props.id} className='table table-striped table-bordered table-condensed' cellSpacing='0'
                       width='100%'>
                    <thead>
                    {this.props.rest ? this.renderFilter() : null}
                    {this.renderHeader()}
                    </thead>
                </Table>
            </div>
        );
    }
});

BasicTable.BasicColumn = React.createClass({
    propTypes: {
        property: React.PropTypes.string,
        value: React.PropTypes.string,
        face: React.PropTypes.oneOf(['text', 'date', 'actions', 'enum', 'detail', 'codelist']),
        niceLabelFunc: React.PropTypes.func,
        width: React.PropTypes.string,
        orderable: React.PropTypes.bool,
        searchable: React.PropTypes.bool,
        visible: React.PropTypes.bool,
        localizationKeyPrefix: React.PropTypes.string,
        labelKey: React.PropTypes.string,
        enumValues: React.PropTypes.object
    },

    getInitialState() {
        return {};
    },

    getDefaultProps() {
        return {
            face: 'text',
            orderable: true,
            searchable: true,
            visible: true
        };
    },

    // Deprecated - viz DataTables
    renderValue(value) {
        switch (this.props.face) {
            case 'date':
            {
                // TODO: Date pattern from localization
                return new Date(value).format("yyyy-mm-dd HH:MM:ss");
            }
            default:
                return value;

        }
    },

    // @Deprecated - viz DataTables
    render() {
        var columnValue = this.state.value;
        this.props.property.split('.').forEach(function (propertyPart) {
            if (columnValue == null) {
                return false;
            }
            columnValue = columnValue[propertyPart];
        });
        return (
            <td className={this.props.className}>
                {this.renderValue(columnValue)}
            </td>
        );
    }
});

BasicTable.FilterComponent = React.createClass({
    propTypes: {
        name: React.PropTypes.string,
        face: React.PropTypes.string,
        placeholder: React.PropTypes.string,
        table: React.PropTypes.object,
        colIdx: React.PropTypes.number,
        enumValues: React.PropTypes.object,
        localizationKeyPix: React.PropTypes.string,
        niceLabelFunc: React.PropTypes.func
    },

    search(value) {
        this.props.table.column(this.props.colIdx)
            .search(value)
            .draw();
    },

    render() {
        switch (this.props.face) {
            case 'actions':
                return <span></span>;
            case 'detail':
                return <span></span>;
            case 'boolean':
                return <span>---</span>;
            case 'codelist':
                return <span>---</span>;
            case 'date':
                return <Datepicker
                    sm
                    labelKey=''
                    name={this.props.name}
                    placeholder={this.props.placeholder}
                    showButtons={false}
                    onChange={this.search}/>;
            case 'enum':
                return <Enumbox
                    sm
                    labelKey=''
                    name={this.props.name}
                    placeholder={this.props.placeholder}
                    options={Enumbox.enumOptions(this.props.enumValues,this.props.localizationKeyPrefix,true)}
                    onChange={this.search}/>;
            default:
                return <InputText
                    sm
                    labelKey=''
                    name={this.props.name}
                    placeholder={this.props.placeholder}
                    onChange={this.search}/>;
        }
    }
});

module.exports = BasicTable;
