var AbstractFormComponent = require('./abstract-form-component.jsx');

var Datepicker = React.createClass({
    mixins: [AbstractFormComponent],

    propTypes: {
        //Input Name
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.number,
        validations: React.PropTypes.string,
        validationsMessage: React.PropTypes.string,
        required: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        labelKey: React.PropTypes.string.isRequired,
        datePattern: React.PropTypes.string,
        timeEnabled: React.PropTypes.bool,
        futureOnly: React.PropTypes.bool,
        minuteStep: React.PropTypes.number,
        showButtons: React.PropTypes.bool,
        weekStart: React.PropTypes.string,
        onChange: React.PropTypes.func,
    },

    getDefaultProps: function () {
        return {
            showButtons: true,
            timeEnabled: false,
            futureOnly: false,
            minuteStep: 5,
            weekStart: '1'
        };
    },

    componentWillMount: function () {
        this.setState({
            firstValidation: true
        })

        this.props.validations = this.props.validations ? this.props.validations : '';

        if (this.props.attachToForm) {
            this.props.attachToForm(this); // Attaching the component to the form
        }
    },

    componentWillUnmount: function () {
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps.value=='') {
            $('#' + this.props.name).datetimepicker('reset');
        } else {
            $('#' + this.props.name).datetimepicker('setDate', new Date(nextProps.value));
        }
    },

    componentDidMount: function () {
        $('#' + this.props.name).datetimepicker({
            // RT: l20n broke context after F5
            // format: ((this.props.datePattern ? this.props.datePattern : l20n.ctx.getSync('patternDate')) + (this.props.timeEnabled ? ' hh:ii' : '')),
            weekStart: this.props.weekStart,
            autoclose: true,
            // language: l20n.ctx.getSync('locale'),
            todayBtn: true,
            todayHighlight: true,
            startDate: (this.props.futureOnly ? '$.datetimepicker.utcToday()' : 'null'),
            minView: (this.props.timeEnabled ? '0' : '2'),
            minuteStep: this.props.minuteStep
        }).on('changeDate', function (ev) {
            this.setState({
                value: ev.date ? ev.date.getTime() + ev.date.getTimezoneOffset() * 60 * 1000 : ''
            }, function () {
                if (this.props.validate) {
                    this.props.validate(this);
                }
                if (this.props.onChange) {
                    this.props.onChange(this.state.value);
                }
            });

        }.bind(this)).datetimepicker('setDate', new Date(this.props.value));
    },

    today: function () {
        if (!this.props.disabled) {
            $('#' + this.props.name).datetimepicker('today');
        }
    },

    showCalendar: function () {
        if (!this.props.disabled) {
            $('#' + this.props.name).datetimepicker('show');
        }
    },

    resetCalendar: function () {
        if (!this.props.disabled) {
            $('#' + this.props.name).datetimepicker('reset');
        }
    },

    previousDay: function () {
        if (!this.props.disabled) {
            $('#' + this.props.name).datetimepicker('increment', -1);
        }
    },

    nextDay: function () {
        if (!this.props.disabled) {
            $('#' + this.props.name).datetimepicker('increment', 1);
        }
    },

    updateFromInput: function (e) {
        if (e.target.value=='') {
            $('#' + this.props.name).datetimepicker('reset');
        } else {
            $('#' + this.props.name).datetimepicker('update');
        }
    },

    render: function () {
        var value = this.state.value;

        var className = '';

        if (this.state.isValid === false && !this.state.firstValidation && !this.state.disabled) {
            className = 'has-error';
        }


        var buttonToday;
        var buttonPrevious;
        var buttonNext;
        if (this.props.showButtons && !this.props.disabled) {
            buttonToday =
                <Button onClick={this.today}
                        sm={this.props.sm}
                        lg={this.props.lg}
                        xs={this.props.xs}
                        outlined
                        bsStyle='info'
                        disabled={this.props.disabled}><Icon bundle='icon-simple-line-icons-target'
                                                             glyph='List'/></Button>;
            buttonPrevious =
                <Button onClick={this.previousDay}
                        sm={this.props.sm}
                        lg={this.props.lg}
                        xs={this.props.xs}
                        outlined
                        bsStyle='info'
                        disabled={this.props.disabled}><Icon bundle='icon-simple-line-icons-arrow-left'
                                                             glyph='List'/></Button>;
            buttonNext =
                <Button onClick={this.nextDay}
                        sm={this.props.sm}
                        lg={this.props.lg}
                        xs={this.props.xs}
                        outlined
                        bsStyle='info'
                        disabled={this.props.disabled}><Icon bundle='icon-simple-line-icons-arrow-right' glyph='List'/></Button>;
        }

        var button;
        var buttonReset;
        if (!this.props.disabled) {
            button =
                <Button onClick={this.showCalendar}
                        sm={this.props.sm}
                        lg={this.props.lg}
                        xs={this.props.xs}
                        outlined
                        bsStyle='primary'><Icon bundle='icon-simple-line-icons-calendar' glyph='List'/></Button>;
            buttonReset =
                <Button onClick={this.resetCalendar}
                        sm={this.props.sm}
                        lg={this.props.lg}
                        xs={this.props.xs}
                        outlined
                        bsStyle='danger'><Icon bundle='outlined' glyph='delete'/></Button>;
        }

        var component =
            <InputGroup className={'datepicker-control'+(this.props.timeEnabled?' timeEnabled':'')}>
                <Input type='text'
                       id={this.props.name}
                       sm={this.props.sm}
                       lg={this.props.lg}
                       xs={this.props.xs}
                       onChange={this.updateFromInput}
                       onKeyPress={this.props.keyPressListener}
                       placeholder={this.props.disabled?'':this.props.placeholder}
                       className={this.props.className}
                       disabled={this.state.disabled}
                       ref={this.props.name}
                    />
                <InputGroupButton>
                    {button}
                    {buttonReset}
                    {buttonToday}
                    {buttonPrevious}
                    {buttonNext}
                </InputGroupButton>
            </InputGroup>


        return this.formGroup(component, className);
    }
});

module.exports = Datepicker;