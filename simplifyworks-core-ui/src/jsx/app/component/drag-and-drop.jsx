var placeholder, dragZone, dropZone, actualWidth;

var DragAndDrop = React.createClass({

    dragStart: function(e) {
        dragZone = document.getElementsByClassName("drag")[0];
        dropZone = document.getElementsByClassName("drop")[0];

        this.dragged = e.currentTarget;
        actualWidth = this.dragged.offsetWidth;
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData("text/html", e.currentTarget); //nutne pro firefox
    },

    dragOver: function(e) {
        e.preventDefault();
        placeholder.style.width = actualWidth + "px";
        this.dragged.style.display = "none";
        if (e.target.className === "placeholder") return;

        var element = e.target;
        if (dropZone.hasChildNodes()) {
            var mark = e.target.offsetWidth / 2;
            var relX = e.clientX - e.target.getBoundingClientRect().left;
            if (element.className === "label-1" || element.className === "label-2") {
                element = element.parentNode.parentNode;
            }
            if (relX > mark) {
                dropZone.insertBefore(placeholder, element.nextSibling);
            } else {
                dropZone.insertBefore(placeholder, element);
            }
        } else {
            dropZone.appendChild(placeholder);
        }
    },

    dragEnd: function(e) {
        this.dragged.style.display = "inline-block";
        dropZone.removeChild(placeholder);
    },

    drop: function(e) {
        this.dragged.style.display = "inline-block";
        this.dragged.className = "fields";
        dropZone.insertBefore(this.dragged, placeholder);
        dropZone.removeChild(placeholder);
    },

    remove: function(e) {
        dragZone = document.getElementsByClassName("drag")[0];
        dropZone = document.getElementsByClassName("drop")[0];

        var item = e.target.parentNode;
        item.className = "possible-fields";
        var index = item.dataset.index;
        if (dragZone.hasChildNodes()) {
            var add = false;
            for (var i = 0; i < dragZone.children.length; i++) {
                if (dragZone.children[i].dataset.index > index) {
                    dragZone.insertBefore(item, dragZone.children[i]);
                    add = true;
                    break;
                }
            }
            if (add === false) dragZone.appendChild(item);
        } else {
            dragZone.appendChild(item);
        }
        dropZone.removeChild(item);
    },

    sort: function(items, property) {
        items.sort(function(item1, item2) {
            var key1 = item1[property], key2 = item2[property];
            if (key1 < key2) return -1;
            if (key1 > key2) return 1;
            return 0;
        });
        return items;
    },

    map: function(items) {
        var elements = items.map((function (item) {
            var idName = item.classname.charAt(0).toLowerCase() + item.classname.slice(1);
            var labelClassName = l20n.ctx.getSync(idName + "_report");
            var labelFieldName = l20n.ctx.getSync(idName + "_" + item.fieldname);
            return (
                <div className={item.order === null ? "possible-fields" : "fields"}
                     key={item.key}
                     data-index={item.index}
                     data-key={item.key}
                     draggable="true"
                     onDragStart={this.dragStart}
                     onDragEnd={this.dragEnd}
                     onDragOver={this.dragOver}
                     onDrop={this.drop}>
                    <div className="label-container">
                        <span className="label-1">{labelClassName}</span>
                        <span className="label-2">{labelFieldName}</span>
                    </div>
                    <div className='icon-ikons-square-delete' title={l20n.ctx.getSync('dnd_delete_tooltip')} onClick={this.remove}></div>
                </div>
            );
        }).bind(this));
        return elements;
    },

    render: function() {
        placeholder = document.createElement('div');
        placeholder.className = "placeholder";
        placeholder.textContent = l20n.ctx.getSync("dnd_drop_here");

        var possibleFields = this.props.possibleFields;
        var fields = this.props.fields;
        if (fields === undefined || fields === null) fields = [];

        var sorted = this.sort(possibleFields.concat(fields), "key"); //vse seradit dle abecedy
        possibleFields = [];
        fields = [];
        for (var i = 0; i < sorted.length; i++) { //roztridit do possibleFields a fields
            sorted[i].index = i;
            if (sorted[i].order === null) {
                possibleFields.push(sorted[i]);
            } else {
                fields.push(sorted[i]);
            }
        }
        possibleFields = this.map(possibleFields);
        fields = this.sort(fields, "order"); //seradit dle order
        fields = this.map(fields);

        return (
            <div className="dnd">
                <label>{l20n.ctx.getSync(this.props.labelDrag)}</label>
                <div className="drag">{possibleFields}</div>
                <label>{l20n.ctx.getSync(this.props.labelDrop)}</label>
                <div className="drop"
                     onDragOver={this.dragOver}
                     onDrop={this.drop}>{fields}</div>
            </div>
        );
    }
});

module.exports = DragAndDrop;