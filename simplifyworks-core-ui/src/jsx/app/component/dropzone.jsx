var Dropzone = React.createClass({
    getDefaultProps: function () {
        return {
            supportClick: true,
            multiple: true
        };
    },

    getInitialState: function () {
        return {
            isDragActive: false
        };
    },

    propTypes: {
        onDrop: React.PropTypes.func.isRequired,
        style: React.PropTypes.object,
        supportClick: React.PropTypes.bool,
        accept: React.PropTypes.string,
        multiple: React.PropTypes.bool,
        titleKey: React.PropTypes.string
    },

    onDragLeave: function (e) {
        this.setState({
            isDragActive: false
        });
    },

    onDragOver: function (e) {
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';

        this.setState({
            isDragActive: true
        });
    },

    onDrop: function (e) {
        e.preventDefault();

        this.setState({
            isDragActive: false
        });

        var files;
        if (e.dataTransfer) {
            files = e.dataTransfer.files;
        } else if (e.target) {
            files = e.target.files;
        }

        var maxFiles = (this.props.multiple) ? files.length : 1;
        for (var i = 0; i < maxFiles; i++) {
            files[i].preview = URL.createObjectURL(files[i]);
        }

        if (this.props.onDrop) {
            files = Array.prototype.slice.call(files, 0, maxFiles);
            this.props.onDrop(files, e);
        }
    },

    onClick: function () {
        if (this.props.supportClick === true) {
            this.open();
        }
    },

    open: function () {
        var dom = this.getDOMNode();
        var fileInput = this.refs.fileInput;
        var fileInputNode = fileInput.getDOMNode();
        fileInputNode.value = null;
        fileInputNode.click();
    },

    render: function () {

        var className = this.props.className || 'dropzone';
        if (this.state.isDragActive) {
            className += ' active';
        }

        var style = this.props.style || {
                borderStyle: this.state.isDragActive ? 'solid' : 'dashed',
                backgroundColor: this.state.isDragActive ? 'aliceblue' : ''
            };

        var label = <span className='dropzone-text'> <Icon bundle='icon-simple-line-icons-cloud-upload'
                                                           glyph='upload'/> {l20n.ctx.getSync(this.props.titleKey ? this.props.titleKey : 'attachment_dropfile')}</span>;

        return (
            React.createElement('div', {
                    className: className,
                    style: style,
                    onClick: this.onClick,
                    onDragLeave: this.onDragLeave,
                    onDragOver: this.onDragOver,
                    onDrop: this.onDrop
                },
                React.createElement('input', {
                    style: {display: 'none'},
                    type: 'file',
                    multiple: this.props.multiple,
                    ref: 'fileInput',
                    onChange: this.onDrop,
                    accept: this.props.accept
                }),
                label,
                this.props.children
            )
        );
    }

});

module.exports = Dropzone;