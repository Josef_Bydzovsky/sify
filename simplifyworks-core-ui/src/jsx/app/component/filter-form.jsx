/**
 * Created by tomiska on 29.4.2015.
 */
var AbstractForm = require('./abstract-form.jsx');
var validator = require('validator');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var FilterForm = React.createClass({

    mixins: [AbstractForm, SidebarMixin, ReactRouter.Navigation, FluxMixin],

    propTypes: {
        name: React.PropTypes.string.isRequired,
        onSubmit: React.PropTypes.func.isRequired,
        routeOnCancel: React.PropTypes.string,
        formColor: React.PropTypes.string,
        disabled: React.PropTypes.bool,
        visibleButtons: React.PropTypes.bool,
        nameKey: React.PropTypes.string,
        resetForm: React.PropTypes.func
    },

    render: function () {
        return (
            <Form id={'form-'+this.props.name}
                  onChange={this.onChange}
                  onSubmit={this.handleSubmit}>


                <div className='filter-body'>
                    <Grid>
                        <Row>
                            <Col xs={12}>
                                <FlashMessages id="formErrors" ref="formErrors"/>
                                {this.props.children}
                            </Col>
                        </Row>
                    </Grid>
                </div>

                <div className='filter-footer'>
                    <ButtonToolbar style={{display: 'inline-block'}}>
                        <Button sm
                            outlined
                            bsStyle='primary'
                            onClick={this.handleSubmit}>
                            <Icon glyph='icon-fontello-filter'/>{' '}
                            <Entity entity={'btn_filter'}/>
                        </Button>
                        <Button sm
                            outlined
                            onlyOnHover
                            bsStyle='danger'
                            onClick={this.resetForm}>
                            <Entity entity={'btn_clear'}/>
                        </Button>
                    </ButtonToolbar>
                </div>
            </Form>

        );
    }
});


module.exports = FilterForm;