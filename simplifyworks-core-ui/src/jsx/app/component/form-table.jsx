var AbstractFormComponent = require('./abstract-form-component.jsx');

var FormTable = React.createClass({
    mixins: [AbstractFormComponent],

    propTypes: {
        //Input Name
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.object,
        required: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        validations: React.PropTypes.string,
        validationsMessage: React.PropTypes.string,
        labelKey: React.PropTypes.string.isRequired,
        rest: React.PropTypes.string,
        filter: React.PropTypes.string,
        order: React.PropTypes.string,
        showNewButton: React.PropTypes.bool,
        parentId: React.PropTypes.string,
        modalDetail: React.PropTypes.func,
        entity: React.PropTypes.string

    },

    //// Whenever the input changes we update the value state
    //// of this component
    //setValue: function (event) {
    //    this.showValidations();
    //    this.setState({
    //        value: event.currentTarget.value
    //        // When the value changes, wait for it to propagate and
    //        // then validate the input
    //    }, function () {
    //        this.props.validate(this);
    //    }.bind(this));
    //},

    componentWillMount() {
        this.setState({
            firstValidation: true
        });
        this.props.validations = this.props.validations ? this.props.validations : '';
        this.props.attachToForm(this); // Attaching the component to the form
    },

    componentWillUnmount() {
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    render() {

        var className = '';
        if (this.state.isValid === false && !this.state.firstValidation && !this.state.disabled) {
            className = 'has-error';
        }

        var component = <BasicTable id={this.props.name}
                                    rest={this.props.rest}
                                    filter={this.props.filter}
                                    value={this.state.value}
                                    paging={false}
                                    searching={false}
                                    info={false}
                                    entity={this.props.entity}
                                    order={this.props.order}
                                    disabled={this.state.disabled}
                                    detail={this.props.detail}
                                    showNewButton={this.props.showNewButton}
                                    parentId={this.props.parentId}
                                    modalDetail={this.props.modalDetail}>
            {this.props.children}
        </BasicTable>

        //if is table new, than we cant show it.
        return this.props.isNew === true ? <div/> : this.formGroup(component, className);
    }
});
module.exports = FormTable;