var AbstractFormComponent = require('./abstract-form-component.jsx');

var InputText = React.createClass({
    mixins: [AbstractFormComponent],

    propTypes: {
        //Input Name
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.string,
        validations: React.PropTypes.string,
        validationsMessage: React.PropTypes.string,
        placeholder: React.PropTypes.string,
        required: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        labelKey: React.PropTypes.string,
        password: React.PropTypes.bool,
        hidden: React.PropTypes.bool,
        changeValueFunc: React.PropTypes.func
    },

    // Whenever the input changes we update the value state
    // of this component
    setValue: function (event) {
        this.showValidations();
        this.setState({
            value: event.currentTarget.value
            // When the value changes, wait for it to propagate and
            // then validate the input
        }, function () {
            if (this.props.validate) {
                this.props.validate(this);
            }
            if (this.props.onChange) {
                this.props.onChange(this.state.value);
            }
        }.bind(this));
    },

    componentWillMount: function () {
        this.setState({
            firstValidation: true
        })

        this.props.validations = this.props.validations ? this.props.validations : '';
        if (this.props.attachToForm) {
            this.props.attachToForm(this); // Attaching the component to the form
        }

    },

    componentWillUnmount: function () {
        console.log('componentWillUnmount: ' + this.props.name);
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    render: function () {
        var value = this.state.value;

        var className = '';

        if (this.state.isValid === false && !this.state.firstValidation && !this.state.disabled) {
            className = 'has-error';
        }


        var component = <Input type={this.props.password?'password':this.props.hidden?'hidden':'text'}
                               id={this.props.name}
                               sm={this.props.sm}
                               lg={this.props.lg}
                               xs={this.props.xs}
                               onChange={this.props.changeValueFunc ? this.props.changeValueFunc : this.setValue}
                               onKeyPress={this.props.keyPressListener}
                               placeholder={this.state.disabled?'':this.props.placeholder}
                               className={this.props.className}
                               disabled={this.state.disabled}  
                               ref={this.props.name}
                               value={this.state.value}/>

        return this.props.hidden ? component : this.formGroup(component, className);
    }
});

module.exports = InputText;
