var AbstractFormComponent = require('./abstract-form-component.jsx');
var ReactSelect = require('react-select');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var MultiSelect = React.createClass({
    mixins: [AbstractFormComponent,FluxMixin, Fluxxor.StoreWatchMixin("FlashStore")],

    propTypes: {
        name: React.PropTypes.string.isRequired,
        value: React.PropTypes.array,           //Array of values (values are full 'entity')
        validations: React.PropTypes.string,
        validationsMessage: React.PropTypes.string,
        required: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        relRest: React.PropTypes.string,       //Rest api for load saved data from table with relations
        relfilter: React.PropTypes.string,     //Parameters for filter saved data in table with relations
        rest: React.PropTypes.string,          //Rest api for load all data
        filter: React.PropTypes.string,        //Parameters for filter records
        idField: React.PropTypes.string,       //Specifies the Field, from which we get id
        labelKey: React.PropTypes.string.isRequired,
        valueFunc: React.PropTypes.array,      //Function for load data. It is for case when we need load data inner component (we don't have data when first render this component).
        targetField: React.PropTypes.string,   //Target field in relation object. It is object when we choose.
        sourceField: React.PropTypes.string,   //Source field in relation object. It is object to which we bind.
        itemPrototypeFunc: React.PropTypes.func, //Is function return instance of new relation object. In this method we can customized new object.
        niceLabelFunc: React.PropTypes.func.isRequired, //Function for create niceLabel from object
        async: React.PropTypes.bool,           //Immediate asynchronous save after choose/remove from multiselect
        placeholder: React.PropTypes.string
    },

    getStateFromFlux: function () {
        return this.getFlux().store("FlashStore").getState();
    },

    //Load data via rest api
    getOptions: function (input, callback) {
        var parent = this;
        var options = [];
        console.log('this.props.niceLabelFunc ' + this.props.niceLabelFunc)
        setTimeout(function () {
            $.ajax({
                async: false,
                url: parent.props.filter ? parent.props.rest+'?filter='+parent.props.filter : parent.props.rest,
                dataType: 'json',
                success: function (data) {
                    var items = data._embedded.resources;
                    for (var i = 0; i < items.length; i++) {
                        options[i + 1] = {
                            value: 'id' + this.getResourceId(null,items[i].resource).id,
                            label: this.props.niceLabelFunc(this.getResourceId(null,items[i].resource))
                        };
                    }
                    parent.setState({targetItems: items});
                }.bind(this)
            });

            callback(null, {
                    options: options,
                    // CAREFUL! Only set this to true when there are no more options,
                    // or more specific queries will not be sent to the server.
                    complete: true
                }
            );
        }.bind(this), 500);
    },

    getInitialState: function () {
        return {
            idValue: null
        };
    },

    getDefaultProps() {
        return {
            async: true
        };
    },

    //Return id of selected item
    getResourceId: function(self, resource){
        return (this.props.idField && resource[this.props.idField]) ? resource[this.props.idField] : resource;
    },

    //we need convert array value to string (for use in ReactSelect)
    evaluateIdValue: function (value) {
        var idValue;
        if (this.props.valueFunc) {
            value = this.props.valueFunc();
        }
        if (value) {
            for (var i = 0; i < value.length; i++) {
                idValue = (idValue ? idValue : '') + 'id' + value[i][this.props.targetField].id + (i < value.length - 1 ? ',' : '');
            }
        }
        return idValue;
    },

    componentDidMount: function () {
        this.setStateValue();
    },

    componentWillReceiveProps: function(){
        if(this.props.async)
            this.setStateValue();
    },  

    setStateValue: function(){
        var idValue;
        var value = this.getDataValues();

        idValue = this.evaluateIdValue(value);

        this.setState({
            value: value,
            idValue: idValue
        })
    },

    getDataValues: function(){
        if(this.props.async === true){
            var values = new Array();
            $.ajax({
                async: false,
                url: this.props.relFilter ? this.props.relRest + '?filter='+this.props.relFilter : this.props.relRest,
                dataType: 'json',
                success: function (data) {
                    if(data._embedded){
                        var items = data._embedded.resources;
                        for (var i = 0; i < items.length; i++) {
                            values.push(this.getResourceId(null,items[i].resource));
                        }
                    }
                }.bind(this)
            });
            return values;
        }else{
            return this.state.value;
        }
    },

    getTargetItemById(id) {
        var items = this.state.targetItems;
        if (items) {
            for (var i in items) {
                if(id == this.getResourceId(null,items[i].resource).id){
                    return this.getResourceId(null,items[i].resource);
                }
            };
        }
        return null;
    },

    asyncSave: function(newItem){
        var postUrl = this.props.relRest ;
        var dataToPost = JSON.stringify(newItem);
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: postUrl,
            dataType: 'json',
            type: 'POST',
            data: dataToPost,
            success: function (data) {
                var i = this.state.value.length;
                this.state.value[i-1] = data.resource; //we have to set saved object with id to multiselect
                this.getFlux().actions.flash.add({
                    messageId: 'formErrors',
                    type: 'success',
                    message: l20n.ctx.getSync('success')
                });
            }.bind(this)
        });
    },

    asyncRemove: function(id){
        if(id && this.props.async === true){
            var postUrl = this.props.relRest + '/' + id;
            $.ajax({
                url: postUrl,
                dataType: 'text',
                type: 'DELETE',
                success: function () {
                    this.getFlux().actions.flash.add({
                        messageId: 'formErrors',
                        type: 'success',
                        message: l20n.ctx.getSync('success')
                    });
                }.bind(this)
            });
        }
    },

    setValue: function (newValue, selectedOptions) {
        this.showValidations();
        var value = this.state.value ? this.state.value : [];

        for (var s = 0; s < selectedOptions.length; s++) {

            var exist = false;
            for (var i = 0; i < value.length; i++) {
                if (('id' + value[i][this.props.targetField].id) === selectedOptions[s].value) {
                    exist = true;
                }
            }
            if (exist === false) {
                console.log('value is new: ' + selectedOptions[s].value)
                var newItem = this.props.itemPrototypeFunc();

                var targetItem = this.getTargetItemById(selectedOptions[s].value.substring(2))
                newItem[this.props.targetField] = targetItem;
                value[value.length] = newItem;

                if (this.props.async === true)
                    this.asyncSave(newItem);
            }
        }
        for (var i = 0; i < value.length; i++) {

            var exist = false;
            for (var s = 0; s < selectedOptions.length; s++) {
                if (('id' + value[i][this.props.targetField].id) === selectedOptions[s].value) {
                    exist = true;
                }
            }
            if (exist === false) {
                console.log('value is for delete: ' + value[i][this.props.targetField].id);
                this.asyncRemove(value[i].id);
                //We remove item from array
                value.splice(i, 1);
            }
        }

        this.setState({value: value, idValue: this.evaluateIdValue(value)}, function () {
            if (this.props.validate) {
                this.props.validate(this);
            }
            if (this.props.onChange) {
                this.props.onChange(this.state.value);
            }
        }.bind(this));
    },

    componentWillMount: function () {
        this.setState({
            firstValidation: true
        })

        this.props.validations = this.props.validations ? this.props.validations : '';
        if (this.props.attachToForm) {
            this.props.attachToForm(this); // Attaching the component to the form
        }

    },

    componentWillUnmount: function () {
        console.log('componentWillUnmount: ' + this.props.name);
        if (this.props.detachFromForm) {
            this.props.detachFromForm(this); // Detaching if unmounting
        }
    },

    render: function () {
        var className = '';
        if (this.state.isValid === false && !this.state.firstValidation && !this.state.disabled) {
            className = 'has-error';
        }


        var component =
            <ReactSelect
                name={this.props.name}
                value={this.state.idValue}
                autoload={true}
                className={className}
                placeholder={this.state.disabled?'':this.props.placeholder}
                multi={true}
                asyncOptions={this.getOptions}
                disabled={this.state.disabled}
                onChange={this.setValue}
                />

        //if the record is new, than we cant show it.
        return this.props.isNew === true ? <div/> : this.formGroup(component, className);
    }
});

module.exports = MultiSelect;