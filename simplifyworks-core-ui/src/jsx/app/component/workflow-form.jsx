/**
 * Do same as basic form. Only add support for workflow (add wf button, change method for save)
 * @author Svanda
 */
var AbstractForm = require('./abstract-form.jsx');
var validator = require('validator');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var WfButtons = require('./wf-buttons.jsx')

var WorkflowForm = React.createClass({

    mixins: [AbstractForm, SidebarMixin, ReactRouter.Navigation, ReactRouter.History, FluxMixin, Fluxxor.StoreWatchMixin("FlashStore")],

    statics: {

        //Universal method for save form (data tu save are prepare in implement template "componentMain")
        saveAndProcessMethod: function (componentMain, basicFormComponent, postUrl, dataToPut, detailRoute, button, processInstanceId, processKey) {

            var actionWorkflowWrapper = {
                "entity": dataToPut,
                "button": button,
                "processKey": processKey,
                "processInstance": (processInstanceId ? processInstanceId : "new")
            }
            var dataJson = JSON.stringify(actionWorkflowWrapper)
            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                url: postUrl + '/wf/process',
                dataType: 'json',
                // type: componentMain.state.isNew === true ? 'POST' : 'PUT',
                type: 'POST',
                data: dataJson,
                success: function (wfActionData) {
                    this.getFlux().actions.flash.add({
                        messageId: 'formErrors',
                        type: 'success',
                        message: l20n.ctx.getSync('success')
                    });
                    var data = wfActionData.resource.entity;
                    var processInstance = wfActionData.resource.processInstance;
                    data.wfProcessInstanceId = processInstance;

                    console.log("Results: " + JSON.stringify(data));
                    basicFormComponent.setState({
                        isSubmitting: false
                    });

                    if (componentMain.state.isNew === true) {
                        //If is this new form, we update state and do redirect to new persisted item
                        componentMain.setState({
                            data: data,
                            id: data.id,
                            loaded: true,
                            isNew: false
                        })
                        console.log('Redirect to: ' + detailRoute + ' id: ' + data.id);
                        componentMain.transitionTo(detailRoute, {id: data.id});
                    }
                    basicFormComponent.downloadButtons(data.wfProcessInstanceId);
                    componentMain.setState({data: data, loaded: true})


                }.bind(componentMain),
                error: function (xhr, status, err) {
                    console.error(componentMain.props.url, status, err.toString());
                    basicFormComponent.setState({
                        isSubmitting: false
                    });
                }
            });
        }
    },


    propTypes: {
        name: React.PropTypes.string.isRequired,
        onSubmit: React.PropTypes.func.isRequired,
        routeOnCancel: React.PropTypes.string,
        formColor: React.PropTypes.string,
        isFormChild: React.PropTypes.bool,
        disabled: React.PropTypes.bool,
        visibleButtons: React.PropTypes.bool,
        visibleSaveButton: React.PropTypes.bool,
        nameKey: React.PropTypes.string,
        resetForm: React.PropTypes.func,
        glyph: React.PropTypes.string,
        modalClose: React.PropTypes.func,
        processDefinition: React.PropTypes.string.isRequired, //Process definition key
        processIdentifier: React.PropTypes.string //Instance process id, when is null, then will be create new instance (after save)
    },


    getInitialState: function () {
        return {
            processIdentifier: this.props.processIdentifier
        };
    },


    //This method is call after reditrect with Route.
    componentWillReceiveProps: function (nextProps) {

        this.setState({
            processIdentifier: nextProps.processIdentifier
        });

    },

    componentWillMount: function () {
        //Load WF buttons
        this.downloadButtons(this.state.processIdentifier);

    },

    //Download wfButtons by instance process id
    downloadButtons: function (processIdentifier) {
        console.log("processIdentifier", processIdentifier);
        var parent = this;
        $.ajax({
            async: false,
            url: "/api/wf/buttons/" + (processIdentifier ? processIdentifier : "new"),
            dataType: 'json',
            success: function (data) {

                console.log("data", data);
                parent.setState({buttons: data.resource});

            }.bind(this)
        });

    },

    //Method for save and process workflow by select WfButton.
    workflowProcess: function (button, e) {
        console.log("wfButton onClick", button,this.props.processDefinition);
        this.handleSubmit(button, this.props.processDefinition);

    },


    render: function () {

        var btnSaveLabel = l20n.ctx.getSync(this.state.isNew ? 'btn_create' : 'btn_save')
        var btnCancelLabel = l20n.ctx.getSync('btn_cancel')

        var componentForm =
            <Form id={'form-'+this.props.name} onChange={this.onChange}
                  onSubmit={this.handleSubmit.bind(this,null, null)}>
                {this.props.children}
            </Form>;

        var componentFooterButton = '';

        //When is entity new, then create button will be not visible
        if (this.props.visibleButtons && !this.props.isFormChild) {
            componentFooterButton =
                <ButtonToolbar style={{display: 'inline-block'}}>
                    <Button outlined
                            onlyOnHover
                            bsStyle='red'
                            disabled={this.state.isSubmitting}
                            onClick={this.cancelForm}>
                        {btnCancelLabel}
                    </Button>

                    {this.props.visibleSaveButton === false || this.state.isNew ? "" : (
                        <Button
                            bsStyle='primary'
                            onClick={this.handleSubmit.bind(this,null, null)}
                            disabled={this.state.isSubmitting}>
                            {btnSaveLabel}
                        </Button >)}
                    <WfButtons name='wfbuttons'
                               processIdentifier={this.state.processIdentifier ? this.state.processIdentifier : "new"}
                               disabled={this.state.isSubmitting}
                               processFunc={this.workflowProcess}
                               value={this.state.buttons}/>
                </ButtonToolbar>;

        }


        if (this.props.isFormChild === true) {
            componentForm = this.props.children;
        }

        var formClassName = '';
        if (this.props.formColor != null) {
            formClassName = 'bg-dark' + this.props.formColor + ' fg-white';
        }

        var component =
            <PanelContainer noOverflow bordered>
                <Panel>
                    <PanelHeader className={formClassName}>
                        <Grid>
                            <Row>
                                <Col xs={12}>
                                    <h3>
                                        {this.props.glyph != null ?
                                            <span><Icon glyph={this.props.glyph}/>{' '}</span>
                                            :
                                            null
                                        }
                                        {l20n.ctx.getSync(this.props.nameKey)}
                                    </h3>
                                </Col>
                            </Row>
                        </Grid>
                    </PanelHeader>
                    <PanelBody className={this.props.isFormChild ?'basic-formChild' : null}>
                        <Grid>
                            <Row>
                                <Col xs={12}>
                                    <FlashMessages id="formErrors" ref="formErrors"/>
                                    {componentForm}
                                </Col>
                            </Row>
                        </Grid>
                    </PanelBody>
                    {componentFooterButton ?
                        <PanelFooter>
                            <Grid>
                                <Row>
                                    <Col xs={12} className='text-right'>
                                        {componentFooterButton}
                                    </Col>
                                </Row>
                            </Grid>
                        </PanelFooter>
                        :
                        null
                    }

                </Panel>
            </PanelContainer>


        return (
            <div>{component}</div>
        )

    }
});


module.exports = WorkflowForm;