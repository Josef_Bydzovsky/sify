var RestUtil = React.createClass({

    statics: {

        loadRest: function (url, filter, order, async) {
            console.log('url: '+url)
            console.log('filter: '+filter)
            var result;
            $.ajax({
                url: url,
                dataType: 'json',
                data: {filter: filter, order: order},
                async: async,
                success: function (data) {
                    console.log('loadRest: '+ JSON.stringify(data))
                    result =  data;
                }
            });
            return result;
        }
    },

    render: function () {

    }
});

module.exports = RestUtil;