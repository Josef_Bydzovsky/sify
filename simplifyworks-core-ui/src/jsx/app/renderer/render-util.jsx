var RenderUtil = React.createClass({

    statics: {

        personNiceLabel: function (person) {
            if(!person){
                return null;
            }
            return (person.titleBefore ? person.titleBefore + ' ' : '')
                + (person.firstname ? person.firstname + ' ' : '')
                + (person.surname ? person.surname + ' ' : '')
                + (person.titleAfter ? person.titleAfter : '');

        },

        userNiceLabel: function (user) {
            if(!user){
                return null;
            }
            return (user.username);

        },

        organizationNiceLabel: function (organization) {
            if(!organization){
                return null;
            }
            return (organization.name);

        },

        userRoleNiceLabel: function (role) {
            if(!role){
                return null;
            }
            return (role.name ? role.name: '');

        },

        personOrganizationNiceLabel: function (personOrganization) {
            if(!personOrganization){
                return null;
            }
            return this.personNiceLabel(personOrganization.person) + " / "+ this.organizationNiceLabel(personOrganization.organization);

        },

    },

    render: function () {

    }


});

module.exports = RenderUtil;