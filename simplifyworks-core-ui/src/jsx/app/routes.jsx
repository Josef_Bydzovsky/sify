var system = require('./routes/app/system/system.jsx');

var systemUsers = require('./routes/app/system/user/users.jsx');
var systemUserList = require('./routes/app/system/user/user-list.jsx');
var systemUserDetail = require('./routes/app/system/user/user-detail.jsx');
var systemUserRoleDetail = require('./routes/app/system/user/user-role-detail.jsx');

var systemPersons = require('./routes/app/system/person/persons.jsx');
var systemPersonList = require('./routes/app/system/person/person-list.jsx');
var systemPersonDetail = require('./routes/app/system/person/person-detail.jsx');

var systemAttachments = require('./routes/app/system/attachment/attachments.jsx');
var systemAttachmentList = require('./routes/app/system/attachment/attachment-list.jsx');
var systemAttachmentDetail = require('./routes/app/system/attachment/attachment-detail.jsx');

var systemRoles = require('./routes/app/system/role/roles.jsx');
var systemRoleList = require('./routes/app/system/role/role-list.jsx');
var systemRoleDetail = require('./routes/app/system/role/role-detail.jsx');

var systemOrganizations = require('./routes/app/system/organization/organizations.jsx');
var systemOrganizationList = require('./routes/app/system/organization/organization-list.jsx');
var systemOrganizationDetail = require('./routes/app/system/organization/organization-detail.jsx');

var systemSetting = require('./routes/app/system/setting/settings.jsx');
var systemSettingList = require('./routes/app/system/setting/setting-list.jsx');
var systemSettingDetail = require('./routes/app/system/setting/setting-detail.jsx');

var systemTemplate = require('./routes/app/system/template/templates.jsx');
var systemTemplateList = require('./routes/app/system/template/template-list.jsx');
var systemTemplateDetail = require('./routes/app/system/template/template-detail.jsx');

var systemWorkflow = require('./routes/app/system/workflow/workflows.jsx');
var systemWorkflowList = require('./routes/app/system/workflow/workflow-list.jsx');
var systemWorkflowDetail = require('./routes/app/system/workflow/workflow-detail.jsx');

module.exports = (
    <Route name='system' path='/system' handler={system}>
        <Route name='users' handler={systemUsers}>
            <Route name='user-list' path='/system/user-list' handler={systemUserList}/>
            <Route name='user-detail' path='/system/user-detail/:id' handler={systemUserDetail}/>
            <Route name='user-role-detail' path='/system/user-role-detail/:id' handler={systemUserRoleDetail}/>
        </Route>

        <Route name='organizations' handler={systemOrganizations}>
            <Route name='organization-list' path='/system/organization-list' handler={systemOrganizationList}/>
            <Route name='organization-detail' path='/system/organization-detail/:id' handler={systemOrganizationDetail}/>
        </Route>

        <Route name='persons' handler={systemPersons}>
            <Route name='person-list' path='/system/person-list' handler={systemPersonList}/>
            <Route name='person-detail' path='/system/person-detail/:id' handler={systemPersonDetail}/>
        </Route>

        <Route name='roles' handler={systemRoles}>
            <Route name='role-list' path='/system/role-list' handler={systemRoleList}/>
            <Route name='role-detail' path='/system/role-detail/:id' handler={systemRoleDetail}/>
            <Route name='role-user-detail' path='/system/role-user-detail/:id' handler={systemUserRoleDetail}/>
        </Route>

        <Route name='settings' handler={systemSetting}>
            <Route name='setting-list' path='/system/setting-list' handler={systemSettingList}/>
            <Route name='setting-detail' path='/system/setting-detail/:id' handler={systemSettingDetail}/>
        </Route>

        <Route name='attachments' handler={systemAttachments}>
            <Route name='attachment-list' path='/system/attachment-list' handler={systemAttachmentList}/>
            <Route name='attachment-detail' path='/system/attachment-detail/:id' handler={systemAttachmentDetail}/>
        </Route>
        <Route name='workflows' handler={systemWorkflow}>
            <Route name='workflow-list' path='/system/workflow-list' handler={systemWorkflowList}/>
            <Route name='workflow-detail' path='/system/workflow-detail/:id' handler={systemWorkflowDetail}/>
        </Route>
        <Route name='templates' handler={systemTemplate}>
            <Route name='template-list' path='/system/template-list' handler={systemTemplateList}/>
            <Route name='template-detail' path='/system/template-detail/:id' handler={systemTemplateDetail}/>
        </Route>

    </Route>
);
