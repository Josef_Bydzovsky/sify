var restEndpoint = '/api/attachments/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var Attachment = require('../../../../component/attachment.jsx')
//var React = require('react');


var Form = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    handleSubmit: function (component) {
        this.state.data.settingKey = component.model.settingKey;
        this.state.data.value = component.model.value;
        this.state.data.system = component.model.system;
        this.state.data.description = component.model.description;

        var postUrl = restEndpoint;
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'setting-detail');
    },


    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            id: this.props.id,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillReceiveProps: function (next) {
        this.setState({id: next.id});
        BasicForm.loadMethod(this, next.url);
    },

    componentWillMount: function () {
        //Universal method for load form
        BasicForm.loadMethod(this, this.props.url);
    },

    //For persentation attachment component only. Can by delete
    getAttachmentRow: function (attachment, changeFunc, removeFunc) {
        return (
            <Row>
                <Col sm={4}>
                    {attachment.name}
                </Col>
                <Col sm={4}><InputText
                    value={attachment.description}
                    placeholder={l20n.ctx.getSync('attachment_description')}
                    name={attachment.contentGuid}
                    changeValueFunc={changeFunc.bind(null, attachment, 'description')}/>
                </Col>
                <Col sm={2}><InputText
                    value={attachment.test}
                    placeholder={l20n.ctx.getSync('test')}
                    name={attachment.contentGuid}
                    changeValueFunc={changeFunc.bind(null, attachment, 'test')}/>
                </Col>
                <Button
                    name={"delete"}
                    outlined
                    className='dropzone-remove-btn'
                    bsStyle='red'
                    onClick={removeFunc.bind(null,attachment)}>
                    X
                </Button>
            </Row>
        );
    },

    // call when changed any component in attachment row
    attachmentComponentChange: function (attachment, item, type, event) {
        if (type == "description") {
            attachment.description = event.currentTarget.value;
            return attachment;
        }
        if (type == "test") {
            attachment.test = event.currentTarget.value;
            return attachment;
        }

    },

    ////////////////////

    render: function () {
        var currentToken = this.getFlux().store("AuthStore").getCurrentToken();

        var downloadUrl = "/api/attachments/" + this.state.data.id + "/download/?access_token=" + currentToken;

        var downloadComponent = <Col sm={1}>
            <div style={{height: 30}}/>
            <a className='dropzone-download' href={downloadUrl}><Icon
                bundle='icon-ikons-download'
                glyph='download'/></a> </Col>;


        var commponent = (
            <Grid>
                <Row>
                    <Col xs={6} xsOffset={3}>
                        <BasicForm name='form-attachment-detail'
                                   nameKey='attachment_detail'
                                   onSubmit={this.handleSubmit}
                                   formColor='blue'
                                   glyph='icon-dripicons-attachment'
                                   routeOnCancel='attachment-list'
                                   isNew={this.state.isNew}
                                   disabled={false}>


                            <Row>

                                <Col sm={this.state.isNew? 12 :11} className='dropzone-name'>
                                    <InputText name='name'
                                               value={this.state.data.name}
                                               disabled={!this.state.isNew}
                                               required={true}
                                               labelKey='attachment_name'/>
                                </Col>
                                {this.state.isNew ? '' : downloadComponent}
                            </Row>


                            <InputText name='contentGuid'
                                       value={this.state.data.contentGuid}
                                       labelKey='attachment_contentGuid'/>

                            <InputText name='objectType'
                                       value={this.state.data.objectType}
                                       disabled={true}
                                       labelKey='attachment_objectType'/>

                            <InputText name='objectIdentifier'
                                       value={this.state.data.objectIdentifier}
                                       labelKey='attachment_objectIdentifier'/>

                            <InputText name='description'
                                      value={this.state.data.description}
                                      labelKey='attachment_description'/>

                            <Attachment name='attachments'
                                        value={this.state.data.attachments}
                                        objectType='Attachment'
                                        objectIdentifier={this.props.id} 
                                        rest={'/api/attachments'}
                                        filter={'objectIdentifier:' + (this.state.id ? this.state.id : '0') }
                                        labelKey='attachment_attachments'/>
                        </BasicForm>
                    </Col>
                </Row>
            </Grid>
        );
        return (
            <div>
                {commponent}
            </div>
        );
    }
});


var SettingDetail = React.createClass({
    mixins: [SidebarMixin, ReactRouter.State, FluxMixin],
    render: function () {

        var id = this.getParams().id;

        return (
            <Container id='body'>
                <Form url={restEndpoint + id}
                      id={id}
                      isNew={BasicForm.isNew(id)}/>
            </Container>
        );
    }
});

module.exports = SettingDetail;
