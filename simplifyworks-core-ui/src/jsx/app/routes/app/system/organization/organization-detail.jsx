// TODO: global app setting
var restEndpoint = '/api/core';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var PersonOrganizationForm = require('../person/person-organization-detail.jsx');


var OrganizationForm = React.createClass({

    mixins: [FluxMixin, ReactRouter.State, ReactRouter.Navigation],

    handleSubmit: function (component) {
        this.state.data.name = component.model.name;
        this.state.data.code = component.model.code;
        this.state.data.abbreviation = component.model.abbreviation;
        this.state.data.organizationId = component.model.organization ? component.model.organization.id : null;

        var postUrl = restEndpoint + '/organizations';
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'organization-detail');
    },

    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            id: this.props.id,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillReceiveProps: function (next) {
        this.setState({id: next.id});
        BasicForm.loadMethod(this, next.url);
    },

    componentWillMount: function () {
        //Universal method for load form
        BasicForm.loadMethod(this, this.props.url);
    },

    getPersonOrganizationForm: function (url, isNew, id, modalClose) {
        return (
            <PersonOrganizationForm url={url} isNew={isNew} id={id} modalClose={modalClose}
                                    organizationId={this.state.data.id}
                                    flux={this.getFlux()}/>
        )
    },

    render: function () {
        if (this.state.loaded) {
            var commponent = (
                <Grid>
                    <Row>
                        <Col xs={12}>
                            <BasicForm name={'form-organization-detail'+this.props.id}
                                       nameKey='organization_detail'
                                       onSubmit={this.handleSubmit}
                                       formColor='blue'
                                       routeOnCancel='organization-list'
                                       glyph='icon-dripicons-home'
                                       isNew={this.state.isNew}>

                                <InputText name='code'
                                           value={this.state.data.code}
                                           required={true}
                                           validations="isLength:1:50"
                                           labelKey='organization_code'/>

                                <InputText name='name'
                                           value={this.state.data.name}
                                           required={true}
                                           validations="isLength:1:100"
                                           labelKey='organization_name'/>

                                <InputText name='abbreviation'
                                           value={this.state.data.abbreviation}
                                           validations="isLength:0:50"
                                           labelKey='organization_abbreviation'/>

                                <CodeList name='organization'
                                          value={this.state.data.organizationId}
                                          rest={'/api/core/organizations'}
                                          width='300px'
                                          required={false}
                                          niceLabelFunc={RenderUtil.organizationNiceLabel}
                                          labelKey='organization_parent_organization'
                                          flux={this.getFlux()}>
                                    <BasicTable.BasicColumn property='id' face='detail' width='20px'/>
                                    <BasicTable.BasicColumn property='code' width='65px' labelKey='organization_code'/>
                                    <BasicTable.BasicColumn property='name' labelKey='organization_name'/>
                                    <BasicTable.BasicColumn property='abbreviation'
                                                            labelKey='organization_abbreviation'/>
                                </CodeList>

                                <FormTable name='organizations-table'
                                           rest={'/api/core/organizations'}
                                           isNew={this.state.isNew}
                                           filter={'organizationId:'+(this.state.id != null && !this.state.isNew ?this.state.id:'0')}
                                           labelKey='organization_sub_organizations'
                                           detail='organization-detail'>
                                    <BasicTable.BasicColumn property='id' face='detail' width='20px'/>
                                    <BasicTable.BasicColumn property='code' width='65px' labelKey='organization_code'/>
                                    <BasicTable.BasicColumn property='name' labelKey='organization_name'/>
                                    <BasicTable.BasicColumn property='abbreviation'
                                                            labelKey='organization_abbreviation'/>
                                </FormTable>

                                <FormTable name='person-organization-table'
                                           rest={'/api/core/person-organizations'}
                                           filter={'organization.id:'+(this.state.id != null && !this.state.isNew ?this.state.id:'0')}
                                           labelKey='corePersonOrganizations'
                                           entity='corePersonOrganization'
                                           isNew={this.state.isNew}
                                           modalDetail={this.getPersonOrganizationForm}>
                                    <BasicTable.BasicColumn property='id' face='detail' width='20px'/>
                                    <BasicTable.BasicColumn property='person' face='codelist'
                                                            niceLabelFunc={RenderUtil.personNiceLabel}/>
                                    <BasicTable.BasicColumn property='validFrom' face='date'/>
                                    <BasicTable.BasicColumn property='validTill' face='date'/>
                                    <BasicTable.BasicColumn property='obligation' face='number'/>
                                </FormTable>

                            </BasicForm>
                        </Col>
                    </Row>

                </Grid>
            );
        }
        return (
            <div>
                {commponent}
            </div>
        );
    }
});


var OrganizationDetail = React.createClass({
    mixins: [SidebarMixin, FluxMixin, ReactRouter.State],

    render: function () {
        var idOrganization = this.getParams().id;
        return (
            <Container id='body'>
                <OrganizationForm url={restEndpoint+'/organizations/' + idOrganization}
                                  id={idOrganization}
                                  isNew={BasicForm.isNew(idOrganization)}/>
            </Container>
        );
    }
});

module.exports = OrganizationDetail;
