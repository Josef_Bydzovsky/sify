var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var OrganizationList = React.createClass({
    render: function () {
        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer noControls={true} bordered>
                                <Panel>
                                    <PanelHeader className='bg-darkblue fg-white'>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <h3>
                                                        <Icon glyph='icon-dripicons-home'/>{' '}
                                                        <Entity entity='organization' data={{property: 'organizations'}}/>
                                                    </h3>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelHeader>
                                    <PanelBody>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <BasicTable id='organization-table'
                                                                rest='/api/core/organizations'
                                                                detail='organization-detail'>
                                                        <BasicTable.BasicColumn property='id' width='15px'
                                                                                face='detail'/>
                                                        <BasicTable.BasicColumn property='code'width='65px'/>
                                                        <BasicTable.BasicColumn property='name'/>
                                                        <BasicTable.BasicColumn property='abbreviation'/>
                                                        <BasicTable.BasicColumn property='id' width='15px'
                                                                                face='actions'/>
                                                    </BasicTable>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelBody>
                                </Panel>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = OrganizationList;
