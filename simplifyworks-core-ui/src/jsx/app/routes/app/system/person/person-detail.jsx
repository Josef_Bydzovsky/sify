// TODO: global app setting
var restEndpoint = '/api/core';

var PersonForm = require('../person/person-form.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);


var PersonDetail = React.createClass({
    mixins: [SidebarMixin, ReactRouter.State, ReactRouter.Navigation, FluxMixin],
    render: function () {

        var idPerson = this.getParams().id;

        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PersonForm
                                url={restEndpoint + '/persons/' + idPerson}
                                isNew={BasicForm.isNew(idPerson)}/>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = PersonDetail;
