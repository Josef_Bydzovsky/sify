// TODO: global app setting
var restEndpoint = '/api/core';

var NewEnum = require('../../../../domain/newEnum.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var RenderUtil = require('../../../../renderer/render-util.jsx');
var PersonOrganizationForm = require('./person-organization-detail.jsx');

PersonForm = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    //In this method we prepare data to save
    handleSubmit: function (component) {
        this.state.data.email = component.model.email;
        this.state.data.firstname = component.model.firstname;
        this.state.data.surname = component.model.surname;
        this.state.data.titleBefore = component.model.titleBefore;
        this.state.data.titleAfter = component.model.titleAfter;
        this.state.data.phone = component.model.phone;
        this.state.data.note = component.model.note;
        this.state.data.testOrg = component.model.testOrg;
        this.state.data.personalNumber = component.model.personalNumber;

        var postUrl = restEndpoint + '/persons';
        console.log('postUrl ' + postUrl);
        //If is this form new, we will create and use POST
        var dataToPut = JSON.stringify(this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'person-detail');

    },

    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            isNew: this.props.isNew === true ? true : false,
            url: this.props.url
        };
    },

    getDefaultProps: function () {
        return {
            visibleTable: true
        };
    },

    componentWillMount: function () {
        // configuring default options for Messenger
        Messenger.options = {
            theme: 'flat'
        };

        this.loadData(this.state.url);

    },

    loadData: function (url) {
        //Universal method for load form
        BasicForm.loadMethod(this, url);

    },

    componentWillReceiveProps: function (nextProps) {
        this.loadData(nextProps.url);
    },

    getPersonOrganizationForm: function (url, isNew, id, modalClose) {
        return (
            <PersonOrganizationForm url={url} isNew={isNew} id={id} modalClose={modalClose} personId={this.state.data.id}
                           flux={this.getFlux()}/>
        )
    },

    render: function () {
        if (this.state.loaded) {
            var commponent = (
                <BasicForm name={'form-person-detail'+this.props.id}
                           glyph='icon-dripicons-user'
                           onSubmit={this.handleSubmit}
                           isFormChild={this.props.isFormChild}
                           attachToForm={this.props.attachToForm}
                           disabled={this.props.disabled}
                           nameKey='person_detail'
                           formColor='blue'
                           isNew={this.state.isNew}
                           visibleButtons={this.props.visibleButtons}
                           routeOnCancel='person-list'>
                    <Grid>
                        <Row>
                            <Col sm={6} collapseRight collapseLeft>
                                <InputText name='firstname'
                                           value={this.state.data.firstname}
                                           validations="isLength:0:50"
                                           labelKey='person_firstname'/>
                            </Col>
                            <Col sm={6} collapseRight>
                                <InputText name='surname' placeholder='surname' ref='surname'
                                           required={true}
                                           value={this.state.data.surname}
                                           labelKey='person_surname'/>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={12} collapseRight collapseLeft>
                                <InputText name='personalNumber'
                                           value={this.state.data.personalNumber}
                                           required={false}
                                           labelKey='person_personalNumber'/>
                                <InputText name='titleBefore'
                                           value={this.state.data.titleBefore}
                                           required={false}
                                           labelKey='person_titleBefore'/>
                                <InputText name='titleAfter'
                                           value={this.state.data.titleAfter}
                                           required={false}
                                           labelKey='person_titleAfter'/>
                                <InputText name='email'
                                           value={this.state.data.email}
                                           required={false}
                                           validations="isEmail,isLength:0:50"
                                           labelKey='person_email'/>
                                <InputText name='phone'
                                           value={this.state.data.phone}
                                           labelKey='person_phone'/>
                                <TextArea name='note'
                                          value={this.state.data.note}
                                          required={false}
                                          labelKey='person_note'/>
                                //Only for test
                                <CodeList name='testOrg'
                                          value={this.state.data.testOrg}
                                          rest={'/api/core/organizations'}
                                          width='300px'
                                          required={false}
                                          niceLabelFunc={RenderUtil.organizationNiceLabel}
                                          labelKey='only for test Permission'
                                          flux={this.getFlux()}>
                                    <BasicTable.BasicColumn property='id' face='detail' width='20px'/>
                                    <BasicTable.BasicColumn property='code' width='65px' labelKey='organization_code'/>
                                    <BasicTable.BasicColumn property='name' labelKey='organization_name'/>
                                    <BasicTable.BasicColumn property='abbreviation'
                                                            labelKey='organization_abbreviation'/>
                                </CodeList>
                                //Only for test
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={12} collapseRight collapseLeft>

                                <FormTable name='user-table'
                                           rest={'/api/core/users'}
                                           filter={'person.id:'+(this.state.url!=null && this.state.data.id?this.state.data.id:'0')}
                                           labelKey='person_users'
                                           isNew={this.state.isNew}
                                           detail='user-detail'>
                                    <BasicTable.BasicColumn property='id'
                                                            face='detail'
                                                            width='20px'/>
                                    <BasicTable.BasicColumn property='username'/>
                                </FormTable>

                                <FormTable name='person-organization-table'
                                           rest={'/api/core/person-organizations'}
                                           filter={'person.id:'+(this.state.url!=null && this.state.data.id?this.state.data.id:'0')}
                                           labelKey='corePersonOrganization_corePersonOrganizations'
                                           entity='corePersonOrganization'
                                           isNew={this.state.isNew}
                                           modalDetail={this.getPersonOrganizationForm}>
                                    <BasicTable.BasicColumn property='id'
                                                            face='detail'
                                                            width='20px'/>
                                    <BasicTable.BasicColumn property='organization' face='codelist' niceLabelFunc={RenderUtil.organizationNiceLabel}/>
                                    <BasicTable.BasicColumn property='validFrom' face='date'/>
                                    <BasicTable.BasicColumn property='validTill' face='date'/>
                                    <BasicTable.BasicColumn property='obligation' face='number'/>
                                </FormTable>
                            </Col>
                        </Row>
                    </Grid>
                </BasicForm>
            );
        }

        return (
            <div>
                {commponent}
            </div>
        );
    }
});

module.exports = PersonForm;
