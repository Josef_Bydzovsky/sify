var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var NewEnum = require('../../../../domain/newEnum.jsx');

var PersonList = React.createClass({
    mixins: [ReactRouter.State, FluxMixin],

    setFilter: function (component) {
        this.state.data.firstname = component.model.firstname;
        this.state.data.surname = component.model.surname;
        this.state.data.type = component.model.type;
        this.state.data.created = component.model.created;

        this.filter();
    },

    filter: function () {
        var filter =
            (this.state.data.firstname ? ',firstname:' + encodeURIComponent(this.state.data.firstname) + ':like' : '') +
            (this.state.data.surname ? ',surname:' + encodeURIComponent(this.state.data.surname) + ':like' : '') +
            (this.state.data.created ? ',created:' + encodeURIComponent(this.state.data.created) + ':equals' : '');

        this.refs.person_table.searchWithCustomFilter(filter);        
        
    },

    defaultFilter: function (callback) {
        this.setState({data: {firstname: '', surname: '', type: 'VALUE2', created: ''}}, function () {
            if (callback) {
                callback();
            }
        }.bind(this));
    },

    componentWillMount: function () {
        //default value of filter
        this.defaultFilter();
    },

    componentDidMount: function () {
        //run filter after mount (because of some default filters)
        this.filter();
    },

    resetFilter: function () {
        this.defaultFilter(this.filter);
    },

    createFilter: function () {
        return (
            <FilterForm name='person_filter'
                        ref='person_filter'
                        nameKey='person_filter'
                        onSubmit={this.setFilter}
                        resetForm={this.resetFilter}>

                <InputText name='surname'
                           labelKey='person_surname'
                           value={this.state.data.surname}/>

                <InputText name='firstname'
                           labelKey='person_firstname'
                           value={this.state.data.firstname}/>

                <Enumbox name='type'
                         labelKey='person_type'
                         options={Enumbox.enumOptions(NewEnum.values,'enum_newEnum_',true)}
                         value={this.state.data.type}/>

                <Datepicker labelKey='created' name='created' placeholder={this.props.placeholder} showButtons={true}
                            value={this.state.data.created}/>

            </FilterForm>
        );
    },
    
    idSummaryExample: function(values) {
    	return this.summaryExample(values, 'id');
    },
    
    versionSummaryExample: function(values) {
    	return this.summaryExample(values, 'version');
    },
    
    summaryExample: function(values, property) {
    	var sum = 0;
    	var min = Number.MAX_SAFE_INTEGER;
    	var max = Number.MIN_SAFE_INTEGER;
    	
    	for(var index = 0; index < values.length; index++) {
    		sum += values[index][property];
    		min = Math.min(min, values[index][property]);
    		max = Math.max(max, values[index][property]);
    	}
    	
    	var avg = values.length === 0 ? null : (sum / values.length);
    	
    	return (
    		<Grid>
    			<Row>
    				<Col sm={12} md={12} lg={12}>
    					Sum: {sum}
    				</Col>
    			</Row>
    			
    			<Row>
					<Col sm={12} md={12} lg={12}>
						Avg: {avg}
					</Col>
				</Row>
			
				<Row>
					<Col sm={12} md={12} lg={12}>
					 	Min: {min}
					</Col>
				</Row>
		
				<Row>
					<Col sm={12} md={12} lg={12}>
					Max: {max}
					</Col>
				</Row>
    		</Grid>
    	);
    },

    render: function () {
    	var customNamedFilters = [];
    	
    	customNamedFilters.push({
    		render: function() {
    			return 'With email';
    		},
    		
    		createFilter: function() {
    			return 'email::is_not_null';
    		}
    	});
    	
    	customNamedFilters.push({
        	render: function() {
        		return 'With name';
        	},
        	
        	createFilter: function() {
        		return 'firstname::is_not_null';
        	}
    	});
    
    	var actions = [];
    	actions.push(AdvancedTable.defaultDeleteAction);
    	
    	actions.push({
    		render: function() {
    			return 'Hi';
    		},
    		
    		execute: function(rest, selectedRecords, searchCallback) {
    			alert('Hi ' + selectedRecords.map(function(record) {return record.surname}));
    			
    			searchCallback(true);
    		},
    		
    		isEnabled: function(rest, record) {
    			return record.id != 10;
    		}
    	});
    	actions.push({
    		render: function() {
    			return 'Bye';
    		},
    		
    		execute: function(rest, selectedRecords, searchCallback) {
    			alert('Bye ' + selectedRecords.map(function(record) {return record.email}));
    			
    			searchCallback(false);
    		},
    		
    		isEnabled: function(rest, record) {
    			return record.id != 9;
    		}
    	});
    
        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col xs={12}>
                            <PanelContainer noControls={true} bordered>
                                <Panel>
                                    <PanelHeader className='bg-darkblue fg-white'>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <h3>
                                                        <Icon glyph='icon-dripicons-user'/>{' '}
                                                        <Entity entity='person' data={{property: 'persons'}}/>
                                                    </h3>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelHeader>
                                    <PanelBody>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                	<AdvancedTable
                                                		id='person-table'
                                                		ref='person_table'
                                                		rest='/api/core/persons'                                                		
                                                		createNewDetail='person-detail'
                                                		customFilterFunc={this.createFilter}
                                                		customNamedFilters={customNamedFilters}
                                                		actionUniqueProperty='id'
                                                		actions={actions}>
                                                	
                                                		<AdvancedTable.Column property='id' face='integer' detail='person-detail' width='15px' summaryRenderFunc={this.idSummaryExample} />
                                                		<AdvancedTable.Column property='surname' tooltip='This is very long tooltip info which will be shown' />
                                                		<AdvancedTable.Column property='firstname' searchable={false}/>
                                                		<AdvancedTable.Column property='email'/>
                                                		<AdvancedTable.Column property='version' face='integer' summaryRenderFunc={this.versionSummaryExample}/>
                                                		<AdvancedTable.Column property='created' face='date' searchable={false} />
                                                	</AdvancedTable>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelBody>
                                </Panel>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = PersonList;
