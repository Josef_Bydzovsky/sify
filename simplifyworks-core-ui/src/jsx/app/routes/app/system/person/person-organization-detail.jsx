var restEndpoint = '/api/core/person-organizations/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);


var PersonOrganizationForm = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    handleSubmit: function (component) {
        this.state.data.person = component.model.person;
        this.state.data.organization = component.model.organization;
        this.state.data.validFrom = component.model.validFrom;
        this.state.data.validTill = component.model.validTill;
        this.state.data.category = component.model.category;
        this.state.data.obligation = component.model.obligation;
        

        var postUrl = restEndpoint;
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.isNew === true ? component.model : this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'person-organization-detail', this.props.modalClose);
    },

    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillMount: function () {
        //Universal method for load form
        BasicForm.loadMethod(this, this.props.url);
    },
    
    render: function () {
        if (this.state.loaded) {
            var commponent = (
                <Grid>
                    <Row>
                        <Col xs={10} xsOffset={1}>
                            <BasicForm name='form-person-organization-detail'
                                       nameKey='corePersonOrganization_detail'
                                       onSubmit={this.handleSubmit}
                                       formColor='blue'
                                       routeOnCancel='person-list'
                                       isNew={this.state.isNew}
                                       modalClose={this.props.modalClose}
                                       flux={this.getFlux()}>
                                <Row>
                                    <Col xs={6}>
                                        <CodeList name='person'
                                                  value={this.state.data.person?this.state.data.person:this.props.personId}
                                                  rest={'/api/core/persons'}
                                                  width='150px'
                                                  required={true}
                                                  niceLabelFunc={RenderUtil.personNiceLabel}
                                                  labelKey='corePersonOrganization_person'
                                                  flux={this.getFlux()}>
                                            <BasicTable.BasicColumn property='id'
                                                                    face='detail'
                                                                    width='20px'/>
                                            <BasicTable.BasicColumn property='firstname' width='200px'/>
                                            <BasicTable.BasicColumn property='surname' width='200px'/>
                                        </CodeList>

                                        <CodeList name='organization'
                                                  value={this.state.data.organization?this.state.data.organization:this.props.organizationId}
                                                  rest={'/api/core/organizations'}
                                                  width='150px'
                                                  required={true}
                                                  niceLabelFunc={RenderUtil.organizationNiceLabel}
                                                  labelKey='corePersonOrganization_organization'
                                                  flux={this.getFlux()}>
                                            <BasicTable.BasicColumn property='id'
                                                                    face='detail'
                                                                    width='20px'/>
                                            <BasicTable.BasicColumn property='name' width='200px'/>
                                        </CodeList>

                                        <InputText name='category'
                                                   value={this.state.data.category}
                                                   labelKey='corePersonOrganization_category'/>
                                    </Col>

                                    <Col xs={6}>
                                        <Datepicker name='validFrom'
                                                    value={this.state.data.validFrom}
                                                    labelKey='corePersonOrganization_validFrom'/>

                                        <Datepicker name='validTill'
                                                    value={this.state.data.validTill}
                                                    labelKey='corePersonOrganization_validTill'/>

                                        <InputText name='obligation'
                                                   value={this.state.data.obligation}
                                                   labelKey='corePersonOrganization_obligation'/>
                                    </Col>
                                </Row>

                            </BasicForm>
                        </Col>
                    </Row>

                </Grid>
            );
        }
        return (
            <div>
                {commponent}
            </div>
        );
    }
});


module.exports = PersonOrganizationForm;
