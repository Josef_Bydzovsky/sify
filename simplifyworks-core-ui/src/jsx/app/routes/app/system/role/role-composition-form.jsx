var restEndpoint = '/api/core/role-compositions/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var RoleCompositionForm = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    handleSubmit: function (component) {
        this.state.data.superiorRole = component.model.superiorRole;
        this.state.data.subRole = component.model.subRole;

        var postUrl = restEndpoint;
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'role-detail', this.props.modalClose);
        this.getFlux().actions.auth.changeRoles();
    },

    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            id: this.props.id,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillReceiveProps: function (next) {
        this.setState({id: next.id});
        BasicForm.loadMethod(this, next.url);
    },

    componentWillMount: function () {
        //Universal method for load form
        BasicForm.loadMethod(this, this.props.url);
    },

    render: function () {
        if (this.state.loaded) {
        	this.state.data.superiorRole = this.props.superiorRole;
            this.state.data.subRole = this.props.subRole;
        	
            var commponent = (
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <BasicForm name={'form-role-composition-detail' + this.props.id}
                                       nameKey='role_composition_detail'
                                       onSubmit={this.handleSubmit}
                                       formColor='blue'
                                       glyph='icon-simple-line-icons-users'
                                       modalClose={this.props.modalClose}
                                       isNew={this.state.isNew}>

	                            <CodeList name='superiorRole'
	                                	  value={this.state.data.superiorRole}
	                                      rest={'/api/core/roles'}
	                                	  width='150px'
	                                      required={true}
	                            	      disabled={this.props.superiorRole}
	                                      niceLabelFunc={RenderUtil.userRoleNiceLabel}
	                                      labelKey='role_composition_superior_role'
	                                      flux={this.getFlux()}>
	                            	<BasicTable.BasicColumn property='id'
	                            							face='detail'
	                            							width='20px'/>
	                            	<BasicTable.BasicColumn property='name' width='200px'/>
	                            </CodeList>
	                            
	                            <CodeList name='subRole'
                              	  	      value={this.state.data.subRole}
                                          rest={'/api/core/roles'}
                              	          width='150px'
                              	          disabled={this.props.subRole}
                                          required={true}
                                          niceLabelFunc={RenderUtil.userRoleNiceLabel}
                                          labelKey='role_composition_sub_role'
                                          flux={this.getFlux()}>
	                            	<BasicTable.BasicColumn property='id'
	                            							face='detail'
	                            							width='20px'/>
	                                <BasicTable.BasicColumn property='name' width='200px'/>
	                            </CodeList>
                            </BasicForm>
                        </Col>
                    </Row>

                </Grid>
            );
        }
        return (
            <div>
                {commponent}
            </div>
        );
    }
});

module.exports = RoleCompositionForm;
