var restEndpoint = '/api/core/roles/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var UserRoleForm = require('./../user/user-role-form.jsx');
var RoleCompositionForm = require('./role-composition-form.jsx');

var Form = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    handleSubmit: function (component) {
        this.state.data.name = component.model.name;
        this.state.data.description = component.model.description;

        var postUrl = restEndpoint;
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'role-detail');
        this.getFlux().actions.auth.changeRoles();
    },

    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            id: this.props.id,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillReceiveProps: function (next) {
        this.setState({id: next.id});
        BasicForm.loadMethod(this, next.url);
    },

    componentWillMount: function () {
        //Universal method for load form
        BasicForm.loadMethod(this, this.props.url);
    },
    
    handleRoleCompositionSubmit: function (component) {
        this.state.data.superiorRole = component.model.superiorRole;
        this.state.data.subRole = component.model.subRole;

        var postUrl = restEndpoint;
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'role-detail');
        this.getFlux().actions.auth.changeRoles();
    },
    
    getUserRoleModalDialog: function (url, isNew, idElement, modalClose) {
        return (
        	<UserRoleForm url={url} isNew={isNew} modalClose={modalClose} parentId={this.state.data.id}
                                 flux={this.getFlux()} role={this.state.data}/>
    )},
    
    getSuperiorRoleModalDialog: function (url, isNew, idElement, modalClose) {
        return (
        	<RoleCompositionForm url={url} isNew={isNew} modalClose={modalClose} parentId={this.state.data.id}
                                 flux={this.getFlux()} subRole={this.state.data}/>
    )},
    
    getSubRoleModalDialog: function (url, isNew, idElement, modalClose) {
        return (
        	<RoleCompositionForm url={url} isNew={isNew} modalClose={modalClose} parentId={this.state.data.id}
                                 flux={this.getFlux()} superiorRole={this.state.data}/>
    )},   

    render: function () {
        if (this.state.loaded) {
            var commponent = (
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <BasicForm name={'form-role-detail'+this.props.id}
                                       nameKey='role_detail'
                                       onSubmit={this.handleSubmit}
                                       formColor='blue'
                                       glyph='icon-simple-line-icons-users'
                                       routeOnCancel='role-list'
                                       isNew={this.state.isNew}>

                                <InputText name='name'
                                           value={this.state.data.name}
                                           disabled={!this.state.isNew}
                                           validations='isLowercase,isLength:3:25'
                                           labelKey='role_name'/>

                                <TextArea name='description'
                                           value={this.state.data.description}
                                           validations='isLength:0:255'
                                           labelKey='role_description'/>
                                           
                            </BasicForm>
                        </Col>
                    </Row>
                    
                    <Row>
                        <Col sm={12}>
                            <PanelContainer noControls={true} bordered>
                            	<Panel>
                            		<PanelHeader className='bg-darkblue fg-white'>
                            			<Grid>
                            				<Row>
                            					<Col xs={12}>
                            						<h3>
                            							<Icon glyph='icon-simple-line-icons-users'/>
                            							<span>{' '}</span>
                            							<Entity entity='role' data={{property: 'users'}} />
                            						</h3>
                            					</Col>
                            				</Row>
                            			</Grid>
                            		</PanelHeader>
                            		
                            		<PanelBody>
                            			<Grid>
                            				<Row>
                            					<Col xs={12}>                            						
	                            					<BasicTable id='user-role-table'
					                                            rest={'/api/core/user-roles'}
					                                            filter={'role.id:'+(this.state.id != null && !this.state.isNew ?this.state.id:'0')}
					                                            labelKey='role_users'
					                                            isNew={this.state.isNew}
					                                            showNewButton={!this.state.isNew}
					                                            modalDetail={this.getUserRoleModalDialog}>
					                                    <BasicTable.BasicColumn property='id'
					                                                            face='detail'
					                                                            width='20px'/>
					
					                                    <BasicTable.BasicColumn property='user.username'
					                                                            labelKey='user_role_user'/>
					                                    <BasicTable.BasicColumn property='validFrom'
					                                                            labelKey='user_role_validFrom'
					                                                            face='date'/>
					                                    <BasicTable.BasicColumn property='validTill'
					                                                            labelKey='user_role_validTill'
					                                                            face='date'/>
					                                    <BasicTable.BasicColumn property='id' width='15px'
					                                                            face='actions'/>
				                                    </BasicTable>
					                            </Col>
					                        </Row>
					                    </Grid>
					                </PanelBody>
					            </Panel>
					        </PanelContainer>
					    </Col>
					</Row>
					
					<Row>    
					    <Col sm={6}>
                            <PanelContainer noControls={true} bordered>
                            	<Panel>
                            		<PanelHeader className='bg-darkblue fg-white'>
                            			<Grid>
                            				<Row>
                            					<Col xs={12}>
                            						<h3>
                            							<Icon glyph='icon-simple-line-icons-users' />
                            							<span>{' '}</span>
                            							<Entity entity='role' data={{property: 'superior_roles'}} />
                            						</h3>
                            					</Col>
                            				</Row>
                            			</Grid>
                            		</PanelHeader>
                            		
                            		<PanelBody>
                            			<Grid>
                            				<Row>
                            					<Col xs={12}>
                            						<BasicTable id='role-superior-roles-table'
                            									rest={'/api/core/role-compositions'}
                            									filter={'subRole.id:' + (this.state.id != null && !this.state.isNew ? this.state.id : '0')}
                            									labelKey='role_superior_roles'
                            									showNewButton={true}
                            									detail='role-detail'
                            									modalDetail={this.getSuperiorRoleModalDialog}>
                            							<BasicTable.BasicColumn property='superiorRole.id'
		                                                            			face='detail'
		                                                            			width='20px'/>
								                        <BasicTable.BasicColumn property='superiorRole.name'
								                                              	labelKey='role_name'/>
								                        <BasicTable.BasicColumn property='id'
								                        						width='15px'
								                        						face='actions'/>
								                     </BasicTable>
                            					</Col>
                            				</Row>
                            			</Grid>
                            		</PanelBody>
                            	</Panel>
                            </PanelContainer>
                        </Col>
                        
                        <Col sm={6}>
	                        <PanelContainer noControls={true} bordered>
	                        	<Panel>
	                        		<PanelHeader className='bg-darkblue fg-white'>
	                        			<Grid>
	                        				<Row>
	                        					<Col xs={12}>
	                        						<h3>
	                        							<Icon glyph='icon-simple-line-icons-users' />
	                        							<span>{' '}</span>
	                        							<Entity entity='role' data={{property: 'sub_roles'}} />
	                        						</h3>
	                        					</Col>
	                        				</Row>
	                        			</Grid>
	                        		</PanelHeader>
	                        		
	                        		<PanelBody>
	                        			<Grid>
	                        				<Row>
	                        					<Col xs={12}>
	                        						<BasicTable id='role-subroles-table'
	                        									rest={'/api/core/role-compositions'}
	                        									filter={'superiorRole.id:' + (this.state.id != null && !this.state.isNew ? this.state.id : '0')}
	                        									labelKey='role_subroles'
	                        									showNewButton={true}
	                        									detail='role-detail'
	                        									modalDetail={this.getSubRoleModalDialog}>

	                            						<BasicTable.BasicColumn property='subRole.id'
		                                                            			face='detail'
		                                                            			width='20px'/>
	                            								
	               	                                    <BasicTable.BasicColumn property='subRole.name'
	               	                                           					labelKey='role_name'/>
	               	                                           	
	               	                                    <BasicTable.BasicColumn property='id'
	               	                                    						width='15px'
	               	                                           					face='actions'/>
	               	                                </BasicTable>
	                        					</Col>
	                        				</Row>
	                        			</Grid>
	                        		</PanelBody>
	                        	</Panel>
	                        </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            );
        }
        return (
            <div>
                {commponent}
            </div>
        );
    }
});


var RoleDetail = React.createClass({
    mixins: [SidebarMixin, ReactRouter.State, FluxMixin],
    render: function () {

        var id = this.getParams().id;

        return (
            <Container id='body'>
                <Form url={restEndpoint + id}
                      id={id}
                      isNew={BasicForm.isNew(id)}/>
            </Container>
        );
    }
});

module.exports = RoleDetail;
