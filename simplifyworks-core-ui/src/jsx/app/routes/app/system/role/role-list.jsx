var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var NewEnum = require('../../../../domain/newEnum.jsx');

var RoleList = React.createClass({
    mixins: [ReactRouter.State, FluxMixin],

    componentWillMount: function () {
        //default value of filter
        //this.setState({data: {firstname: '', surname: '', type: 'VALUE2'}});
    },

    componentDidMount: function () {
        //run filter after mount (because of some default filters)
        // this.filter(this.refs.person_table.refs.person_filter);
    },

    render: function () {
        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col xs={12}>
                            <PanelContainer noControls={true} bordered>
                                <Panel>
                                    <PanelHeader className='bg-darkblue fg-white'>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <h3>
                                                        <Icon glyph='icon-simple-line-icons-users'/>{' '}
                                                        <Entity entity='role' data={{property: 'roles'}}/>
                                                    </h3>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelHeader>
                                    <PanelBody>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <BasicTable id='role-table'
                                                                rest='/api/core/roles'
                                                                ref='role_table'
                                                                detail='role-detail'>
                                                        <BasicTable.BasicColumn property='id' width='15px'
                                                                                face='detail'/>
                                                        <BasicTable.BasicColumn property='name'/>
                                                        <BasicTable.BasicColumn property='description' width='250px'/>
                                                        <BasicTable.BasicColumn property='modifier'/>
                                                        <BasicTable.BasicColumn property='modified' face='date'/>
                                                        <BasicTable.BasicColumn property='creator'/>
                                                        <BasicTable.BasicColumn property='created' face='date'/>
                                                        <BasicTable.BasicColumn property='id' face='actions'
                                                                                width='15px'/>
                                                    </BasicTable>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelBody>
                                </Panel>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = RoleList;
