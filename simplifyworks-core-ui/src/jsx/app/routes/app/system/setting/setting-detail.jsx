var restEndpoint = '/api/core/settings/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);


var Form = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    handleSubmit: function (component) {
        this.state.data.settingKey = component.model.settingKey;
        this.state.data.value = component.model.value;
        this.state.data.system = component.model.system;

        var postUrl = restEndpoint;
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'setting-detail');
    },

    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            id: this.props.id,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillReceiveProps: function (next) {
        this.setState({id: next.id});
        BasicForm.loadMethod(this, next.url);
    },

    componentWillMount: function () {
        //Universal method for load form
        BasicForm.loadMethod(this, this.props.url);
    },

    render: function () {
        if (this.state.loaded) {
            var commponent = (
                <Grid>
                    <Row>
                        <Col xs={6} xsOffset={3}>
                            <BasicForm name={'form-setting-detail'+this.props.id}
                                       nameKey='setting_detail'
                                       onSubmit={this.handleSubmit}
                                       formColor='blue'
                                       glyph='icon-simple-line-icons-settings'
                                       routeOnCancel='setting-list'
                                       isNew={this.state.isNew}
                                       disabled={this.state.data.system}>

                                <InputText name='settingKey'
                                           value={this.state.data.settingKey}
                                           disabled={!this.state.isNew}
                                           required={true}
                                           labelKey='setting_settingKey'/>

                                <InputText name='value'
                                           value={this.state.data.value}
                                           labelKey='setting_value'/>

                                <Checkbox name='system'
                                          value={this.state.data.system}
                                          labelKey='setting_system'/>

                            </BasicForm>
                        </Col>
                    </Row>

                </Grid>
            );
        }
        return (
            <div>
                {commponent}
            </div>
        );
    }
});


var SettingDetail = React.createClass({
    mixins: [SidebarMixin, ReactRouter.State, FluxMixin],
    render: function () {

        var id = this.getParams().id;

        return (
            <Container id='body'>
                <Form url={restEndpoint + id}
                      id={id}
                      isNew={BasicForm.isNew(id)}/>
            </Container>
        );
    }
});

module.exports = SettingDetail;
