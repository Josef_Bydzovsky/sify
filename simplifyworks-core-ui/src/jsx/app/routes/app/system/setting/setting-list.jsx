var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var SettingList = React.createClass({

    mixins: [FluxMixin],

    render: function () {
        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer noControls={true} bordered>
                                <Panel>
                                    <PanelHeader className='bg-darkblue fg-white'>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <h3>
                                                        <Icon glyph='icon-simple-line-icons-settings'/>{' '}
                                                        <Entity entity='core_navigation' data={{item: 'setting'}}/>
                                                    </h3>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelHeader>
                                    <PanelBody>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <BasicTable id='setting-table' rest='/api/core/settings' detail='setting-detail'>
                                                        <BasicTable.BasicColumn property='id' width='15px'
                                                                                face='detail'/>
                                                        <BasicTable.BasicColumn property='settingKey'/>
                                                        <BasicTable.BasicColumn property='value'/>
                                                        <BasicTable.BasicColumn property='system' face='boolean'/>
                                                        <BasicTable.BasicColumn property='modifier'/>
                                                        <BasicTable.BasicColumn property='modified' face='date'/>
                                                        <BasicTable.BasicColumn property='creator'/>
                                                        <BasicTable.BasicColumn property='created' face='date'/>
                                                        <BasicTable.BasicColumn property='version'/>
                                                        <BasicTable.BasicColumn property='id' width='15px'
                                                                                face='actions'/>
                                                    </BasicTable>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelBody>
                                </Panel>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = SettingList;
