var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var TemplateList = React.createClass({

    mixins: [FluxMixin],

    render: function () {
        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer noControls={true} bordered>
                                <Panel>
                                    <PanelHeader className='bg-darkblue fg-white'>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <h3>
                                                        <Icon glyph='icon-simple-line-icons-grid'/>{' '}
                                                        <Entity entity='core_navigation' data={{item: 'template'}}/>
                                                    </h3>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelHeader>
                                    <PanelBody>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <BasicTable id='template-table' rest='/api/core/templates' detail='template-detail'>
                                                        <BasicTable.BasicColumn property='id' width='15px'
                                                                                face='detail'/>
                                                        <BasicTable.BasicColumn property='code'/>
                                                        <BasicTable.BasicColumn property='description'/>
                                                        <BasicTable.BasicColumn property='modifier'/>
                                                        <BasicTable.BasicColumn property='modified' face='date'/>
                                                        <BasicTable.BasicColumn property='creator'/>
                                                        <BasicTable.BasicColumn property='created' face='date'/>
                                                        <BasicTable.BasicColumn property='version'/>
                                                        <BasicTable.BasicColumn property='id' width='15px'
                                                                                face='actions'/>
                                                    </BasicTable>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelBody>
                                </Panel>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = TemplateList;
