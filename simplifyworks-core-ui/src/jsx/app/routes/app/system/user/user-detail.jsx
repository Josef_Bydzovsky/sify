// TODO: global app setting
var restEndpoint = '/api/core';

var PersonForm = require('../person/person-form.jsx');
var RenderUtil = require('../../../../renderer/render-util.jsx');
var RestUtil = require('../../../../domain/rest-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);
var UserRoleForm = require('./user-role-form.jsx');

var UserForm = React.createClass({

    mixins: [FluxMixin, ReactRouter.State, ReactRouter.Navigation],

    handleSubmit: function (component, button, processDefinition) {
        this.state.data.username = component.model.username;
        this.state.data.newPassword = component.model.newPassword;
        this.state.data.person = component.model.person;
        this.state.data.roles = component.model.roles;


        var postUrl = restEndpoint + '/users';
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.data);
        console.log(dataToPut);

        if (button) {
            //Universal method for save form and process workflow
            WorkflowForm.saveAndProcessMethod(this, component, postUrl, this.state.data, 'user-detail', button, this.state.data.wfProcessInstanceId, processDefinition);
        } else {

            //Universal method for save form
            BasicForm.saveMethod(this, component, postUrl, dataToPut, 'user-detail');
        }
    },


    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            id: this.props.id,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillReceiveProps: function (next) {
        this.setState({id: next.id});
        this.load(next.url);
    },

    componentWillMount: function () {
        //Universal method for load form
        this.load(this.props.url);
    },

    getUserRoles() {
        var result = RestUtil.loadRest('/api/core/user-roles', 'user.id:' + (this.state.id != null && !this.state.isNew ? this.state.id : '0'), null, false)
        return result._embedded ? result._embedded.resources : null;
    },

    load(url) {
        BasicForm.loadMethod(this, url);
    },

    changePerson(component) {

        //we will invoke change only on personForm
        if (component.state.value == null) {
            this.refs['personForm'].setState({isNew: true}, function () {
                this.refs['personForm'].loadData()
            }.bind(this))
        } else {
            var url = restEndpoint + '/persons/' + component.state.value.id;
            this.refs['personForm'].setState({url: url, isNew: false}, function () {
                this.refs['personForm'].loadData(url)
            }.bind(this))
        }
    },


    getNewUserRole() {
        return {
            user: this.state.data['@id'],
            role: null,
            validTill: null,
            validFrom: new Date()
        }
    },

    getUserRoleModalDialog: function (url, isNew, idElement, modalClose) {
        return (
        	<UserRoleForm url={url} isNew={isNew} modalClose={modalClose} parentId={this.state.data.id}
                                 flux={this.getFlux()} user={this.state.data}/>
    )},

    render: function () {

        if (this.state.loaded) {
            var commponent = (
                <Grid>
                	<Row>
                    	<Col xs={12}>                        
	                    	<WorkflowForm name={'form-user-detail'+this.props.id}
                            	nameKey='user_detail'
                            	onSubmit={this.handleSubmit}
	                    		formColor='blue'
	                    		routeOnCancel='user-list'
	                    		glyph='icon-simple-line-icons-user'
	                    		isNew={this.state.isNew}
	                    		processDefinition="sify_1"
	                    		processIdentifier={this.state.data.wfProcessInstanceId}>
	
	                    		<InputText name='username'
	                    			value={this.state.data.username}
	                    			required={true}
	                    			validations="isLength:1:50"
	                    			disabled={!this.state.isNew}
	                    			labelKey='user_username'/>
	
	                    		<InputText name='newPassword'
	                    			value={this.state.data.newPassword}
	                    			//validations="matches:(.{8,}|(^$))"
	                    			required={this.state.isNew}
	                    			password={true}
	                    			labelKey='user_password'/>
	
	                    		<CodeList name='person'
	                    			value={this.state.data.person}
	                    			rest={'/api/core/persons'}
	                    			width='300px'
	                    			required={false}
	                    			changeListener={this.changePerson}
	                    			niceLabelFunc={RenderUtil.personNiceLabel}
	                    			labelKey='user_person'
	                    			flux={this.getFlux()}>
	
	                    			<BasicTable.BasicColumn property='id'
	                    				face='detail'
	                    				width='20px'/>
	                    			<BasicTable.BasicColumn property='surname' width='400px'/>
	                    			<BasicTable.BasicColumn property='firstname'/>
	                    			<BasicTable.BasicColumn property='email'/>
	                    		</CodeList>
	                    	</WorkflowForm>
                        </Col>
                    </Row>
                    
                    <Row>
                        <Col sm={12}>
	                        <PanelContainer noControls={true} bordered>
	                        	<Panel>
	                        		<PanelHeader className='bg-darkblue fg-white'>
	                        			<Grid>
	                        				<Row>
	                        					<Col xs={12}>
	                        						<h3>
	                        							<Icon glyph='icon-simple-line-icons-users' />
	                        							<span>{' '}</span>
	                        							<Entity entity='role' data={{property: 'roles'}} />
	                        						</h3>
	                        					</Col>
	                        				</Row>
	                        			</Grid>
	                        		</PanelHeader>
	                        		
	                        		<PanelBody>
	                        			<Grid>
	                        				<Row>		                                                
		                                        <Col xs={12}>
	                        						<BasicTable id='user-role-table'
	                        							rest={'/api/core/user-roles'}
	                        							filter={'user.id:' + (this.state.id != null && !this.state.isNew ? this.state.id : '0')}
	                        							labelKey='user_roles'
	                        							showNewButton={true}
	                        							modalDetail={this.getUserRoleModalDialog}>
	                        						
	                        							<BasicTable.BasicColumn property='id'
	                        								face='detail'
	                        								width='20px'/>
	                        							<BasicTable.BasicColumn property='role.name'
	                        								labelKey='user_role_role'/>
	                        							<BasicTable.BasicColumn property='validFrom'
	                        								labelKey='user_role_validFrom'
	                        								face='date'/>
	                        							<BasicTable.BasicColumn property='validTill'
	                        								labelKey='user_role_validTill'
	                        								face='date'/>
	                        							<BasicTable.BasicColumn property='id' width='15px'
	                        								face='actions'/>
								                     </BasicTable>
	                        					</Col>
	                        				</Row>
	                        			</Grid>
	                        		</PanelBody>
	                        	</Panel>
	                        </PanelContainer>
	                    </Col>
                    </Row>
                </Grid>
            );
        }
        return (
            <div>
                {commponent}
            </div>
        );
    }
});


var UserDetail = React.createClass({
    mixins: [SidebarMixin, FluxMixin, ReactRouter.State],

    render: function () {
        var idUser = this.getParams().id;
        return (
            <Container id='body'>
                <UserForm url={restEndpoint + '/users/' + idUser}
                          id={idUser}
                          isNew={BasicForm.isNew(idUser)}/>
            </Container>
        );
    }
});

module.exports = UserDetail;
