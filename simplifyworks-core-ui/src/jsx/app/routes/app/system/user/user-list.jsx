var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var UserList = React.createClass({

    mixins: [FluxMixin],

    renderFunc: function(row, column, data) {
    	return (
    		<div>row:{row}, column:{column}, data:{data}</div>
    	);
    },
    
    modalFunc: function(url, isNew, idElement, modalClose) {
    	return (
    		<div>Hello world!</div>
    	)
    },
    
    render: function () {
        return (
            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer noControls={true} bordered>
                                <Panel>
                                    <PanelHeader className='bg-darkblue fg-white'>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <h3>
                                                        <Icon glyph='icon-simple-line-icons-user'/>{' '}
                                                        <Entity entity='user' data={{property: 'users'}}/>
                                                    </h3>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelHeader>
                                    <PanelBody>
                                        <Grid>
                                            <Row>                                            
                                                <Col xs={12}>
                                                    <BasicTable id='user-table'
                                                                rest='/api/core/users'
                                                                detail='user-detail'
                                                                showFulltext={true}>
                                                        <BasicTable.BasicColumn property='id' width='15px'
                                                                                face='detail'/>
                                                        <BasicTable.BasicColumn property='username'/>
                                                        <BasicTable.BasicColumn property='lastLogin' face='date'/>
                                                        <BasicTable.BasicColumn property='version'/>
                                                        <BasicTable.BasicColumn property='person.firstname'/>
                                                        <BasicTable.BasicColumn property='person.surname'/>
                                                        <BasicTable.BasicColumn property='person.email'/>
                                                        <BasicTable.BasicColumn property='id' width='15px'
                                                                                face='actions'/>
                                                    </BasicTable>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelBody>
                                </Panel>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = UserList;