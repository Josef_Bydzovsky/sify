var restEndpoint = '/api/core/user-roles/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);


var Form = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    handleSubmit: function (component) {
        this.state.data.user = component.model.user;
        this.state.data.role = component.model.role;
        this.state.data.validFrom = component.model.validFrom;
        this.state.data.validTill = component.model.validTill;

        var postUrl = restEndpoint;
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.isNew === true ? component.model : this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'user-role-detail');
        this.getFlux().actions.auth.changeRoles();
    },

    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillMount: function () {
        //Universal method for load form
        BasicForm.loadMethod(this, this.props.url);
    },

    changePerson: function (component) {
        this.state.data.person = component.state.value;
        this.setState({data: this.state.data});
    },


    render: function () {
        if (this.state.loaded) {
            var commponent = (
                <Grid>
                    <Row>
                        <Col xs={10} xsOffset={1}>
                            <BasicForm name='form-user-role-detail'
                                       nameKey='user_role_detail'
                                       onSubmit={this.handleSubmit}
                                       formColor='blue'
                                       routeOnCancel='user-list'
                                       isNew={this.state.isNew}>

                                <Row>
                                    <Col xs={6}>
                                        <CodeList name='user'
                                                  value={this.state.data.user}
                                                  rest={'/api/core/users'}
                                                  width='150px'
                                                  required={true}
                                                  niceLabelFunc={RenderUtil.userNiceLabel}
                                                  labelKey='user_role_user'
                                                  flux={this.getFlux()}>
                                            <BasicTable.BasicColumn property='id'
                                                                    face='detail'
                                                                    width='20px'/>
                                            <BasicTable.BasicColumn property='username' width='200px'/>
                                        </CodeList>

                                        <CodeList name='role'
                                                  value={this.state.data.role}
                                                  rest={'/api/core/roles'}
                                                  width='150px'
                                                  required={true}
                                                  niceLabelFunc={RenderUtil.userRoleNiceLabel}
                                                  labelKey='user_role_role'
                                                  flux={this.getFlux()}>
                                            <BasicTable.BasicColumn property='id'
                                                                    face='detail'
                                                                    width='20px'/>
                                            <BasicTable.BasicColumn property='name' width='200px'/>
                                        </CodeList>
                                    </Col>

                                    <Col xs={6}>
                                        <Datepicker name='validFrom'
                                                    value={this.state.data.validFrom}
                                                    labelKey='user_role_validFrom'/>

                                        <Datepicker name='validTill'
                                                    value={this.state.data.validTill}
                                                    labelKey='user_role_validTill'/>
                                    </Col>
                                </Row>

                            </BasicForm>
                        </Col>
                    </Row>

                </Grid>
            );
        }
        return (
            <div>
                {commponent}
            </div>
        );
    }
});


var UserRoleDetail = React.createClass({
    mixins: [SidebarMixin, ReactRouter.State, FluxMixin],
    render: function () {

        var idUser = this.getParams().id;
        return (
                <Container id='body'>
                    <Form url={restEndpoint + idUser}
                          idUser={idUser}
                          isNew={BasicForm.isNew(idUser)}/>
                </Container>
        );
    }
});

module.exports = UserRoleDetail;
