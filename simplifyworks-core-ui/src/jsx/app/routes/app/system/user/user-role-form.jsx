var restEndpoint = '/api/core/user-roles/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);

var UserRoleForm = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    handleSubmit: function (component) {
        this.state.data.user = component.model.user;
        this.state.data.role = component.model.role;

        var postUrl = restEndpoint;
        console.log('postUrl ' + postUrl);
        var dataToPut = JSON.stringify(this.state.data);
        console.log(dataToPut);

        //Universal method for save form
        BasicForm.saveMethod(this, component, postUrl, dataToPut, 'role-detail', this.props.modalClose);
        this.getFlux().actions.auth.changeRoles();
    },

    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            id: this.props.id,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillReceiveProps: function (next) {
        this.setState({id: next.id});
        BasicForm.loadMethod(this, next.url);
    },

    componentWillMount: function () {
        //Universal method for load form
        BasicForm.loadMethod(this, this.props.url);
    },

    render: function () {
        if(this.state.loaded) {
        	if(this.props.user) {
        		this.state.data.user = this.props.user;
        	}
        	
        	if(this.props.role) {
        		this.state.data.role = this.props.role;
        	}
        	
            var commponent = (
                <Grid>
                    <Row>
                        <Col sm={12}>
	                        <BasicForm name='form-user-role-detail'
	                            nameKey='user_role_detail'
	                            onSubmit={this.handleSubmit}
	                            formColor='blue'
	                            modalClose={this.props.modalClose}
	                            isNew={this.state.isNew}>
	
	                        	<Row>
	                        		<Col xs={6}>
	                        			<CodeList name='user'
	                        				value={this.state.data.user}
	                                       	rest={'/api/core/users'}
	                                       	width='150px'
	                                       	required={true}
	                                       	niceLabelFunc={RenderUtil.userNiceLabel}
	                                       	labelKey='user_role_user'
	                                       	flux={this.getFlux()}>
	                        			
	                        				<BasicTable.BasicColumn property='id'
	                        					face='detail'
	                                            width='20px'/>
	                                        <BasicTable.BasicColumn property='username' width='200px'/>
	                                    </CodeList>
	
	                        			<CodeList name='role'
	                        				value={this.state.data.role}
	                                       	rest={'/api/core/roles'}
	                                       	width='150px'
	                                       	required={true}
	                                       	niceLabelFunc={RenderUtil.userRoleNiceLabel}
	                                       	labelKey='user_role_role'
	                                       	flux={this.getFlux()}>
	                                 
	                        				<BasicTable.BasicColumn property='id'
	                        					face='detail'
	                                            width='20px'/>
	                                        <BasicTable.BasicColumn property='name' width='200px'/>
	                                    </CodeList>
	                        		</Col>
	
	                        		<Col xs={6}>
	                        			<Datepicker name='validFrom'
	                        				value={this.state.data.validFrom}
	                                        labelKey='user_role_validFrom'/>
	
	                                    <Datepicker name='validTill'
	                                    	value={this.state.data.validTill}
	                                        labelKey='user_role_validTill'/>
	                                </Col>
	                        	</Row>
	                        </BasicForm>
                        </Col>
                    </Row>

                </Grid>
            );
        }
        return (
            <div>
                {commponent}
            </div>
        );
    }
});

module.exports = UserRoleForm;
