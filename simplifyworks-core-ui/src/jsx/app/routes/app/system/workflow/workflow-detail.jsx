var restEndpoint = '/api/wf/definitions/';

var RenderUtil = require('../../../../renderer/render-util.jsx');
var Fluxxor = require('fluxxor');
var FluxMixin = Fluxxor.FluxMixin(React);


var Form = React.createClass({

    mixins: [ReactRouter.State, ReactRouter.Navigation, FluxMixin],

    handleSubmit: function (component) {
        //this.state.data.settingKey = component.model.settingKey;
        //this.state.data.value = component.model.value;
        //this.state.data.system = component.model.system;
        //this.state.data.attachments = component.model.attachments;
        //
        //var postUrl = restEndpoint;
        //console.log('postUrl ' + postUrl);
        //var dataToPut = JSON.stringify(this.state.data);
        //console.log(dataToPut);

        //Universal method for save form
        //BasicForm.saveMethod(this, component, postUrl, dataToPut, 'workflow-detail');
    },


    getInitialState: function () {
        return {
            data: [],
            loaded: false,
            id: this.props.id,
            isNew: this.props.isNew === true ? true : false
        };
    },

    componentWillReceiveProps: function (next) {
        this.setState({id: next.id});
        BasicForm.loadMethod(this, next.url);
    },

    componentWillMount: function () {
        //Universal method for load form
        BasicForm.loadMethod(this, this.props.url);
    },


    render: function () {
        var currentToken = this.getFlux().store("AuthStore").getCurrentToken();

        var downloadUrl = "/api/wf/deployments/" + this.state.data.deploymentId + "/download/?access_token=" + currentToken;

        var downloadComponent = <Col sm={1}>
            <div style={{height: 30}}/>
            <a className='dropzone-download' href={downloadUrl}><Icon
                bundle='icon-ikons-download'
                glyph='download'/></a> </Col>;


        var commponent = (
            <Grid>
                <Row>
                    <Col xs={6} xsOffset={3}>
                        <BasicForm name='form-workflow-detail'
                                   nameKey='workflow_detail'
                                   onSubmit={this.handleSubmit}
                                   formColor='blue'
                                   glyph='icon-fontello-flow-branch'
                                   routeOnCancel='workflow-list'
                                   isNew={this.state.isNew}
                                   visibleSaveButton={false}
                                   disabled={true}>


                            <Row>

                                <Col sm={this.state.isNew? 12 :11} className='dropzone-name'>
                                    <InputText name='name'
                                               value={this.state.data.name}
                                               disabled={!this.state.isNew}
                                               required={true}
                                               labelKey='workflow_name'/>
                                </Col>
                                {this.state.isNew ? '' : downloadComponent}
                            </Row>

                            <InputText name='key'
                                       value={this.state.data.key}
                                       labelKey='workflow_key'/>

                            <InputText name='category'
                                       value={this.state.data.category}
                                       labelKey='workflow_category'/>

                            <InputText name='version'
                                       value={this.state.data.version}
                                       labelKey='workflow_version'/>
                            <InputText name='resourceName'
                                       value={this.state.data.resourceName}
                                       labelKey='workflow_resourceName'/>
                            <TextArea name='description'
                                      value={this.state.data.description}
                                      labelKey='workflow_description'/>


                        </BasicForm>
                    </Col>
                </Row>
            </Grid>
        );
        return (
            <div>
                {commponent}
            </div>
        );
    }
});


var WorkflowDetail = React.createClass({
    mixins: [SidebarMixin, ReactRouter.State, FluxMixin],
    render: function () {

        var id = this.getParams().id;

        return (
            <Container id='body'>
                <Form url={restEndpoint + id}
                      id={id}
                      isNew={BasicForm.isNew(id)}/>
            </Container>
        );
    }
});

module.exports = WorkflowDetail;
