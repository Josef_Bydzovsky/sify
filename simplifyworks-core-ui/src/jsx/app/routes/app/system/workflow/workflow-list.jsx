var Fluxxor = require('fluxxor');
var RestApiUrl = '/api/wf/definitions/';
var FluxMixin = Fluxxor.FluxMixin(React);

var SettingList = React.createClass({

    mixins: [FluxMixin, ReactRouter.Navigation],

    componentWillMount: function () {
        this.loadData();
    },

    loadData: function () {
        $.when(
            $.ajax({
                url: RestApiUrl,
                dataType: 'json',
                async: true,
                success: function (data) {
                    result = data;
                }.bind(this),
                error: function (xhr, status, error) {
                    this.getFlux().actions.flash.add({
                        messageId: 'formErrors',
                        type: 'error',
                        message: l20n.ctx.getSync("workflow_error_load_def") + " (" + error + ")!"
                    });
                }.bind(this)
            })
        ).done(
            // read user information
            function (result) {
                this.setState({value: result.resource})
            }.bind(this)
        )
    },

    removeDefinition: function (id) {
        $.ajax({
            url: RestApiUrl + id,
            dataType: 'text',
            async: true,
            type: 'DELETE',
            success: function (data) {
                this.loadData();
                this.getFlux().actions.flash.add({
                    messageId: 'formSuccess',
                    type: 'success',
                    message: l20n.ctx.getSync("workflow_success_remove_def")
                });
            }.bind(this),
            error: function (xhr, status, error) {
                this.getFlux().actions.flash.add({
                    messageId: 'formErrors',
                    type: 'error',
                    message: l20n.ctx.getSync("workflow_error_remove_def") + " (" + error + ")!"
                });
            }.bind(this)
        });
    },

    onDrop: function (files) {
        for (var i = 0; i < files.length; i++) {
            if (files[i].name.endsWith('bpmn20.xml')) {
                this.uploadFile(files[i]);

            } else {
                this.getFlux().actions.flash.add({
                    messageId: 'formErrors',
                    type: 'error',
                    message: l20n.ctx.getSync("workflow_ext_only_bpmn") + " (" + files[i].name + ")!"
                });
            }
        }

    },

    uploadFile: function (file) {

        var fd = new FormData();
        fd.append('data', file);
        fd.append('deploymentName', file.name);
        fd.append('fileName', file.name);

        var parent = this;
        $.ajax({
            url: '/api/wf/deploy',
            data: fd,
            dataType: 'json',
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            type: 'POST',
            error: function (xhr, status, err) {
                parent.getFlux().actions.flash.add({
                    messageId: 'formErrors',
                    type: 'error',
                    message: l20n.ctx.getSync("workflow_not_deployed") + " (" + l20n.ctx.getSync(xhr.responseJSON.exception) + ")!"
                });

            },
            success: function (success) {
                parent.getFlux().actions.flash.add({
                    messageId: 'success',
                    type: 'success',
                    message: l20n.ctx.getSync("workflow_deployed") + " (" + success.resource.name + ")."
                });
                parent.loadData();
            }
        });
    },

    removeItem: function (id, table) {
        console.log('remove WF definitions id: ' + id);
        this.removeDefinition(id);
    },

    render: function () {
        var basicTable = this.state ? <BasicTable id='workflow-table'
                                                  value={this.state.value}
                                                  showNewButton={false}
                                                  removeItemFunc={this.removeItem}
                                                  detail='workflow-detail'
                                                  entity='workflow'>

            <BasicTable.BasicColumn property='id' width='15px'
                                    face='detail'/>
            <BasicTable.BasicColumn property='key'/>
            <BasicTable.BasicColumn property='name'/>
            <BasicTable.BasicColumn property='category'/>
            <BasicTable.BasicColumn property='version'/>
            <BasicTable.BasicColumn property='description'/>
            <BasicTable.BasicColumn property='resourceName'/>

            <BasicTable.BasicColumn property='id' width='15px'
                                    face='actions'/>
        </BasicTable> : null;

        return (

            <Container id='body'>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <PanelContainer noControls={true} bordered>
                                <Panel>
                                    <PanelHeader className='bg-darkblue fg-white'>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <h3>
                                                        <Icon glyph='icon-fontello-flow-branch'/>{' '}
                                                        <Entity entity='core_navigation' data={{item: 'workflow'}}/>
                                                    </h3>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelHeader>
                                    <PanelBody>
                                        <Grid>
                                            <Row>
                                                <Col xs={12}>
                                                    <Dropzone ref="dropzone"
                                                              multiple={true}
                                                              accept='.bpmn20.xml'
                                                              titleKey='workflow_upload'
                                                              onDrop={this.onDrop}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col xs={12}>
                                                    {basicTable}
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </PanelBody>
                                </Panel>
                            </PanelContainer>
                        </Col>
                    </Row>
                </Grid>
            </Container>
        );
    }
});

module.exports = SettingList;
