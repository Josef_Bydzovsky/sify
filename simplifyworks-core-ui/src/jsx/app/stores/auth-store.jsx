var constants = require('../actions/auth-constants.jsx');
var Fluxxor = require('fluxxor');

var AuthStore = Fluxxor.createStore({

    initialize() {
        this._initAjax();
        this.bindActions(
            constants.AUTH_LOGIN, this.onLogin,
            constants.AUTH_LOGOUT, this.onLogout,
            constants.AUTH_CHANGE_ROLES, this.reloadRoles
        );
    },

    getState() {
        return {
            currentUser: this.getCurrentUser(),
            isAuthenticated: this.isAuthenticated()
        };
    },

    _initAjax() {
        var headers = {
            //VS: Commented because is override header Content-Type for upload file (boundary part)
           // 'Accept': 'application/json',
           // 'Content-Type': 'application/json'
        };
        if(this.getCurrentToken() != null) {
            headers.Authorization = 'Bearer ' + this.getCurrentToken();
        }
        // TODO: global setting for whole app - where is the best place for this?
        $.ajaxSetup({
            dataType: 'json',
            headers: headers
            //timeout: 10000
        });
    },

    onLogin(payload) {
        $.when(
            // login
            $.post('/oauth/token?password=' + payload.password + '&username=' + payload.username + '&grant_type=password&scope=write read')
        ).done(
            // read user information
            function (tokenInfo) {
                //alert(JSON.stringify(tokenInfo));
                $.when(
                    $.ajax({
                        url: '/api/core/users/profile',
                        type: "GET",
                        headers: {
                            'Authorization': 'Bearer ' + tokenInfo.access_token
                        }
                    })
                ).done(
                    function (userInfoResponse) {
                        // set authorization for all ajax requests
                        // store user data
                        var userInfo = userInfoResponse.resource;
                        userInfo.tokenInfo = tokenInfo;
                        // roles
                        for (var i in userInfoResponse.resource.roles) {
                            userInfo.roles[i] = userInfo.roles[i].role.name;
                        }
                        //alert(JSON.stringify(userInfo));
                        sessionStorage.setItem('currentUser', JSON.stringify(userInfo));
                        this._initAjax();
                        this.emit("change");
                        if(payload.successCallback !== undefined) {
                            payload.successCallback(userInfo);
                        }
                    }.bind(this)
                ).fail(
                    function (xhr, status, error) {
                        console.error('read userinfo failed', status, JSON.stringify(xhr));
                        if(payload.failedCallback !== undefined) {
                            payload.failedCallback(xhr, status, error);
                        } else {
                            this.flux.store("FlashStore").ajaxError({xhr: xhr, status:status, error: error});
                        }
                    }.bind(this)
                );
            }.bind(this)
        ).fail(
            function (xhr, status, error) {
                console.error('login failed', status, JSON.stringify(xhr));
                if(payload.failedCallback !== undefined) {
                    payload.failedCallback(xhr, status, error);
                } else {
                    this.flux.store("FlashStore").ajaxError({xhr: xhr, status:status, error: error});
                }
            }.bind(this)
        );
    },

    onLogout(payload) {
        $.ajax({
            headers: {
                'Accept': 'plain/text',
                'Content-Type': 'application/json'
            },
            url: '/oauth/token/revoke?token=' + this.getCurrentToken(),
            type: 'POST',
            dataType: 'text',
            success: function (data) {
                sessionStorage.removeItem("currentUser");
                this.flux.store("FlashStore").clear();
                this.flux.store("FlashStore").add({
                    type: 'success',
                    message: l20n.ctx.getSync('logout_success')
                });
                this.emit("change");
                if(payload.successCallback !== undefined) {
                    payload.successCallback();
                }
            }.bind(this)
        });
    },

    reloadRoles(successCallback, failedCallback) {
        // TODO: DRY - onLogin
        var currentUser = this.getCurrentUser();
        if(currentUser == null) {
            return;
        }
        $.ajax({
        	url: '/api/core/users/profile',
            type: "GET",
            success: function (json) {
                currentUser.roles = [];
                for (var i in json.resource.roles) {
                    currentUser.roles.push(json.resource.roles[i].role.name);
                }
                sessionStorage.setItem('currentUser', JSON.stringify(currentUser));
                if(successCallback !== undefined) {
                    successCallback(currentUser);
                }
                // alert("load: " + JSON.stringify(currentUser));
                this.emit("change");
            }.bind(this),
            error: function (xhr, status, error) {
                console.error('read user roles failed', status, JSON.stringify(xhr));
                if(failedCallback !== undefined) {
                    failedCallback(xhr, status, error);
                } else {
                    this.flux.store("FlashStore").ajaxError({xhr: xhr, status:status, error: error});
                }
            }.bind(this)
        });
    },

    isAuthenticated() {
        return this.getCurrentUser() != null;
    },

    hasRole(role) {
        var userInfo = this.getCurrentUser();
        if(userInfo == null) {
            return false;
        }
        return $.inArray(role, userInfo.roles) > -1;
    },

    getCurrentUser() {
        if(sessionStorage.getItem('currentUser') == null) {
            return null;
        }
        try {
            return JSON.parse(sessionStorage.getItem('currentUser'));
        } catch(ex) {
            // TODO: exception handling and show message
            console.error('read user profile failed', ex);
            return null;
        }
    },

    getCurrentToken() {
        var userInfo = this.getCurrentUser();
        if(userInfo == null) {
            return null;
        }
        return userInfo.tokenInfo.access_token;
    }
});

module.exports = AuthStore;
