var constants = require('../actions/flash-constants.jsx');
var Fluxxor = require('fluxxor');

var FlashStore = Fluxxor.createStore({

    initialize() {
        this.flashMessages = [];
        this.bindActions(
            constants.FLASH_TOAST, this.toast,
            constants.FLASH_ADD, this.add,
            constants.FLASH_CLEAR, this.clear,
            constants.FLASH_AJAX_ERROR, this.ajaxError
        );
    },

    getState() {
        return {
            flashMessages: this.flashMessages
        };
    },


    add(options) {
        //
        options = this._options(options);
        var container = options.container;
        // TODO: named array
        if(container !== undefined && container != null) {
            var messageId = options.messageId;
            var inserted = false;
            if(messageId !== undefined && messageId != null) {
                for(i = 0; i < this.flashMessages.length; i++) {
                    if(this.flashMessages[i].container == container && this.flashMessages[i].messageId == messageId) {
                        this.flashMessages[i] = options;
                        inserted = true;
                        break;
                    }
                }
            }
            if(!inserted) {
                this.flashMessages.push(options);
            }
            this.emit("change");
            return;
        }

        var actions = [];
        if(options.showHelpdeskButton) {
            actions.helpdesk = {
                label: 'Helpdesk',
                title: 'Send to helpdesk',
                action: function() {
                    vex.dialog.alert('Not implemented');
                }
            }
        };

        Messenger({
            extraClasses: 'messenger-fixed messenger-on-top'
        }).post({
            id: options.messageId,
            hideAfter: 30,
            hideOnNavigate: true,
            type: options.type,
            singleton: false,
            message: React.renderToString(<span>{options.message}</span>),
            showCloseButton: true,
            actions: actions
        });
    },

    remove(container, messageId) {
        for(i = 0; i < this.flashMessages.length; i++) {
            if(this.flashMessages[i].container !== undefined && this.flashMessages[i].container == container
                && this.flashMessages[i].messageId !== undefined && this.flashMessages[i].messageId == messageId) {
                this.flashMessages.splice(i, 1);
                this.emit("change");
                break;
            }
        }
    },

    toast(options) {
        options = this._options(options);
        // todo: options
        Messenger().post({
            id: options.messageId,
            type: options.type,
            singleton: false,
            message: React.renderToString(<span>{options.message}</span>),
            showCloseButton: true
        });
    },

    clear(container) {
        if(container === undefined || container == null) {
            Messenger().hideAll();
        } else {
            var messages = [];
            for(i = 0; i < this.flashMessages.length; i++) {
                if(this.flashMessages[i].container === undefined || this.flashMessages[i].container != container) {
                    messages.push(this.flashMessages[i]);
                }
            }
            this.flashMessages = messages;
        }
        this.emit("change");
    },

    ajaxError(payload) {
        var xhr = payload.xhr;
        var status = payload.status;
        var error = payload.error;
        var options = this._options(payload.options);

        var defaultMessage = {
            container: options.container,
            type: 'error',
            message:
                (<div>
                    <div>{l20n.ctx.getSync('error', {message: 'uncaught', status: (xhr !== undefined ? xhr.status : '')})} {xhr !== undefined && xhr.responseText !== undefined && xhr.responseText != '' ? ':' : ''}</div>
                    <div>
                        {xhr !== undefined && xhr.responseText !== undefined ? xhr.responseText.substring(0,2000) : ''}
                        {xhr !== undefined && xhr.responseText !== undefined && xhr.responseText.length > 2000 ? '...':''}
                    </div>
                </div>)
        };
        try {
            // parse error code
            var errors = [];
            if(status == 'timeout') {
                errors.push({error: status});
            } else {
                var jsonResponse = JSON.parse(xhr.responseText);
                if (jsonResponse.error !== undefined) { // spring error
                    errors.push(jsonResponse);
                } else { // vnd errors
                    for(var k in jsonResponse) {
                        if(jsonResponse[k].logref !== undefined) {
                            errors.push(jsonResponse[k]);
                        }
                    }
                }
            }
            // show localized error
            if(errors.length == 0) {
                this.add(defaultMessage);
            } else {
                errors.forEach(function(error){
                    var errorCode = null;
                    if (error.error !== undefined) {
                        errorCode = error.error;
                    } else {
                        errorCode = error.logref;
                    }
                    var message = [];
                    var messageDetails = {};
                    if(error.details != undefined) {
                        messageDetails = error.details;
                    }
                    messageDetails.message = errorCode;
                    var text = l20n.ctx.getSync('error', messageDetails);
                    if (text == errorCode || text == 'error') {
                        message.push(<div>{errorCode}</div>);
                        if (error.error_description !== undefined) {
                            message.push(<div>{error.error_description}</div>);
                        }
                        if (error.message !== undefined) {
                            message.push(<div>{error.message}</div>);
                        }
                    } else {
                        message = text;
                    }
                    this.add(
                        {
                            container: options.container,
                            type: 'error',
                            message: (<div>{message}</div>)
                        }
                    );
                }.bind(this));
            }
        } catch(ex) {
            // if json parsing failed - show default message
            this.add(defaultMessage);
        }
    },

    _options(options) {
        if(options === undefined) {
            options = [];
        }
        options.type = options.type == null ? 'success' : options.type;
        options.message = (<span>{options.message == null ? 'Ok' : options.message}</span>);
        options.showHelpdeskButton = options.showHelpdeskButton === null ? false : options.showHelpdeskButton;
        return options;
    }
});

module.exports = FlashStore;
