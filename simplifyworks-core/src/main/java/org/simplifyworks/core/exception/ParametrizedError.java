/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.exception;

import java.util.Map;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.VndErrors;

/**
 * Add parameters to standard VndError
 * 
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public class ParametrizedError extends VndErrors.VndError {

	private Map<String, Object> details;
	
	public ParametrizedError(String logref, String message) {
		super(logref, message);
	}
	
	public ParametrizedError(String logref, String message, Link... links) {
		super(logref, message, links);
	}

	public ParametrizedError(String logref, String message, Map<String, Object> details, Link... links) {
		super(logref, message, links);
		this.details = details;
	}

	public Map<String, Object> getDetails() {
		return details;
	}	
	
}
