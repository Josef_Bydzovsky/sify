/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.domain;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.model.domain.filteroperator.EqualsFilterOperator;
import org.simplifyworks.core.model.domain.filteroperator.GreaterEqualsFilterOperator;
import org.simplifyworks.core.model.domain.filteroperator.GreaterFilterOperator;
import org.simplifyworks.core.model.domain.filteroperator.IntervalFilterOperator;
import org.simplifyworks.core.model.domain.filteroperator.IsNotNullFilterOperator;
import org.simplifyworks.core.model.domain.filteroperator.IsNullFilterOperator;
import org.simplifyworks.core.model.domain.filteroperator.LessEqualsFilterOperator;
import org.simplifyworks.core.model.domain.filteroperator.LessFilterOperator;
import org.simplifyworks.core.model.domain.filteroperator.LikeFilterOperator;
import org.simplifyworks.core.model.domain.filteroperator.LikeMaskFilterOperator;
import org.simplifyworks.core.model.domain.filteroperator.NotEqualsFilterOperator;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.service.ElasticsearchService;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.collect.Lists;

/**
 * http://www.ascii-code.com/html-symbol.php
 *
 * @author idea: hanak, edited by siroky
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes({
	@JsonSubTypes.Type(value = EqualsFilterOperator.class, name = EqualsFilterOperator.NAME),
	@JsonSubTypes.Type(value = NotEqualsFilterOperator.class, name = NotEqualsFilterOperator.NAME),
	@JsonSubTypes.Type(value = GreaterEqualsFilterOperator.class, name = GreaterEqualsFilterOperator.NAME),
	@JsonSubTypes.Type(value = GreaterFilterOperator.class, name = GreaterFilterOperator.NAME),
	@JsonSubTypes.Type(value = IntervalFilterOperator.class, name = IntervalFilterOperator.NAME),
	@JsonSubTypes.Type(value = IsNotNullFilterOperator.class, name = IsNotNullFilterOperator.NAME),
	@JsonSubTypes.Type(value = IsNullFilterOperator.class, name = IsNullFilterOperator.NAME),
	@JsonSubTypes.Type(value = LessEqualsFilterOperator.class, name = LessEqualsFilterOperator.NAME),
	@JsonSubTypes.Type(value = LessFilterOperator.class, name = LessFilterOperator.NAME),
	@JsonSubTypes.Type(value = LikeFilterOperator.class, name = LikeFilterOperator.NAME),
	@JsonSubTypes.Type(value = LikeMaskFilterOperator.class, name = LikeMaskFilterOperator.NAME)})
public abstract class FilterOperator implements Serializable {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(FilterOperator.class);

	public static final FilterOperator EQUALS = new EqualsFilterOperator();
	public static final FilterOperator NOT_EQUALS = new NotEqualsFilterOperator();
	public static final FilterOperator GREATER_EQUALS = new GreaterEqualsFilterOperator();
	public static final FilterOperator GREATER = new GreaterFilterOperator();
	public static final FilterOperator INTERVAL = new IntervalFilterOperator();
	public static final FilterOperator IS_NOT_NULL = new IsNotNullFilterOperator();
	public static final FilterOperator IS_NULL = new IsNullFilterOperator();
	public static final FilterOperator LESS_EQUALS = new LessEqualsFilterOperator();
	public static final FilterOperator LESS = new LessFilterOperator();
	public static final FilterOperator LIKE = new LikeFilterOperator();
	public static final FilterOperator LIKE_MASK = new LikeMaskFilterOperator();
	//add new value to valueOf and json annotation too

	/**
	 * @return the label
	 */
	@JsonIgnore
	public abstract String getLabel();

	@JsonIgnore
	public List<Object> getFilledValues(List<Object> values) {
		List<Object> filledValues = Lists.newArrayList();
		for (Object value : values) {
			if (value != null && !StringUtils.isEmpty(value.toString())) {
				filledValues.add(value);
			}
		}

		return filledValues;
	}

	public abstract Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager);

	/**
	 * Filter part construction
	 *
	 * TODO: data type recognition is just prepared, have to be implemented
	 *
	 * @param dtoClass dtoClass for data type recognition
	 * @param filterValue filled value / values
	 * @return
	 */
	public abstract FilterBuilder createFilter(Class<? extends AbstractDto> dtoClass, FilterValue filterValue);

	@JsonIgnore
	public boolean isEmpty(List<Object> values) {
		return getFilledValues(values).isEmpty();
	}
	
	public String toElasticsearchDate(Object value) {
		if(value instanceof Date) {
			return ElasticsearchService.DATE_TIME_FORMAT.format(value);
		} else if (value instanceof Long) {
			return ElasticsearchService.DATE_TIME_FORMAT.format(new Date((Long) value));
		} else if (value instanceof String) {
			return ((String) value).isEmpty() ? null : (String) value;
		} else {
			return null;
		}			
	}

	public static FilterOperator valueOf(String value) {
		switch (value.toUpperCase()) {
			case EqualsFilterOperator.NAME:
				return EQUALS;
			case NotEqualsFilterOperator.NAME:
				return NOT_EQUALS;
			case GreaterEqualsFilterOperator.NAME:
				return GREATER_EQUALS;
			case GreaterFilterOperator.NAME:
				return GREATER;
			case IntervalFilterOperator.NAME:
				return INTERVAL;
			case IsNotNullFilterOperator.NAME:
				return IS_NOT_NULL;
			case IsNullFilterOperator.NAME:
				return IS_NULL;
			case LessEqualsFilterOperator.NAME:
				return LESS_EQUALS;
			case LessFilterOperator.NAME:
				return LESS;
			case LikeFilterOperator.NAME:
				return LIKE;
			case LikeMaskFilterOperator.NAME:
				return LIKE_MASK;
		}

		return EQUALS;
	}

	protected FilterBuilder computeNestedFilters(Class<? extends AbstractDto> dtoClass, FilterValue filterValue, FilterBuilder filterPart) {
		try {
			Class clazz = dtoClass;
			StringBuilder path = new StringBuilder();
			List<String> nestedFilterPaths = new ArrayList<>();
			for (String property : filterValue.getPropertyName().split("\\.")) {
				if (path.length() > 0) {
					path.append(".");
				}
				path.append(property);

				PropertyDescriptor pd = new PropertyDescriptor(property, clazz);
				clazz = pd.getReadMethod().getReturnType();
				if (Collection.class.isAssignableFrom(clazz)) {
					ParameterizedType genericSuperclass = (ParameterizedType) pd.getReadMethod().getGenericReturnType();
					clazz = (Class) genericSuperclass.getActualTypeArguments()[0];

					nestedFilterPaths.add(0, path.toString());
				}
			}
			for (String nestedFilterPath : nestedFilterPaths) {
				filterPart = FilterBuilders.nestedFilter(nestedFilterPath, filterPart);
			}
		} catch (IntrospectionException ex) {
			LOG.error("", ex);
		}
		return filterPart;
	}
}
