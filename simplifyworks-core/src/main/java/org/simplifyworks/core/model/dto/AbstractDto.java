/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.simplifyworks.core.service.PermissionElement;

/**
 * Base dto mapping
 * 
 * TODO: Serializable interface
 * 
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public abstract class AbstractDto implements Serializable {

	private static final long serialVersionUID = -5187898779275725184L;
	
	private Long id;	
	private Date created;
	private Date modified;
	private String creator;
	private String modifier;
	private boolean locked = false;
	private int version;
	private List<PermissionElement> permissions;

	public AbstractDto() {
	}

	public AbstractDto(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}
	
	public List<PermissionElement> getPermissions() {
		if(permissions == null) {
			permissions = new ArrayList<>();
		}
		
		return permissions;
	}
	
	public void setPermissions(List<PermissionElement> permissions) {
		this.permissions = permissions;
	}

	@Override
	public String toString() {
		return getClass().getCanonicalName() + "[ id=" + getId() + " ]";
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (getId() != null ? getId().hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null || !object.getClass().equals(getClass())) {
			return false;
		}

		AbstractDto other = (AbstractDto) object;
		if ((this.getId() == null && other.getId() != null)
						|| (this.getId() != null && !this.getId().equals(other.getId()))
						|| (this.getId() == null && other.getId() == null && this != other)) {
			return false;
		}
		return true;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
