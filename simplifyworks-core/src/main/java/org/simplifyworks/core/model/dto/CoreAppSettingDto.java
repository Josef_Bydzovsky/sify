package org.simplifyworks.core.model.dto;

import org.simplifyworks.uam.model.annotation.Permission;

/**
 * Application setting
 * 
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Permission(createRoles = "admin", deleteRoles = "admin", readRoles = "admin", writeRoles = "admin")
public class CoreAppSettingDto extends AbstractDto {

	private String settingKey;
	private String value;
	private boolean system;

	public CoreAppSettingDto() {
	}

	public CoreAppSettingDto(Long id) {
		super(id);
	}

	public String getSettingKey() {
		return settingKey;
	}

	public void setSettingKey(String settingKey) {
		this.settingKey = settingKey;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isSystem() {
		return system;
	}

	public void setSystem(boolean system) {
		this.system = system;
	}
}
