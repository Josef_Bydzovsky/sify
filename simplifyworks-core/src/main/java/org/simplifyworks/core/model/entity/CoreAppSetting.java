/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Application seting key / value
 * 
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Entity
public class CoreAppSetting extends AbstractEntity {

	@NotNull
	@Column(name = "SETTING_KEY", nullable = false)
	private String settingKey;
	@Column(name = "VALUE")
	private String value;
	@NotNull
	@Column(name = "SYSTEM", nullable = false)
	private boolean system;

	public CoreAppSetting() {
	}

	public CoreAppSetting(String settingKey, String value) {
		this.settingKey = settingKey;
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	public boolean getSystem() {
		return system;
	}

	public void setSystem(boolean system) {
		this.system = system;
	}

	public boolean isSystem() {
		return system;
	}

	@Override
	public boolean isLocked() {
		return isSystem(); // system parameter can't be changed neither edited
	}

	public String getSettingKey() {
		return settingKey;
	}

	public void setSettingKey(String settingKey) {
		this.settingKey = settingKey;
	}
}
