/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.jpa;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.AbstractPreDatabaseOperationEvent;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreInsertEventListener;
import org.hibernate.event.spi.PreUpdateEvent;
import org.hibernate.event.spi.PreUpdateEventListener;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.simplifyworks.core.model.entity.AuditInfo;
import org.simplifyworks.security.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Plnic audit-info atributu
 * 
 * POZOR: V kombinaci s jRebel nefunguje
 *
 * info:
 * http://anshuiitk.blogspot.cz/2010/11/hibernate-pre-database-opertaion-event.html
 * http://stackoverflow.com/questions/10602342/how-do-i-set-up-an-event-listener-on-a-hibernate-4-entity
 * http://stackoverflow.com/questions/8616146/eventlisteners-using-hibernate-4-0-with-spring-3-1-0-release
 * jinak (nefungovalo mi):
 * http://docs.jboss.org/hibernate/orm/4.1/devguide/en-US/html_single/#integrators
 *
 *
 * @author hanak
 */
@Component
public class AuditInfoCharger implements PreInsertEventListener, PreUpdateEventListener {

	static final Logger logger = LoggerFactory.getLogger(AuditInfoCharger.class);
	
	@Autowired
	private HibernateEntityManagerFactory entityManagerFactory;

	@Autowired
	private SecurityService securityService;

	@Override
	public boolean onPreInsert(PreInsertEvent pie) {
		if (pie.getEntity() instanceof AuditInfo) {
			Date date = new Date();
			AuditInfo entity = (AuditInfo) pie.getEntity();
			String username = securityService.getOriginalUsername();
			//
			setValue(pie.getState(), pie, "created", date);
			entity.setCreated(date);
			//
			if (entity.getCreator() == null) {
				setValue(pie.getState(), pie, "creator", username);
				entity.setCreator(username);
			}
		}
		return false;
	}

	@Override
	public boolean onPreUpdate(PreUpdateEvent pue) {
		if (pue.getEntity() instanceof AuditInfo) {
			Date date = new Date();
			AuditInfo entity = (AuditInfo) pue.getEntity();
			String username = securityService.getOriginalUsername();
			setValue(pue.getState(), pue, "modified", date);
			entity.setModified(date);
			//
			setValue(pue.getState(), pue, "modifier", username);
			entity.setModifier(username);
		}
		return false;
	}

	/**
	 * nastaveni hodnoty atributu v poli properties, coz jsou v podstate
	 * parametry pro ins/upd SQL
	 *
	 * @param currentState
	 * @param event
	 * @param propertyToSet
	 * @param value
	 */
	private void setValue(Object[] currentState, AbstractPreDatabaseOperationEvent event, String propertyToSet, Object value) {
		String[] propertyNames = event.getPersister().getPropertyNames();
		int index = ArrayUtils.indexOf(propertyNames, propertyToSet);
		if (index >= 0) {
			currentState[index] = value;
		} else {
			logger.error("Field '" + propertyToSet + "' not found on entity '" + event.getEntity().getClass() + "'.");
		}
	}

	/**
	 * zaregistrovani event-listeneru do hibernate
	 */
	@PostConstruct
	public void register() {
		logger.debug("wire event listener");
		SessionFactoryImpl sessionFactoryImpl = (SessionFactoryImpl) entityManagerFactory.getSessionFactory();
		EventListenerRegistry registry = sessionFactoryImpl.getServiceRegistry().getService(EventListenerRegistry.class);
		registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(this);
		registry.getEventListenerGroup(EventType.PRE_UPDATE).appendListener(this);
	}
}
