package org.simplifyworks.core.service;

/**
 * Enumeration for specification type of access for some object
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public enum AccessType {
	
	/**
	 * Object is needed for reading
	 */
	READ,
	
	/**
	 * Object is needed for updating
	 */
	UPDATE,
	
	/**
	 * Object is needed for deletion
	 */
	DELETE;
		
	// TODO refactor this method
	public String getPropertyName() {
		switch(this) {
			case READ:
				return "permissions.canRead";
			case UPDATE:
				return "permissions.canUpdate";
			case DELETE:
				return "permissions.canDelete";
			default:
				throw new UnsupportedOperationException("Unknown type " + this);
		}
	}
}
