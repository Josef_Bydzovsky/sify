/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.service;

import org.simplifyworks.core.model.dto.CoreAppSettingDto;
import org.simplifyworks.core.model.entity.CoreAppSetting;

/**
 * Application setting
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface CoreAppSettingManager extends ReadWriteManager<CoreAppSettingDto, CoreAppSetting> {
    String getValueByKey(String key);
}
