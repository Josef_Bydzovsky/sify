/**
 * 
 */
package org.simplifyworks.core.service;

/**
 * Service for administration tasks with ES index
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface ElasticsearchAdminService {
	
	/**
	 * Refreshes all invalid mappings and related records. 
	 * 
	 * This operation may be VERY time consuming (depending on data).
	 */
	public void refreshAll();
	
	/**
	 * Rebuilds all records (whole index).
	 * 
	 * This operation may be VERY time consuming (depending on data).
	 */
	public void reindexAll();
}
