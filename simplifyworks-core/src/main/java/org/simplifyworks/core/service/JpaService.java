/**
 * 
 */
package org.simplifyworks.core.service;

import java.util.List;

import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 * JPA service for common purposes
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface JpaService {
	
	/**
	 * Creates entity (persists it)
	 * 
	 * @param entity entity to persist
	 * @return persisted entity
	 */
	public <E extends AbstractEntity> E create(E entity);
	
	/**
	 * Updates entity
	 * 
	 * @param entity entity to update
	 * @return updated entity
	 */
	public <E extends AbstractEntity> E update(E entity);
	
	/**
	 * Removes entity if exists
	 * 
	 * @param type type of removed entity
	 * @param id id of removed entity
	 * @return removed entity or null if does not exist
	 */
	public <E extends AbstractEntity> E remove(Class<E> type, long id);
	
	/**
	 * Returns all entities for type
	 * 
	 * @param type type of entities
	 * @return all entities for type
	 */
	public <E extends AbstractEntity> List<E> findAll(Class<E> type);
}
