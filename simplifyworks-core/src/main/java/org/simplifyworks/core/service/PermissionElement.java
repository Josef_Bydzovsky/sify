package org.simplifyworks.core.service;

import java.text.MessageFormat;
import java.util.Objects;

/**
 * Elementary permission for indexed record (permissions are created accordingly to role and organization hierarchy).
 * 
 * There are 3 types basically:
 * <ul>
 * 	<li>user related (only userId filled),</li>
 *  <li>role related (only roleId filled),</li>
 *  <li>role in organization related (only roleId and organizationId filled).</li>
 * </ul>
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class PermissionElement {

	private String userName;	
	private String roleName; 
	private String organizationName;
	
	private boolean canRead;
	private boolean canUpdate;
	private boolean canRemove;	
	
	public PermissionElement() {
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	public String getOrganizationName() {
		return organizationName;
	}
	
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public boolean isCanRead() {
		return canRead;
	}
	
	public void setCanRead(boolean canRead) {
		this.canRead = canRead;
	}
	
	public boolean isCanUpdate() {
		return canUpdate;
	}
	
	public void setCanUpdate(boolean canUpdate) {
		this.canUpdate = canUpdate;
	}
	
	public boolean isCanRemove() {
		return canRemove;
	}
	
	public void setCanRemove(boolean canRemove) {
		this.canRemove = canRemove;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PermissionElement) {
			PermissionElement that = (PermissionElement) obj;
									
			return Objects.equals(this.userName, that.userName)
					&& Objects.equals(this.roleName, that.roleName)
					&& Objects.equals(this.organizationName, that.organizationName);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(userName, roleName, organizationName);
	}
	
	@Override
	public String toString() {
		return MessageFormat.format("PermissionElement (userName={0}, roleName={1}, organizationName={2}, canRead={3}, canUpdate={4}, canRemove={5})",
				userName, roleName, organizationName, canRead, canUpdate, canRemove);
	}
}
