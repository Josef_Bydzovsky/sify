package org.simplifyworks.core.service;

import java.util.List;

/**
 * Factory that creates elementary permissions for indexed records using role and organization structure.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface PermissionElementFactory {

	/**
	 * Creates permission elements for specified object
	 * 
	 * @param objectType type of object
	 * @param objectIdentifier identifier of object
	 * @return set of permission elements (permissions are evaluated using role and organization structure)
	 */
	public List<PermissionElement> createPermissionElements(Class<?> objectType, long objectIdentifier);
}
