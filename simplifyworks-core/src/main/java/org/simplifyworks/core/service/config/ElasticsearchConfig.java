package org.simplifyworks.core.service.config;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.simplifyworks.core.exception.CoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;


/**
 * Elasticsearch configuration
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Configuration
public class ElasticsearchConfig {

        @Autowired
	private Environment env;

	private static final Logger LOG = LoggerFactory.getLogger(ElasticsearchConfig.class);
	@Value("${spring.data.elasticsearch.cluster-nodes:}")
	private String localNodes;
        private String envNodes;

	@Value("${spring.data.elasticsearch.localPathData:}")
	private String localPathData;

	@Value("${spring.data.elasticsearch.localPathLogs:}")
	private String localPathLogs;

	@Value("${spring.data.elasticsearch.localShards:}")
        private String localConfigShards;
	private byte localShards;

        @Value("${spring.data.elasticsearch.localReplicas:}")
	private String localConfigReplicas;
        private byte localReplicas;
        
        private byte getShards(String aNumericValue) {
            try {
                return(StringUtils.isBlank(aNumericValue) ? (Byte.valueOf(aNumericValue)) : (1));
            } catch (NumberFormatException e) {
                LOG.warn("Number of local shards should be ... a number. Significantly small. Positive.");
                return(1);
            }
        }
        
        private byte getReplicas(String aNumericValue) {
            try {
                return(StringUtils.isBlank(aNumericValue) ? (Byte.valueOf(aNumericValue)) : (1));
            } catch (NumberFormatException e) {
                LOG.warn("Number of local replicas should be ... a number. Significantly small. Positive.");
                return(1);
            }
        }

	@Bean
	public Client client() {
		Client client;
                if (StringUtils.isBlank(localNodes)) {
                    envNodes = env.getProperty("elasticsearch.nodes");
                    if (! StringUtils.isBlank(envNodes)) {
                        localNodes = envNodes;
                    }
                }
                
		if (StringUtils.isBlank(localNodes)) {
			//  embedded client
			LOG.warn("Starting embedded elesticsearch server, please configure ElasticSearch server before production usage [parameter: spring.data.elasticsearch.cluster-nodes]");
                        if (StringUtils.isBlank(localPathData)) {
                            localPathData = (! StringUtils.isBlank(env.getProperty("elasticsearch.path"))) ? (env.getProperty("elasticsearch.path")) : ("/tmp/localES/data");
                        }
                        if (StringUtils.isBlank(localPathLogs)) {
                            localPathLogs = (! StringUtils.isBlank(env.getProperty("elasticsearch.logs"))) ? (env.getProperty("elasticsearch.logs")) : ("/tmp/localES/logs");
                        }
                        localShards = (StringUtils.isBlank(localConfigShards)) ? (getShards(env.getProperty("elasticsearch.shards"))) : (getShards(localConfigShards));
                        localReplicas = (StringUtils.isBlank(localConfigReplicas)) ? (getReplicas(env.getProperty("elasticsearch.replicas"))) : (getReplicas(localConfigReplicas));

                        // create settings for the local instance and get client
			client = NodeBuilder.nodeBuilder().clusterName("SimplifyworksES")
                                .settings(ImmutableSettings.settingsBuilder()
                                    .put("path.logs", localPathLogs)
                                    .put("path.data", localPathData)
                                    .put("index.number_of_shards", localShards)
                                    .put("index.number_of_replicas", localReplicas)
                                    .build())
                                .local(true).node().client();
		} else {
			try {
                                Settings settings = (ImmutableSettings.settingsBuilder()
                                        .put("client.transport.ignore_cluster_name", true)
                                        .put("client.transport.sniff", true)
                                        .build());
				client = new TransportClient(settings);
				for (String server : localNodes.split(",")) {
					String[] serverPort = server.trim().split(":");
					((TransportClient) client).addTransportAddress(new InetSocketTransportAddress(serverPort[0], Integer.valueOf(serverPort[1])));
				}
			} catch (ElasticsearchException | NumberFormatException | IndexOutOfBoundsException ex) {
				throw new CoreException("ElasticSearch nodes [" + localNodes + "] are not properly configured (use syntax: server1:port,server2:port). Please fix the configuration file and restart the application.", ex);
			}
		}
		return client;
	}

	@Bean
	public ElasticsearchTemplate elasticsearchTemplate() {
		return new ElasticsearchTemplate(client());
	}
}
