/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.service.impl;

import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.dto.CoreAppSettingDto;
import org.simplifyworks.core.model.entity.CoreAppSetting;
import org.simplifyworks.core.model.entity.CoreAppSetting_;
import org.simplifyworks.core.service.CoreAppSettingManager;
import org.springframework.stereotype.Service;

import java.util.List;
import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.service.EntityEvent;

/**
 * Nastaveni aplikace
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CoreAppSettingManagerImpl extends DefaultReadWriteManager<CoreAppSettingDto, CoreAppSetting> implements CoreAppSettingManager {

	public void onCoreAppSettingEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CoreAppSetting_.id.getName(), event.getEntity().getId()));
	}

	@Override
	public String getValueByKey(String key) {
		SearchParameters searchParameters = new SearchParameters();
		FilterValue filterValue = new FilterValue();
		filterValue.setPropertyName(CoreAppSetting_.settingKey.getName());
		filterValue.setValue(key);
		filterValue.setOperator(FilterOperator.EQUALS);
		searchParameters.addFilter(filterValue);

		List<CoreAppSettingDto> search = search(searchParameters);
		if (search == null || search.isEmpty()) {
			return null;
		}
		if (search.size() > 1) {
			throw new CoreException("To many items found for key " + key + "!");
		}
		return search.get(0).getValue();

	}

}
