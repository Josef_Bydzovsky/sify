/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.service.impl;

import java.io.IOException;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.service.ElasticsearchAdminService;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.core.service.MappingFactory;
import org.simplifyworks.core.service.ReadManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Default implementation of {@link ElasticsearchAdminService}
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultElasticsearchAdminService implements ElasticsearchAdminService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultElasticsearchAdminService.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ElasticsearchTemplate template;

    @Autowired
    private MappingFactory mappingFactory;

    private ObjectMapper jsonMapper;

    public DefaultElasticsearchAdminService() {
        jsonMapper = new ObjectMapper();
        jsonMapper.setDateFormat(ElasticsearchService.DATE_TIME_FORMAT);
    }

    @PostConstruct
    @SuppressWarnings("rawtypes")
    public void refreshAll() {
        Collection<ReadManager> managers = applicationContext.getBeansOfType(ReadManager.class).values();
        if (LOG.isInfoEnabled()) {
            LOG.info("Initialize index for " + managers.size() + " manager(s).");
        }

        if (!template.indexExists(ElasticsearchService.INDEX_NAME)) {
            template.createIndex(ElasticsearchService.INDEX_NAME);
        }

        for (ReadManager manager : managers) {
            Class<? extends AbstractDto> type = manager.getDtoClass();
            String typeName = type.getSimpleName();

            try {
                XContentBuilder oldMapping = getOldMapping(typeName);
                XContentBuilder newMapping = mappingFactory.createMapping(type);

                // JSON tree comparison must be used
                // because of missing equals implementation in XContentBuilder
                if (oldMapping == null || !jsonMapper.readTree(newMapping.string()).equals(jsonMapper.readTree(oldMapping.string()))) {
                    template.deleteType(ElasticsearchService.INDEX_NAME, typeName);
                    template.putMapping(ElasticsearchService.INDEX_NAME, typeName, newMapping);

                    manager.reindexAll();

                    if (LOG.isInfoEnabled()) {
                        LOG.info("Mapping created/replaced for " + typeName);
                    }
                } else if (LOG.isInfoEnabled()) {
                    LOG.info("Mapping unchanged for " + typeName);
                }
            } catch (Throwable ex) {
                LOG.error(typeName + " is mismatched, mapping refresh skipped", ex);
            }
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void reindexAll() {
        Collection<ReadManager> managers = applicationContext.getBeansOfType(ReadManager.class).values();
        if (LOG.isInfoEnabled()) {
            LOG.info("Reindexing all [managers:" + managers.size() + "].");
        }
        for (ReadManager manager : managers) {
            long reindexed = manager.reindexAll();
            if (LOG.isInfoEnabled()) {
                LOG.info("Reindexed all [" + manager.getDtoClass().getSimpleName() + ":" + reindexed + "].");
            }
        }
    }

    @SuppressWarnings("unchecked")
    private XContentBuilder getOldMapping(String typeName) throws IOException {
        if (template.typeExists(ElasticsearchService.INDEX_NAME, typeName)) {
            if (LOG.isInfoEnabled()) {
                LOG.info(typeName + " mapping exists in index");
            }

            return XContentFactory.jsonBuilder().map(template.getMapping(ElasticsearchService.INDEX_NAME, typeName));
        } else {
            if (LOG.isInfoEnabled()) {
                LOG.info(typeName + " mapping does not exist in index");
            }

            return null;
        }
    }
}
