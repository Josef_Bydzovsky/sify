/**
 * 
 */
package org.simplifyworks.core.service.impl;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.service.MappingFactory;
import org.simplifyworks.core.service.PermissionElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link MappingFactory}.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
class DefaultMappingFactory implements MappingFactory {
	
	private static final Logger LOG = LoggerFactory.getLogger(DefaultMappingFactory.class);
	
	@Override
	public XContentBuilder createMapping(Class<? extends AbstractDto> type) throws IOException {
		XContentBuilder builder = XContentFactory.jsonBuilder();
		
		builder.startObject().field("dynamic", "false").startObject("properties");
		appendMapping(builder, type, new HashSet<>(Arrays.asList(type)));
		builder.endObject().endObject();
		
		return builder;
	}
	
	private void appendMapping(XContentBuilder builder, Class<?> type, Set<Class<?>> typeStack) throws IOException {
		if(!Object.class.equals(type.getSuperclass())) {
			appendMapping(builder, type.getSuperclass(), typeStack);
		}
		
		for(Field field : type.getDeclaredFields()) {
			if(!Modifier.isStatic(field.getModifiers())) {			
				appendMapping(builder, typeStack, field);
			}
		}
	}
	
	private void appendMapping(XContentBuilder builder, Set<Class<?>> typeStack, Field field) throws IOException {
		String fieldName = field.getName();
		Class<?> fieldType = field.getType();
		Type fieldGenericType = field.getGenericType();
		
		if (boolean.class.equals(fieldType) || Boolean.class.equals(fieldType)) {
			appendBooleanMapping(builder, fieldName);
		} else if (int.class.equals(fieldType) || Integer.class.equals(fieldType)) {
			appendIntegerMapping(builder, fieldName);
		} else if (long.class.equals(fieldType) || Long.class.equals(fieldType)) {
			appendLongMapping(builder, fieldName);
		} else if (float.class.equals(fieldType) || Float.class.equals(fieldType)) {
			appendFloatMapping(builder, fieldName);
		} else if (double.class.equals(fieldType) || Double.class.equals(fieldType)) {
			appendDoubleMapping(builder, fieldName);
		} else if (String.class.equals(fieldType) || char[].class.equals(fieldType) || fieldType.isEnum() ) {
			appendStringOrEnumMapping(builder, fieldName);
		} else if (Date.class.equals(fieldType)) {
			appendDateMapping(builder, fieldName);
		} else if(BigDecimal.class.equals(fieldType) || BigInteger.class.equals(fieldType)) {
			appendBigNumberMapping(builder, fieldName);
		} else if(Collection.class.isAssignableFrom(fieldType)) {
			appendCollectionMapping(builder, typeStack, fieldName, (Class<?>) ((ParameterizedType)fieldGenericType).getActualTypeArguments()[0]);
		} else if(AbstractDto.class.isAssignableFrom(fieldType)){
			appendObjectMapping(builder, typeStack, fieldName, fieldType);
		} else if(LOG.isWarnEnabled()){
			LOG.warn("Field [" + field +"] cannot be mapped");
		}
	}
	
	private void appendBooleanMapping(XContentBuilder builder, String fieldName) throws IOException {
		builder.startObject(fieldName).field("type", "boolean").endObject();
	}
	
	private void appendIntegerMapping(XContentBuilder builder, String fieldName) throws IOException {
		builder.startObject(fieldName).field("type", "integer").endObject();
	}
	
	private void appendLongMapping(XContentBuilder builder, String fieldName) throws IOException {
		builder.startObject(fieldName).field("type", "long").endObject();
	}
	
	private void appendFloatMapping(XContentBuilder builder, String fieldName) throws IOException {
		builder.startObject(fieldName).field("type", "float").endObject();
	}
	
	private void appendDoubleMapping(XContentBuilder builder, String fieldName) throws IOException {
		builder.startObject(fieldName).field("type", "double").endObject();
	}
	
	private void appendStringOrEnumMapping(XContentBuilder builder, String fieldName) throws IOException {
		builder.startObject(fieldName).field("type", "string").endObject();
	}
	
	private void appendDateMapping(XContentBuilder builder, String fieldName) throws IOException {
		builder.startObject(fieldName).field("type", "date").field("format", "basic_date_time").endObject();
	}
	
	private void appendBigNumberMapping(XContentBuilder builder, String fieldName) throws IOException {
		builder.startObject(fieldName).field("type", "string").field("index", "not_analyzed").endObject();
	}
	
	private void appendCollectionMapping(XContentBuilder builder, Set<Class<?>> typeStack, String fieldName, Class<?> elementType) throws IOException {
		if(!typeStack.contains(elementType)) {
			typeStack.add(elementType);
			
			builder.startObject(fieldName).field("type", "nested").field("dynamic", "false").startObject("properties");					
			appendMapping(builder, elementType, typeStack);
			builder.endObject().endObject();
			
			if(!PermissionElement.class.equals(elementType)) {
				typeStack.remove(elementType);
			}
		}
	}
	
	private void appendObjectMapping(XContentBuilder builder, Set<Class<?>> typeStack, String fieldName, Class<?> fieldType) throws IOException {
		if(!typeStack.contains(fieldType)) {
			typeStack.add(fieldType);
			
			builder.startObject(fieldName).field("dynamic", "false").startObject("properties");			
			appendMapping(builder, fieldType, typeStack);
			builder.endObject().endObject();
			
			typeStack.remove(fieldType);
		}
	}
}
