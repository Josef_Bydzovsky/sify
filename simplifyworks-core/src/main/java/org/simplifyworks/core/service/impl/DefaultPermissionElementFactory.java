package org.simplifyworks.core.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.service.PermissionElement;
import org.simplifyworks.core.service.PermissionElementFactory;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.model.entity.CorePermission_;
import org.simplifyworks.uam.service.OrganizationStructureService;
import org.simplifyworks.uam.service.RoleStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link PermissionElementFactory}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
class DefaultPermissionElementFactory implements PermissionElementFactory {

	@PersistenceContext(unitName = "default")
	protected EntityManager entityManager;
	
	@Autowired
	private RoleStructureService roleStructureService;
		
	@Autowired
	private OrganizationStructureService organizationStructureService;
	
	@Override
	public List<PermissionElement> createPermissionElements(Class<?> objectType, long objectIdentifier) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CorePermission> criteria = cb.createQuery(CorePermission.class);
		Root<CorePermission> from = criteria.from(CorePermission.class);
		criteria.where(cb.equal(from.get(CorePermission_.objectIdentifier), objectIdentifier), cb.equal(from.get(CorePermission_.objectType), objectType.getName()));
		
		List<CorePermission> permissions = entityManager.createQuery(criteria).getResultList();
		Set<PermissionElement> permissionElements = new HashSet<>();
		
		// TODO remove
		addAdminPermissionElement(permissionElements);
		
		// iterate through object related permissions and create appropriate elementary permissions for indexed record
		for(CorePermission permission : permissions) {
			addPermissionElements(permissionElements, permission);
		}
		
		return new ArrayList<>(permissionElements);
	}
	
	private void addAdminPermissionElement(Set<PermissionElement> permissionElements) {
		PermissionElement element = new PermissionElement();
		
		element.setRoleName("admin");
		
		element.setCanRead(true);
		element.setCanUpdate(true);
		element.setCanRemove(true);
		
		permissionElements.add(element);
	}
	
	/**
	 * Adds elementary permissions according to permission
	 * 
	 * @param permissionElements target set for elementary permissions
	 * @param permission permission
	 */
	private void addPermissionElements(Set<PermissionElement> permissionElements, CorePermission permission) {		
		if(permission.getUser() != null) { // user permission
			addUserPermissionElements(permissionElements, permission);
		} else if(permission.getRole() != null) {			
			if(permission.getOrganization() != null) { // role in organization permission
				addRoleInOrganizationPermissionElements(permissionElements, permission);
			} else { // role permission
				addRolePermissions(permissionElements, permission);
			}
		} else {
			throw new CoreException("Invalid permission " + permission);
		}
	}
	
	/**
	 * Adds user related elementary permissions using specified permission as template
	 * 
	 * @param permissionElements target set for created elementary permissions
	 * @param permission user related permission
	 */
	private void addUserPermissionElements(Set<PermissionElement> permissionElements, CorePermission permission) {
		PermissionElement indexRecordPermission = new PermissionElement();
		
		indexRecordPermission.setUserName(permission.getUser().getUsername());
		copyFlags(permission, indexRecordPermission);
		
		permissionElements.add(indexRecordPermission);
	}
	
	/**
	 * Adds role in organization related elementary permissions using specified permission as template
	 * 
	 * @param permissionElements target set for created elementary permissions
	 * @param permission role in organization related permission
	 */
	private void addRoleInOrganizationPermissionElements(Set<PermissionElement> permissionElements, CorePermission permission) {		
			List<String> roleNames = findAllAncestorRoles(permission.getRole().getName());
					
			for(String roleName : roleNames) {
				List<String> organizationNames = findAllAncestorOrganizations(permission.getOrganization().getName());
				
				for(String organizationName : organizationNames) {
					PermissionElement indexRecordPermission = new PermissionElement();
					
					indexRecordPermission.setRoleName(roleName);
					indexRecordPermission.setOrganizationName(organizationName);
					
					copyFlags(permission, indexRecordPermission);									
					
					permissionElements.add(indexRecordPermission);
				}
			}
	}
	
	/**
	 * Adds role related elementary permissions using specified permission as template
	 * 
	 * @param permissionElements target set for created elementary permissions
	 * @param permission role related permission
	 */
	private void addRolePermissions(Set<PermissionElement> permissionElements, CorePermission permission) {
		List<String> roleNames = findAllAncestorRoles(permission.getRole().getName());
		
		for(String roleName : roleNames) {
			PermissionElement indexRecordPermission = new PermissionElement();
			
			indexRecordPermission.setRoleName(roleName);			
			copyFlags(permission, indexRecordPermission);					
			
			permissionElements.add(indexRecordPermission);
		}
	}
	
	/**
	 * Copies access flags from permissions (source) to elementary permission (target) 
	 * 
	 * @param source permission
	 * @param target elementary permission
	 */
	private void copyFlags(CorePermission source, PermissionElement target) {
		target.setCanRead(source.getCanRead());
		target.setCanUpdate(source.getCanWrite());
		target.setCanRemove(source.getCanDelete());	
	}
	
	/**
	 * Finds all ancestor role names
	 * 
	 * @param roleName name of role where to start searching 
	 * @return all ancestor role names (including roleName)
	 */
	private List<String> findAllAncestorRoles(String roleName) {
		return roleStructureService.findAllAncestorRoles(roleName);		
	}
	
	/**
	 * Finds all ancestor organization names
	 * 
	 * @param organizationName name of organization where to start searching
	 * @return all ancestor organization names (including organizationName)
	 */
	private List<String> findAllAncestorOrganizations(String organizationName) {
		return organizationStructureService.findAllAncestorOrganizations(organizationName);
	}
}