package org.simplifyworks.core.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.dozer.Mapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterGroup;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.AccessType;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.ReadManager;
import org.simplifyworks.security.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class DefaultReadManager<T extends AbstractDto, E extends AbstractEntity> implements ReadManager<T, E> {

	public static final String INDEX_EVENT_METHOD_NAME_PREFIX = "on";
	public static final String INDEX_EVENT_METHOD_NAME_SUFFIX = "Event";

	protected static final Logger LOG = LoggerFactory.getLogger(DefaultReadManager.class);

	protected final Class<T> dtoClass;

	protected final Class<E> entityClass;

	@Autowired
	protected Mapper mapper;

	@PersistenceContext(unitName = "default")
	protected EntityManager entityManager;

	@Autowired
	protected Client client;

	@Autowired
	protected ElasticsearchService elasticsearchService;

	@Autowired
	protected SecurityService securityService;

	@SuppressWarnings("unchecked")
	public DefaultReadManager() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		dtoClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
		entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[1];
	}

	@Override
	public Class<T> getDtoClass() {
		return dtoClass;
	}

	@Override
	public Class<E> getEntityClass() {
		return entityClass;
	}

	@Override
	public T toDto(E entity) {
		return entity == null ? null : mapper.map(entity, dtoClass);
	}

	@Override
	public List<T> toDtoList(List<E> entities) {
		if (entities == null) {
			return null;
		}

		List<T> objects = new ArrayList<T>();

		entities.stream().forEach((e) -> {
			objects.add(toDto(e));
		});

		return objects;
	}

	@Override
	/**
	 * Activiti needs separate method (different name than search) for search
	 * entity by ID.
	 */
	public T get(long id) {
		return search(id);
	}

	@Override
	public T search(long id) {
		return elasticsearchService.search(dtoClass, id, AccessType.READ);
	}

	@Override
	public List<T> search(SearchParameters parameters) {
		return elasticsearchService.search(dtoClass, parameters, createCustomSearchParametersMapping(parameters), AccessType.READ);
	}

	@Override
	public long count(SearchParameters parameters) {
		return elasticsearchService.count(dtoClass, parameters, createCustomSearchParametersMapping(parameters), AccessType.READ);
	}

	@Override
	public void onApplicationEvent(EntityEvent event) {
		String methodName = prepareMethodName(event);

		try {
			// try to publish event to concrete method
			getClass().getMethod(methodName, EntityEvent.class).invoke(this, event);
		} catch (NoSuchMethodException e) {
			// ignore event if method is not found
			if (LOG.isDebugEnabled()) {
				LOG.debug("Method " + methodName + " not found on " + this.getClass().getSimpleName() + ", event will not be propagated");
			}
		} catch (InvocationTargetException | IllegalAccessException | SecurityException e) {
			throw new CoreException(e);
		}
	}

	private String prepareMethodName(EntityEvent event) {
		String objectClassName = event.getEntity().getClass().getSimpleName();

		return INDEX_EVENT_METHOD_NAME_PREFIX + objectClassName + INDEX_EVENT_METHOD_NAME_SUFFIX;
	}

	protected Map<FilterValue, FilterBuilder> createCustomSearchParametersMapping(SearchParameters parameters) {
		Map<FilterValue, FilterBuilder> customSearchParametersMapping = new HashMap<>();

		for (FilterGroup filterGroup : parameters.getFilterGroupsList()) {
			for (FilterValue filterValue : parameters.getFilters(filterGroup.getName()).values()) {
				FilterBuilder builder = createCustomSearchParametersMapping(filterValue);

				if (builder != null) {
					customSearchParametersMapping.put(filterValue, builder);
				}
			}
		}

		return customSearchParametersMapping;
	}

	protected FilterBuilder createCustomSearchParametersMapping(FilterValue filterValue) {
		return null;
	}

	@Override
	@Transactional
	public long reindexAll() {
		SearchHits searchHits = client.prepareSearch(ElasticsearchService.INDEX_NAME)
				.setTypes(dtoClass.getSimpleName()).get().getHits();

		if (searchHits.getTotalHits() > 0) {
			BulkRequestBuilder bulkDelete = client.prepareBulk();

			for (SearchHit searchHit : searchHits.getHits()) {
				bulkDelete.add(client.prepareDelete(searchHit.getIndex(), searchHit.getType(), searchHit.getId()));
			}
			bulkDelete.get();
		}

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<E> criteria = cb.createQuery(entityClass);
		criteria.from(entityClass);

		long counter = 0;
		for (E entity : entityManager.createQuery(criteria).getResultList()) {

			T dto = toDto(entity);
			computeAdditionalFieldsForIndex(dto, entity);
			elasticsearchService.indexWithPermissions(dto, entityClass, entity.getId());

			counter++;
		}

		return counter;
	}

	protected void computeAdditionalFieldsForIndex(T dto, E entity) {
	}
	
	protected void reindexChangedDtos(FilterBuilder builder) {
		for (T dto : elasticsearchService.search(dtoClass, builder)) {
			elasticsearchService.indexWithPermissions(toDto(entityManager.find(entityClass, dto.getId())), dtoClass, dto.getId());
		}
	}

	protected void reindexRemovedDtos(FilterBuilder builder) {
		for (T dto : elasticsearchService.search(dtoClass, builder)) {
			elasticsearchService.removeFromIndex(dtoClass, dto.getId());
		}
	}
	
	protected void reindexItself(EntityEvent event, FilterBuilder builder) {
		switch (event.getOperation()) {
			case CREATE:
			case UPDATE:
				reindexChangedDtos(builder);
				break;
			case REMOVE:
				reindexRemovedDtos(builder);
		}
	}
}
