/**
 *
 */
package org.simplifyworks.core.service.impl;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.AccessType;
import org.simplifyworks.core.service.ElasticsearchService;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.EntityEvent.EntityEventOperation;
import org.simplifyworks.core.service.JpaService;
import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.annotation.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class DefaultReadWriteManager<T extends AbstractDto, E extends AbstractEntity>
		extends DefaultReadManager<T, E> implements ReadWriteManager<T, E> {

	@Autowired
	protected JpaService jpaService;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@SuppressWarnings("unchecked")
	public DefaultReadWriteManager() {
		super();
	}

	@Override
	public E toEntity(T object) {
		return object == null ? null : mapper.map(object, entityClass);
	}

	@Override
	public List<E> toEntityList(List<T> objects) {
		if (objects == null) {
			return null;
		}

		List<E> entities = new ArrayList<>();
		objects.stream().forEach((e) -> {
			entities.add(toEntity(e));
		});

		return entities;
	}

	@Override
	@Transactional
	public T create(T object) {
		Assert.notNull(object, "object has to be filled");

		Permission permission = entityClass.getAnnotation(Permission.class);
		String[] requiredRoleNames = (permission == null ? new String[]{"admin"} : permission.createRoles());

		if (!securityService.hasAnyRole(requiredRoleNames)) {
			throw new CoreException(
					"Create failed. You don't have required roles \"" + Arrays.toString(permission.createRoles()) + "\"");
		}

		E entity = jpaService.create(toEntity(object));
		object.setId(entity.getId());

		elasticsearchService.indexWithPermissions(object, entityClass, entity.getId());

		eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.CREATE, entity));

		return toDto(entity);
	}

	@Override
	@Transactional
	public T update(T object) {
		Assert.notNull(object, "object has to be filled");
		Assert.notNull(object.getId(), "id has to be filled");

		checkAccess(object.getId(), AccessType.UPDATE);

		E entity = jpaService.update(toEntity(object));
		// add permissions using annotation Permission

		elasticsearchService.indexWithPermissions(object, entityClass, entity.getId());

		eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.UPDATE, entity));

		return toDto(entity);
	}

	@Override
	@Transactional
	public void remove(long id) {
		checkAccess(id, AccessType.DELETE);

		E entity = jpaService.remove(entityClass, id);

		elasticsearchService.removeFromIndex(dtoClass, id);

		eventPublisher.publishEvent(new EntityEvent(this, EntityEventOperation.REMOVE, entity));
	}

	protected void checkAccess(long id, AccessType accessType) {
		if (!isAccessible(id, accessType)) {
			throw new CoreException("Record with id [" + id + "] does not exist or is not accessible");
		}
	}

	protected boolean isAccessible(long id, AccessType accessType) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue("id", FilterOperator.EQUALS, Collections.singletonList(id)));

		return elasticsearchService.count(dtoClass, parameters, createCustomSearchParametersMapping(parameters), accessType) == 1;
	}
}
