package org.simplifyworks.core.service.impl;

import org.apache.commons.io.IOUtils;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.simplifyworks.core.service.VelocityService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.StringWriter;

/**
 * Velocity service
 * Created by Svanda on 13.8.2015.
 */

@Service
public class VelocityServiceImpl implements VelocityService {

    @PostConstruct
    public void init() {
        Velocity.init();
    }

    /**
     * Return string result from template and variables
     *
     * @param context  Context with variables
     * @param template
     * @return
     */
    @Override
    public String evaluate(Context context, String template) {
        init();
        StringWriter w = null;
        try {
            w = new StringWriter();
            Velocity.evaluate(context, w, "content", template);
            return w.toString();
        } finally {
            IOUtils.closeQuietly(w);
        }
    }

}
