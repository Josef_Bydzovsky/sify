/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.util;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.persistence.metamodel.Attribute;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.entity.IValidableEntity;

/**
 * Utility pro zjednoduseni prace s entitami
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public class EntityUtils {

	private EntityUtils() {
	}

	/**
	 * Vraci property descriptor TODO: Spatny api - prohodit parametry
	 *
	 * @param entityClass
	 * @param property V property muze byt i teckova konvence
	 * @return
	 */
	public static PropertyDescriptor getPropertyDescriptor(Class entityClass, String property) {
		try {
			Class clazz = entityClass;
			String[] properties = property.split("\\.");
			PropertyDescriptor pd = null;
			for (String partialProperty : properties) {
				pd = new PropertyDescriptor(partialProperty, clazz);
				clazz = pd.getReadMethod().getReturnType();
				if (Collection.class.isAssignableFrom(clazz)) {
                    ParameterizedType genericSuperclass = (ParameterizedType) pd.getReadMethod().getGenericReturnType();
                    clazz = (Class) genericSuperclass.getActualTypeArguments()[0];
				}
			}
			return pd;
		} catch (IntrospectionException ex) {
			throw new CoreException("Chyba pri ziskavani property [" + property + "] od tridy [" + entityClass + "]", ex);
		}
	}

	public static Object getPropertyValue(Object entity, String property) throws Exception {
		String[] properties = property.split("\\.");
		Object propertyValue = entity;
		for (String partialProperty : properties) {
			if (propertyValue == null) {
				return null;
			}
			propertyValue = EntityUtils.getPropertyDescriptor(propertyValue.getClass(), partialProperty).getReadMethod().invoke(propertyValue);
		}
		return propertyValue;
	}

	/**
	 * Vraci property descriptor - nelze pres standardni descriptor, protoye
	 * prekrzvame datove typy a defaultne se hleda serializable ...
	 *
	 * @param clazz
	 * @param attribute
	 * @return
	 * @throws IntrospectionException
	 * @throws NoSuchMethodException
	 */
	public static PropertyDescriptor getPropertyDescriptor(Class clazz, Attribute attribute) throws IntrospectionException, NoSuchMethodException {
		// TODO: a co treba malej boolean?
		PropertyDescriptor propertyDescriptor = new PropertyDescriptor(attribute.getName(),
				clazz.getMethod("get" + StringUtils.capitalize(attribute.getName())),
				clazz.getMethod("set" + StringUtils.capitalize(attribute.getName()), attribute.getJavaType()));
		return propertyDescriptor;
	}

	/**
	 * Vraci true, pokud ma clazza (ci predek) alespon jeden anotovany field
	 *
	 * @param clazz
	 * @param annotation
	 * @return
	 */
	public static boolean hasAnnotatedField(Class clazz, Class<? extends Annotation> annotation) {
		Class processedClazz = clazz;
		while (processedClazz != Object.class) {
			for (Field field : processedClazz.getDeclaredFields()) {
				if (field.isAnnotationPresent(annotation)) {
					return true;
				}
			}
			processedClazz = processedClazz.getSuperclass();
		}
		return false;
	}

	public static boolean hasAnnotation(Class clazz, String property, Class<? extends Annotation> annotation) {
		Class processedClazz = clazz;
		while (processedClazz != Object.class) {
			for (Field field : processedClazz.getDeclaredFields()) {
				if (field.getName().equals(property) && field.isAnnotationPresent(annotation)) {
					return true;
				}
			}
			processedClazz = processedClazz.getSuperclass();
		}
		return false;
	}

	/**
	 * Vrati true, pokud je entita platna
	 *
	 * @param entity
	 * @return
	 */
	public static boolean isValid(IValidableEntity entity) {
		if (entity == null) {
			return false;
		}
		return (entity.getValidFrom() == null || DateUtils.truncate(entity.getValidFrom(), Calendar.DATE).compareTo(DateUtils.truncate(new Date(), Calendar.DATE)) <= 0)
				&& (entity.getValidTill() == null || DateUtils.truncate(entity.getValidTill(), Calendar.DATE).compareTo(DateUtils.truncate(new Date(), Calendar.DATE)) >= 0);
	}
}
