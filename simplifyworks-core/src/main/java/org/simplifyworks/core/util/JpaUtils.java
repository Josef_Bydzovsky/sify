package org.simplifyworks.core.util;

import javax.persistence.EntityManager;

public class JpaUtils {

	private JpaUtils() {
	}

	public static boolean isMySQL(EntityManager entityManager) {
		return (entityManager.getEntityManagerFactory().getProperties().get("hibernate.dialect").toString().toLowerCase().contains("mysql"));
	}

	public static boolean isOracle(EntityManager entityManager) {
		return (entityManager.getEntityManagerFactory().getProperties().get("hibernate.dialect").toString().toLowerCase().contains("oracle"));
	}

	public static String resolveDateFunction(EntityManager entityManager) {
		return (isMySQL(entityManager) ? ("date") : ("trunc"));
	}
}
