package org.simplifyworks.core.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.CONSTRUCTOR, ElementType.LOCAL_VARIABLE})
@Retention(RetentionPolicy.RUNTIME)
public @interface SuppressWarningsJUnit {
	
	public static final String SKIP_SEQUENCE_UNIT_TEST = "skip-sequence-unit-test";
	public static final String SKIP_NOTNULL_UNIT_TEST = "skip-notnull-unit-test";
	
	String[] value();
}
