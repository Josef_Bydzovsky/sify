/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.controller.rest;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.exception.EntityNotFoundException;
import org.simplifyworks.core.model.domain.FilterGroup;
import org.simplifyworks.core.model.domain.RecordRange;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.domain.SortValue;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.core.web.domain.table.DtoResource;
import org.simplifyworks.core.web.domain.table.Metadata;
import org.simplifyworks.core.web.domain.table.RequestParser;
import org.simplifyworks.core.web.domain.table.ResponseWrapper;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.workflow.model.dto.WorkflowActionDto;
import org.simplifyworks.workflow.service.WorkflowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.hateoas.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.collect.Lists;

/**
 * Base rest controler for table data = pagination, ordering, filters
 * <p>
 * TODO: security annotation
 *
 * @param <T> dTo class
 * @param <E> Entity class
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public abstract class BasicTableController<T extends AbstractDto, E extends AbstractEntity> {

    private static final Logger LOG = LoggerFactory.getLogger(BasicTableController.class);
    public static final String ENCODING = "UTF-8";
    public static final String PARAMETER_INCLUDE_METADATA = "includeMetadata";
    
    @Autowired
    private SecurityService securityService;
    
    @Autowired
    private WorkflowService workflowService;

    protected abstract ReadWriteManager<T, E> getManager();

    /**
     * Return filtered dto page
     *
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseWrapper getPage(HttpServletRequest request) {
        return getData(request, null);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/filter")
    public ResponseWrapper getPage(HttpServletRequest request, @RequestBody SearchParameters jsonSearchParameters) {
        SearchParametersParser searchParametersParser = new SearchParametersParser();
        searchParametersParser.process(jsonSearchParameters);
        return getData(request, jsonSearchParameters);
    }

    /**
     * TODO: return json message?
     *
     * @return
     */
    @PreAuthorize("hasRole('admin')")
    @RequestMapping(method = RequestMethod.GET, value = "/reindex")
    public long reindex() {
        long reindexAll = getManager().reindexAll();
        
        if (LOG.isDebugEnabled()) {
            LOG.debug("Reindexed all records [" + reindexAll + "] for [" + getManager().getDtoClass().getSimpleName() + "] by user [" + securityService.getUsername() + "]");
        }
        return reindexAll;
    }

    /**
     * Method for generate example json serialized SearchParameters based on
     * simple rest filter. This json can be used in /filter method as filter.
     *
     * @param request
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/buildfilter")
    public Resource<SearchParameters> getPageFiltered(HttpServletRequest request) {

        RestSimpleFilterRequestParser restSimpleFilterRequestParser = new RestSimpleFilterRequestParser(request);
        SearchParameters restSimpleSearchParameters = restSimpleFilterRequestParser.toSearchParameters();

        Resource r = new Resource<>(restSimpleSearchParameters);

        return r;
    }

    private ResponseWrapper getData(HttpServletRequest request, SearchParameters jsonSearchParameters) {
        RequestParser requestParser = new RequestParser(request);
        SearchParameters dataTablesSearchParameters = requestParser.toSearchParameters();

        RestSimpleFilterRequestParser restSimpleFilterRequestParser = new RestSimpleFilterRequestParser(request);
        SearchParameters restSimpleSearchParameters = restSimpleFilterRequestParser.toSearchParameters();

        //merge all search-parameters
        SearchParameters searchParameters = mergeSearchParameters(jsonSearchParameters, restSimpleSearchParameters, dataTablesSearchParameters);

        if (searchParameters.getRange() == null) {
            RecordRange recordRange = new RecordRange();
            recordRange.setFirstRow(0);
            recordRange.setRows(1000);
            searchParameters.setRange(recordRange);
        }

        searchParameters.setLocale(LocaleContextHolder.getLocale());
        
        Long recordsFiltered = getManager().count(searchParameters);               
        
        ResponseWrapper<T> response;
        
        if (recordsFiltered > 0) {
        	response = new ResponseWrapper(toResources(getManager().search(searchParameters), request.getParameter(PARAMETER_INCLUDE_METADATA) != null), this);
        } else {
        	response = new ResponseWrapper(Lists.newArrayList(), this);
        }
        
        searchParameters.clearNonDefaultFilters();
        Long recordsTotal = getManager().count(searchParameters);
        
        
        response.setRecordsFiltered(recordsFiltered);
        response.setRecordsTotal(recordsTotal);        
        response.setDraw(requestParser.getDraw());
        
        return response;
    }    
    
    /**
     * Merge searchParametersList values using "AND"
     */
    private SearchParameters mergeSearchParameters(SearchParameters... searchParametersList) {
        SearchParameters result = new SearchParameters();
        for (SearchParameters searchParameters : searchParametersList) {
            if (searchParameters == null) {
                continue;
            }

            for (FilterGroup group : searchParameters.getFilterGroups().values()) {
                result.addFilterGroup(group);
            }

            // TODO: this is not good, becose last searchParameters wins
            if (searchParameters.getRange() != null) {
                result.setRange(searchParameters.getRange());
            }
            if (StringUtils.isNotBlank(searchParameters.getFulltext())) {
                result.setFulltext(searchParameters.getFulltext());
            }
        }

        for (int index = searchParametersList.length - 1; index >= 0; index--) {
            if (searchParametersList[index] == null) {
                continue;
            }
            ArrayList<SortValue> values = Lists.newArrayList(searchParametersList[index].getSorts().values());
            for (SortValue sort : values) {
                result.addSort(sort);
            }
        }

        return result;
    }

    /**
     * Load record by id
     *
     * @param id
     * @param includeMetadata
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public DtoResource<T> get(@PathVariable Long id, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {
        T result = getManager().search(id);
        if (result == null) {
            throw new EntityNotFoundException(id);
        }
        return toResource(result, includeMetadata != null);
    }

    /**
     * Create new record
     *
     * @param dto
     * @param includeMetadata
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public DtoResource<T> create(@RequestBody T dto, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {
        return toResource(getManager().create(dto), includeMetadata != null);
    }

    /**
     * @param dto
     * @param includeMetadata
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public DtoResource<T> update(@RequestBody T dto, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("update entity [" + dto.getId() + "]");
        }
        T result = getManager().update(dto);
        if (result == null) {
            throw new EntityNotFoundException(dto.getId());
        }
        return toResource(result, includeMetadata != null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable Long id) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("remove entity [" + id + "]");
        }
        getManager().remove(id);
    }

    /**
     * Make update entity and call workflow engine with wfButton
     *
     * @param wfActionDto
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wf/process")
    @Transactional
    public ResourceWrapper process(@RequestBody WorkflowActionDto<T> wfActionDto, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {

        Assert.notNull(wfActionDto, "Insert please workflowActionDto!");
        Assert.notNull(wfActionDto.getButton(), "Insert please wf button as item  to workflowActionDto!");
        Assert.notNull(wfActionDto.getEntity(), "Insert please entity as item to workflowActionDto !");
        Assert.notNull(wfActionDto.getProcessInstance(), "Insert please processInstanceId as item  to workflowActionDto!");
        Assert.notNull(wfActionDto.getProcessKey(), "Insert please processKey as item to workflowActionDto !");

        DtoResource<T> savedEntity = null;
        if (wfActionDto.getEntity().getId() != null) {
            savedEntity = this.update(wfActionDto.getEntity(), includeMetadata != null);
        } else {
            savedEntity = this.create(wfActionDto.getEntity(), includeMetadata != null);
        }
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put(WorkflowService.PARAMETER_ENTITY_ID, savedEntity.getResource().getId());

        //we need extract genericType (we can't use type class of dto (entity only))
        Type genericType = getClass().getGenericSuperclass();
        Type t = ((ParameterizedType) genericType).getActualTypeArguments()[1];
        variables.put(WorkflowService.PARAMETER_ENTITY_TYPE, t.getTypeName());
        Map<String, Object> result = workflowService.process(wfActionDto.getProcessKey(), wfActionDto.getProcessInstance(), variables, wfActionDto.getButton());

        //Put result variables from workflow and fresh entity to response
        T resourceEntity = this.get(savedEntity.getResource().getId(), includeMetadata != null).getResource();
        wfActionDto.setEntity(resourceEntity);
        wfActionDto.setVariables(result);
        wfActionDto.setProcessInstance((String) result.get(WorkflowService.PARAMETER_PROCESS_INSTANCE));

        ResourceWrapper resourceWrapper = new ResourceWrapper(wfActionDto);
        return resourceWrapper;
    }

    /**
     * HATEOAS resources construction
     *
     * @param data
     * @param includeMetadata
     * @return
     */
    protected List<DtoResource<T>> toResources(List<T> data, boolean includeMetadata) {
        List<DtoResource<T>> resources = Lists.newArrayList();
        data.stream().forEach((abstractDto) -> {
            resources.add(toResource(abstractDto, includeMetadata));
        });
        return resources;
    }

    /*
    private DtoResource<T> toResource(T dto) {
        return toResource(dto, false);
    }
    */

    /**
     * HATEOAS resource construction
     *
     * @param dto
     * @param includeMetadata
     * @return
     */
    protected DtoResource<T> toResource(T dto, boolean includeMetadata) {
        DtoResource<T> dtoResource = new DtoResource<>(dto, this);
        if (includeMetadata) {
            Metadata metadata = getMetadata(dto);
            dtoResource.setMetadata(metadata);
        }
        return dtoResource;
    }

    /**
     * Creates metadata for given object. Can be overridden.
     */
    protected Metadata getMetadata(T dto) {
        Metadata metadata = new Metadata();
        metadata.setCanDelete(!dto.isLocked());
        metadata.setCanEdit(!dto.isLocked());
        return metadata;
    }
}
