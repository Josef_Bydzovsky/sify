
package org.simplifyworks.core.web.controller.rest;

import org.simplifyworks.core.model.dto.CoreAppSettingDto;
import org.simplifyworks.core.model.entity.CoreAppSetting;
import org.simplifyworks.core.service.CoreAppSettingManager;
import org.simplifyworks.core.service.ReadWriteManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Application setting
 * 
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RestController
@RequestMapping("/api/core/settings")
public class CoreAppSettingController extends BasicTableController<CoreAppSettingDto, CoreAppSetting> {

	@Autowired
	private CoreAppSettingManager appSettingManager;

	@Override
	protected ReadWriteManager<CoreAppSettingDto, CoreAppSetting> getManager() {
		return appSettingManager;
	}	
}
