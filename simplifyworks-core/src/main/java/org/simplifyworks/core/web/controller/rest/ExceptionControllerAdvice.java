/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.controller.rest;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.simplifyworks.core.exception.EntityNotFoundException;
import org.simplifyworks.core.exception.ParametrizedError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.common.collect.Maps;

/**
 * Wrap application exceptions for rest controllers ("exception decorator")
 *
 * TODO: Wrap common exceptions. Choose right log level (error / warn / info)
 * TODO: return localized message?
 * TODO: Move error code definiton ro exception?
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@ControllerAdvice
public class ExceptionControllerAdvice {

	private static final Logger LOG = LoggerFactory.getLogger(BasicTableController.class);

	@ResponseBody
	@ExceptionHandler(EntityNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	VndErrors hadleEntityNotFoundException(EntityNotFoundException ex) {
		LOG.warn("", ex);
		Map<String, Object> params = Maps.newLinkedHashMap();
		params.put("entity_id", ex.getEntityId());
		return new VndErrors(new ParametrizedError("entity_not_found", ex.getMessage(), params));
	}

	@ResponseBody
	@ExceptionHandler(BadCredentialsException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	VndErrors handleBadCredentialsException(BadCredentialsException ex) {
		LOG.warn("", ex);
		return new VndErrors("invalid_grant", ex.getMessage());
	}
	
	@ResponseBody
	@ExceptionHandler(value = Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	VndErrors defaultErrorHandler(HttpServletRequest req, Exception ex) throws Exception {
		LOG.error("", ex);
		// If the exception is annotated with @ResponseStatus rethrow it and let
		// the framework handle it - like the OrderNotFoundException example
		// at the start of this post.
		if (AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class) != null) {
			throw ex;
		}
		return new VndErrors(new ParametrizedError(ex.getMessage(), ex.getMessage()));
	}
}
