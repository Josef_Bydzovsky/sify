package org.simplifyworks.core.web.controller.rest;

import org.simplifyworks.core.model.domain.FilterGroup;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;

/**
 * Created by Siroky on 3.6.2015.
 */
public class SearchParametersParser {

    public void process(SearchParameters searchParameters) {
        for (FilterGroup group : searchParameters.getFilterGroups().values()) {
            for (FilterValue value : group.getFilters().values()) {
                value.setFilterGroup(group);
            }
        }
    }
}
