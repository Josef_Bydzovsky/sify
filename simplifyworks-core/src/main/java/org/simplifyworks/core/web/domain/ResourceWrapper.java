package org.simplifyworks.core.web.domain;

/**
 * Created by Svanda on 1.7.2015.
 */
public class ResourceWrapper {
    Object resource;

    public ResourceWrapper(Object resource){
        this.resource = resource;
    }

    public Object getResource() {
        return resource;
    }

    public void setResource(Object resource) {
        this.resource = resource;
    }
}
