/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.core.Relation;

import java.io.Serializable;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * @author Svanda
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Relation(value = "resource", collectionRelation = "resources")
public class ResponseWrapper<T> implements Serializable {

	@JsonProperty
	T resource;

	public ResponseWrapper(T data){
		this.resource = data;
	}

	public T getResource() {
		return resource;
	}

	public void setResource(T resource) {
		this.resource = resource;
	}
}
