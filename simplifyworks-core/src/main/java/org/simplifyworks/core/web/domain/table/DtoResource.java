/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.domain.table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * HATEOAS resource support
 * 
 * http://keaplogik.blogspot.cz/2015/01/spring-hateoas-embedded-resources-with.html
 * 
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 * @param <T>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Relation(value = "resource", collectionRelation = "resources")
public class DtoResource<T extends AbstractDto> extends ResourceSupport {

	private final T resource;
    @JsonProperty
    private Metadata metadata;

	public DtoResource(T data, BasicTableController basicTableController) {
		this.resource = data;
		this.add(linkTo(methodOn(basicTableController.getClass()).get(data.getId(), false)).withSelfRel());
	}

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public T getResource() {
		return resource;
	}
}
