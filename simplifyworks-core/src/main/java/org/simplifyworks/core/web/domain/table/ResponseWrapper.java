/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.domain.table;

import java.util.List;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.springframework.hateoas.Resources;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Data for tha DataTables
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 * @param <T>
 */
public class ResponseWrapper<T extends AbstractDto> extends Resources<DtoResource<T>> {

	private int draw = 1;
	private long recordsTotal = 0;
	private long recordsFiltered = 0;
	private long recordsReturned = 0;

	public ResponseWrapper(List<DtoResource<T>> data, BasicTableController basicTableController) {
		// data
		super(data);
		recordsReturned = data.size();
		// linky
		this.add(linkTo(basicTableController.getClass()).withSelfRel());
		// TODO: slozit paramentry volani searche
	}

	public void setRecordsTotal(long recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public long getRecordsTotal() {
		return recordsTotal;
	}

	public long getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(long recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public long getRecordsReturned() {
		return recordsReturned;
	}

	public void setRecordsReturned(long recordsReturned) {
		this.recordsReturned = recordsReturned;
	}
}
