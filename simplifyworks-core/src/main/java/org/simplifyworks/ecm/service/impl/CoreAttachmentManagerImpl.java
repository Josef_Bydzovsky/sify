package org.simplifyworks.ecm.service.impl;

import java.io.InputStream;
import java.util.List;

import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;

import org.simplifyworks.ecm.model.entity.CoreAttachment;
import org.simplifyworks.ecm.service.AttachmentManager;
import org.simplifyworks.ecm.service.CoreAttachmentManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.ecm.model.entity.CoreAttachment_;

/**
 * Save attachment on FS
 *
 * @author Svanda
 */
@Service
@Transactional
public class CoreAttachmentManagerImpl extends DefaultAttachmentManager<CoreAttachmentDto, CoreAttachment> implements CoreAttachmentManager {

	public void onCoreAttachmentEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.orFilter(
				FilterBuilders.termFilter(CoreAttachment_.nextVersion.getName() + "." + CoreAttachment_.id.getName(), event.getEntity().getId()), 
				FilterBuilders.termFilter(CoreAttachment_.parent.getName() + "." + CoreAttachment_.id.getName(), event.getEntity().getId()))
				);
		reindexItself(event, FilterBuilders.termFilter(CoreAttachment_.id.getName(), event.getEntity().getId()));
	}

	@Override
	public void remove(long entityId) {
		removeAttachment(entityId);

	}


	@Override
	public CoreAttachmentDto search(long entityId) {
		CoreAttachmentDto coreAttachmentDto = super.search(entityId);
		//for dev and presetantion only
		List<CoreAttachmentDto> coreAttachmentDtos = getAttachments("Attachment", String.valueOf(coreAttachmentDto.getId()));
		return coreAttachmentDto;
	}


	@Override
	@Transactional
	public CoreAttachmentDto update(CoreAttachmentDto attachmentDto) {
		if(attachmentDto != null){
			if(attachmentDto.getId() == null){
				InputStream tempFileData = getTempFileData(attachmentDto);
				attachmentDto.setInputData(tempFileData);
				attachmentDto = saveAttachment(attachmentDto);
			}else{
				attachmentDto = super.update(attachmentDto);
			}
		}
		return attachmentDto;
	}

}
