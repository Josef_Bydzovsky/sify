
package org.simplifyworks.ecm.web.controller.rest;

import java.io.IOException;
import java.io.InputStream;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;
import org.simplifyworks.ecm.model.entity.CoreAttachment;
import org.simplifyworks.ecm.service.CoreAttachmentManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.InvalidMimeTypeException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * VS
 */
@RestController
@RequestMapping(value = "/api/attachments")
public class AttachmentServiceController extends BasicTableController<CoreAttachmentDto, CoreAttachment> {

    @Autowired
    private CoreAttachmentManager attachmentManager;


    @Override
    protected ReadWriteManager<CoreAttachmentDto, CoreAttachment> getManager() {
        return attachmentManager;
    }

    @RequestMapping(value = "{id}/download", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> download(@PathVariable Long id)
            throws IOException {
        Assert.notNull(id, "Insert id attachment!");

        CoreAttachmentDto attachment = attachmentManager.getAttachment(id);
        Assert.notNull(attachment, "Attachment not found!");
        InputStream attachmentData = attachmentManager.getAttachmentData(attachment.getId());

        MediaType mediaType = null;
        try {
            mediaType = MediaType.parseMediaType(attachment.getMimetype());
        } catch (InvalidMimeTypeException ex) {
            //
        } catch (InvalidMediaTypeException ex) {
            //
        }

        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(mediaType == null ? MediaType.MULTIPART_FORM_DATA : mediaType);
        respHeaders.setContentLength(attachment.getFilesize());
        respHeaders.setContentDispositionFormData("attachment", attachment.getName());


        InputStreamResource isr = new InputStreamResource(attachmentData);
        return new ResponseEntity<InputStreamResource>(isr, respHeaders, HttpStatus.OK);
    }


    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public CoreAttachmentDto saveAttachment(CoreAttachmentDto attachmentDto, MultipartFile file) throws IOException {
        Assert.notNull(attachmentDto, "Insert attachment!");
        Assert.notNull(file, "Insert binary data!");
        attachmentDto.setInputData(file.getInputStream());
        return attachmentManager.saveAttachment(attachmentDto);
    }


    @RequestMapping(value = "/upload-temp", method = RequestMethod.POST)
    public CoreAttachmentDto uploadTempFile(CoreAttachmentDto tempAttachmentDto, MultipartFile file) throws Exception {
        Assert.notNull(tempAttachmentDto, "Insert temp attachment!");
        Assert.notNull(file, "Insert binary data!");
        tempAttachmentDto.setInputData(file.getInputStream());
        return attachmentManager.saveTempFile(tempAttachmentDto, file.getInputStream());
    }

    @RequestMapping(value = "/temp-to-attachment", method = RequestMethod.POST)
    public CoreAttachmentDto convertTempToAttachment(CoreAttachmentDto tempAttachmentDto) throws Exception {
        Assert.notNull(tempAttachmentDto, "Insert temp attachment!");
        InputStream tempFileData = attachmentManager.getTempFileData(tempAttachmentDto);
        tempAttachmentDto.setInputData(tempFileData);
        return attachmentManager.saveAttachment(tempAttachmentDto);
    }

}
