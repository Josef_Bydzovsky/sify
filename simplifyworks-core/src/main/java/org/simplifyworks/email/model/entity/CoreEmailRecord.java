/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.model.entity;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Formula;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.ecm.model.entity.Attachable;
import org.simplifyworks.ecm.model.entity.CoreAttachment;
import org.simplifyworks.email.domain.EmailAttachment;
import org.simplifyworks.email.domain.EmailSetting;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Table for email records
 * @author svanda
 */
@Entity
@Table(name = "CORE_EMAIL_RECORD")
public class CoreEmailRecord extends AbstractEntity implements Attachable {

	public static final String RECIPIENT_SEPARATOR = ",";

	@Size(max = 255)
	@Column(name = "EMAIL_FROM", length = 255)
	private String emailFrom;
	@Size(max = 512)
	@Column(name = "RECIPIENTS", length = 512)
	private String recipients;
	@Size(max = 512)
	@Column(name = "CC", length = 512)
	private String cc;
	@Size(max = 512)
	@Column(name = "BCC", length = 512)
	private String bcc;
	@Size(max = 255)
	@Column(name = "SUBJECT", length = 255)
	private String subject;
	@Lob
	@Column(name = "MESSAGE", length = -1)
	private String message;
	@NotNull
	@Size(min = 1, max = 25)
	@Basic(optional = false)
	@Column(name = "ENCODING", nullable = false, length = 25)
	private String encoding;
	@ManyToOne
	@JoinColumn(name = "PARENT_ID", referencedColumnName = "ID")
	private CoreEmailRecord parent;
	@ManyToOne
	@JoinColumn(name = "PREVIOUS_VERSION", referencedColumnName = "ID")
	private CoreEmailRecord previousVersion;

	//
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SENT")
	private Date sent;
	@Size(max = 2000)
	@Column(name = "SENT_LOG", length = 2000)
	private String sentLog;
	// transient - sami se starame o ukladani - nacitacni
	private transient List<CoreAttachment> attachments;

	public CoreEmailRecord() {
	}

	/**
	 * Premapovak z puvodniho emailSetting
	 *
	 * @param emailSetting
	 * @return
	 */
	public static CoreEmailRecord getInstance(EmailSetting emailSetting) {
		CoreEmailRecord emailRecord = new CoreEmailRecord();
		emailRecord.setSubject(emailSetting.getSubject());
		emailRecord.setMessage(emailSetting.getHtml());
		emailRecord.setRecipients(StringUtils.join(emailSetting.getRecipients(), RECIPIENT_SEPARATOR));
		emailRecord.setBcc(StringUtils.join(emailSetting.getBcc(), RECIPIENT_SEPARATOR));
		emailRecord.setCc(StringUtils.join(emailSetting.getCc(), RECIPIENT_SEPARATOR));
		emailRecord.setEncoding(emailSetting.getCharset());
		emailRecord.setEmailFrom(emailSetting.getFrom());
		for (EmailAttachment emailAttachment : emailSetting.getAttachments()) {
			CoreAttachment attachment = new CoreAttachment();
			attachment.setEntity(emailRecord);
			attachment.setMimetype(emailAttachment.getMimetype());
			attachment.setName(emailAttachment.getName());
			attachment.setInputData(emailAttachment.getInputStream());
			emailRecord.getAttachments().add(attachment);
		}
		return emailRecord;
	}


	public String getRecipients() {
		return recipients;
	}

	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getSent() {
		return sent;
	}

	public void setSent(Date sent) {
		this.sent = sent;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getSentLog() {
		return sentLog;
	}

	public void setSentLog(String sentLog) {
		this.sentLog = sentLog;
	}

	public List<CoreAttachment> getAttachments() {
		if (attachments == null) {
			attachments = Lists.newArrayList();
		}
		return attachments;
	}

	public void setAttachments(List<CoreAttachment> attachments) {
		this.attachments = attachments;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public CoreEmailRecord getParent() {
		return parent;
	}

	public void setParent(CoreEmailRecord parent) {
		this.parent = parent;
	}

	public CoreEmailRecord getPreviousVersion() {
		return previousVersion;
	}

	public void setPreviousVersion(CoreEmailRecord previousVersion) {
		this.previousVersion = previousVersion;
	}
	
	@Override
	public String getObjectType() {
		return this.getClass().getCanonicalName();
	}

	@Override
	public String getObjectIdentifier() {
		if(getId() == null){
			return null;
		}
		return String.valueOf(getId());
	}

//	@Override
//	public Date getModified() {
//		return modified;
//	}
//
//	@Override
//	public void setModified(Date modified) {
//		this.modified = modified;
//	}
}
