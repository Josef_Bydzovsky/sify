/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.service;


import org.simplifyworks.email.domain.EmailAttachment;
import org.simplifyworks.email.domain.EmailSetting;
import org.simplifyworks.email.model.entity.CoreEmailRecord;

import java.util.List;

/**
 * Interface for send emails
 *
 * @author Svanda
 */
public interface Emailer {
	
	static final String DEFAULT_CHARSET = "UTF-8";

	static final String PARAMETER_TEST_ENABLED = "emailer.test.enabled"; // If is test = true, than will be emails send only on addresses with allowed suffix (PARAMETER_TEST_RECIPIENTS_ALLOWED_SUFFIX).
	static final String PARAMETER_TEST_COPY_RECIPIENTS = "emailer.test.recipients"; //On this address will send copy of emails always (if test.enabled = true)
	static final String PARAMETER_TEST_RECIPIENTS_ALLOWED = "emailer.test.recipients.allowed"; // Lists of emails addresses for standard send emails (test.enabled is no matter)
	static final String PARAMETER_TEST_RECIPIENTS_ALLOWED_SUFFIX = "emailer.test.recipients.allowed.suffix"; //Only on this addresses (with this suffix) is emails send while is test.enabled = true. Default value is "ders.cz".

	static final String DEFAULT_TEST_RECIPIENTS_ALLOWED_SUFFIX = "@ders.cz";

	/**
	 * Send email with attachments
	 *
	 * @param subject Subject.
	 * @param body Message.
	 * @param recipients
	 * @return Return true if was email sent successfully
	 * @param attachments Attachments.
	 */
	Boolean sendEmail(String subject, String body, List<String> recipients, List<EmailAttachment> attachments);

	/**
	 * Send email to only one recipient
	 *
	 * @param subject Subject.
	 * @param body Message.
	 * @param recipient
	 * @return Return true if was email sent successfully
	 */
	Boolean sendEmail(String subject, String body, String recipient);

	/**
	 * Test email
	 *
	 * @param recipient
	 * @return Return true if was email sent successfully
	 */
	Boolean sendTestEmail(String recipient);

	/**
	 *
	 * @param subject Subject.
	 * @param body Message.
	 * @param recipient
	 * @return Return true if was email sent successfully
	 */
	Boolean sendEmail(String subject, String body, List<String> recipient);

	/**
	 * Send email
	 *
	 * @param emailSetting
	 * @return
	 */
	Boolean sendEmail(EmailSetting emailSetting);
  
  /**
   * Repeat send email
   * 
   * @param emailRecord
   * @return
   */
  Boolean sendEmail(CoreEmailRecord emailRecord);
}
