package org.simplifyworks.security.service;

import java.util.Set;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * Service for security support
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface SecurityService {

	/**
	 * Returns true if there is associated and authenticated user for the current thread
	 * 
	 * @return true if user is associated and authenticated; false otherwise
	 */
	public boolean isAuthenticated();
	
	/**
	 * Returns current user details
	 * 
	 * @return current user details if authenticated; null otherwise
	 */
	public UserDetails getUserDetails();
	
	/**
	 * Returns current user name
	 * 
	 * @return current user name if authenticated; null otherwise
	 */
	public String getUsername();
	
	/**
	 * Returns all role names for the current user
	 * 
	 * @return non-empty set if user is authenticated; empty set otherwise
	 */
	public Set<String> getAllRoleNames();
	
	/**
	 * Returns true if current user has at least one role from the set
	 * 
	 * @param roleNames set of role names (required)
	 * @return true if current user has at least one role from the set; false otherwise
	 */
	public boolean hasAnyRole(String... roleNames);
	
	/**
	 * Returns true if current user has all roles from the set
	 * 
	 * @param roleNames set of role names (required)
	 * @return true if current user has all roles from the set; false otherwise
	 */
	public boolean hasAllRoles(String... roleNames);
	
	/**
	 * Returns true if current user has the role in the organization
	 * 
	 * @param roleName role name (required)
	 * @param organizationName organization name (required)
	 * @return true if current user has the role in the organization; false otherwise
	 */
	public boolean hasRoleInOrganization(String roleName, String organizationName);
	
	/**
	 * Returns true if current user is switched one 
	 * 
	 * @return true if current user is switched one; false otherwise
	 */
	public boolean isSwitchedUser();
	
	/**
	 * Returns original user details
	 * 
	 * @return original user details if user is switched one; null otherwise
	 */
	public UserDetails getOriginalUserDetails();
	
	/**
	 * Returns original user name
	 * 
	 * @return original user name if user is switched one; null otherwise
	 */
	public String getOriginalUsername();
}
