package org.simplifyworks.security.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.switchuser.SwitchUserGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * Default implementation of {@link SecurityService}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
class DefaultSecurityService implements SecurityService {

	@Override
	public boolean isAuthenticated() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		return authentication != null && !(authentication instanceof AnonymousAuthenticationToken);
	}

	@Override
	public UserDetails getUserDetails() {
		if(isAuthenticated()) {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			
			return new User(authentication.getName(), authentication.getCredentials().toString(), authentication.getAuthorities());
		}
		
		return null;
	}

	@Override
	public String getUsername() {
		return isAuthenticated() ? SecurityContextHolder.getContext().getAuthentication().getName() : null;
	}
	
	@Override
	public Set<String> getAllRoleNames() {
		if(isAuthenticated()) {
			Set<String> roleNames = new HashSet<>();
			
			for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
				if(authority instanceof RoleOrganizationGrantedAuthority) {
					roleNames.add(((RoleOrganizationGrantedAuthority) authority).getRoleName());					
				}
			}
			
			return roleNames;
		}
		
		return Collections.emptySet();
	}

	@Override
	public boolean hasAnyRole(String... roleNames) {
		Assert.notEmpty(roleNames, "roleNames must be filled");
		
		Set<String> requiredRoleNames = new HashSet<>(Arrays.asList(roleNames));		
		Set<String> allRoleNames = getAllRoleNames();
		
		return CollectionUtils.containsAny(requiredRoleNames, allRoleNames);
	}

	@Override
	public boolean hasAllRoles(String... roleNames) {
		Assert.notEmpty(roleNames, "roleNames must be filled");
		
		Set<String> requiredRoleNames = new HashSet<>(Arrays.asList(roleNames));		
		Set<String> allRoleNames = getAllRoleNames();
		
		return CollectionUtils.isSubCollection(requiredRoleNames, allRoleNames);
	}

	@Override
	public boolean hasRoleInOrganization(String roleName, String organizationName) {
		Assert.notNull(roleName, "roleName must be filled");
		Assert.notNull(organizationName, "organizationName must be filled");
		
		if(isAuthenticated()) {	
			for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
				if(authority instanceof RoleOrganizationGrantedAuthority) {
					RoleOrganizationGrantedAuthority roleOrganizationAuthority = (RoleOrganizationGrantedAuthority) authority;
					
					if(roleName.equals(roleOrganizationAuthority.getRoleName()) && organizationName.equals(roleOrganizationAuthority.getOrganizationName())) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	@Override
	public boolean isSwitchedUser() {
		if(isAuthenticated()) {
			for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
				if (authority instanceof SwitchUserGrantedAuthority) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	@Override
	public UserDetails getOriginalUserDetails() {
		if(isAuthenticated()) {
			for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
				if (authority instanceof SwitchUserGrantedAuthority) {
					return (UserDetails) ((SwitchUserGrantedAuthority) authority).getSource().getPrincipal();
				}
			}
		}
		
		return null;
	}
	
	@Override
	public String getOriginalUsername() {
		if(isAuthenticated()) {
			for(GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
				if (authority instanceof SwitchUserGrantedAuthority) {
					return ((SwitchUserGrantedAuthority) authority).getSource().getName();
				}
			}
		}
		
		return null;
	}
}