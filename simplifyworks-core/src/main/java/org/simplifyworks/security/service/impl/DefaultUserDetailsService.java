package org.simplifyworks.security.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUserRole;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of {@link UserDetailsService}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class DefaultUserDetailsService implements UserDetailsService {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultUserDetailsService.class);
	
	@PersistenceContext(unitName = "default")
	protected EntityManager entityManager;
		
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if(LOG.isDebugEnabled()) {
			LOG.debug("Authenticating user [" + username + "]");
		}
		
		try {
			CoreUser user = findUser(username);			
			UserDetails userDetails = new User(username, String.valueOf(user.getPassword()), getGrantedAuthorities(user));
			
			if(LOG.isDebugEnabled()) {
				LOG.debug("User [" + username + "] authenticated");
			}
					
			return userDetails;
		} catch(NoResultException e) {
			LOG.debug("Cannot find user", e);
			
			throw new UsernameNotFoundException("User [" + username + "] not found");
		}
	}
	
	private CoreUser findUser(String username) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreUser> criteria = cb.createQuery(CoreUser.class);
		Root<CoreUser> from = criteria.from(CoreUser.class);
		criteria.where(cb.equal(from.get(CoreUser_.username), username));
		
		return entityManager.createQuery(criteria).getSingleResult();
	}
	
	private List<GrantedAuthority> getGrantedAuthorities(CoreUser user) {
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		
		for(CoreUserRole coreUserRole : user.getRoles()) {
			CoreRole role = coreUserRole.getRole();
			CoreOrganization organization = coreUserRole.getOrganization();
				
			grantedAuthorities.add(new RoleOrganizationGrantedAuthority(role == null ? null : role.getName(), organization == null ? null : organization.getName()));
		}
		
		return grantedAuthorities;
	}
}
