package org.simplifyworks.template.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Wrapper class for variables in workflow (we need builder pattern)
 * Created by Svanda on 13.8.2015.
 */
public class TemplateVariablesWrapper {
    private Map<String, String> map;

    public TemplateVariablesWrapper(){
        map = new HashMap<String, String>();
    }
    public TemplateVariablesWrapper put(String key,String value){
        map.put(key,value);
        return this;
    }

    public Map<String, String> getMap() {
        return map;
    }
}
