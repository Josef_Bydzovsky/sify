package org.simplifyworks.template.model.dao;

import org.simplifyworks.core.model.dao.AnyTypeDao;
import org.simplifyworks.template.model.entity.CoreTemplate;

/**
 * @author SimplifyWorks Generator
 * @since 2015-08-12
 */
public interface CoreTemplateDao extends AnyTypeDao<CoreTemplate> {

}
