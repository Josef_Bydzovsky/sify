package org.simplifyworks.template.service;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.template.domain.TemplateVariablesWrapper;
import org.simplifyworks.template.model.dto.CoreTemplateDto;
import org.simplifyworks.template.model.entity.CoreTemplate;

import java.util.Map;

/**
 * @author SimplifyWorks Generator
 * @since 2015-08-12
 */
public interface CoreTemplateManager extends ReadWriteManager<CoreTemplateDto, CoreTemplate> {

    TemplateVariablesWrapper createVariables();

    String getHtmlByTemplate(String templateCode, TemplateVariablesWrapper variables);
}