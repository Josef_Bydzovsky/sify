package org.simplifyworks.template.service.impl;

import org.apache.velocity.VelocityContext;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.VelocityService;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.template.domain.TemplateVariablesWrapper;
import org.simplifyworks.template.model.entity.CoreTemplate_;
import org.simplifyworks.template.service.CoreTemplateManager;
import org.simplifyworks.template.model.dto.CoreTemplateDto;
import org.simplifyworks.template.model.entity.CoreTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;
import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.service.EntityEvent;

/**
 * @author SimplifyWorks Generator
 * @since 2015-08-12
 */
@Service("coreTemplateManager")
public class CoreTemplateManagerImpl extends DefaultReadWriteManager<CoreTemplateDto, CoreTemplate> implements CoreTemplateManager {

	@Autowired
	VelocityService velocityService;

	public void onCoreTemplateEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CoreTemplate_.id.getName(), event.getEntity().getId()));
	}

	@Override
	public TemplateVariablesWrapper createVariables() {
		TemplateVariablesWrapper map = new TemplateVariablesWrapper();
		return map;
	}

	/**
	 * Generate html string from template and variables (by Velocity)
	 *
	 * @param templateCode Template code from CoreTemplate
	 * @param variables Variables wrapped in TemplateVariablesWrapper (need for
	 * workflow definition)
	 * @return
	 */
	@Override
	public String getHtmlByTemplate(String templateCode, TemplateVariablesWrapper variables) {

		Assert.notNull(templateCode, "Template code must be fill!");

		SearchParameters searchParameters = new SearchParameters();
		FilterValue filterValue = new FilterValue();
		filterValue.setPropertyName(CoreTemplate_.code.getName());
		filterValue.setValue(templateCode.toLowerCase());
		filterValue.setOperator(FilterOperator.EQUALS);
		searchParameters.addFilter(filterValue);

		List<CoreTemplateDto> result = search(searchParameters);
		if (result == null || result.isEmpty()) {
			throw new CoreException("Template not found (" + templateCode + ")!");
		}
		if (result.size() > 1) {
			throw new CoreException("To many templates found for " + templateCode + "!");
		}

		CoreTemplateDto template = result.get(0);

		VelocityContext context = new VelocityContext();

		Set<Map.Entry<String, String>> entries = variables.getMap().entrySet();

		for (String key : variables.getMap().keySet()) {
			context.put(key, variables.getMap().get(key));
		}

		//call velocity with template and variables
		String resultVelocity = velocityService.evaluate(context, template.getBody());

		return resultVelocity;
	}

}
