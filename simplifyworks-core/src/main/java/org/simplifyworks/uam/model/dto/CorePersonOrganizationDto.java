package org.simplifyworks.uam.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.uam.model.annotation.Permission;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CorePersonOrganizationDto extends AbstractDto {

	private CorePersonDto person;
	private CoreOrganizationDto organization;
	private String category;
	private Date validFrom;
	private Date validTo;
	private BigDecimal obligation;

	public CorePersonOrganizationDto() {
	}

	public CorePersonOrganizationDto(Long id) {
		super(id);
	}

	public CorePersonDto getPerson() {
		return person;
	}

	public void setPerson(CorePersonDto person) {
		this.person = person;
	}

	public CoreOrganizationDto getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganizationDto organization) {
		this.organization = organization;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getObligation() {
		return obligation;
	}

	public void setObligation(BigDecimal obligation) {
		this.obligation = obligation;
	}

}
