/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreRoleCompositionDto extends AbstractDto {

	private CoreRoleDto superiorRole;
	private CoreRoleDto subRole;

	public CoreRoleCompositionDto() {
	}

	public CoreRoleCompositionDto(Long id) {
		super(id);
	}

	public CoreRoleDto getSuperiorRole() {
		return superiorRole;
	}

	public void setSuperiorRole(CoreRoleDto superiorRole) {
		this.superiorRole = superiorRole;
	}

	public CoreRoleDto getSubRole() {
		return subRole;
	}

	public void setSubRole(CoreRoleDto subRole) {
		this.subRole = subRole;
	}

}
