package org.simplifyworks.uam.model.dto;

import java.util.List;

public class CoreUserWithPersonAndRolesDto extends CoreUserWithPersonDto {

	private List<CoreUserRoleDto> roles;
	
	public List<CoreUserRoleDto> getRoles() {
		return roles;
	}
	
	public void setRoles(List<CoreUserRoleDto> roles) {
		this.roles = roles;
	}
}
