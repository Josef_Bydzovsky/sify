/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.dto;

import org.simplifyworks.workflow.model.entity.Workflowable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * Security user
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class)
public class CoreUserWithPersonDto extends CoreUserDto implements Workflowable {

	private CorePersonDto person;

	public CoreUserWithPersonDto() {
	}

	public CoreUserWithPersonDto(Long id) {
		super(id);
	}

	public CoreUserWithPersonDto(String username, char[] password) {
		super(username, password);
	}

	public CorePersonDto getPerson() {
		return person;
	}

	public void setPerson(CorePersonDto person) {
		this.person = person;
	}
}
