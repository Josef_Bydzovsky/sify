/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
public class CoreAddress extends AbstractEntity {

	@Size(max = 200)
	@Column(name = "NOTE")
	private String note;
	@Size(max = 50)
	@Column(name = "STREET")
	private String street;
	@Size(max = 50)
	@Column(name = "CITY")
	private String city;
	@Size(max = 10)
	@Column(name = "POSTAL_CODE")
	private String postalCode;
	@Size(max = 50)
	@Column(name = "COUNTRY")
	private String country;

	@JoinColumn(name = "CORE_PERSON_ID", referencedColumnName = "ID")
	@ManyToOne(optional = true) // adresa se muze vazat i na jine entity pres vazebku napr.
	private CorePerson person;

	public CoreAddress() {
	}

	public CoreAddress(Long id) {
		super(id);
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public CorePerson getPerson() {
		return person;
	}

	public void setPerson(CorePerson person) {
		this.person = person;
	}

}
