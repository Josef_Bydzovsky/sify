/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
public class CoreContact extends AbstractEntity {

	@Size(max = 255)
	@Column(name = "CONTACT")
	private String contact;
	@Basic(optional = false)
	@NotNull
	@JoinColumn(name = "CORE_PERSON_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne
	private CorePerson person;

	public CoreContact() {
	}

	public CoreContact(Long id) {
		super(id);
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public CorePerson getPerson() {
		return person;
	}

	public void setPerson(CorePerson person) {
		this.person = person;
	}

}
