/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Inheritance (strategy = InheritanceType.JOINED)
public class CoreOrganization extends AbstractEntity {

	@NotNull
	@Basic(optional = false)
	@Column(nullable = false, length = 100)
	@Size(max = 100)
	private String name;
	@JoinColumn(name = "PARENT_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne
	private CoreOrganization organization;
	@Column(name = "PARENT_ID")
	private Long organizationId;
	@Size(max = 50)
	@Column(length = 50)
	private String code;
	@Size(max = 50)
	@Column(length = 50)
	private String abbreviation;

	@OneToMany(mappedBy = "organization")
	private List<CoreUserRole> userRoles;
	@OrderBy("name")
	@OneToMany(mappedBy = "organization")
	private List<CoreOrganization> organizations;

	public CoreOrganization() {
	}

	public CoreOrganization(Long id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CoreOrganization getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganization organization) {
		this.organization = organization;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

}
