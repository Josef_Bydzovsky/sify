
package org.simplifyworks.uam.model.entity;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;
import org.simplifyworks.uam.model.domain.PermissionActionType;
import org.simplifyworks.uam.model.domain.PermissionFor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Basic permission for entity
 *
 * @author Vít Švanda
 */
@Entity
@Inheritance (strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn (name="DISCRIMINATOR")
@DiscriminatorValue("ENTITY")
@Table(name = "CORE_PERMISSION")
//@Table(name = "CORE_PERMISSION", uniqueConstraints = @UniqueConstraint(columnNames = {"WF_DEFINITION_ID", "WF_USER_TASK", "WF_BUTTON_KEY"}))
public class CorePermission extends AbstractEntity {

    @NotNull
    @Column(name = "ACTIVE", nullable = false)
    private Boolean active = Boolean.TRUE;
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "PERMISSION_FOR", nullable = false)
    private PermissionFor permissionFor = PermissionFor.ENTITY;
    @Column(name = "OBJECT_IDENTIFIER")
    @Size(max = 100)
    private String objectIdentifier;
    @NotNull
    @Size(max = 150)
    @Column(name = "OBJECT_TYPE", nullable = false)
    private String objectType;
    @JoinColumn(name = "CORE_USER_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private CoreUser user;
    @JoinColumn(name = "CORE_ORGANIZATION_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private CoreOrganization organization;
    @JoinColumn(name = "CORE_ROLE_ID", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private CoreRole role;
    //Action type
    @NotNull
    @Column(name = "CAN_READ", nullable = false)
    private Boolean canRead = Boolean.FALSE;
    @NotNull
    @Column(name = "CAN_WRITE", nullable = false)
    private Boolean canWrite = Boolean.FALSE;
    @NotNull
    @Column(name = "CAN_DELETE", nullable = false)
    private Boolean canDelete = Boolean.FALSE;


    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public PermissionFor getPermissionFor() {
        return permissionFor;
    }

    public void setPermissionFor(PermissionFor permissionFor) {
        this.permissionFor = permissionFor;
    }

    public String getObjectIdentifier() {
        return objectIdentifier;
    }

    public void setObjectIdentifier(String objectIdentifier) {
        this.objectIdentifier = objectIdentifier;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public CoreUser getUser() {
        return user;
    }

    public void setUser(CoreUser user) {
        this.user = user;
    }

    public CoreOrganization getOrganization() {
        return organization;
    }

    public void setOrganization(CoreOrganization organization) {
        this.organization = organization;
    }

    public Boolean getCanRead() {
        return canRead;
    }

    public void setCanRead(Boolean canRead) {
        this.canRead = canRead;
    }

    public Boolean getCanWrite() {
        return canWrite;
    }

    public void setCanWrite(Boolean canWrite) {
        this.canWrite = canWrite;
    }

    public Boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    public CoreRole getRole() {
        return role;
    }

    public void setRole(CoreRole role) {
        this.role = role;
    }
}
