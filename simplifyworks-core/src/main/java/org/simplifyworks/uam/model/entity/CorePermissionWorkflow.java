
package org.simplifyworks.uam.model.entity;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.domain.PermissionActionType;
import org.simplifyworks.uam.model.domain.PermissionFor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Workflow permission
 *
 * @author Vít Švanda
 */
@Entity
@DiscriminatorValue("WORKFLOW")
public class CorePermissionWorkflow extends CorePermission {

    @Column(name = "WF_DEFINITION_ID")
    private String wfDefinitionId;
    @Column(name = "WF_BUTTON_KEY")
    private String wfButtonKey;
    @Column(name = "WF_USER_TASK")
    private String wfUserTask;
    @Enumerated(EnumType.STRING)
    @Column(name = "WF_ACTION_TYPE")
    private PermissionActionType permissionActionType = PermissionActionType.NONE;

    public String getWfButtonKey() {
        return wfButtonKey;
    }

    public void setWfButtonKey(String wfButtonKey) {
        this.wfButtonKey = wfButtonKey;
    }

    public String getWfUserTask() {
        return wfUserTask;
    }

    public void setWfUserTask(String wfUserTask) {
        this.wfUserTask = wfUserTask;
    }

    public PermissionActionType getPermissionActionType() {
        return permissionActionType;
    }

    public void setPermissionActionType(PermissionActionType permissionActionType) {
        this.permissionActionType = permissionActionType;
    }

    public String getWfDefinitionId() {
        return wfDefinitionId;
    }

    public void setWfDefinitionId(String wfDefinitionId) {
        this.wfDefinitionId = wfDefinitionId;
    }

}
