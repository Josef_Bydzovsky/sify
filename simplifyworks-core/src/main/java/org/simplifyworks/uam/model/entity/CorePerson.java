/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Permission(workplaceField = "testOrg", createRoles = {"admin"}, readRoles = {"admin"}, writeRoles = {"admin"}, deleteRoles = {"admin"})
public class CorePerson extends AbstractEntity {

	@Column(name = "PERSONAL_NUMBER")
	private String personalNumber;
	@NotNull
	@Column(name = "SURNAME", nullable = false)
	@Size(max = 100)
	private String surname;
	@Column(name = "FIRSTNAME")
	@Size(max = 100)
	private String firstname;
	@Column(name = "TITLE_BEFORE")
	@Size(max = 100)
	private String titleBefore;
	@Column(name = "TITLE_AFTER")
	@Size(max = 100)
	private String titleAfter;
	@Email
	@Column(name = "EMAIL")
	@Size(max = 255)
	private String email;
	@Column(name = "PHONE")
	@Size(max = 30)
	private String phone;
	@Size(max = 200)
	@Column(name = "NOTE")
	private String note;
	@Size(max = 100)
	@Column(name = "ADDRESSING_PHRASE")
	private String addressingPhrase;

	@OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
	private List<CoreAddress> addresses;
	@OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
	private List<CoreContact> contacts;
	@OneToMany(mappedBy = "person")
	private List<CoreUser> users;
	@OneToMany(mappedBy = "person")
	private List<CorePersonOrganization> personOrganizations;

	//Only for test Permission
	@JoinColumn(name = "test_org", referencedColumnName = "ID")
	@ManyToOne
	private CoreOrganization testOrg;
	//Only for test Permission

	public CorePerson() {
	}

	public CoreOrganization getTestOrg() {
		return testOrg;
	}

	public void setTestOrg(CoreOrganization testOrg) {
		this.testOrg = testOrg;
	}

	public CorePerson(Long id) {
		super(id);
	}

	public String getPersonalNumber() {
		return personalNumber;
	}

	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getTitleBefore() {
		return titleBefore;
	}

	public void setTitleBefore(String titleBefore) {
		this.titleBefore = titleBefore;
	}

	public String getTitleAfter() {
		return titleAfter;
	}

	public void setTitleAfter(String titleAfter) {
		this.titleAfter = titleAfter;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getAddressingPhrase() {
		return addressingPhrase;
	}

	public void setAddressingPhrase(String addressingPhrase) {
		this.addressingPhrase = addressingPhrase;
	}

}
