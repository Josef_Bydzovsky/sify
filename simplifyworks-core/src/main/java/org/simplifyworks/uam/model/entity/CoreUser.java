/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Formula;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;
import org.simplifyworks.workflow.model.entity.Workflowable;

import com.google.common.collect.Lists;

/**
 * Security user
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Entity
@Permission(createRoles = {"admin","user"}, readRoles = {"user","admin"}, writeRoles = {"admin"}, deleteRoles = {"admin"})
public class CoreUser extends AbstractEntity implements Workflowable {

	@NotNull
	@Size(min = 3, max = 50)
	@Column(length = 50, nullable = false, unique = true, updatable = false)
	private String username;
	@Column
	private char[] password;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin;
	@JoinColumn(name = "CORE_PERSON_ID", referencedColumnName = "ID")
	@ManyToOne(optional = true)
	private CorePerson person;
	@OneToMany(mappedBy = "user")
	private List<CoreUserRole> roles;
	@Formula(value = "(select WF.WF_PROCESS_INSTANCE_ID from core_workflow_entity WF where WF.OBJECT_IDENTIFIER=id AND WF.OBJECT_TYPE = 'org.simplifyworks.uam.model.entity.CoreUser')")
	private String wfProcessInstanceId;


	public CoreUser() {
	}

	public CoreUser(Long id) {
		super(id);
	}

	public CoreUser(String username, char[] password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public CorePerson getPerson() {
		return person;
	}

	public void setPerson(CorePerson person) {
		this.person = person;
	}

	public List<CoreUserRole> getRoles() {
		if(roles == null) {
			roles = Lists.newArrayList();
		}
		return roles;
	}

	public void setRoles(List<CoreUserRole> roles) {
		this.roles = roles;
	}

	/**
	 * TODO: move to additional user information
	 *
	 * @return
	 */
	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	@Override
	public String getWfProcessInstanceId() {
		return wfProcessInstanceId;
	}

	@Override
	public void setWfProcessInstanceId(String wfProcessInstanceId) {
		this.wfProcessInstanceId = wfProcessInstanceId;
	}
}
