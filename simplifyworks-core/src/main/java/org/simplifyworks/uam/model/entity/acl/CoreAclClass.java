/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity.acl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
@Entity
public class CoreAclClass extends AbstractEntity {

	@NotNull
	@Column(name = "class", nullable = false)
	private String clazz;

	public CoreAclClass() {
	}

	public CoreAclClass(Long id) {
		super(id);
	}

	public CoreAclClass(String clazz) {
		this.clazz = clazz;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
}
