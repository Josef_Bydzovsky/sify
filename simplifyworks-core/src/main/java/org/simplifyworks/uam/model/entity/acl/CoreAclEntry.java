/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity.acl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
@Entity
public class CoreAclEntry extends AbstractEntity implements Comparable<CoreAclEntry> {

	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "acl_object_identity", referencedColumnName = "ID", nullable = false)
	private CoreAclObjectIdentity aclObjectIdentity;
	@NotNull
	@Column(name = "ace_order", nullable = false)
	private int aceOrder;
	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "sid", referencedColumnName = "ID", nullable = false)
	private CoreAclSid sid;
	@NotNull
	@Column(name = "mask", nullable = false)
	private int mask;
	@NotNull
	@Column(name = "granting", nullable = false)
	private boolean granting;
	@NotNull
	@Column(name = "audit_success", nullable = false)
	private boolean auditSuccess;
	@NotNull
	@Column(name = "audit_failure", nullable = false)
	private boolean auditFailure;

	public CoreAclObjectIdentity getAclObjectIdentity() {
		return aclObjectIdentity;
	}

	public void setAclObjectIdentity(CoreAclObjectIdentity aclObjectIdentity) {
		this.aclObjectIdentity = aclObjectIdentity;
	}

	public int getAceOrder() {
		return aceOrder;
	}

	public void setAceOrder(int aceOrder) {
		this.aceOrder = aceOrder;
	}

	public CoreAclSid getSid() {
		return sid;
	}

	public void setSid(CoreAclSid sid) {
		this.sid = sid;
	}

	public int getMask() {
		return mask;
	}

	public void setMask(int mask) {
		this.mask = mask;
	}

	public boolean isGranting() {
		return granting;
	}

	public void setGranting(boolean granting) {
		this.granting = granting;
	}

	public boolean isAuditSuccess() {
		return auditSuccess;
	}

	public void setAuditSuccess(boolean auditSuccess) {
		this.auditSuccess = auditSuccess;
	}

	public boolean isAuditFailure() {
		return auditFailure;
	}

	public void setAuditFailure(boolean auditFailure) {
		this.auditFailure = auditFailure;
	}

	@Override
	public int compareTo(CoreAclEntry o) {
		return Integer.valueOf(this.aceOrder).compareTo(o.aceOrder);
	}
}
