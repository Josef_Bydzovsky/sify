/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity.acl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
@Entity
public class CoreAclObjectIdentity extends AbstractEntity {

	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "OBJECT_ID_CLASS", referencedColumnName = "ID", nullable = false)
	private CoreAclClass objectIdClass;
	@NotNull
	@Column(name = "OBJECT_ID_IDENTITY", nullable = false)
	private Long objectIdIdentity;
	@ManyToOne
	@JoinColumn(name = "PARENT_OBJECT", referencedColumnName = "ID")
	private CoreAclObjectIdentity parentObject;
	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "OWNER_SID", referencedColumnName = "ID", nullable = false)
	private CoreAclSid ownerSid;
	@NotNull
	@Column(name = "ENTRIES_INHERITING", nullable = false)
	private boolean entriesInheriting;

	public CoreAclClass getObjectIdClass() {
		return objectIdClass;
	}

	public void setObjectIdClass(CoreAclClass objectIdClass) {
		this.objectIdClass = objectIdClass;
	}

	public Long getObjectIdIdentity() {
		return objectIdIdentity;
	}

	public void setObjectIdIdentity(Long objectIdIdentity) {
		this.objectIdIdentity = objectIdIdentity;
	}

	public CoreAclObjectIdentity getParentObject() {
		return parentObject;
	}

	public void setParentObject(CoreAclObjectIdentity parentObject) {
		this.parentObject = parentObject;
	}

	public CoreAclSid getOwnerSid() {
		return ownerSid;
	}

	public void setOwnerSid(CoreAclSid ownerSid) {
		this.ownerSid = ownerSid;
	}

	public boolean isEntriesInheriting() {
		return entriesInheriting;
	}

	public void setEntriesInheriting(boolean entriesInheriting) {
		this.entriesInheriting = entriesInheriting;
	}
}
