/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity.acl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
@Entity
public class CoreAclSid extends AbstractEntity {

	@NotNull
	@Column(name = "principal", nullable = false)
	private boolean principal;
	@NotNull
	@Column(name = "sid", nullable = false)
	private String sid;

	public CoreAclSid() {
	}

	public CoreAclSid(Long id) {
		super(id);
	}

	public CoreAclSid(boolean principal, String sid) {
		this.principal = principal;
		this.sid = sid;
	}

	public boolean isPrincipal() {
		return principal;
	}

	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}
}
