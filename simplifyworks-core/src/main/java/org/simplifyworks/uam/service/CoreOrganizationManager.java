/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service;

import java.util.List;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.entity.CoreOrganization;

/**
 * Bludistak
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface CoreOrganizationManager extends ReadWriteManager<CoreOrganizationDto, CoreOrganization> {
    
    public List<CoreOrganizationDto> getOrganizationByCode(String org);

}
