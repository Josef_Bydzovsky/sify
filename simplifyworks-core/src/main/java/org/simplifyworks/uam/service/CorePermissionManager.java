package org.simplifyworks.uam.service;

import java.util.List;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.annotation.Permission;
import org.simplifyworks.uam.model.dto.CorePermissionDto;
import org.simplifyworks.uam.model.entity.CorePermission;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
public interface CorePermissionManager extends ReadWriteManager<CorePermissionDto, CorePermission> {

    List<CorePermissionDto> createPermissions(Permission permission, AbstractEntity entity);
}