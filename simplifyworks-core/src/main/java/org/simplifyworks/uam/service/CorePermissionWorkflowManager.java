package org.simplifyworks.uam.service;

import java.util.List;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.dto.CorePermissionWorkflowDto;
import org.simplifyworks.uam.model.entity.CorePermissionWorkflow;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
public interface CorePermissionWorkflowManager extends ReadWriteManager<CorePermissionWorkflowDto, CorePermissionWorkflow> {

    List<CorePermissionWorkflowDto> searchPermissionsForWorkflow(String definitionId, String buttonKey, String userTaskId);

    /**
     * Check workflow permission on write right
     * @param permissions
     * @return
     */
    boolean hasPermissionForWrite(List<CorePermissionWorkflowDto> permissions);
}