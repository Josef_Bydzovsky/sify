/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.simplifyworks.uam.model.entity.CorePerson;

/**
 * Bludistak
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface CorePersonManager extends ReadWriteManager<CorePersonDto, CorePerson> {

}
