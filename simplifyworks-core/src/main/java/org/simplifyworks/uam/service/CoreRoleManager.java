
package org.simplifyworks.uam.service;

import java.util.List;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.entity.CoreRole;

/**
 * Role manager
 *
 * @author VŠ
 */
public interface CoreRoleManager extends ReadWriteManager<CoreRoleDto, CoreRole> {
	
     public List<CoreRoleDto> getRoleByName(String role);
}
