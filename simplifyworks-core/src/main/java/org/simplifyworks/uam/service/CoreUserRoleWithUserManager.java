/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreUserRoleWithUserDto;
import org.simplifyworks.uam.model.entity.CoreUserRole;

/**
 * 
 * @author Svanda
 */
public interface CoreUserRoleWithUserManager extends ReadWriteManager<CoreUserRoleWithUserDto, CoreUserRole> {

}
