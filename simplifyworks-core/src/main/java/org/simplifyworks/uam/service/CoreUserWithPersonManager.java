
package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonDto;
import org.simplifyworks.uam.model.entity.CoreUser;

/**
 * User manager
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface CoreUserWithPersonManager extends ReadWriteManager<CoreUserWithPersonDto, CoreUser> {

    public CoreUserWithPersonDto findByUsername(String username);
}
