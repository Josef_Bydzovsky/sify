package org.simplifyworks.uam.service;

import java.util.List;

/**
 * Service that provide access to organization structure
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface OrganizationStructureService {	
	
	/**
	 * Finds all ancestor organization names
	 * 
	 * @param organizationName name of organization where searching starts 
	 * @return names of all ancestors (including organizationName)
	 */
	public List<String> findAllAncestorOrganizations(String organizationName);
	
	/**
	 * Rebuilds whole structure
	 */
	public void rebuild();	
}
