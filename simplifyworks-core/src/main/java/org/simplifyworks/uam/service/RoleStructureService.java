package org.simplifyworks.uam.service;

import java.util.List;

/**
 * Service that provide access to role structure 
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface RoleStructureService {
	
	/**
	 * Finds all ancestor role names
	 * 
	 * @param roleName name of role where searching starts 
	 * @return names of all ancestors (including roleName)
	 */
	public List<String> findAllAncestorRoles(String roleName);
	
	/**
	 * Rebuilds whole structure
	 */
	public void rebuild();
}
