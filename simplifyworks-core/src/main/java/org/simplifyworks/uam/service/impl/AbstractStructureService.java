package org.simplifyworks.uam.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.search.SearchHit;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.ElasticsearchService;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Index and template based implementation for structure services
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
abstract class AbstractStructureService<E extends AbstractEntity, C extends AbstractEntity> {

	@Autowired
	private Client client;
	
	private ObjectMapper jsonMapper;
	
	public AbstractStructureService() {
		jsonMapper = new ObjectMapper();
	}
	
	/**
	 * Finds all ids of ancestors for specified node.
	 * 
	 * @param type type of structure
	 * @param id id of initial node
	 * @return ids of all ancestors (including id)
	 */
	protected List<String> findAllAncestors(String type, String id) {
		return findAllRelated(type, id, false);
	}
	
	/**
	 * Finds all ids of descendants for specified node.
	 * 
	 * @param type type of structure
	 * @param id id of initial node
	 * @return ids of all descendants (including id)
	 */
	protected List<String> findAllDescendants(String type, String id) {
		return findAllRelated(type, id, true);
	}
	
	private List<String> findAllRelated(String type, String id, boolean descendants) {
		try {
			List<String> relatedIds = new ArrayList<>();
			
			Iterable<SearchHit> hits = client
					.prepareSearch(ElasticsearchService.INDEX_NAME)
					.setTypes(type)
					.setQuery(FilterBuilders.termFilter(descendants ? "parent" : "child", id)
					.buildAsBytes())
					.get().getHits();
			
			for(SearchHit hit : hits) {
				StructureElement element = jsonMapper.readValue(hit.getSourceAsString(), StructureElement.class);
				
				relatedIds.add(descendants ? element.getChild() : element.getParent());
			}
			
			return relatedIds;
		} catch (IOException e) {
			throw new CoreException(e);
		}
	}

	/**
	 * Rebuilds (deletes and creates) index using type.
	 *  
	 * @param type type of structure
	 */
	protected void rebuild(String type) {
		try {
			if(existsIndex()) {			
				deleteStructure(type);
			} else {
				createIndex();
			}

			createStructure(type);
		} catch (Exception e) {
			throw new CoreException(e);
		}
	}
	
	/**
	 * Finds all entities needed for structure
	 * 
	 * @return all entities
	 */
	protected abstract List<E> findAllEntities();
	
	/**
	 * Finds all compositions needed for structure
	 * 
	 * @return all compositions
	 */
	protected abstract List<C> findAllCompositions();
	
	/**
	 * Returns parent entity for specified composition
	 * 
	 * @param composition composition (returned from {@link findAllCompositions})
	 * @return parent entity for composition
	 */
	protected abstract E getParent(C composition);
	
	/**
	 * Returns child entity for specified composition
	 * 
	 * @param composition composition (returned from {@link findAllCompositions})
	 * @return child entity for composition
	 */
	protected abstract E getChild(C composition);
	
	/**
	 * Returns some unique identifier for specified entity
	 * 
	 * @param entity entity
	 * @return unique identifier (will be indexed)
	 */
	protected abstract String getId(E entity);
	
	private boolean existsIndex() {
		return client.admin().indices().prepareExists(ElasticsearchService.INDEX_NAME).get().isExists();
	}
	
	private void createIndex() {
		client.admin().indices().prepareCreate(ElasticsearchService.INDEX_NAME).get();
	}
	
	/**
	 * Deletes whole structure in index using bulk delete
	 */
	private void deleteStructure(String type) {		
		BulkRequestBuilder builder = client.prepareBulk();

		for(SearchHit hit : client.prepareSearch(ElasticsearchService.INDEX_NAME).setTypes(type).get().getHits()) {
			builder.add(client.prepareDelete(ElasticsearchService.INDEX_NAME, type, hit.getId()));
		}

		if(builder.numberOfActions() > 0) {
			builder.get();
		}
	}

	/**
	 * Creates whole structure in index using bulk create
	 */
	private void createStructure(String type) throws JsonProcessingException {
		BulkRequestBuilder builder = client.prepareBulk();

		for (StructureElement structureElement : prepareAllStructureElements()) {
			builder.add(client.prepareIndex(ElasticsearchService.INDEX_NAME, type)
					.setSource(jsonMapper.writeValueAsBytes(structureElement)).request());
		}

		if(builder.numberOfActions() > 0) {
			builder.get();
		}
	}

	/**
	 * Prepares all structure elements using descendants of all objects
	 */
	private Set<StructureElement> prepareAllStructureElements() {
		Set<StructureElement> structureElements = new HashSet<>();

		List<E> entities = findAllEntities();
		List<C> compositions = findAllCompositions();

		for (E entity : entities) {
			for (E descendant : prepareDescendants(entity, compositions)) {
				structureElements.add(new StructureElement(getId(entity), getId(descendant)));
			}
		}

		return structureElements;
	}

	/**
	 * Finds descendants (all generations of children) of parent
	 */
	private Set<E> prepareDescendants(E entity, List<C> compositions) {
		Set<E> descendants = new HashSet<>();
		descendants.add(entity);

		for (E child : prepareChildren(entity, compositions)) {
			descendants.addAll(prepareDescendants(child, compositions));
		}

		return descendants;
	}

	/**
	 * Finds children (only nearest generation) of parent
	 */
	private Set<E> prepareChildren(E entity, List<C> compositions) {
		Set<E> childs = new HashSet<>();

		for (C composition : compositions) {
			if (entity.equals(getParent(composition))) {
				childs.add(getChild(composition));
			}
		}

		return childs;
	}
}