/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.List;
import org.elasticsearch.index.query.FilterBuilders;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.service.CoreOrganizationManager;
import org.springframework.stereotype.Service;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CoreOrganizationManagerImpl extends DefaultReadWriteManager<CoreOrganizationDto, CoreOrganization> implements CoreOrganizationManager {

	public void onCoreOrganizationEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CoreOrganization_.id.getName(), event.getEntity().getId()));
	}

	@Override
	public List<CoreOrganizationDto> getOrganizationByCode(String org) {
		SearchParameters searchParameters = new SearchParameters();
		FilterValue filterValue = new FilterValue();
		filterValue.setPropertyName(CoreOrganization_.code.getName());
		filterValue.setValue(org);
		filterValue.setOperator(FilterOperator.EQUALS);
		searchParameters.addFilter(filterValue);
		List<CoreOrganizationDto> resultOrg = search(searchParameters);
		return resultOrg;
	}

}
