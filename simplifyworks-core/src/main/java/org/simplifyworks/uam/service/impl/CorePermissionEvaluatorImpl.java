/**
 * VŠ
 */
package org.simplifyworks.uam.service.impl;

import java.io.Serializable;

import org.simplifyworks.uam.service.PermissionEvaluator;
import org.springframework.security.core.Authentication;

/**
 *
 * @author Svanda
 */
public class CorePermissionEvaluatorImpl implements PermissionEvaluator {

	@Override
	public boolean hasPermission(Authentication a, Object o, Object role) {
		//TODO: rekurzivna overit z role a organizace jestli máme právo (budeme potřebovat plochý seznam UserInOrg)	
		return true;
	}

	@Override
	public boolean hasPermission(Authentication a, Serializable srlzbl, String string, Object o) {
			throw new UnsupportedOperationException("Not supported yet.");
	}

}
