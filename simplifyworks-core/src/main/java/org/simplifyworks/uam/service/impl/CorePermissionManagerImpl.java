package org.simplifyworks.uam.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.annotation.Permission;
import org.simplifyworks.uam.model.domain.PermissionFor;
import org.simplifyworks.uam.model.dto.CorePermissionDto;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.service.CorePermissionManager;
import org.simplifyworks.uam.service.CoreRoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.model.entity.CoreRole_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.model.entity.CorePermission_;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
@Service
public class CorePermissionManagerImpl extends DefaultReadWriteManager<CorePermissionDto, CorePermission> implements CorePermissionManager {

	@Autowired
	private CoreRoleManager coreRoleManager;

	public void onCoreOrganizationEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CorePermission_.organization.getName() + "." + CoreOrganization_.id.getName(), event.getEntity().getId()));
	}

	public void onCorePermissionEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CorePermission_.id.getName(), event.getEntity().getId()));
	}

	public void onCoreRoleEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CorePermission_.role.getName() + "." + CoreRole_.id.getName(), event.getEntity().getId()));
	}

	public void onCoreUserEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CorePermission_.user.getName() + "." + CoreUser_.id.getName(), event.getEntity().getId()));
	}

	@Override
	public List<CorePermissionDto> createPermissions(Permission permission, AbstractEntity entity) {
		String orgField = permission.workplaceField();
		CoreOrganization org = null;

		if (!StringUtils.isEmpty(orgField)) {
			String methodName = "get" + orgField.substring(0, 1).toUpperCase() + orgField.substring(1);
			try {
				Method method = entity.getClass().getMethod(methodName);
				org = (CoreOrganization) method.invoke(entity);

			} catch (NoSuchMethodException e) {
				throw new CoreException("Organization not found.", e);
			} catch (InvocationTargetException e) {
				throw new CoreException("Organization not found.", e);
			} catch (IllegalAccessException e) {
				throw new CoreException("Organization not found.", e);
			}

		}

		Map<String, CorePermission> roleMap = new HashMap<String, CorePermission>();

		List<CorePermissionDto> permissionResult = Lists.newArrayList();

		createPermission(permission.deleteRoles(), roleMap, entity, org, true, false, false);
		createPermission(permission.readRoles(), roleMap, entity, org, false, true, false);
		createPermission(permission.writeRoles(), roleMap, entity, org, false, false, true);

		Collection<CorePermission> values = roleMap.values();
		for (CorePermission p : values) {
			permissionResult.add(create(toDto(p)));
		}
		return permissionResult;
	}

	private void createPermission(String[] roles, Map<String, CorePermission> roleMap, AbstractEntity entity, CoreOrganization org, boolean delete, boolean read, boolean write) {

		if (roles != null) {
			for (String role : roles) {
				CorePermission permission = null;
				if (!roleMap.containsKey(role)) {
					permission = new CorePermission();
					List<CoreRoleDto> resultRoles = coreRoleManager.getRoleByName(role);
					if (resultRoles == null || resultRoles.isEmpty() || resultRoles.size() != 1) {
						throw new CoreException("Cannot found role with name " + role);
					}
					permission.setRole(coreRoleManager.toEntity(resultRoles.get(0)));
				} else {
					permission = roleMap.get(role);
				}

				if (delete) {
					permission.setCanDelete(Boolean.TRUE);
				}
				if (read) {
					permission.setCanRead(Boolean.TRUE);
				}
				if (write) {
					permission.setCanWrite(Boolean.TRUE);
				}
				permission.setObjectType(entity.getClass().getCanonicalName());
				permission.setObjectIdentifier(String.valueOf(entity.getId()));
				permission.setActive(Boolean.TRUE);
				permission.setOrganization(org);
				permission.setPermissionFor(PermissionFor.ENTITY);

				if (!roleMap.containsKey(role)) {
					roleMap.put(role, permission);
				}

			}
		}
	}

}
