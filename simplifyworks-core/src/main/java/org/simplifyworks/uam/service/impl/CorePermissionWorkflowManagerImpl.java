package org.simplifyworks.uam.service.impl;

import java.util.List;
import org.elasticsearch.index.query.FilterBuilders;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.dto.CorePermissionWorkflowDto;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleWithUserDto;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.model.entity.CorePermissionWorkflow;
import org.simplifyworks.uam.model.entity.CorePermissionWorkflow_;
import org.simplifyworks.uam.model.entity.CorePermission_;
import org.simplifyworks.uam.model.entity.CoreRole_;
import org.simplifyworks.uam.model.entity.CoreUserRole_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CorePermissionWorkflowManager;
import org.simplifyworks.uam.service.CoreUserRoleWithUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
@Service
public class CorePermissionWorkflowManagerImpl extends DefaultReadWriteManager<CorePermissionWorkflowDto, CorePermissionWorkflow> implements CorePermissionWorkflowManager {

	@Autowired
	private CoreUserRoleWithUserManager coreUserRoleManager;

	public void onCoreOrganizationEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CorePermission_.organization.getName() + "." + CoreOrganization_.id.getName(), event.getEntity().getId()));
	}

	public void onCorePermissionEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CorePermission_.id.getName(), event.getEntity().getId()));
	}

	public void onCoreRoleEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CorePermission_.role.getName() + "." + CoreRole_.id.getName(), event.getEntity().getId()));
	}

	public void onCoreUserEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CorePermission_.user.getName() + "." + CoreUser_.id.getName(), event.getEntity().getId()));
	}

	@Override
	public List<CorePermissionWorkflowDto> searchPermissionsForWorkflow(String definitionId, String buttonKey, String userTaskId) {

		SearchParameters searchParameters = new SearchParameters();
//		FilterValue filterValue = new FilterValue();
//		filterValue.setPropertyName(CorePermissionWorkflow_.wfDefinitionId.getName());
//		filterValue.setValue(definitionId.toLowerCase());
//		filterValue.setOperator(FilterOperator.EQUALS);
//		searchParameters.addFilter(filterValue);

		FilterValue filterValueUserTask = new FilterValue();
		filterValueUserTask.setPropertyName(CorePermissionWorkflow_.wfUserTask.getName());
		filterValueUserTask.setValue(userTaskId);
		filterValueUserTask.setOperator(FilterOperator.EQUALS);
		searchParameters.addFilter(filterValueUserTask);
//
		FilterValue filterValueButton = new FilterValue();
		filterValueButton.setPropertyName(CorePermissionWorkflow_.wfButtonKey.getName());
		filterValueButton.setValue(buttonKey.toLowerCase());
		filterValueButton.setOperator(FilterOperator.EQUALS);
		searchParameters.addFilter(filterValueButton);

		return search(searchParameters);
	}

	@Override
	public boolean hasPermissionForWrite(List<CorePermissionWorkflowDto> permissions) {
		if (permissions == null) {
			return false;
		}
		for (CorePermissionWorkflowDto p : permissions) {
			if (p.getCanWrite()) {
				CoreRoleDto role = p.getRole();
				CoreOrganizationDto organization = p.getOrganization();

				String currentUsername = securityService.getUsername();

				SearchParameters searchParameters = new SearchParameters();
				FilterValue filterValueUser = new FilterValue();
				filterValueUser.setPropertyName(CoreUserRole_.user.getName() + "." + CoreUser_.username.getName());
				filterValueUser.setValue(currentUsername);
				filterValueUser.setOperator(FilterOperator.EQUALS);
				searchParameters.addFilter(filterValueUser);

				FilterValue filterValueRole = new FilterValue();
				filterValueRole.setPropertyName(CoreUserRole_.role.getName() + "." + CoreRole_.name.getName());
				filterValueRole.setValue(role.getName());
				filterValueRole.setOperator(FilterOperator.EQUALS);
				searchParameters.addFilter(filterValueRole);

				if (organization != null) {
					FilterValue filterValueOrg = new FilterValue();
					filterValueOrg.setPropertyName(CoreUserRole_.organization.getName() + "." + CoreOrganization_.code.getName());
					filterValueOrg.setValue(organization.getCode());
					filterValueOrg.setOperator(FilterOperator.EQUALS);
					searchParameters.addFilter(filterValueOrg);
				}

				List<CoreUserRoleWithUserDto> result = coreUserRoleManager.search(searchParameters);

				if (result != null && !result.isEmpty()) {
					return true;
				}
			}
		}
		return false;
	}
}
