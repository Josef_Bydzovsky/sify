/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationDto;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.model.entity.CorePersonOrganization;
import org.simplifyworks.uam.model.entity.CorePersonOrganization_;
import org.simplifyworks.uam.model.entity.CorePerson_;
import org.simplifyworks.uam.service.CorePersonOrganizationManager;
import org.springframework.stereotype.Service;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CorePersonOrganizationManagerImpl extends DefaultReadWriteManager<CorePersonOrganizationDto, CorePersonOrganization> implements CorePersonOrganizationManager {

	public void onCoreOrganizationEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CorePersonOrganization_.organization.getName() + "." + CoreOrganization_.id.getName(), event.getEntity().getId()));
	}

	public void onCorePersonEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CorePersonOrganization_.person.getName() + "." + CorePerson_.id.getName(), event.getEntity().getId()));
	}

	public void onCorePersonOrganizationEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CorePersonOrganization_.id.getName(), event.getEntity().getId()));
	}
}
