/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreRoleCompositionDto;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.model.entity.CoreRoleComposition;
import org.simplifyworks.uam.model.entity.CoreRoleComposition_;
import org.simplifyworks.uam.model.entity.CoreRole_;
import org.simplifyworks.uam.service.CoreRoleCompositionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Svanda
 */
@Service
public class CoreRoleCompositionManagerImpl extends DefaultReadWriteManager<CoreRoleCompositionDto, CoreRoleComposition> implements CoreRoleCompositionManager {

	public void onCoreRoleCompositionEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CoreRoleComposition_.id.getName(), event.getEntity().getId()));
	}

	public void onCoreRoleEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.orFilter(
				FilterBuilders.termFilter(CoreRoleComposition_.subRole.getName() + "." + CoreRole_.id.getName(), event.getEntity().getId()),
				FilterBuilders.termFilter(CoreRoleComposition_.superiorRole.getName() + "." + CoreRole_.id.getName(), event.getEntity().getId())));
	}

	@Override
	@Transactional
	public CoreRoleCompositionDto create(CoreRoleCompositionDto dto) {
		return checkResultOfModification(super.create(dto));
	}

	@Override
	@Transactional
	public CoreRoleCompositionDto update(CoreRoleCompositionDto dto) {
		return checkResultOfModification(super.update(dto));
	}

	/**
	 * Checks result of modification (i.e. checks role tree consistency)
	 *
	 * @param result result of modification (create or update)
	 * @return result of modification if role tree is consistent
	 * @throws CoreException if role tree is inconsistent after modification
	 */
	private CoreRoleCompositionDto checkResultOfModification(CoreRoleCompositionDto result) {
		CoreRole searchedRole = toEntity(result).getSubRole();

		if (checkRoleTree(searchedRole, searchedRole)) {
			return result;
		} else {
			throw new CoreException("Role tree inconsistency");
		}
	}

	/**
	 * Checks role tree consistency (looks for forbidden cycles). Recursively
	 * iterates over superiors and looks for specified role (BFS in oriented
	 * graph).
	 *
	 * @param initialRole role where searching starts
	 * @param searchedRole role which is searched
	 * @return true if the role tree is consistent (searched role does not exist
	 * in any of its own superiors - no cycles are detected), false otherwise
	 */
	private boolean checkRoleTree(CoreRole initialRole, CoreRole searchedRole) {
		// FIXME check role tree

		/*
		 SearchParameters searchParameters = new SearchParameters();
		 searchParameters.addFilter(new FilterValue("subRole.id", FilterOperator.EQUALS, Collections.singletonList(initialRole.getId())));
		
		 List<CoreRoleComposition> relatedCompositions = coreRoleCompositionDao.search(searchParameters);
		
		 for(CoreRoleComposition relatedComposition : relatedCompositions) {
		 CoreRole superiorRole = relatedComposition.getSuperiorRole();
			
		 return !superiorRole.equals(searchedRole) && checkRoleTree(superiorRole, searchedRole);
		 }*/
		return true;
	}
}
