package org.simplifyworks.uam.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.uam.model.dto.CoreRoleCompositionDto;
import org.simplifyworks.uam.service.CoreRoleCompositionManager;
import org.simplifyworks.uam.service.CoreRoleHierarchy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.stereotype.Service;

/**
 * Load and set composition coreRoles.
 *
 * @author Svanda
 */
@Service
public class CoreRoleHierarchyImpl extends RoleHierarchyImpl implements CoreRoleHierarchy {

	@Autowired
	private CoreRoleCompositionManager coreRoleCompositionManager;

	private static final Logger LOG = LoggerFactory.getLogger(CoreRoleHierarchyImpl.class);

	@Override
	@PostConstruct
	public void init() {
		List<CoreRoleCompositionDto> allRoleComp = coreRoleCompositionManager.search(new SearchParameters());
		if (allRoleComp == null) {
			return;
		}
		StringBuilder sb = new StringBuilder();
		for (CoreRoleCompositionDto rc : allRoleComp) {
			if (rc.getSubRole() != null && rc.getSuperiorRole() != null) {
				sb.append(rc.getSuperiorRole().getName().toUpperCase());
				sb.append(" > ");
				sb.append(rc.getSubRole().getName().toUpperCase());
				sb.append(" ");
			}
		}
		LOG.debug("Roles hierarchy from DB: " + sb.toString());
		setHierarchy(sb.toString());

	}

}
