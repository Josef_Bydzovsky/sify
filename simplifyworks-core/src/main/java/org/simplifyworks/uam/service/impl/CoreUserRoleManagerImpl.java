/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreUserRoleWithUserDto;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.model.entity.CoreRole_;
import org.simplifyworks.uam.model.entity.CoreUserRole;
import org.simplifyworks.uam.model.entity.CoreUserRole_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CoreUserRoleWithUserManager;
import org.springframework.stereotype.Service;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CoreUserRoleManagerImpl extends DefaultReadWriteManager<CoreUserRoleWithUserDto, CoreUserRole> implements CoreUserRoleWithUserManager {

	public void onCoreOrganizationEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CoreUserRole_.organization.getName() + "." + CoreOrganization_.id.getName(), event.getEntity().getId()));
	}

	public void onCoreRoleEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CoreUserRole_.role.getName() + "." + CoreRole_.id.getName(), event.getEntity().getId()));
	}

	public void onCoreUserEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CoreUserRole_.user.getName() + "." + CoreUser_.id.getName(), event.getEntity().getId()));
	}

	public void onCoreUserRoleEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CoreUserRole_.id.getName(), event.getEntity().getId()));
	}
}
