/**
 *
 */
package org.simplifyworks.uam.service.impl;

import java.util.Collections;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.elasticsearch.index.query.FilterBuilders;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.AccessType;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.impl.DefaultReadManager;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonAndRolesDto;
import org.simplifyworks.uam.model.entity.CorePerson_;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CoreUserWithPersonAndRolesManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class CoreUserWithPersonAndRolesManagerImpl extends DefaultReadManager<CoreUserWithPersonAndRolesDto, CoreUser> implements CoreUserWithPersonAndRolesManager {

	public void onCorePersonEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CoreUser_.person.getName() + "." + CorePerson_.id.getName(), event.getEntity().getId()));
	}

	public void onCoreUserEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CoreUser_.id.getName(), event.getEntity().getId()));
	}

	@Override
	public CoreUserWithPersonAndRolesDto findByUsername(String username) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue("username", FilterOperator.EQUALS, Collections.singletonList(username)));

		return elasticsearchService.searchForSingleResult(dtoClass, parameters, AccessType.READ);
	}
}
