/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.Collections;

import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.TermFilterBuilder;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.service.AccessType;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonDto;
import org.simplifyworks.uam.model.entity.CorePerson_;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CoreUserWithPersonManager;
import org.simplifyworks.workflow.model.entity.CoreWorkflowEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
public class CoreUserWithPersonManagerImpl extends DefaultReadWriteManager<CoreUserWithPersonDto, CoreUser> implements CoreUserWithPersonManager {

	public void onCoreUserEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CoreUser_.id.getName(), event.getEntity().getId()));
	}

	public void onCorePersonEvent(EntityEvent event) {
		reindexChangedDtos(FilterBuilders.termFilter(CoreUser_.person.getName() + "." + CorePerson_.id.getName(), event.getEntity().getId()));
	}

	public void onCoreWorkflowEntityEvent(EntityEvent event) {
		switch (event.getOperation()) {
			case CREATE:
			case UPDATE:
			case REMOVE:
				CoreWorkflowEntity coreWorkflowEntity = (CoreWorkflowEntity) event.getEntity();
				if (CoreUser.class.getCanonicalName().equals((coreWorkflowEntity).getObjectType())) {
					TermFilterBuilder builder = FilterBuilders.termFilter("id", (coreWorkflowEntity).getObjectIdentifier());

					for (CoreUserWithPersonDto user : elasticsearchService.search(CoreUserWithPersonDto.class, builder)) {
						CoreUser entity = entityManager.find(CoreUser.class, user.getId());
						entity.setWfProcessInstanceId((coreWorkflowEntity).getWfProcessInstanceId());
						elasticsearchService.indexWithPermissions(toDto(entity), CoreUser.class, user.getId());
					}
				}
			default:
				break;
		}
	}

	@Override
	@Transactional(readOnly = true)
	public CoreUserWithPersonDto findByUsername(String username) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue("username", FilterOperator.EQUALS, Collections.singletonList(username)));

		return elasticsearchService.searchForSingleResult(dtoClass, parameters, AccessType.READ);
	}

}
