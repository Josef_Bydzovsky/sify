package org.simplifyworks.uam.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.service.OrganizationStructureService;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link OrganizationStructureService}.
 * 
 * Implementation uses {@link CoreOrganization} as entity and composition too (there is no special composition entity).
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
class DefaultOrganizationStructureService extends AbstractStructureService<CoreOrganization, CoreOrganization>
	implements OrganizationStructureService, ApplicationListener<EntityEvent> {
	
	/**
	 * Document type in index
	 */
	public static final String INDEX_DOCUMENT_TYPE = "organization_structure";
	
	@PersistenceContext(unitName = "default")
	protected EntityManager entityManager;
		
	@Override
	public List<String> findAllAncestorOrganizations(String organizationName) {
		return findAllAncestors(INDEX_DOCUMENT_TYPE, organizationName);
	}
	
	@Override
	public void rebuild() {
		rebuild(INDEX_DOCUMENT_TYPE);
	}
	
	// TODO using @EventListener in Spring 4.2 - add annotation to rebuildFlatOrganizationStructure method
	@Override
	public void onApplicationEvent(EntityEvent event) {
		if(event.getEntity() instanceof CoreOrganization) {
			rebuild();
		}
	}

	@Override
	protected List<CoreOrganization> findAllEntities() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreOrganization> criteria = cb.createQuery(CoreOrganization.class);
		criteria.from(CoreOrganization.class);
		
		return entityManager.createQuery(criteria).getResultList();
	}

	@Override
	protected List<CoreOrganization> findAllCompositions() {
		return findAllEntities();
	}

	@Override
	protected CoreOrganization getParent(CoreOrganization organization) {
		return organization.getOrganization();
	}

	@Override
	protected CoreOrganization getChild(CoreOrganization organization) {
		return organization;
	}
	
	@Override
	protected String getId(CoreOrganization entity) {
		return entity.getName();
	}
}