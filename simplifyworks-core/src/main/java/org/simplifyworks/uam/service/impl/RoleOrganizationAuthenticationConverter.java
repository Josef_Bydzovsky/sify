/**
 * 
 */
package org.simplifyworks.uam.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.simplifyworks.uam.model.domain.RoleOrganizationGrantedAuthority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;

/**
 * Default implementation of {@link UserAuthenticationConverter} which uses {@link RoleOrganizationGrantedAuthority}
 * instead of {@link SimpleGrantedAuthority}. 
 * 
 * TODO check switch user authority
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class RoleOrganizationAuthenticationConverter implements UserAuthenticationConverter {

	private static final Logger LOG = LoggerFactory.getLogger(RoleOrganizationAuthenticationConverter.class);
	
	@Override
	public Authentication extractAuthentication(Map<String, ?> map) {
		if (map.containsKey(USERNAME) && map.containsKey(AUTHORITIES)) {
			if(LOG.isDebugEnabled()) {
				LOG.debug("Extracting authentication from [" + map + "]");				
			}
			
			return new UsernamePasswordAuthenticationToken(map.get(USERNAME), "N/A", getAsAuthoritiesCollection(map.get(AUTHORITIES)));
		} else {			
			if(LOG.isDebugEnabled()) {
				LOG.debug("Missing username and/or authorities in [" + map + "]");				
			}
			
			return null;
		}
	}
	
	@Override
	public Map<String, ?> convertUserAuthentication(Authentication authentication) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		
		map.put(USERNAME, authentication.getName());
		if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
			map.put(AUTHORITIES, AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
		}
		
		if(LOG.isDebugEnabled()) {
			LOG.debug("Converting authentication to [" + map + "]");				
		}
		
		return map;
	}
	
	@SuppressWarnings("unchecked")
	private Collection<GrantedAuthority> getAsAuthoritiesCollection(Object object) {
		List<GrantedAuthority> authoritiesCollection = new ArrayList<>();
		Collection<String> authoritiesStringCollection = Collections.emptyList();
				
		if(object instanceof String) {
			authoritiesStringCollection = Arrays.asList(((String) object).split(","));
		} else if (object instanceof Collection) {
			authoritiesStringCollection = (Collection<String>) object;							
		} else {
			if(LOG.isDebugEnabled()) {
				LOG.debug("Unexpected type of authorities [" + object + "]");
			}
		}
		
		for(String string : authoritiesStringCollection) {
			authoritiesCollection.add(RoleOrganizationGrantedAuthority.parse(string));
		}	
		
		return authoritiesCollection;
	}
}
