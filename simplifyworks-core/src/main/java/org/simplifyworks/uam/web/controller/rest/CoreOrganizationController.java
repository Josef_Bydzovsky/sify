package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.service.CoreOrganizationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RestController
@RequestMapping(value = "/api/core/organizations")
public class CoreOrganizationController extends BasicTableController<CoreOrganizationDto, CoreOrganization> {

	@Autowired
	private CoreOrganizationManager coreOrganizationManager;

	@Override
	protected ReadWriteManager<CoreOrganizationDto, CoreOrganization> getManager() {
		return coreOrganizationManager;
	}

}
