package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.uam.model.dto.CorePermissionDto;
import org.simplifyworks.uam.model.entity.CorePermission;
import org.simplifyworks.uam.service.CorePermissionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
@RestController
@RequestMapping(value = "/api/security/permissions")
public class CorePermissionController extends BasicTableController<CorePermissionDto, CorePermission> {

	@Autowired
	private CorePermissionManager corePermissionManager;

	@Override
	protected ReadWriteManager<CorePermissionDto, CorePermission> getManager() {
		return corePermissionManager;
	}

}