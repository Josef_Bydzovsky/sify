package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.uam.model.dto.CorePermissionWorkflowDto;
import org.simplifyworks.uam.model.entity.CorePermissionWorkflow;
import org.simplifyworks.uam.service.CorePermissionWorkflowManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
@RestController
@RequestMapping(value = "/api/security/permissions-wf")
public class CorePermissionWorkflowController extends BasicTableController<CorePermissionWorkflowDto, CorePermissionWorkflow> {

    @Autowired
    private CorePermissionWorkflowManager corePermissionWorkflowManager;

    @Override
    protected ReadWriteManager<CorePermissionWorkflowDto, CorePermissionWorkflow> getManager() {
        return corePermissionWorkflowManager;
    }

}