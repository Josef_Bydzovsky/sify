package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.exception.EntityNotFoundException;
import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.core.web.domain.table.DtoResource;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonDto;
import org.simplifyworks.uam.model.dto.CoreUserWithPersonAndRolesDto;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.service.CoreUserWithPersonManager;
import org.simplifyworks.uam.service.CoreUserWithPersonAndRolesManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RestController
@RequestMapping("/api/core/users")
public class CoreUserController extends BasicTableController<CoreUserWithPersonDto, CoreUser> {

	@Autowired
	private SecurityService securityService;
	
	@Autowired
	private CoreUserWithPersonManager coreUserManager;
	
	@Autowired
	private CoreUserWithPersonAndRolesManager coreUserWithRolesManager;	
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	protected ReadWriteManager<CoreUserWithPersonDto, CoreUser> getManager() {
		return coreUserManager;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public DtoResource<CoreUserWithPersonAndRolesDto> profile() {
		if (!securityService.isAuthenticated()) {
			return null;
		}
		
		return new DtoResource(coreUserWithRolesManager.findByUsername(securityService.getUsername()), this);
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public DtoResource<CoreUserWithPersonDto> get(@PathVariable Long id, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {
		DtoResource<CoreUserWithPersonDto> resource = super.get(id, includeMetadata != null);
		return resource;
	}

	@Override
	@RequestMapping(method = RequestMethod.POST)
	public DtoResource<CoreUserWithPersonDto> create(@RequestBody CoreUserWithPersonDto dto, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {
		DtoResource<CoreUserWithPersonDto> resource = super.create(dto, includeMetadata != null);
		return resource;
	}

	@Override
	@RequestMapping(method = RequestMethod.PUT)
	public DtoResource<CoreUserWithPersonDto> update(@RequestBody CoreUserWithPersonDto dto, @RequestParam(value = PARAMETER_INCLUDE_METADATA, required = false) Boolean includeMetadata) {
		CoreUserWithPersonDto refresh = getManager().search(dto.getId());
		char[] password = dto.getNewPassword();

		if (password == null || password.length == 0) {
			dto.setPassword(refresh.getPassword());
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append(password);
			password = passwordEncoder.encode(sb).toCharArray();
			dto.setPassword(password);
		}

		CoreUserWithPersonDto result = getManager().update(dto);
		if (result == null) {
			throw new EntityNotFoundException(dto.getId());
		}

		return new DtoResource(result, this);
	}

}
