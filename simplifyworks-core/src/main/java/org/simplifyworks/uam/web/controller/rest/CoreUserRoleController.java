/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.web.controller.rest;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.core.web.controller.rest.BasicTableController;
import org.simplifyworks.uam.model.dto.CoreUserRoleWithUserDto;
import org.simplifyworks.uam.model.entity.CoreUserRole;
import org.simplifyworks.uam.service.CoreUserRoleWithUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Svanda
 */
@RestController
@RequestMapping(value= "/api/core/user-roles")
public class CoreUserRoleController extends BasicTableController<CoreUserRoleWithUserDto, CoreUserRole> {

	@Autowired
	private CoreUserRoleWithUserManager coreUserRoleManager;
	
	@Override
	protected ReadWriteManager<CoreUserRoleWithUserDto, CoreUserRole> getManager() {
		return coreUserRoleManager;
	}
	
}
