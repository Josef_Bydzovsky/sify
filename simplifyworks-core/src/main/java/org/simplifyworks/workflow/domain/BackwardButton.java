/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.workflow.domain;

import java.util.Properties;
import org.activiti.engine.task.Task;
import org.simplifyworks.workflow.model.dto.WfButtonPropertiesDto;
import org.springframework.context.MessageSource;

/**
 * Basic button for disapprove
 *
 * @author Svanda
 */
public class BackwardButton extends WfButton {

	public BackwardButton(WfButtonPropertiesDto properties, Task task) {
		setForward(false);
		setKey(BACKWARD);
		setLabelMessage(getButtonLabel(properties, task, BACKWARD));
		setShowWarning(true);
		setStyleClass(getButtonCss(properties, task, BACKWARD));
	}

}
