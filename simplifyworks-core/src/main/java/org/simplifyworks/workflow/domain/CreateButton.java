/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.workflow.domain;

import org.activiti.engine.task.Task;
import org.simplifyworks.workflow.model.dto.WfButtonPropertiesDto;

/**
 * Basic button for start workflow
 *
 * @author Svanda
 */
public class CreateButton extends WfButton {

    public CreateButton() {
        setForward(false);
        setKey(CREATE);
        setLabelMessage(CREATE);
        setShowWarning(false);
        setStyleClass(CSS_BUTTON_CREATE);
    }

}
