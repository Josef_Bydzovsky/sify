package org.simplifyworks.workflow.domain;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.List;
import org.activiti.bpmn.model.FieldExtension;
import org.activiti.bpmn.model.SendTask;
import org.activiti.bpmn.model.ServiceTask;
import org.activiti.engine.impl.bpmn.behavior.MailActivityBehavior;
import org.activiti.engine.impl.bpmn.helper.ClassDelegate;
import org.activiti.engine.impl.bpmn.parser.FieldDeclaration;
import org.activiti.engine.impl.bpmn.parser.factory.DefaultActivityBehaviorFactory;
import org.simplifyworks.email.service.Emailer;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author tomiska
 */
public class CustomActivityBehaviorFactory extends DefaultActivityBehaviorFactory {

	@Autowired
	private Emailer emailer;

	/**
	 * Pouziti vlastniho emaileru
	 * @param taskId
	 * @param fields
	 * @return 
	 */


	@Override
	protected MailActivityBehavior createMailActivityBehavior(String taskId, List<FieldExtension> fields) {
    List<FieldDeclaration> fieldDeclarations = createFieldDeclarations(fields);
    CustomMailActivityBehavior customMailActivityBehavior = (CustomMailActivityBehavior) ClassDelegate.instantiateDelegate(CustomMailActivityBehavior.class, fieldDeclarations);
		customMailActivityBehavior.setEmailer(emailer);
		return customMailActivityBehavior;
  }

	public void setEmailer(Emailer emailer) {
		this.emailer = emailer;
	}
}
