/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.workflow.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.activiti.engine.form.FormProperty;
import org.activiti.engine.impl.form.EnumFormType;
import org.activiti.engine.task.Task;
import org.simplifyworks.workflow.model.dto.WfButtonPropertiesDto;
import org.springframework.context.MessageSource;

/**
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public class EnumButton extends WfButton {

    private static final transient String VALUES = "values";

    public EnumButton(String keyEnum, WfButtonPropertiesDto properties, Task task) {
        keyEnum = keyEnum.trim().toLowerCase();
        setForward(properties.isForward());
        setLabelMessage(getButtonLabel(properties, task, keyEnum));
        setStyleClass(getButtonCss(properties, task, keyEnum));
        setKey(keyEnum);

        setShowWarning(properties.isShowWarning() == null ? false : properties.isShowWarning());
        setShowComment(properties.isShowComment() == null ? false : properties.isShowComment());
        //setValidationMap(getValidations(keyEnum, enumProperty));
        setDetailPopupTemplate(properties.getShowComponent());
        setHasDetailPopupTemplate(getDetailPopupTemplate() != null);

    }

    /**
     * Vrátí seznam hodnot pro daný klíč enumerace
     *
     * @param key
     * @param enumProperty
     * @return
     */
    private String[] getEnumValues(String key, FormProperty enumProperty) {
        if (enumProperty != null && enumProperty.getType() != null
                && enumProperty.getType() instanceof EnumFormType) {
            EnumFormType enumType = (EnumFormType) enumProperty.getType();
            HashMap valuesMap = (HashMap) enumType.getInformation(VALUES);
            String values = (String) valuesMap.get(key.toUpperCase());
            if (values == null) {
                return null;
            }
            String[] valuesArray = values.split(",");
            return valuesArray;
        }
        return null;
    }

    /**
     * Zjistí zdali existuje daný prametr u konkrétní hodnoty enumerace
     *
     * @param key
     * @param enumProperty
     * @param parameter
     * @return
     */
    private boolean isEnumParameter(String key, FormProperty enumProperty, String parameter) {
        String[] enumValues = getEnumValues(key, enumProperty);
        if (enumValues == null) {
            return false;
        }
        for (String value : enumValues) {
            if (value.trim().toLowerCase().startsWith(parameter)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return Map validations and instances
     *
     * @param key
     * @param enumProperty
     * @return
     */
//    private Map<String, IValidationWf> getValidations(String key, FormProperty enumProperty) {
//        String[] enumValues = getEnumValues(key, enumProperty);
//        Map<String, IValidationWf> map = new HashMap<String, IValidationWf>();
//
//        //Create validation
//        IValidationWf instanceOfValidator = typeValidationEnum.getInstanceOfValidator();
//        map.put(keyValidition, instanceOfValidator);
//
//
//        return map;
//    }

    /**
     * Return parametr of attribute example "show_detail(/faces/jenero/form/view/form-instance-list.xhtml)" return
     * "/faces/jenero/form/view/form-instance-list.xhtml"
     *
     * @param parametrKey
     * @param key
     * @param enumProperty
     * @return
     */
    private String getEnumParametr(String key, FormProperty enumProperty, String parametrKey) {
        String[] enumValues = getEnumValues(key, enumProperty);
        if (enumValues == null || parametrKey == null) {
            return null;
        }
        for (String value : enumValues) {
            String v = value.trim();
            if (v.startsWith(parametrKey)) {
                if (v.length() < parametrKey.length() + 2) {
                    //minimum lenght parametr is lenght prametrKey plus braktes
                    continue;
                }
                //jedná se o správný parametr ... vytáhneme vnitřní parametr
                String substring = v.substring(parametrKey.length() + 1, v.length() - 1);
                return substring;
            }
        }
        return null;
    }

    /**
     * Return list values of enumeration
     *
     * @param enumProperty
     * @return
     */
    public static Object[] getEnumKyes(FormProperty enumProperty) {
        if (enumProperty != null && enumProperty.getType() != null
                && enumProperty.getType() instanceof EnumFormType) {
            EnumFormType enumType = (EnumFormType) enumProperty.getType();
            HashMap valuesMap = (HashMap) enumType.getInformation(VALUES);
            Set keySet = valuesMap.keySet();
            return keySet.toArray();
        }
        return null;
    }

}
