/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.workflow.domain;

import java.util.Properties;
import org.activiti.engine.task.Task;
import org.simplifyworks.workflow.model.dto.WfButtonPropertiesDto;
import org.springframework.context.MessageSource;

/**
 * Basic forward button
 * 
 * @author Svanda
 */
public class ForwardButton extends WfButton {

	public ForwardButton(WfButtonPropertiesDto properties, Task task) {
		setForward(true);
		setKey(FORWARD);
		setLabelMessage(getButtonLabel(properties, task, FORWARD));
		setStyleClass(getButtonCss(properties, task, FORWARD));
	}
}
