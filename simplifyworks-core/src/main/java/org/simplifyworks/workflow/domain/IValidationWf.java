
package org.simplifyworks.workflow.domain;

/**
 * Interface for validating workflow attributes
 * @author Svanda
 */
public interface IValidationWf {

	String getMessage();

	/**
	 * Returns true if attribute is valid
	 * @param value
	 * @return 
	 */
	boolean validate(Object value);
	
	/**
	 * Type of validator
	 * @return 
	 */
	public ValidationWfEnum getType();
	
}
