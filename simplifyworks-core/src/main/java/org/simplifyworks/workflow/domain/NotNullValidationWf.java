
package org.simplifyworks.workflow.domain;

/**
 * Validation atributes on notnull
 * @author Svanda
 */
public class NotNullValidationWf implements IValidationWf {

	private String message;
	private ValidationWfEnum type;

	public NotNullValidationWf(ValidationWfEnum type) {
		this.type = type;
	}

	
	@Override
	public boolean validate(Object value) {
		//budeme kontrolovat zdali atribut není null;
		if (value == null) {
			message = "message.required";
			return false;
		}
		return true;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public ValidationWfEnum getType() {
		return type;
	}
}
