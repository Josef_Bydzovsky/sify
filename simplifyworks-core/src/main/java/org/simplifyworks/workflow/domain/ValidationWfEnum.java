package org.simplifyworks.workflow.domain;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Enumerace typů validací
 *
 * @author Svanda
 */
public enum ValidationWfEnum {

	NOT_NULL("not_null", NotNullValidationWf.class);
	private String key;
	private Class validator;

	private ValidationWfEnum(String key, Class validator) {
		this.key = key;
		this.validator = validator;
	}

	public String getKey() {
		return key;
	}

	public IValidationWf getInstanceOfValidator() {
		try {

			return (IValidationWf) validator.getConstructor(ValidationWfEnum.class).newInstance(this);
		} catch (InstantiationException ex) {
			Logger.getLogger(ValidationWfEnum.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(ValidationWfEnum.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalArgumentException ex) {
			Logger.getLogger(ValidationWfEnum.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InvocationTargetException ex) {
			Logger.getLogger(ValidationWfEnum.class.getName()).log(Level.SEVERE, null, ex);
		} catch (NoSuchMethodException ex) {
			Logger.getLogger(ValidationWfEnum.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SecurityException ex) {
			Logger.getLogger(ValidationWfEnum.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
}
