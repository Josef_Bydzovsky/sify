/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.workflow.domain;

import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.simplifyworks.workflow.model.dto.WfButtonPropertiesDto;
import org.springframework.context.MessageSource;

/**
 * Button ve workflow
 * 
 * TODO: zlepseni prace s i18n - tady uz je navic
 * 
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public class WfButton {

	protected static final transient String FORWARD = "forward";
	protected static final transient String BACKWARD = "backward";
	protected static final transient String CREATE = "create";
	protected static final transient String BUTTON = "button";
	protected static final transient String WF_PREFIX = "wf.";
	protected static final transient String BUTTON_SUFFIX = "." + BUTTON;
	protected static final transient String CSS_SUFFIX = ".css";
	protected static final transient String CSS_BUTTON_FORWARD = "primary";
	protected static final transient String CSS_BUTTON_CREATE = "success";
	protected static final transient String CSS_BUTTON_BACKWARD = "danger";

	private String labelMessage;
	private String styleClass;
	private boolean forward = true;
	private String key;
	private boolean showWarning = false;
	private Map<String, IValidationWf> validationMap;
	private boolean showComment = false;
	private boolean nessaseryComment = false;
	private transient String returnDialog;
	private transient String detailPopupTemplate;
	private transient boolean hasDetailPopupTemplate;
	private String tooltip;
	private boolean editable = true;

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public boolean isForward() {
		return forward;
	}

	public void setForward(boolean forward) {
		this.forward = forward;
	}

	public String getLabelMessage() {
		return labelMessage;
	}

	public void setLabelMessage(String labelMessage) {
		this.labelMessage = labelMessage;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isShowWarning() {
		return showWarning;
	}

	public void setShowWarning(boolean showWarning) {
		this.showWarning = showWarning;
	}

	public Map<String, IValidationWf> getValidationMap() {
		return validationMap;
	}

	public void setValidationMap(Map<String, IValidationWf> validationMap) {
		this.validationMap = validationMap;
	}

	public boolean isShowComment() {
		return showComment;
	}

	public void setShowComment(boolean showComment) {
		this.showComment = showComment;
	}

	public String getReturnDialog() {
		return returnDialog;
	}

	public void setReturnDialog(String returnDialog) {
		this.returnDialog = returnDialog;
	}

	public String getDetailPopupTemplate() {
		return detailPopupTemplate;
	}

	public void setDetailPopupTemplate(String detailPopupTemplate) {
		this.detailPopupTemplate = detailPopupTemplate;
	}

	public boolean isHasDetailPopupTemplate() {
		return hasDetailPopupTemplate;
	}

	public void setHasDetailPopupTemplate(boolean hasDetailPopupTemplate) {
		this.hasDetailPopupTemplate = hasDetailPopupTemplate;
	}

	public boolean isNessaseryComment() {
		return nessaseryComment;
	}

	public void setNessaseryComment(boolean nessaseryComment) {
		this.nessaseryComment = nessaseryComment;
	}


	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * Label for WF button
	 *
	 * @param properties
	 * @param task
	 * @param buttonKey
	 * @return
	 */
	protected String getButtonLabel(WfButtonPropertiesDto properties, Task task, String buttonKey) {
		String label = properties.getLabel();
		if (StringUtils.isEmpty(label)) {
			label = WF_PREFIX + task.getTaskDefinitionKey() + BUTTON_SUFFIX + "." + buttonKey;
		}
		return label;
	}

	/**
	 * CSS for WF button
	 *
	 * @param properties
	 * @param task
	 * @param buttonKey
	 * @return
	 */
	protected String getButtonCss(WfButtonPropertiesDto properties, Task task, String buttonKey) {
		String css = properties.getCss();
		if (StringUtils.isEmpty(css)) {
			css = WF_PREFIX + task.getTaskDefinitionKey() + BUTTON_SUFFIX + "." + buttonKey + CSS_SUFFIX;
		}

		return css;
	}

}
