package org.simplifyworks.workflow.domain;

import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.event.*;
import org.activiti.engine.delegate.event.impl.ActivitiActivityEventImpl;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.uam.model.domain.PermissionFor;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.dto.CorePermissionWorkflowDto;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.service.CoreOrganizationManager;
import org.simplifyworks.uam.service.CorePermissionWorkflowManager;
import org.simplifyworks.uam.service.CoreRoleManager;
import org.simplifyworks.workflow.model.dto.PermissionPropertiesDto;
import org.simplifyworks.workflow.model.dto.UserTaskPropertiesDto;
import org.simplifyworks.workflow.model.dto.WfButtonPropertiesDto;
import org.simplifyworks.workflow.service.WorkflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Listener catch events from workflow engine
 * Created by Svanda on 27.7.2015.
 */
@Component
public class WorkflowListener implements ActivitiEventListener {

    @Autowired
    CorePermissionWorkflowManager corePermissionWorkflowManager;
    @Autowired
    CoreOrganizationManager coreOrganizationManager;
    @Autowired
    CoreRoleManager coreRoleManager;
    @Autowired
    WorkflowService workflowService;

    @Override
    public void onEvent(ActivitiEvent event) {

        //We create permission only after created UserTask
        if (ActivitiEventType.TASK_CREATED == event.getType()) {

            if (event instanceof ActivitiEntityEvent) {
                ActivitiEntityEvent activitEvent = (ActivitiEntityEvent) event;
                TaskEntity entity = (TaskEntity) activitEvent.getEntity();
                //Parse metadata from user task (from documentation field)
                UserTaskPropertiesDto loadProperties = workflowService.loadProperties(entity);
                if (loadProperties != null && loadProperties.getEnumButtons() != null) {
                    for (WfButtonPropertiesDto button : loadProperties.getEnumButtons()) {
                        List<CorePermissionWorkflowDto> permissions = corePermissionWorkflowManager.searchPermissionsForWorkflow(entity.getProcessDefinitionId(), button.getKey(), entity.getId());
                        if (permissions != null && !permissions.isEmpty()) {
                            throw new CoreException("Permissions for task " + entity.getId() + " and button " + button.getKey() + " already exists!");
                        }
                        //We need definition permissions from wf button
                        if (button.getPermissions() != null) {
                            for (PermissionPropertiesDto pd : button.getPermissions()) {

                                String role = pd.getRole();
                                if (StringUtils.isEmpty(role)) {
                                    throw new CoreException("Role is mandatory in wf button (" + button.getKey() + ") permission");
                                }
                                List<CoreRoleDto> resultRoles = coreRoleManager.getRoleByName(role);
                                if (resultRoles == null || resultRoles.isEmpty() || resultRoles.size() != 1) {
                                    throw new CoreException("Cannot found role with name " + role);
                                }

                                String workplace = pd.getWorkplace();
                                CoreOrganizationDto org = null;

                                if (!StringUtils.isEmpty(workplace)) {
                                    List<CoreOrganizationDto> resultOrgs = coreOrganizationManager.getOrganizationByCode(workplace);
                                    if (resultOrgs == null || resultOrgs.isEmpty() || resultOrgs.size() != 1) {
                                        throw new CoreException("Cannot found organization with name " + workplace);
                                    }
                                    org = resultOrgs.get(0);
                                }

                                Map<String, Object> processVariables = entity.getExecution().getVariables();
                                if (processVariables == null || !processVariables.containsKey(WorkflowService.PARAMETER_ENTITY_ID) || !processVariables.containsKey(WorkflowService.PARAMETER_ENTITY_TYPE)) {
                                    throw new CoreException("Variables entityType and entityId are mandatory!");
                                }

                                //Create new workflow permission
                                CorePermissionWorkflowDto permission = new CorePermissionWorkflowDto();
                                permission.setPermissionFor(PermissionFor.ENTITY);
                                permission.setActive(Boolean.TRUE);
                                permission.setRole(resultRoles.get(0));
                                permission.setOrganization(org);
                                permission.setCanRead(true);
                                permission.setCanWrite(true); //When we create permission for workflow button, we need edit connecting element
                                permission.setCanDelete(pd.isCanDelete());
                                permission.setWfButtonKey(button.getKey());
                                permission.setWfUserTask(entity.getId());
                                permission.setWfDefinitionId(entity.getProcessDefinitionId());
                                permission.setObjectIdentifier(String.valueOf(processVariables.get(WorkflowService.PARAMETER_ENTITY_ID)));
                                permission.setObjectType(String.valueOf(processVariables.get(WorkflowService.PARAMETER_ENTITY_TYPE)));

                                //create new permission
                                corePermissionWorkflowManager.create(permission);

                            }
                        }
                    }
                }
            }

        } else if (ActivitiEventType.TASK_COMPLETED == event.getType()) {
            //Maybe good place for delete / deactivation old permission
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}
