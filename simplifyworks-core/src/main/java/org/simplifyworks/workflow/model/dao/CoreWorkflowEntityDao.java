package org.simplifyworks.workflow.model.dao;

import org.simplifyworks.core.model.dao.AnyTypeDao;
import org.simplifyworks.workflow.model.entity.CoreWorkflowEntity;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-07
 */
public interface CoreWorkflowEntityDao extends AnyTypeDao<CoreWorkflowEntity> {

}
