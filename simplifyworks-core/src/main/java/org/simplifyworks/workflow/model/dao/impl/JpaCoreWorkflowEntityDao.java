package org.simplifyworks.workflow.model.dao.impl;

import org.simplifyworks.core.model.dao.impl.JpaAnyTypeDao;
import org.simplifyworks.workflow.model.entity.CoreWorkflowEntity;
import org.simplifyworks.workflow.model.dao.CoreWorkflowEntityDao;
import org.springframework.stereotype.Repository;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-07
 */
@Repository 
public class JpaCoreWorkflowEntityDao extends JpaAnyTypeDao<CoreWorkflowEntity> implements CoreWorkflowEntityDao {

}
