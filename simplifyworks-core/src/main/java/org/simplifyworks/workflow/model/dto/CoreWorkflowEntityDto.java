package org.simplifyworks.workflow.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-07
 */
public class CoreWorkflowEntityDto extends AbstractDto {

    private String wfProcessInstanceId;
    private String objectIdentifier;
    private String objectType;

    public CoreWorkflowEntityDto() {
    }

    public CoreWorkflowEntityDto(Long id) {
        super(id);
    }

    public String getWfProcessInstanceId() {
        return wfProcessInstanceId;
    }

    public void setWfProcessInstanceId(String wfProcessInstanceId) {
        this.wfProcessInstanceId = wfProcessInstanceId;
    }

    public String getObjectIdentifier() {
        return objectIdentifier;
    }

    public void setObjectIdentifier(String objectIdentifier) {
        this.objectIdentifier = objectIdentifier;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }
}