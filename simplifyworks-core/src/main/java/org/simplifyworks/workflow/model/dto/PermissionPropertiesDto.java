package org.simplifyworks.workflow.model.dto;

/**
 * Permission for WfButton
 * Created by Svanda on 24.7.2015.
 */
public class PermissionPropertiesDto {

    private String role; //Name of role
    private String workplace; //Code of organization
    private boolean entityWorkplace; //when is true than will be get workplace from entity (by @Permission) *THIS option is not implemented now*
    private boolean canRead;
    private boolean canWrite;
    private boolean canDelete; //Can be entity deleted when is in this activity


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public boolean isEntityWorkplace() {
        return entityWorkplace;
    }

    public void setEntityWorkplace(boolean entityWorkplace) {
        this.entityWorkplace = entityWorkplace;
    }

    public boolean isCanRead() {
        return canRead;
    }

    public void setCanRead(boolean canRead) {
        this.canRead = canRead;
    }

    public boolean isCanWrite() {
        return canWrite;
    }

    public void setCanWrite(boolean canWrite) {
        this.canWrite = canWrite;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }
}
