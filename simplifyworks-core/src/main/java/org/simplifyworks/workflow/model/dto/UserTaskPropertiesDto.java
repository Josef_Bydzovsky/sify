package org.simplifyworks.workflow.model.dto;

import java.util.List;

/**
 * Created by Svanda on 8.7.2015.
 */
public class UserTaskPropertiesDto {
    private List<WfButtonPropertiesDto> enumButtons;


    public List<WfButtonPropertiesDto> getEnumButtons() {
        return enumButtons;
    }

    public void setEnumButtons(List<WfButtonPropertiesDto> enumButtons) {
        this.enumButtons = enumButtons;
    }
}
