package org.simplifyworks.workflow.model.dto;

import java.util.List;

/**
 * Created by Svanda on 7.7.2015.
 */
public class WfButtonPropertiesDto {
    String key;
    String label;
    String css;
    Boolean forward;
    Boolean showComment;
    Boolean showWarning;
    String showComponent;
    List<PermissionPropertiesDto> permissions;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public Boolean isForward() {
        return forward;
    }

    public void setForward(Boolean forward) {
        this.forward = forward;
    }

    public Boolean isShowComment() {
        return showComment;
    }

    public void setShowComment(Boolean showComment) {
        this.showComment = showComment;
    }

    public Boolean isShowWarning() {
        return showWarning;
    }

    public void setShowWarning(Boolean showWarning) {
        this.showWarning = showWarning;
    }

    public String getShowComponent() {
        return showComponent;
    }

    public void setShowComponent(String showComponent) {
        this.showComponent = showComponent;
    }

    public List<PermissionPropertiesDto> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionPropertiesDto> permissions) {
        this.permissions = permissions;
    }
}
