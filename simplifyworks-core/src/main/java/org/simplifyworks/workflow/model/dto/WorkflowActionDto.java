package org.simplifyworks.workflow.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.workflow.domain.WfButton;

import java.util.Map;

/**
 * Created by Svanda on 9.7.2015.
 */
public class WorkflowActionDto<T extends  AbstractDto> {

    private WfButton button;
    private T entity;
    private Map<String, Object> variables;
    String processKey;
    String processInstance;


    public WfButton getButton() {
        return button;
    }

    public void setButton(WfButton button) {
        this.button = button;
    }

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }

    public String getProcessKey() {
        return processKey;
    }

    public void setProcessKey(String processKey) {
        this.processKey = processKey;
    }

    public String getProcessInstance() {
        return processInstance;
    }

    public void setProcessInstance(String processInstance) {
        this.processInstance = processInstance;
    }
}
