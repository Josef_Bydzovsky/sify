
package org.simplifyworks.workflow.model.entity;

import org.simplifyworks.core.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Relationship for workflow and entity
 *
 * @author Svanda
 */
@Entity
@Table(name="CORE_WORKFLOW_ENTITY", uniqueConstraints = @UniqueConstraint(columnNames = {"OBJECT_IDENTIFIER", "OBJECT_TYPE"}))
public class CoreWorkflowEntity extends AbstractEntity {

    @NotNull
    @Size(min = 1, max = 128)
    @Basic(optional = false)
    @Column(name = "OBJECT_TYPE", nullable = false, length = 128)
    private String objectType;
    @NotNull
    @Size(min = 1, max = 36)
    @Basic(optional = false)
    @Column(name = "OBJECT_IDENTIFIER", nullable = false, length = 36)
    private String objectIdentifier;
    @Size(max = 255)
    @Column(name = "WF_PROCESS_INSTANCE_ID", length = 255)
    private String wfProcessInstanceId;


    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectIdentifier() {
        return objectIdentifier;
    }

    public void setObjectIdentifier(String objectIdentifier) {
        this.objectIdentifier = objectIdentifier;
    }

    public String getWfProcessInstanceId() {
        return wfProcessInstanceId;
    }

    public void setWfProcessInstanceId(String wfProcessInstanceId) {
        this.wfProcessInstanceId = wfProcessInstanceId;
    }
}
