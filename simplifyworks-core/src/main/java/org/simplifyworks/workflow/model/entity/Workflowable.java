/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.workflow.model.entity;

/**
 * Object controlling by workflow engine
 * 
 * @author Svanda
 */
public interface Workflowable{

	/**
	 * Workflow instance identifier in workflow engine
	 * @return 
	 */
	String getWfProcessInstanceId();

	/**
	 * Workflow instance identifier in workflow engine
	 * @param wfProcessInstanceId 
	 */
	void setWfProcessInstanceId(String wfProcessInstanceId);

}
