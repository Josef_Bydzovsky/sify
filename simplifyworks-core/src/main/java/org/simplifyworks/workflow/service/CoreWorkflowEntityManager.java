package org.simplifyworks.workflow.service;

import org.simplifyworks.core.service.ReadWriteManager;
import org.simplifyworks.workflow.model.dto.CoreWorkflowEntityDto;
import org.simplifyworks.workflow.model.entity.CoreWorkflowEntity;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-07
 */
public interface CoreWorkflowEntityManager extends ReadWriteManager<CoreWorkflowEntityDto, CoreWorkflowEntity> {

}