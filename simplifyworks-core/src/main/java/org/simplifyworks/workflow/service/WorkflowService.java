
package org.simplifyworks.workflow.service;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.simplifyworks.core.model.domain.SortOrder;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Comment;
import org.simplifyworks.workflow.domain.WfButton;
import org.simplifyworks.workflow.model.dto.ProcessDefinitionDto;
import org.simplifyworks.workflow.model.dto.UserTaskPropertiesDto;

/**
 * @author svanda
 */
public interface WorkflowService {

    public static final String PARAMETER_ENTITY = "entity";
    public static final String PARAMETER_NEW = "new";
    public static final String PARAMETER_WF_BUTTON = "button";
    public static final String PARAMETER_ENTITY_ID = "entityId";
    public static final String PARAMETER_ENTITY_TYPE = "entityType";
    public static final String PARAMETER_PROCESS_INSTANCE = "processInstanceId";


    /**
     * Deploy new process
     *
     * @param deploymentName
     * @param inputStream
     * @param fileName
     * @return
     */
    public Deployment deploy(String deploymentName, String fileName, InputStream inputStream);

    /**
     * Process workflow
     *
     * @param processKey
     * @param processInstanceId
     * @param variables
     * @param wfButton
     * @return
     */
    public Map<String, Object> process(String processKey, String processInstanceId, Map<String, Object> variables, WfButton wfButton);


    /**
     * Return history of process
     *
     * @param processInstanceId
     * @return
     */

    HistoricProcessInstance getHistoricProcess(String processInstanceId);

    /**
     * Return history user tasks from process
     *
     * @param processInstanceId
     * @param sortOrder
     * @return
     */
    List<HistoricTaskInstance> historicUserTasks(String processInstanceId, SortOrder sortOrder);

    /**
     * Return all history activities from process
     *
     * @param processInstanceId
     * @param sortOrder
     * @return
     */
    List<HistoricActivityInstance> historicActivities(String processInstanceId, SortOrder sortOrder);

    /**
     * Return processInstanceIds, where process not ended and user did some complete user task.
     *
     * @param username
     * @return
     */
    Set<String> userTasks(String username);

    /**
     * Cancel workflow
     *
     * @param processInstanceId
     * @param deleteReason
     */
    void cancelWorkflow(String processInstanceId, String deleteReason);


    /**
     * Return value by key form process
     *
     * @param processInstanceId
     * @param variableKey
     * @return
     */
    Object getVariableByKey(String processInstanceId, String variableKey);

    UserTaskPropertiesDto loadProperties(Task task);

    /**
     * Return true if is process ended
     *
     * @param processInstanceId
     * @return
     */
    boolean isProcessEnded(String processInstanceId);

    /**
     * Return true if was process started (process can be ended too)
     *
     * @param processInstanceId
     * @return
     */
    boolean isProcessStarted(String processInstanceId);

    /**
     * Add comment to current activity in process
     *
     * @param processInstanceId
     * @param comment
     */
    void addComment(String processInstanceId, String comment);

    /**
     * Return comment which was set to previous task at the process
     *
     * @param processInstanceId
     * @return
     */
    Comment getPreviousComment(String processInstanceId);

    /**
     * All comments at the process (key is id task)
     *
     * @param processInstanceId
     * @return
     */
    Map<String, List<Comment>> getTaskComments(String processInstanceId);

    /**
     * Return last comment at the process
     *
     * @param processInstanceId
     * @return
     */
    public Comment getLastComment(String processInstanceId);

    /**
     * Return all comments at the process
     *
     * @param processInstanceId
     * @return
     */
    public List<Comment> getAllComments(String processInstanceId);

    InputStream getDeploymentStream(String deploymentId, String resourceName);

    List<Deployment> getDeployments();

    Deployment getDeployment(String deploymentId);

    List<ProcessDefinition> getProcessDefinitions();

    ProcessDefinition getProcessDefinition(String processDefinitionId);

    void deleteDeployment(String deploymentId);

    ProcessDefinition getProcessDefinitionByDeployment(String deploymentId);

    ProcessDefinition getProcessDefinitionByKey(String processDefinitionKey);

    /**
     * Remove activities from history, which wasn't in positive way.
     *
     * @param activityInstances
     * @return
     */
    public List<HistoricActivityInstance> removeCirclesFromHistoricActivities(List<HistoricActivityInstance> activityInstances);

    /**
     * Return last comment for task
     *
     * @param taskInstanceId
     * @return
     */
    public Comment getLastCommentTask(String taskInstanceId);

    public List<WfButton> getWfButtons(String processInstanceId);

    ProcessDefinitionDto processDefinitionToDto(ProcessDefinition processDefinition);

    WfButton wfButtonFromMap(Map<String, Object> data);
}
