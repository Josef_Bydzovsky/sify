package org.simplifyworks.workflow.service.impl;

import org.elasticsearch.index.query.FilterBuilders;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.impl.DefaultReadWriteManager;
import org.simplifyworks.workflow.model.dto.CoreWorkflowEntityDto;
import org.simplifyworks.workflow.model.entity.CoreWorkflowEntity;
import org.simplifyworks.workflow.model.entity.CoreWorkflowEntity_;
import org.simplifyworks.workflow.service.CoreWorkflowEntityManager;
import org.springframework.stereotype.Service;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-07
 */
@Service
public class CoreWorkflowEntityManagerImpl extends DefaultReadWriteManager<CoreWorkflowEntityDto, CoreWorkflowEntity> implements CoreWorkflowEntityManager {

	public void onCoreWorkflowEntityEvent(EntityEvent event) {
		reindexItself(event, FilterBuilders.termFilter(CoreWorkflowEntity_.id.getName(), event.getEntity().getId()));
	}

}
