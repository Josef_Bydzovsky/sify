
package org.simplifyworks.workflow.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;
import org.simplifyworks.core.model.domain.SortOrder;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.dto.CorePermissionWorkflowDto;
import org.simplifyworks.uam.service.CorePermissionManager;
import org.simplifyworks.uam.service.CorePermissionWorkflowManager;
import org.simplifyworks.workflow.domain.CreateButton;
import org.simplifyworks.workflow.domain.EnumButton;
import org.simplifyworks.workflow.domain.WfButton;
import org.simplifyworks.workflow.domain.WorkflowListener;
import org.simplifyworks.workflow.model.dto.CoreWorkflowEntityDto;
import org.simplifyworks.workflow.model.dto.ProcessDefinitionDto;
import org.simplifyworks.workflow.model.dto.UserTaskPropertiesDto;
import org.simplifyworks.workflow.model.dto.WfButtonPropertiesDto;
import org.simplifyworks.workflow.service.CoreWorkflowEntityManager;
import org.simplifyworks.workflow.service.WorkflowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * Osluzna sluzba nad Activiti workflow
 *
 * @author VŠ
 */
@Service
public class WorkflowServiceImpl implements WorkflowService {

    public static final transient String PARAMETER_APPROVED = "approved";
    private static final transient Logger logger = LoggerFactory.getLogger(WorkflowServiceImpl.class);
    
    @Autowired
    private SecurityService securityService;
    
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private IdentityService identityService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private FormService formService;
    @Autowired(required = false)
    private MessageSource i18n;
    @Autowired(required = false)
    private CoreWorkflowEntityManager coreWorkflowEntityManager;
    
    private final ObjectMapper jsonMapper = new ObjectMapper();
    @Autowired
    WorkflowListener workflowListener;
    @Autowired
    CorePermissionWorkflowManager corePermissionWorkflowManager;
    @Autowired
    CorePermissionManager corePermissionManager;


    @PostConstruct
    public void init() {
        runtimeService.addEventListener(workflowListener);
    }


    @Override
    /**
     * Move workflow and create new if not exist.
     */
    @Transactional
    public Map<String, Object> process(String processKey, String processInstanceId, Map<String, Object> variables, WfButton wfButton) {
        identityService.setAuthenticatedUserId(securityService.getOriginalUsername());
        logger.debug("WF [" + processKey + "]  with action [" + (wfButton == null ? "N/A" : wfButton.getKey()) + "] under user [" + securityService.getOriginalUsername() + "]");
        if (variables == null) {
            variables = new HashMap<String, Object>();
        }
        variables.put(PARAMETER_APPROVED, wfButton == null ? true : wfButton.isForward());

        Map<String, Object> resultVariables = null;

        if (!isProcessStarted(processInstanceId)) { // We will start workflow
            ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processKey, variables);
            if (!processInstance.isEnded()) {
                resultVariables = runtimeService.getVariables(processInstance.getId());
                resultVariables.put(PARAMETER_PROCESS_INSTANCE, processInstance.getId());

                Long entityId = (Long) variables.get(WorkflowService.PARAMETER_ENTITY_ID);
                String entityType = (String) variables.get(WorkflowService.PARAMETER_ENTITY_TYPE);
                resultVariables.put(WorkflowService.PARAMETER_ENTITY_ID, entityId);

                //We create new CoreWorkflowEntity for store relationship between workflow and entity
                CoreWorkflowEntityDto coreWorkflowEntity = new CoreWorkflowEntityDto();
                coreWorkflowEntity.setObjectIdentifier(String.valueOf(entityId));
                coreWorkflowEntity.setObjectType(entityType);
                coreWorkflowEntity.setWfProcessInstanceId(processInstance.getId());
                //Save coreWorkflowEntity
                coreWorkflowEntityManager.create(coreWorkflowEntity);
            }
        } else {
            Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
            //If I have key, than i put him as true (select from enum)
            //Others params in enum I have to set on false
            if (wfButton != null && wfButton.getKey() != null) {

                UserTaskPropertiesDto userTaskPropertiesDto = loadProperties(task);

                if (userTaskPropertiesDto != null && userTaskPropertiesDto.getEnumButtons() != null) {
                    for (WfButtonPropertiesDto bp : userTaskPropertiesDto.getEnumButtons()) {
                        String key = bp.getKey().toLowerCase();
                        if (wfButton.getKey().equals(key)) {
                            variables.put(wfButton.getKey(), true);
                        } else {
                            variables.put(key, false);
                        }
                    }
                }
            }
            //assign task to current user - need for audit
            taskService.setAssignee(task.getId(), securityService.getOriginalUsername());
            taskService.complete(task.getId(), variables);
            if (!isProcessEnded(processInstanceId)) {
                resultVariables = runtimeService.getVariables(processInstanceId);
            } else {
                resultVariables = new HashMap<String, Object>();
            }
            resultVariables.put(PARAMETER_PROCESS_INSTANCE, processInstanceId);
        }
        logger.debug("WF [" + processKey + "] with action [" + (wfButton == null ? "N/A" : wfButton.getKey()) + "] under user [" + securityService.getOriginalUsername() + "]");
        //
        return resultVariables;
    }

    @Override
    /**
     * Return workflow buttons by user permissions
     */
    public List<WfButton> getWfButtons(String processInstanceId) {
        logger.debug("Load buttons for process [" + processInstanceId + "]");
        List<WfButton> buttons = Lists.newArrayList();
        if (!isProcessStarted(processInstanceId)) {
            //when is process not start, we add only create button
            WfButton buttonCreate = new CreateButton();
            buttons.add(buttonCreate);
            return buttons;
        }
        // task
        Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
        if (task == null) {
            return buttons;
        }
        buttons = getWfButtons(task);
        return buttons;
    }

    private List<WfButton> getWfButtons(Task task) {
        logger.debug("Load buttons for task [key:" + task.getTaskDefinitionKey() + "] [id:" + task.getId() + "]");
        List<WfButton> buttons = Lists.newArrayList();
        // form
        TaskFormData taskFormData = formService.getTaskFormData(task.getId());
        // wf button
        UserTaskPropertiesDto userTaskPropertiesDto = loadProperties(taskFormData.getTask());


        if (userTaskPropertiesDto != null && userTaskPropertiesDto.getEnumButtons() != null) {

            for (WfButtonPropertiesDto bp : userTaskPropertiesDto.getEnumButtons()) {

                boolean hasPermission = false;
                List<CorePermissionWorkflowDto> permissions = corePermissionWorkflowManager.searchPermissionsForWorkflow(task.getProcessDefinitionId(), bp.getKey(), task.getId());
                if (permissions != null && !permissions.isEmpty()) {
                    hasPermission = corePermissionWorkflowManager.hasPermissionForWrite(permissions);

                }
                //We show button only if user have permission
                if(hasPermission) {
                    WfButton wfButton = new EnumButton(bp.getKey(), bp, task);
                    buttons.add(wfButton);
                }


            }
        }
        return buttons;
    }

    @Override
    public Deployment deploy(String deploymentName, String fileName, InputStream inputStream) {

        try {
            return repositoryService.createDeployment().addInputStream(fileName, inputStream).name(deploymentName).deploy();
        } catch (ActivitiException ae) {
            logger.error(ae.getLocalizedMessage(), ae);
        }
        return null;
    }

    @Override
//	@Cacheable(value = "cz.ders.jenero.workflow.history.process", key = "#processInstanceId")
    public HistoricProcessInstance getHistoricProcess(String processInstanceId) {
        return historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
    }

    /**
     * Historie procesu uzivatelskych aktivit
     *
     * @param processInstanceId
     * @return
     */
    @Override
//	@Cacheable(value = "cz.ders.jenero.workflow.history.userTask")
    public List<HistoricTaskInstance> historicUserTasks(String processInstanceId, SortOrder sortOrder) {
        HistoricTaskInstanceQuery query = historyService.createHistoricTaskInstanceQuery().processInstanceId(processInstanceId).orderByHistoricTaskInstanceEndTime();
        if (sortOrder == SortOrder.ASC) {
            query.asc();
        } else {
            query.desc();
        }
        return query.list();
    }

    /**
     * Vrati processInstanceIds, ktere nejsou ukonceny a provedl jsem alespon jeden user task
     *
     * @param username
     */
    @Override
    public Set<String> userTasks(String username) {
        HistoricTaskInstanceQuery query = historyService.createHistoricTaskInstanceQuery().taskAssignee(username).orderByHistoricTaskInstanceEndTime().desc();
        Set<String> results = Sets.newLinkedHashSet();
        for (HistoricTaskInstance historicTaskInstance : query.listPage(0, 750)) {
            results.add(historicTaskInstance.getProcessInstanceId());
        }
        return results;

    }

    /**
     * Historie procesu vsech aktivit
     *
     * @param processInstanceId
     * @return
     */
    @Override
    public List<HistoricActivityInstance> historicActivities(String processInstanceId, SortOrder sortOrder) {
        HistoricActivityInstanceQuery query = historyService
                .createHistoricActivityInstanceQuery()
                .processInstanceId(processInstanceId)
                .orderByHistoricActivityInstanceStartTime();
        if (sortOrder == SortOrder.ASC) {
            query.asc();
        } else {
            query.desc();
        }
        return query.list();
    }

    @Override
    public void cancelWorkflow(String processInstanceId, String deleteReason) {
        HistoricProcessInstance historicProcess = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        if (historicProcess != null && historicProcess.getEndTime() == null) {
            //Pokud process ještě běží, tak ho smažem
            identityService.setAuthenticatedUserId(securityService.getOriginalUsername());
            runtimeService.deleteProcessInstance(processInstanceId, deleteReason);
        }
    }

    @Override
    public void addComment(String processInstanceId, String comment) {
        identityService.setAuthenticatedUserId(securityService.getOriginalUsername());
        logger.debug("Ukladam WF komentar pro aktualni task pro process [" + processInstanceId + "]");
        Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
        taskService.addComment(task.getId(), processInstanceId, comment);
    }

    @Override
    public Comment getPreviousComment(String processInstanceId) {
        if (!isProcessStarted(processInstanceId)) {
            return null;
        }
        List<HistoricTaskInstance> historicUserTasks = historicUserTasks(processInstanceId, SortOrder.DESC);
        if (historicUserTasks.size() < 2) {
            return null;
        }
        List<Comment> comments = getTaskComments(processInstanceId).get(historicUserTasks.get(1).getId());
        if (comments == null || comments.isEmpty()) {
            return null;
        }
        return comments.get(0);
    }

    @Override
    public Comment getLastComment(String processInstanceId) {
        if (!isProcessStarted(processInstanceId)) {
            return null;
        }
        List<Comment> comments = taskService.getProcessInstanceComments(processInstanceId);
        if (comments == null || comments.isEmpty()) {
            return null;
        }
        return comments.get(0);
    }

    @Override
    public Comment getLastCommentTask(String taskInstanceId) {
        if (!isProcessStarted(taskInstanceId)) {
            return null;
        }
        List<Comment> comments = taskService.getTaskComments(taskInstanceId);
        if (comments == null || comments.isEmpty()) {
            return null;
        }
        return comments.get(0);
    }

    @Override
    public List<Comment> getAllComments(String processInstanceId) {
        if (!isProcessStarted(processInstanceId)) {
            return null;
        }
        return taskService.getProcessInstanceComments(processInstanceId);
    }

    @Override
    public Map<String, List<Comment>> getTaskComments(String processInstanceId) {
        Map<String, List<Comment>> result = Maps.newLinkedHashMap();
        if (!isProcessStarted(processInstanceId)) {
            return result;
        }
        List<Comment> comments = taskService.getProcessInstanceComments(processInstanceId);
        for (Comment comment : comments) {
            if (!result.containsKey(comment.getTaskId())) {
                result.put(comment.getTaskId(), new ArrayList<Comment>());
            }
            result.get(comment.getTaskId()).add(comment);
        }
        return result;
    }

    @Override
    public UserTaskPropertiesDto loadProperties(Task task) {
        UserTaskPropertiesDto userTaskPropertiesDto = null;
        if (task != null && task.getDescription() != null) {
            try {
                userTaskPropertiesDto = jsonMapper.readValue(task.getDescription(), UserTaskPropertiesDto.class);

            } catch (IOException e) {
                logger.debug("Extraction properties from task (enum) was not success [" + task.getDescription() + "]", e);

            }
        }
        return userTaskPropertiesDto;
    }

    @Override
    public boolean isProcessEnded(String processInstanceId) {
        if (!isProcessStarted(processInstanceId)) {
            return false;
        }
        HistoricProcessInstance historicProcess = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        if (historicProcess == null) {
            return true;
        }
        Date endTime = historicProcess.getEndTime();
        if (endTime == null) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isProcessStarted(String processInstanceId) {
        return !(StringUtils.isEmpty(processInstanceId) || processInstanceId.equals(WorkflowService.PARAMETER_NEW));
    }

    @Override
    public Object getVariableByKey(String processInstanceId, String variableKey) {
        if (variableKey == null || processInstanceId == null) {
            return null;
        }
        if (isProcessStarted(processInstanceId) && !isProcessEnded(processInstanceId)) {
            return runtimeService.getVariable(processInstanceId, variableKey);
        }
        return null;
    }


    @Override
    public InputStream getDeploymentStream(String deploymentId, String resourceName) {
        return repositoryService.getResourceAsStream(deploymentId, resourceName);
    }

    @Override
    public List<Deployment> getDeployments() {
        return repositoryService.createDeploymentQuery().orderByDeploymenTime().desc().list();
    }

    @Override
    public Deployment getDeployment(String deploymentId) {
        return repositoryService.createDeploymentQuery().deploymentId(deploymentId).singleResult();
    }

    @Override
    public List<ProcessDefinition> getProcessDefinitions() {
        return repositoryService.createProcessDefinitionQuery().latestVersion().orderByProcessDefinitionName().asc().list();
    }

    @Override
    public ProcessDefinition getProcessDefinition(String processDefinitionId) {
        return repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefinitionId).singleResult();
    }

    @Override
    public void deleteDeployment(String deploymentId) {
        repositoryService.deleteDeployment(deploymentId);
    }

    @Override
    public ProcessDefinition getProcessDefinitionByDeployment(String deploymentId) {
        return repositoryService.createProcessDefinitionQuery().deploymentId(deploymentId).singleResult();
    }

    @Override
    public ProcessDefinition getProcessDefinitionByKey(String processDefinitionKey) {
        return repositoryService.createProcessDefinitionQuery().processDefinitionKey(processDefinitionKey).latestVersion().singleResult();
    }


    @Override
    public List<HistoricActivityInstance> removeCirclesFromHistoricActivities(List<HistoricActivityInstance> activityInstances) {
        Map<String, HistoricActivityInstance> lastActivity = new HashMap<String, HistoricActivityInstance>();
        for (HistoricActivityInstance activityInstance : activityInstances) {
            lastActivity.put(activityInstance.getActivityId(), activityInstance);
        }

        List<HistoricActivityInstance> result = Lists.newArrayList();
        HistoricActivityInstance waitTo = null;

        for (HistoricActivityInstance activityInstance : activityInstances) {
            //čekám dokud nenarazím na zapamatovanou aktivitu
            if (waitTo != null && !waitTo.equals(activityInstance)) {
                continue;
            } else {
                waitTo = null;
            }

            HistoricActivityInstance last = lastActivity.get(activityInstance.getActivityId());

            if (last.equals(activityInstance)) {
                //je-li tato instance aktivita totožná s posledním průchodem touto aktivitou (jsem zde poprvé a naposledy), pak je součástí zkrácené cesty
                result.add(activityInstance);
            } else {
                //jinak čakám dokud nenarazím na poslední průchod touto aktivitou
                waitTo = last;
            }
        }

        return result;
    }

    @Override
    public ProcessDefinitionDto processDefinitionToDto(ProcessDefinition processDefinition) {
        ProcessDefinitionDto processDefinitionDto = new ProcessDefinitionDto();
        processDefinitionDto.setKey(processDefinition.getKey());
        processDefinitionDto.setCategory(processDefinition.getCategory());
        processDefinitionDto.setDeploymentId(processDefinition.getDeploymentId());
        processDefinitionDto.setDiagramResourceName(processDefinition.getDiagramResourceName());
        processDefinitionDto.setResourceName(processDefinition.getResourceName());
        processDefinitionDto.setVersion(processDefinition.getVersion());
        processDefinitionDto.setTenantId(processDefinition.getTenantId());
        processDefinitionDto.setDescription(processDefinition.getDescription());
        processDefinitionDto.setName(processDefinition.getName());
        processDefinitionDto.setId(processDefinition.getId());

        return processDefinitionDto;

    }

    @Override
    public WfButton wfButtonFromMap(Map<String, Object> data) {
        WfButton btn = new WfButton();
        btn.setKey((String) data.get("key"));
        btn.setDetailPopupTemplate((String) data.get("detailPopupTemplate"));
        btn.setForward((Boolean) data.get("forward"));
        btn.setLabelMessage((String) data.get("labelMessage"));
        btn.setShowComment((Boolean) data.get("showComment"));
        btn.setShowWarning((Boolean) data.get("showWarning"));
        btn.setStyleClass((String) data.get("styleClass"));

        return btn;
    }

}
