
package org.simplifyworks.workflow.web.controller.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.simplifyworks.core.web.domain.ResponseWrapper;
import org.simplifyworks.workflow.domain.WfButton;
import org.simplifyworks.workflow.model.dto.ProcessDefinitionDto;
import org.simplifyworks.workflow.service.WorkflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

/**
 * VS
 */
@RestController
@RequestMapping(value = "/api/wf")
public class WorkflowServiceController {


    private final ObjectMapper jsonMapper = new ObjectMapper();

    @Autowired
    private WorkflowService workflowService;

    @RequestMapping(method = RequestMethod.GET, value = "/is-started")
    public ResponseWrapper isProcessStarted(String processInstanceId) {
        ResponseWrapper responseWrapper = new ResponseWrapper(workflowService.isProcessStarted(processInstanceId));
        return responseWrapper;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/is-ended")
    public ResponseWrapper isProcessEnded(String processInstanceId) {
        ResponseWrapper responseWrapper = new ResponseWrapper(workflowService.isProcessEnded(processInstanceId));
        return responseWrapper;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/deploy")
    public ResponseWrapper deployProcess(String deploymentName, String fileName, MultipartFile data) throws IOException {
        Deployment deployment = workflowService.deploy(deploymentName, fileName, data.getInputStream());
        ProcessDefinition processDefinitionByDeployment = workflowService.getProcessDefinitionByDeployment(deployment.getId());
        ResponseWrapper responseWrapper = new ResponseWrapper(workflowService.processDefinitionToDto(processDefinitionByDeployment));
        return responseWrapper;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/definitions")
    public ResponseWrapper processDefinitions() {
        List<ProcessDefinition> processDefinitions = workflowService.getProcessDefinitions();
        List<ResponseWrapper> processDefinitionDtos = Lists.newArrayList();
        if (processDefinitions != null) {
            for (ProcessDefinition pd : processDefinitions) {
                processDefinitionDtos.add(new ResponseWrapper(workflowService.processDefinitionToDto(pd)));
            }
        }
        ResponseWrapper<List<ProcessDefinitionDto>> responseWrapper = new ResponseWrapper(processDefinitionDtos);
        return responseWrapper;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/definitions/{id}")
    public ResponseWrapper processDefinitions(@PathVariable Object id) {
        Assert.notNull(id, "Insert please id process definition!");
        ProcessDefinition pd = workflowService.getProcessDefinition((String) id);
        ProcessDefinitionDto processDefinitionDto = workflowService.processDefinitionToDto(pd);
        return new ResponseWrapper(processDefinitionDto);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/definitions/{id}")
    public void deleteProcessDefinitions(@PathVariable Object id) {
        Assert.notNull(id, "Insert please id process definition!");
        ProcessDefinition pd = workflowService.getProcessDefinition((String) id);
        workflowService.deleteDeployment(pd.getDeploymentId());
    }

    @RequestMapping(value = "/deployments/{id}/download", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> downloadDeployment(@PathVariable Long id)
            throws IOException {
        Assert.notNull(id, "Insert please id deployment!");

        Deployment deployment = workflowService.getDeployment(String.valueOf(id));
        InputStream deploymentStream = workflowService.getDeploymentStream(deployment.getId(), deployment.getName());

        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        //respHeaders.setContentLength(attachment.getFilesize());
        respHeaders.setContentDispositionFormData("deployment", deployment.getName());


        InputStreamResource isr = new InputStreamResource(deploymentStream);
        return new ResponseEntity<InputStreamResource>(isr, respHeaders, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/buttons/{processInstanceId}")
    public ResponseWrapper getWfButtons(@PathVariable Object processInstanceId) {
        List<WfButton> wfButtons = workflowService.getWfButtons(processInstanceId.equals(WorkflowService.PARAMETER_NEW) ? null : (String) processInstanceId);
        return new ResponseWrapper(wfButtons);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/instances/process")
    public ResponseWrapper process(String processKey, String processInstance, @RequestBody Map<String, Object> variables) {
        Object button = variables.get(WorkflowService.PARAMETER_WF_BUTTON);
        String idEntity = (String) variables.get(WorkflowService.PARAMETER_ENTITY_ID);
        Assert.notNull(button, "Insert please wf button as item (button) to map variables !");
        Assert.notNull(idEntity, "Insert please entity id as item (entityId) to map variables !");
        Map<String, Object> result = workflowService.process(processKey, processInstance, variables, workflowService.wfButtonFromMap((Map<String, Object>) button) );
        ResponseWrapper responseWrapper = new ResponseWrapper(result);
        return responseWrapper;
    }

}
