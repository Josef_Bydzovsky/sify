/**
 * 
 */
package org.simplifyworks.core.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.junit.Test;
import org.simplifyworks.core.model.dto.AbstractDto;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Test for {@link DefaultMappingFactory}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class DefaultMappingFactoryTest {
	
	private ObjectMapper jsonMapper = new ObjectMapper();
	
	private DefaultMappingFactory defaultMappingFactory = new DefaultMappingFactory();
	
	/**
	 * Tests boolean mapping
	 */
	@Test
	public void testCreateMapping1() throws IOException {
		// execution of tested method
		XContentBuilder result = defaultMappingFactory.createMapping(TestBoolean.class);
		
		// result verification
		JsonNode json = jsonMapper.readTree(result.string());
		
		JsonNode propertiesNode = json.get("properties");
		assertNotNull(propertiesNode);
		
		JsonNode booleanFieldNode = propertiesNode.get("booleanField");
		assertNotNull(booleanFieldNode);
		assertEquals(booleanFieldNode.get("type").asText(), "boolean");
		
		JsonNode booleanObjectFieldNode = propertiesNode.get("booleanObjectField");
		assertNotNull(booleanObjectFieldNode);
		assertEquals(booleanObjectFieldNode.get("type").asText(), "boolean");			
	}
	
	/**
	 * Tests number mapping
	 */
	@Test
	public void testCreateMapping2() throws IOException {
		// execution of tested method
		XContentBuilder result = defaultMappingFactory.createMapping(TestNumber.class);
			
		// result verification
		JsonNode json = jsonMapper.readTree(result.string());
		
		JsonNode propertiesNode = json.get("properties");
		assertNotNull(propertiesNode);
		
		JsonNode intFieldNode = propertiesNode.get("intField");
		assertNotNull(intFieldNode);
		assertEquals(intFieldNode.get("type").asText(), "integer");
		
		JsonNode intObjectFieldNode = propertiesNode.get("intObjectField");
		assertNotNull(intObjectFieldNode);
		assertEquals(intObjectFieldNode.get("type").asText(), "integer");
		
		JsonNode longFieldNode = propertiesNode.get("longField");
		assertNotNull(longFieldNode);
		assertEquals(longFieldNode.get("type").asText(), "long");
		
		JsonNode longObjectFieldNode = propertiesNode.get("longObjectField");
		assertNotNull(longObjectFieldNode);
		assertEquals(longObjectFieldNode.get("type").asText(), "long");
		
		JsonNode floatFieldNode = propertiesNode.get("floatField");
		assertNotNull(floatFieldNode);
		assertEquals(floatFieldNode.get("type").asText(), "float");
		
		JsonNode floatObjectFieldNode = propertiesNode.get("floatObjectField");
		assertNotNull(floatObjectFieldNode);
		assertEquals(floatObjectFieldNode.get("type").asText(), "float");
		
		JsonNode doubleFieldNode = propertiesNode.get("doubleField");
		assertNotNull(doubleFieldNode);
		assertEquals(doubleFieldNode.get("type").asText(), "double");
		
		JsonNode doubleObjectFieldNode = propertiesNode.get("doubleObjectField");
		assertNotNull(doubleObjectFieldNode);
		assertEquals(doubleObjectFieldNode.get("type").asText(), "double");
	}
	
	/**
	 * Test big number mapping
	 */
	@Test
	public void testCreateMapping3() throws IOException {
		// execution of tested method
		XContentBuilder result = defaultMappingFactory.createMapping(TestBigNumber.class);
			
		// result verification
		JsonNode json = jsonMapper.readTree(result.string());
		
		JsonNode propertiesNode = json.get("properties");
		assertNotNull(propertiesNode);
		
		JsonNode bigIntegerFieldNode = propertiesNode.get("bigIntegerField");
		assertNotNull(bigIntegerFieldNode);
		assertEquals(bigIntegerFieldNode.get("type").asText(), "string");
		
		JsonNode bigDecimalFieldNode = propertiesNode.get("bigDecimalField");
		assertNotNull(bigDecimalFieldNode);
		assertEquals(bigDecimalFieldNode.get("type").asText(), "string");
	}
	
	/**
	 * Test string, char[] and enum mapping
	 */
	@Test
	public void testCreateMapping4() throws IOException {
		// execution of tested method
		XContentBuilder result = defaultMappingFactory.createMapping(TestStringAndEnum.class);
			
		// result verification
		JsonNode json = jsonMapper.readTree(result.string());
		
		JsonNode propertiesNode = json.get("properties");
		assertNotNull(propertiesNode);
		
		JsonNode stringFieldNode = propertiesNode.get("stringField");
		assertNotNull(stringFieldNode);
		assertEquals(stringFieldNode.get("type").asText(), "string");
		
		JsonNode charArrayFieldNode = propertiesNode.get("charArrayField");
		assertNotNull(charArrayFieldNode);
		assertEquals(charArrayFieldNode.get("type").asText(), "string");
		
		JsonNode enumFieldNode = propertiesNode.get("enumField");
		assertNotNull(enumFieldNode);
		assertEquals(enumFieldNode.get("type").asText(), "string");
	}
	
	/**
	 * Test date mapping
	 */
	@Test
	public void testCreateMapping5() throws IOException {
		// execution of tested method
		XContentBuilder result = defaultMappingFactory.createMapping(TestDate.class);
			
		// result verification
		JsonNode json = jsonMapper.readTree(result.string());
		
		JsonNode propertiesNode = json.get("properties");
		assertNotNull(propertiesNode);
		
		JsonNode dateFieldNode = propertiesNode.get("dateField");
		assertNotNull(dateFieldNode);
		assertEquals(dateFieldNode.get("type").asText(), "date");
		assertEquals(dateFieldNode.get("format").asText(), "basic_date_time");
	}
	
	/**
	 * Test static fields ignore mapping
	 */
	@Test
	public void testCreateMapping6() throws IOException {
		// execution of tested method
		XContentBuilder result = defaultMappingFactory.createMapping(TestDate.class);
			
		// result verification
		JsonNode json = jsonMapper.readTree(result.string());
		
		JsonNode propertiesNode = json.get("properties");
		assertNotNull(propertiesNode);
		
		JsonNode dateFieldNode = propertiesNode.get("staticField");
		assertNull(dateFieldNode);
	}
	
	/**
	 * Test inheritance mapping
	 */
	@Test
	public void testCreateMapping7() throws IOException {
		// execution of tested method
		XContentBuilder result = defaultMappingFactory.createMapping(TestInheritance.class);
			
		// result verification
		JsonNode json = jsonMapper.readTree(result.string());
		
		JsonNode propertiesNode = json.get("properties");
		assertNotNull(propertiesNode);
		
		JsonNode integerFieldNode = propertiesNode.get("intField");
		assertNotNull(integerFieldNode);
		assertEquals(integerFieldNode.get("type").asText(), "integer");
		
		JsonNode dateFieldNode = propertiesNode.get("dateField");
		assertNotNull(dateFieldNode);
		assertEquals(dateFieldNode.get("type").asText(), "date");
		assertEquals(dateFieldNode.get("format").asText(), "basic_date_time");
	}
	
	/**
	 * Test object mapping
	 */
	@Test
	public void testCreateMapping8() throws IOException {
		// execution of tested method
		XContentBuilder result = defaultMappingFactory.createMapping(TestObject.class);
			
		// result verification
		JsonNode json = jsonMapper.readTree(result.string());
		
		JsonNode propertiesNode = json.get("properties");
		assertNotNull(propertiesNode);
		
		JsonNode objectFieldNode = propertiesNode.get("objectField");
		assertNotNull(objectFieldNode);
		
		JsonNode objectPropertiesNode = objectFieldNode.get("properties");
		assertNotNull(objectPropertiesNode);
		
		JsonNode dateFieldNode = objectPropertiesNode.get("dateField");
		assertNotNull(dateFieldNode);
		assertEquals(dateFieldNode.get("type").asText(), "date");
		assertEquals(dateFieldNode.get("format").asText(), "basic_date_time");
	}
	
	/**
	 * Test collection mapping
	 */
	@Test
	public void testCreateMapping9() throws IOException {
		// execution of tested method
		XContentBuilder result = defaultMappingFactory.createMapping(TestCollection.class);
			
		// result verification
		JsonNode json = jsonMapper.readTree(result.string());
		
		JsonNode propertiesNode = json.get("properties");
		assertNotNull(propertiesNode);
		
		JsonNode collectionFieldNode = propertiesNode.get("collectionField");
		assertNotNull(collectionFieldNode);
		
		JsonNode collectionTypeNode = collectionFieldNode.get("type");
		assertEquals(collectionTypeNode.asText(), "nested");
		
		JsonNode collectionPropertiesNode = collectionFieldNode.get("properties");
		assertNotNull(collectionPropertiesNode);
		
		JsonNode dateFieldNode = collectionPropertiesNode.get("dateField");
		assertNotNull(dateFieldNode);
		assertEquals(dateFieldNode.get("type").asText(), "date");
		assertEquals(dateFieldNode.get("format").asText(), "basic_date_time");
	}
	
	/**
	 * Test ignored mapping
	 */
	@Test
	public void testCreateMapping10() throws IOException {
		// execution of tested method
		XContentBuilder result = defaultMappingFactory.createMapping(TestIgnore.class);
			
		// result verification
		JsonNode json = jsonMapper.readTree(result.string());
		
		JsonNode propertiesNode = json.get("properties");
		assertNotNull(propertiesNode);
		
		JsonNode ignoredFieldNode = propertiesNode.get("ignoredField");
		assertNull(ignoredFieldNode);
	}
}

class TestBoolean extends AbstractDto {
	
	boolean booleanField;
	Boolean booleanObjectField;
}

class TestNumber extends AbstractDto {
	
	int intField;
	Integer intObjectField;
	long longField;
	Long longObjectField;
	
	float floatField;
	Float floatObjectField;
	double doubleField;
	Double doubleObjectField;	
}

class TestBigNumber extends AbstractDto {

	BigInteger bigIntegerField;
	BigDecimal bigDecimalField;
}

class TestStringAndEnum extends AbstractDto {
	
	String stringField;
	char[] charArrayField;
	TestEnum enumField;
}

class TestDate extends AbstractDto {
	
	Date dateField;
}

class TestStatic extends AbstractDto {
	
	static boolean staticField;
}

class TestInheritance extends TestDate {
	
	int intField;
}

class TestObject extends AbstractDto {
	
	TestDate objectField;
}

class TestCollection extends AbstractDto {
	
	Collection<TestDate> collectionField;
}

class TestIgnore extends AbstractDto {
	
	InputStream ignoredField;
}

enum TestEnum {
}