package org.simplifyworks.test.model.domain;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class Errors {

	private StringBuilder messages = new StringBuilder();
	private int nullableErrors = 0;
	private int stringLengthErrors = 0;
	private int booleanErrors = 0;
	private int dateErrors = 0;
	private int clobErrors = 0;
	private int enumErrors = 0;
	private int otherErrors = 0;
	private int enumLengthErrors = 0;
	private int numberErrors = 0;
	private int optionalErrors = 0;
	private int triggerErrors = 0;
	private int foreignErrors = 0;

	public StringBuilder getMessages() {
		return messages;
	}

	public void setMessages(StringBuilder messages) {
		this.messages = messages;
	}

	public int getNullableErrors() {
		return nullableErrors;
	}

	public void addNullableErrors() {
		this.nullableErrors++;
	}

	public int getStringLengthErrors() {
		return stringLengthErrors;
	}

	public void addStringLengthErrors() {
		this.stringLengthErrors++;
	}

	public int getBooleanErrors() {
		return booleanErrors;
	}

	public void addBooleanErrors() {
		this.booleanErrors++;
	}

	public int getDateErrors() {
		return dateErrors;
	}

	public void addDateErrors() {
		this.dateErrors++;
	}

	public int getClobErrors() {
		return clobErrors;
	}

	public void addClobErrors() {
		this.clobErrors++;
	}

	public int getEnumErrors() {
		return enumErrors;
	}

	public void addEnumErrors() {
		this.enumErrors++;
	}

	public int getOtherErrors() {
		return otherErrors;
	}

	public void addOtherErrors() {
		this.otherErrors++;
	}

	public int getEnumLengthErrors() {
		return enumLengthErrors;
	}

	public void addEnumLengthErrors() {
		this.enumLengthErrors++;
	}

	public int getNumberErrors() {
		return numberErrors;
	}

	public void addNumberErrors() {
		this.numberErrors++;
	}

	public int getOptionalErrors() {
		return optionalErrors;
	}

	public void addOptionalErrors() {
		this.optionalErrors++;
	}

	public int getTriggerErrors() {
		return triggerErrors;
	}

	public void addTriggerErrors() {
		this.triggerErrors++;
	}

	public int getForeignErrors() {
		return foreignErrors;
	}

	public void addForeignErrors() {
		this.foreignErrors++;
	}
}
