package org.simplifyworks.test.model.entity;

import com.google.common.collect.Lists;
import java.util.List;
import org.simplifyworks.test.model.domain.ObjectType;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class TableData implements Cloneable {

	private Class clazz;
	private List<TableColumn> tableColumns;
	private List<String> checkConstraints;
	private ObjectType objectType;
	private boolean generalization = false;
	private TableColumn primaryColumn;
	private boolean auditTable = false;
	private String tableName;
	private boolean embeddableId = false;
	private boolean skipClassTesting = false;

	public boolean isAuditTable() {
		return auditTable;
	}

	public void setAuditTable(boolean auditTable) {
		this.auditTable = auditTable;
	}

	public boolean isEmbeddableId() {
		return embeddableId;
	}

	public void setEmbeddableId(boolean embeddableId) {
		this.embeddableId = embeddableId;
	}

	public TableColumn getPrimaryColumn() {
		return primaryColumn;
	}

	public void setPrimaryColumn(TableColumn primaryColumn) {
		this.primaryColumn = primaryColumn;
	}

	public ObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectType objectType) {
		this.objectType = objectType;
	}

	public Class getClazz() {
		return clazz;
	}

	public void setClazz(Class clazz) {
		this.clazz = clazz;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<TableColumn> getTableColumns() {
		if (tableColumns == null) {
			tableColumns = Lists.newArrayList();
		}
		return tableColumns;
	}

	public void setTableColumns(List<TableColumn> tableColumns) {
		this.tableColumns = tableColumns;
	}

	public List<String> getCheckConstraints() {
		return checkConstraints;
	}

	public void setCheckConstraints(List<String> checkConstraints) {
		this.checkConstraints = checkConstraints;
	}

	public boolean isGeneralization() {
		return generalization;
	}

	public void setGeneralization(boolean generalization) {
		this.generalization = generalization;
	}

	public boolean isSkipClassTesting() {
		return skipClassTesting;
	}

	public void setSkipClassTesting(boolean skipClassTesting) {
		this.skipClassTesting = skipClassTesting;
	}

	@Override
	public TableData clone() throws CloneNotSupportedException {
		return (TableData) super.clone();
	}
}
