package org.simplifyworks.test.utils;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class DbNamingConventions {

	private static final String TRIGGER_NAME_MASK = "TRG_[TABLE_NAME]_ID";
	private static final String SEQUENCE_NAME_MASK = "SEQ_[TABLE_NAME]_ID";

	/**
	 * Generates check constraint name by given tableName and columName
	 */
	public static String createCheckConstraintName(String tableName, String columnName) {
		String name = null;
		while (name == null || name.length() > 30) {
			name = tableName + "_" + columnName + "_" + "C";
			if (tableName.length() > 10) {
				tableName = tableName.substring(0, tableName.length() - 2);
			} else {
				columnName = columnName.substring(0, columnName.length() - 2);
			}
		}
		return name.replaceAll("__", "_");
	}

	/**
	 * Generates foreign constraint name by given tableName and columName
	 */
	public static String createForeignConstraintName(String tableName, String columnName) {
		String name = null;
		while (name == null || name.length() > 30) {
			name = "FK_" + tableName + "_" + columnName;
			if (tableName.length() > 10) {
				tableName = tableName.substring(0, tableName.length() - 2);
			} else {
				columnName = columnName.substring(0, columnName.length() - 2);
			}
		}
		return name.replaceAll("__", "_");
	}

	/**
	 * Generates trigger name by given tableName
	 */
	public static String createTriggerName(String tableName) {
		String name = null;
		while (name == null || name.length() > 30) {
			name = TRIGGER_NAME_MASK.replace("[TABLE_NAME]", tableName);

			tableName = tableName.substring(0, tableName.length() - 2);
		}
		return name.replaceAll("__", "_");
	}

	/**
	 * Generates sequence name by given tableName
	 */
	public static String createSequenceName(String tableName) {
		String name = null;
		while (name == null || name.length() > 30) {
			name = SEQUENCE_NAME_MASK.replace("[TABLE_NAME]", tableName);

			tableName = tableName.substring(0, tableName.length() - 2);
		}
		return name.replaceAll("__", "_");
	}

	/**
	 * Generates index name by given tableName and columName
	 */
	public static String createIndexConstraintName(String tableName, String columnName) {
		String name = null;
		while (name == null || name.length() > 30) {
			name = tableName + "_" + columnName + "_IX";
			if (tableName.length() > 10) {
				tableName = tableName.substring(0, tableName.length() - 2);
			} else {
				columnName = columnName.substring(0, columnName.length() - 2);
			}
		}
		return name.replaceAll("__", "_");
	}
}
