package org.simplifyworks.test.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.File;
import java.io.FileInputStream;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.NoResultException;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceException;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.persistence.metamodel.EntityType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.Type;
import org.simplifyworks.core.model.dao.impl.JpaAnyTypeDao;
import org.simplifyworks.core.model.domain.RecordRange;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.util.JpaUtils;
import org.simplifyworks.core.util.SuppressWarningsJUnit;
import org.simplifyworks.test.model.domain.Errors;
import org.simplifyworks.test.model.domain.ObjectType;
import org.simplifyworks.test.model.entity.ForeignConstraint;
import org.simplifyworks.test.model.entity.Index;
import org.simplifyworks.test.model.entity.Sequence;
import org.simplifyworks.test.model.entity.TableColumn;
import org.simplifyworks.test.model.entity.TableData;
import org.simplifyworks.test.model.entity.Trigger;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.core.service.EntityEvent;
import org.simplifyworks.core.service.ReadManager;

public class TestUtils {

	private static final Logger LOG = LoggerFactory.getLogger(TestUtils.class);

	// configuration
	private String dbPrefix = "";
	private boolean precisionScaleCheck = true;
	private boolean maxNumberCheck = true;
	private boolean foreignIndexCheck = true;
	private boolean foreignKeyNameCheck = true;
	private boolean triggersCheck = true;
	private boolean sequencesCheck = true;
	private boolean minimalJavaDataType = true; //lze nastavit kontrolu na nejmensi mozne datove typy v jave
	private boolean mandatoryEnums = true; //pole typu enumerace jsou vyžadována jako not null
	private boolean booleanCheck = true;
	private boolean varcharCharUsed = true;
	private EntityManager entityManager;
	private List<String> packagesToSkip;

	private final Errors errors = new Errors();
	private List<ForeignConstraint> foreignConstraints = null;
	private List<Index> indexes = null;
	private List<Trigger> triggers = null;
	private List<Sequence> sequences = null;
	private final Map<String, TableData> tables = new HashMap<String, TableData>();

	/**
	 * @param dbPrefix DB prefix pro view poskutující systémové informace
	 * @param precisionScaleCheck Kontrolovat scale, precision atribut pro
	 * číselné fieldy u Column anotací
	 * @param maxNumberCheck Kontrolovat hodnotu anotace Max pro číselné fieldy
	 * @param foreignIndexCheck Kontrola cizích klíčů na db pro vazební fieldy
	 * @param triggersCheck Kontrola existence triggeru vyplňující userMod a
	 * dateMod při insertu/updatu záznamu
	 * @param minimalJavaDataType Kontrola použití minimálních datových typů v
	 * javě v závislosti na rozsahu number v db
	 */
	public TestUtils(String dbPrefix, boolean precisionScaleCheck, boolean maxNumberCheck, boolean foreignIndexCheck, boolean triggersCheck, boolean minimalJavaDataType, boolean mandatoryEnums, boolean booleanCheck, boolean varcharCharUsed) {
		this();
		this.dbPrefix = dbPrefix;
		this.precisionScaleCheck = precisionScaleCheck;
		this.maxNumberCheck = maxNumberCheck;
		this.foreignIndexCheck = foreignIndexCheck;
		this.triggersCheck = triggersCheck;
		this.minimalJavaDataType = minimalJavaDataType;
		this.mandatoryEnums = mandatoryEnums;
		this.booleanCheck = booleanCheck;
		this.varcharCharUsed = varcharCharUsed;
	}

	public TestUtils(String dbPrefix, boolean precisionScaleCheck, boolean maxNumberCheck, boolean foreignIndexCheck, boolean triggersCheck, boolean minimalJavaDataType, boolean mandatoryEnums) {
		this(dbPrefix, precisionScaleCheck, maxNumberCheck, foreignIndexCheck, triggersCheck, minimalJavaDataType, mandatoryEnums, true, true);
	}

	public TestUtils() {
	}

	public void setDbPrefix(String dbPrefix) {
		this.dbPrefix = dbPrefix;
	}

	public void setPrecisionScaleCheck(boolean precisionScaleCheck) {
		this.precisionScaleCheck = precisionScaleCheck;
	}

	public void setForeignIndexCheck(boolean foreignIndexCheck) {
		this.foreignIndexCheck = foreignIndexCheck;
	}

	public void setTriggersCheck(boolean triggersCheck) {
		this.triggersCheck = triggersCheck;
	}

	public void setMinimalJavaDataType(boolean minimalJavaDataType) {
		this.minimalJavaDataType = minimalJavaDataType;
	}

	public void setMandatoryEnums(boolean mandatoryEnums) {
		this.mandatoryEnums = mandatoryEnums;
	}

	public void setBooleanCheck(boolean booleanCheck) {
		this.booleanCheck = booleanCheck;
	}

	public void setVarcharCharUsed(boolean varcharCharUsed) {
		this.varcharCharUsed = varcharCharUsed;
	}

	public void setSequencesCheck(boolean sequencesCheck) {
		this.sequencesCheck = sequencesCheck;
	}

	public void setMaxNumberCheck(boolean maxNumberCheck) {
		this.maxNumberCheck = maxNumberCheck;
	}

	public void setForeignKeyNameCheck(boolean foreignKeyNameCheck) {
		this.foreignKeyNameCheck = foreignKeyNameCheck;
	}

	public static boolean skipTestOnJenkins() {
		return "jenkins".equals(System.getProperty("user.name"));
	}

	/**
	 * Try to load first maxResults elements for each entity in package
	 */
	public String testGetInstances(Integer maxResults) {
		StringBuilder messages = new StringBuilder();
		int errorsCount = 0;

		SearchParameters searchParameters = new SearchParameters();
		searchParameters.setRange(new RecordRange(0, maxResults));
		for (EntityType<?> entity : entityManager.getMetamodel().getEntities()) {
			Class<?> clazz = entity.getBindableJavaType();
			if (skipClassTesting(clazz) || packagesToSkip.contains(clazz.getPackage().getName())) {
				continue;
			}
			try {
				JpaAnyTypeDao dao = new JpaAnyTypeDao(clazz);
				dao.setEntityManager(entityManager);
				List result = dao.search(searchParameters);
				System.out.println("Entity " + clazz + ": first " + result.size() + " element(s) selected");
			} catch (Exception ex) {
				errorsCount++;
				messages.append("Selecting entities for ").append(clazz).append(" failed ").append(" \n");
				ex.printStackTrace();
			}
		}

		if (messages.length() > 0) {
			messages.append(" \nResult: \ntotalErrors=").append(errorsCount);
		}

		return messages.toString();
	}

	/**
	 * Tests db structure with bean class structure
	 */
	public String testStruktury() throws SecurityException {

		initDbStructure();

		for (TableData tableData : tables.values()) {
			if (tableData.isSkipClassTesting()) {
				continue;
			}

			for (TableColumn tableColumn : tableData.getTableColumns()) {
				testColumn(tableColumn, tableData);
			}

			if (!tableData.isAuditTable()) {
				checkTriggers(tableData);
			}

			//kontrola cizích klíčů u podtabulek (v rámci generalizace) na pk nadřízené tabulky
			if (tableData.isGeneralization()) {
				checkGeneralization(tableData);
			}
		}

		printResult();

		return errors.getMessages().toString();
	}

	public String managerReindexTest() {
		StringBuilder messages = new StringBuilder();
		int errorsCount = 0;

		Set<String> scanedPackages = new HashSet<>();

		Set<EntityType<?>> entities = entityManager.getMetamodel().getEntities();
		for (EntityType<?> entityType : entities) {
			Class entity = entityType.getBindableJavaType();

			if (skipClassTesting(entity) || packagesToSkip.contains(entity.getPackage().getName())) {
				continue;
			}

			String path = entity.getPackage().getName();
			String pathToManagerImpl = path.substring(0, path.lastIndexOf(".model.entity")) + ".service.impl";

			//every package check only once
			if (scanedPackages.contains(pathToManagerImpl)) {
				continue;
			}
			scanedPackages.add(pathToManagerImpl);

			List<Class<?>> managers = getClassesInPackage(pathToManagerImpl);
			for (Class manager : managers) {
				if (!ReadManager.class.isAssignableFrom(manager) || Modifier.isAbstract(manager.getModifiers())) {
					continue;
				}

				java.lang.reflect.Type genericSuperclass = manager.getGenericSuperclass();

				Class dto = null;
				try {
					dto = (Class) ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
				} catch (Exception ex) {
					errorsCount++;
					messages.append(manager.getSimpleName() + " does not have parametrized superClass (" + ex.getMessage() + ").\n");
					continue;
				}

				Class superClass = dto;
				Class baseEntity = superClass;
				while (superClass != AbstractDto.class) {
					baseEntity = superClass;
					superClass = superClass.getSuperclass();
				}

				List<Field> allFields = getAllFields(dto);

				List<Object> allClassesToReindex = Lists.newArrayList();

				for (Field field : allFields) {
					if (field.getType().getName().startsWith("org.simplifyworks") && !Enum.class.isAssignableFrom(field.getType())) {
						allClassesToReindex.add(field);
					}
				}

				//reindex itself (in case more managers for one entity)
				allClassesToReindex.add(0, dto);

				for (Object o : allClassesToReindex) {
					superClass = (o instanceof Field ? ((Field) o).getType() : (Class) o);
					Class baseClass = superClass;
					while (superClass != AbstractDto.class) {
						baseClass = superClass;
						superClass = superClass.getSuperclass();
					}

					String baseClassName = baseClass.getSimpleName();
					baseClassName = baseClassName.substring(0, baseClassName.lastIndexOf("Dto"));

					try {
						manager.getMethod("on" + baseClassName + "Event", EntityEvent.class);
					} catch (NoSuchMethodException ex) {
						errorsCount++;
						String baseEntityName = baseEntity.getSimpleName();
						baseEntityName = baseEntityName.substring(0, baseEntityName.lastIndexOf("Dto"));

						messages.append("In " + manager.getSimpleName() + " - missing method on" + baseClassName + "Event: "
								+ (o instanceof Field
										? "public void on" + baseClassName + "Event(EntityEvent event) {"
										+ "		reindexChangedDtos(FilterBuilders.termFilter(" + baseEntityName + "_." + ((Field) o).getName() + ".getName()+\".\"+" + baseClassName + "_.id.getName(), event.getEntity().getId()));"
										+ "}\n"
										: "public void on" + baseClassName + "Event(EntityEvent event) {"
										+ "		reindexItself(event, FilterBuilders.termFilter(" + baseClassName + "_.id.getName(), event.getEntity().getId()));"
										+ "}\n"));
					}
				}
			}
		}

		if (messages.length() > 0) {
			messages.append(" \nResult: \ntotal errors=").append(errorsCount);
		}

		return messages.toString();
	}

	public String dbEntityDtoTest() {
		StringBuilder messages = new StringBuilder();
		int errorsCount = 0;

		Map<String, List<Class<?>>> packageClasses = new HashMap<>();

		Set<EntityType<?>> entities = entityManager.getMetamodel().getEntities();
		for (EntityType<?> entityType : entities) {
			Class entity = entityType.getBindableJavaType();

			if (skipClassTesting(entity) || packagesToSkip.contains(entity.getPackage().getName())) {
				continue;
			}

			String entityName = entity.getSimpleName();

			String path = entity.getPackage().getName();
			String pathToDto = path.substring(0, path.lastIndexOf(".entity")) + ".dto";

			if (!packageClasses.containsKey(pathToDto)) {
				packageClasses.put(pathToDto, getClassesInPackage(pathToDto));
			}

			int dtoCount = 0;
			for (Class dto : packageClasses.get(pathToDto)) {
				Class superClass = dto;
				while (superClass != null && !superClass.getSimpleName().equals(entityName + "Dto") && superClass != AbstractDto.class) {
					superClass = superClass.getSuperclass();
				}

				if (superClass == AbstractDto.class || superClass == null) {
					continue;
				}

				dtoCount++;

				String dtoName = dto.getSimpleName();
				List<Field> fields = getAllFields(entity);

				for (Field fieldEntity : fields) {
					String fieldName = fieldEntity.getName();

					try {
						Field fieldDto = getField(dto, fieldEntity.getName());
						String fieldDtoTypeName = fieldDto.getType().getSimpleName();
						String fieldEntityTypeName = fieldEntity.getType().getSimpleName();

						//rozdelit do samostatnych metod?
						//kontrola enumerace
						if (Enum.class.isAssignableFrom(fieldDto.getType())) {
							if (fieldDto.getType() != fieldEntity.getType()) {
								errorsCount++;
								messages.append(entityName).append(" - enum of field ").append(fieldName).append(" don't match: ")
										.append(fieldEntityTypeName).append(" X ").append(fieldDtoTypeName).append(" \n");
							}
							continue;
						}

						//kontrola vlastnich trid
						if (fieldDto.getType().getName().startsWith("org.simplifyworks")) {
							if (!fieldDtoTypeName.matches(fieldEntityTypeName + ".*Dto")) {
								errorsCount++;
								messages.append(dtoName).append(" - class of field ").append(fieldName).append(" is not for dto: ")
										.append(fieldDtoTypeName).append(" \n");
							}
							continue;
						}

						//kontrola kolekci
						if (Collection.class.isAssignableFrom(fieldEntity.getType())) {
							ParameterizedType entityListType = (ParameterizedType) fieldEntity.getGenericType();
							Class<?> entityListClass = (Class<?>) entityListType.getActualTypeArguments()[0];
							fieldEntityTypeName = entityListClass.getSimpleName();

							ParameterizedType dtoListType = (ParameterizedType) fieldDto.getGenericType();
							Class<?> dtoListClass = (Class<?>) dtoListType.getActualTypeArguments()[0];
							fieldDtoTypeName = dtoListClass.getSimpleName();

							if (fieldDto.getType().getName().startsWith("org.simplifyworks")) { //jedna se o List<vlastni trida>
								if (!fieldDtoTypeName.matches(fieldEntityTypeName + ".*Dto")) {
									errorsCount++;
									messages.append(dtoName).append(" - classes from collection of field ").append(fieldName).append(" are not for dto: ")
											.append(dtoListClass.getSimpleName()).append(" \n");
								}
							} else { //jedna se o List<String>, atd
								if (fieldDto.getType() != fieldEntity.getType()) {
									;
									errorsCount++;
									messages.append(entityName).append(" - classes from collection of field ").append(fieldName).append(" don't match: ")
											.append(fieldEntityTypeName).append(" X ").append(fieldDtoTypeName).append(" \n");
								}
							}
							continue;
						}

						//kontrola ostatnich - String, Integer, atd
						if (fieldDto.getType() != fieldEntity.getType()) {
							errorsCount++;
							messages.append(entityName).append("/").append(dtoName).append(" - data type of field ").append(fieldName).append(" don't match: ")
									.append(fieldEntityTypeName).append(" X ").append(fieldDtoTypeName).append(" \n");
						}

					} catch (NoSuchFieldException e) {
						//LOG.info(entityName + " - corresponding field for " + fieldName + " not found in " + dtoName);
						//e.printStackTrace();
					}
				}
			}

			if (dtoCount == 0) {
				errorsCount++;
				messages.append("No dto for " + entityName + " not found.\n");
			}
		}

		if (messages.length() > 0) {
			messages.append(" \nResult: \ntotal errors=").append(errorsCount);
		}

		return messages.toString();
	}

	public static final List<Class<?>> getClassesInPackage(String packageName) {
		String path = packageName.replace(".", File.separator);
		String pathInJar = packageName.replace(".", "/");
		List<Class<?>> classes = new ArrayList<>();
		String[] classPathEntries = System.getProperty("java.class.path").split(
				System.getProperty("path.separator")
		);

		String name;
		for (String classpathEntry : classPathEntries) {
			if (classpathEntry.endsWith(".jar")) {
				File jar = new File(classpathEntry);
				try {
					JarInputStream is = new JarInputStream(new FileInputStream(jar));
					JarEntry entry;
					while ((entry = is.getNextJarEntry()) != null) {
						name = entry.getName();
						if (name.endsWith(".class")) {
							if (name.contains(pathInJar) && name.endsWith(".class")) {
								String classPath = name.substring(0, entry.getName().length() - 6);
								classPath = classPath.replaceAll("[\\|/]", ".");
								classes.add(Class.forName(classPath));
							}
						}
					}
				} catch (Exception ex) {
					// Silence is gold
				}
			} else {
				try {
					File base = new File(classpathEntry + File.separatorChar + path);
					for (File file : base.listFiles()) {
						name = file.getName();
						if (name.endsWith(".class")) {
							name = name.substring(0, name.length() - 6);
							classes.add(Class.forName(packageName + "." + name));
						}
					}
				} catch (Exception ex) {
					// Silence is gold
				}
			}
		}

		return classes;
	}

	/**
	 * Checks number type that corresponds with db
	 */
	private void checkNumberType(String dataType, ObjectType objectType, Column column, Max max, Field field, Class clazz, BigInteger dataPrecision, BigInteger dataScale, String tableName, TableData tableData) {
		//v auditovacích tabulkách generovaných a plněných pouze přes hibernate je povolen rozsah o 1 větší
		int auditTableCorrection = (tableData.isAuditTable() ? 1 : 0);
		int precisionBigInteger = 18;
		int precisionLong = 9;
		int precisionInteger = 4;

		if (field.getType() == Boolean.class || field.getType() == Boolean.TYPE) {
			return;
		}

		if (field.getType().isEnum() && field.getAnnotation(Enumerated.class).value() == EnumType.ORDINAL) {
			return;
		}

		if (dataPrecision == null || dataScale == null) {
			if (objectType == ObjectType.TABLE) {
				errors.getMessages().append("--checkNumberType, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(", db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append(") \n");
				int precision = BigInteger.class == field.getType() ? 38 : (Long.class == field.getType() ? precisionBigInteger + auditTableCorrection : (Integer.class == field.getType() ? precisionLong + auditTableCorrection : precisionInteger + auditTableCorrection));
				errors.getMessages().append("ALTER TABLE ").append(tableData.getTableName()).append(" ADD (TMP NUMBER(").append(precision).append(",0));\n" + "UPDATE ").append(tableData.getTableName()).append(" SET TMP=").append(column.name()).append(";\n" + "ALTER TABLE ").append(tableData.getTableName()).append(" DROP COLUMN ").append(column.name()).append(";\n" + "ALTER TABLE ").append(tableData.getTableName()).append(" RENAME COLUMN TMP TO ").append(column.name()).append(";\n");
				errors.addNumberErrors();
			}

			return;
		}

		if (maxNumberCheck && !field.isAnnotationPresent(Id.class) && (objectType == ObjectType.TABLE)) {
			if (max != null) {
				if (BigDecimal.valueOf(10).pow(dataPrecision.intValue() - dataScale.intValue()).compareTo(BigDecimal.valueOf(max.value())) <= 0) {
					errors.getMessages().append("--checkNumberType, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append("(").append(column.precision()).append(",").append(column.scale()).append("), db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append("), max: " + max.value() + " \n");
					errors.addNumberErrors();
					return;
				}
			} else {
				BigDecimal scale10 = BigDecimal.valueOf(10).pow(dataScale.intValue());
				BigDecimal maxPossibleValue = BigDecimal.valueOf(10).pow(dataPrecision.intValue()).subtract(BigDecimal.ONE).divide(scale10);
				if (maxPossibleValue.compareTo(BigDecimal.valueOf(Long.MAX_VALUE)) < 1) {
					errors.getMessages().append("--checkNumberType, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append("(").append(column.precision()).append(",").append(column.scale()).append("), db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append("), chybí max value " + maxPossibleValue + " \n");
					errors.addNumberErrors();
				}
				return;
			}
		}

		if (precisionScaleCheck && objectType == ObjectType.TABLE) {
			if (column.precision() != dataPrecision.intValue() || column.scale() != dataScale.intValue()) {
				errors.getMessages().append("--checkNumberType, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append("(").append(column.precision()).append(",").append(column.scale()).append("), db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append(") \n");
				errors.addNumberErrors();
				return;
			}
		}

		if (dataScale.intValue() == 0) {
			if (dataPrecision.intValue() > precisionBigInteger + 1 /* + auditTableCorrection*/) {
				if (BigInteger.class != field.getType()) {
					errors.getMessages().append("--checkNumberJavaType, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(" (can be BigInteger), db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append(") \n");
					errors.addNumberErrors();
				}
			} else if (dataPrecision.intValue() > precisionLong + 1 /* + auditTableCorrection*/) {
				if (Long.class != field.getType() && Long.TYPE != field.getType() && (minimalJavaDataType || BigInteger.class != field.getType())) {
					errors.getMessages().append("--checkNumberJavaType, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(" (can be Long), db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append(") \n");
					errors.addNumberErrors();
				}
			} else if (dataPrecision.intValue() > precisionInteger + 1 /* + auditTableCorrection*/) {
				if (Integer.class != field.getType() && Integer.TYPE != field.getType() && (minimalJavaDataType || Long.class != field.getType()) && (minimalJavaDataType || BigInteger.class != field.getType())) {
					errors.getMessages().append("--checkNumberJavaType, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(" (can be Integer), db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append(") \n");
					errors.addNumberErrors();
				}
			} else {
				if (Short.class != field.getType() && Short.TYPE != field.getType() && (minimalJavaDataType || Integer.class != field.getType()) && (minimalJavaDataType || Long.class != field.getType()) && (minimalJavaDataType || BigInteger.class != field.getType())) {
					errors.getMessages().append("--checkNumberJavaType, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(" (can be Short), db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append(") \n");
					errors.addNumberErrors();
				}
			}
		} else {
			if (dataPrecision.intValue() > 15) {
				if (BigDecimal.class != field.getType()) {
					errors.getMessages().append("--checkNumberJavaType, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(" (can be BigDecimal), db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append(") \n");
					errors.addNumberErrors();
				}
			} else {
				if (Double.class != field.getType() && Double.TYPE != field.getType() && (minimalJavaDataType || BigDecimal.class != field.getType())) {
					errors.getMessages().append("--checkNumberJavaType, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(", (can be Double) db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append(") \n");
					errors.addNumberErrors();
				}
			}
		}
	}

	/**
	 * Checks clob type that corresponds with db
	 */
	private void checkClobType(Field field, Class clazz, Column column) {
		if (field.getType() != String.class || !field.isAnnotationPresent(Lob.class) || column.length() != -1) {
			errors.getMessages().append("--checkClobType, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(", length: ").append(column.length()).append(", @Lob: ").append(field.isAnnotationPresent(Lob.class)).append(", db-type: CLOB \n");
			errors.addClobErrors();
		}
	}

	/**
	 * Checks date type that corresponds with db
	 */
	private void checkDateType(String dataType, Class clazz, Field field) {
		if (!"DATE".equalsIgnoreCase(dataType) && !"DATETIME".equalsIgnoreCase(dataType) && !dataType.startsWith("TIMESTAMP")) {
			errors.getMessages().append("--checkDateType, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", type: Date, db-type: ").append(dataType).append(" \n");
			errors.addDateErrors();
		}
	}

	/**
	 * Checks db for constraint "COLUMN IN (0,1)"
	 */
	private void checkBooleanConstraint(List<String> constraints, Column column, Class clazz, Field field, String tableName) {
		if (JpaUtils.isMySQL(getEntityManager())) {
			return; // SIFY-64 Do not check constraints for MySQL connections
		}

		String existingBooleanConstraint = null;
		for (String constraint : constraints) {
			if (constraint.toUpperCase().matches("^[\"']?" + column.name().toUpperCase() + "[\"']?\\s*IN\\s*\\(\\s*0\\s*,\\s*1\\s*\\).*")) {
				existingBooleanConstraint = constraint;
			}
		}
		if (existingBooleanConstraint == null) { //kontrola na chybějící constraint
			errors.getMessages().append("--checkBooleanConstraint, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(", missing check constraint on db:\n" + "ALTER TABLE ").append(tableName).append(" ADD CONSTRAINT ").append(DbNamingConventions.createCheckConstraintName(tableName, column.name())).append(" CHECK (").append(column.name()).append(" IN (0,1)); \n");
			errors.addBooleanErrors();
		}
	}

	/**
	 * Checks db type for NUMBER(1,0)
	 */
	private void checkBooleanType(String dataType, BigInteger dataPrecision, BigInteger dataScale, Class clazz, Field field) {
		if ((!"NUMBER".equalsIgnoreCase(dataType) || dataPrecision == null || dataPrecision.intValue() != 1 || dataScale == null || dataScale.intValue() != 0)
				&& (!"BIT".equalsIgnoreCase(dataType))
				&& (!"TINYINT".equalsIgnoreCase(dataType))) {
			errors.getMessages().append("--checkBooleanType, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", type: Boolean, db-type: ").append(dataType).append("(").append(dataPrecision != null ? dataPrecision.intValue() : "null").append(",").append(dataScale != null ? dataScale.intValue() : "null").append(") \n");
			errors.addBooleanErrors();
		}
	}

	/**
	 * Checks db constraint for manyToOne field
	 */
	private void checkForeignConstraint(Map<String, TableData> tables, List<ForeignConstraint> constraints, List<Index> indexes, TableColumn tableColumn, TableData tableData) {
		Field field = tableColumn.getField();
		JoinColumn joinColumn = tableColumn.getJoinColumn();
		JoinColumns joinColumns = tableColumn.getJoinColumns();
		Class clazz = tableData.getClazz();
		String tableName = tableData.getTableName();

		Entity referencedTable = null;
		String referencedTableName = null;
		Class<?> type = field.getType();
		while (referencedTable == null) {
			referencedTable = type.getAnnotation(Entity.class);
			referencedTableName = javaNameToDbName(type.getSimpleName());
			type = type.getSuperclass();
		}

		//jedná-li se o tabulku/view aplikace, pak kontrolujeme zda je to tabulka, pokud je z jiné aplikace (package) pak informaci o typu nemám a kontrolu zatím provádím, chtělo by to doladit
		if (tables.get(referencedTableName) != null && tables.get(referencedTableName).getObjectType() != ObjectType.TABLE) {
			return;
		}

		//skip special "fake" fields
		if (joinColumn != null && joinColumn.insertable() == false) {
			return;
		}

		if (joinColumn != null) {
			checkSingleConstraint(constraints, tableName, referencedTableName, clazz, indexes, joinColumn.name(), joinColumn.referencedColumnName(), field);
		} else if (joinColumns != null) {
			//tuto vazbu kontrolujeme pouze pro první sloupec ze složeného klíče, další by se volal se stejnými parametry a vyhodil by znovu tutéž chybu
			if (!tableColumn.isSkipJoinColumns()) {
				checkMultiConstraint(constraints, tableName, referencedTableName, clazz, indexes, joinColumns, field);
			}
		}
	}

	/**
	 * Checks db for constraint "COLUMN IN (enum.values())" and vice versa
	 */
	private void checkEnumConstraints(List<String> constraints, Column column, Field field, Class clazz, String tableName, String nullable) {
		if (JpaUtils.isMySQL(getEntityManager())) {
			return; // SIFY-64 Do not check constraints for MySQL connections
		}

		String existingCheckConstraint = null;
		for (String constraint : constraints) {
			String inRegexp = "^[\"']?" + column.name().toUpperCase() + "[\"']?\\s*IN\\s*\\(.*";
			String equalsRegexp = "^[\"']?" + column.name().toUpperCase() + "[\"']?\\s*=\\s*'.*'";
			if (constraint.toUpperCase().matches(inRegexp) || constraint.toUpperCase().matches(equalsRegexp)) {
				existingCheckConstraint = constraint;
			}
		}

		if (mandatoryEnums && field.getType().isEnum() && "Y".equalsIgnoreCase(nullable)) {
			errors.getMessages().append("--checkEnumConstraints, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(", missing nullable=false on db: \n"
					+ "ALTER TABLE ").append(tableName).append(" MODIFY (").append(column.name()).append(" NOT NULL); \n");
			errors.addEnumErrors();
		}

		if (field.getType().isEnum()) {
			StringBuilder constraintConstants = new StringBuilder();

			for (Object e : field.getType().getEnumConstants()) {
				constraintConstants.append("'").append(((Enum) e).name()).append("',");
			}
			constraintConstants.setLength(constraintConstants.length() - 1);

			if (existingCheckConstraint == null) { //kontrola na chybějící constraint
				errors.getMessages().append("--checkEnumConstraints, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(", missing check constraint on db: \n" + "ALTER TABLE ").append(tableName).append(" ADD CONSTRAINT ").append(DbNamingConventions.createCheckConstraintName(tableName, column.name())).append(" CHECK (").append(column.name()).append(" IN (").append(constraintConstants).append(")); \n");
				errors.addEnumErrors();
			} else {//kontrola konstant v enumeraci/constraintu
				String values;
				if (existingCheckConstraint.contains("(")) {
					values = existingCheckConstraint.substring(existingCheckConstraint.indexOf("(") + 1, existingCheckConstraint.indexOf(")") - 1);
				} else {
					values = existingCheckConstraint.substring(existingCheckConstraint.indexOf("=") + 2, existingCheckConstraint.length() - 1);
				}
				values = values.replaceAll("^'", "").replaceAll("'$", "");
				List<String> existingConstraintConstants = Arrays.asList(values.split("'?\\s*,\\s*'?"));
				List<String> enumConstants = new ArrayList<String>();

				for (Object e : field.getType().getEnumConstants()) {
					enumConstants.add(((Enum) e).name());
				}
				if (!existingConstraintConstants.containsAll(enumConstants)) {
					errors.getMessages().append("--checkEnumConstraints, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", constraint on db does not contain all enum values: \n")
							.append("ALTER TABLE ").append(tableName).append(" ADD CONSTRAINT ").append(DbNamingConventions.createCheckConstraintName(tableName, column.name())).append(" CHECK (").append(column.name()).append(" IN (").append(constraintConstants).append(")); )\n");
					errors.addEnumErrors();
				}
				if (!enumConstants.containsAll(existingConstraintConstants)) {
					ArrayList<String> clone = new ArrayList<String>(existingConstraintConstants);
					for (String s : enumConstants) {
						clone.remove(s);
					}
					errors.getMessages().append("--checkEnumConstraints, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", enum does not contain all constraint values ").append(clone.toString()).append("\n");
					errors.addEnumErrors();
				}
			}
		} else if (existingCheckConstraint != null) { //obrácená kontrola na chybějící enumeraci
			if (field.getType() == Boolean.class && field.isAnnotationPresent(Type.class)) {
				return; //TODO T/F na db mapované na boolean zatím ignorujeme
			}

			errors.getMessages().append("--checkEnumConstraints, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", missing enum in java for constrained value on db \n");
			errors.addEnumErrors();
		}
	}

	/**
	 * Checks length of varchar on db and string
	 */
	private void checkStringLength(Field field, Column column, Size size, BigInteger charLength, TableData tableData) {
		if ((field.getType() != String.class) || (column.length() != charLength.intValue() || (size == null || size.max() != charLength.intValue()))) {
			if (field.getType() == Boolean.class && field.isAnnotationPresent(Type.class)) {
				return; //TODO T/F na db mapované na boolean zatím ignorujeme
			}

			errors.getMessages().append("--checkStringLength, Class: ").append(tableData.getClazz().getSimpleName()).append(", table: ").append(tableData.getTableName()).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append("(").append(column.length()).append("), db-type: VARCHAR2(").append(charLength.intValue()).append(") \n");
			errors.addStringLengthErrors();
		}
	}

	/**
	 * Checks length of all enum constants with size of db column
	 */
	private void checkEnumLength(Field field, BigInteger charLength, Class clazz, String tableName) {
		int maxEnumConstantLength = 0;
		for (Object e : field.getType().getEnumConstants()) {
			if (maxEnumConstantLength < ((Enum) e).name().length()) {
				maxEnumConstantLength = ((Enum) e).name().length();
			}
		}
		if (charLength.intValue() < maxEnumConstantLength) {
			errors.getMessages().append("--checkEnumLength, Class: ").append(clazz.getSimpleName()).append(", table: ").append(tableName).append(", field: ").append(field.getName()).append(", type ").append(field.getType().getSimpleName()).append(" contains longer constants than ").append(charLength.intValue()).append(" \n");
			errors.addEnumLengthErrors();
		}
	}

	/**
	 * Checks nullable value of field and of column
	 */
	private void checkNullable(Column column, String tableName, String nullable, Class clazz, Field field) {
		if ((column.nullable() != "Y".equalsIgnoreCase(nullable)) && !field.isAnnotationPresent(Id.class) && !field.isAnnotationPresent(Version.class)) {

			//kontrolu lze vypnout pomocí anotace u fieldu
			SuppressWarningsJUnit suppressWarningsJUnit = field.getAnnotation(SuppressWarningsJUnit.class);
			if (suppressWarningsJUnit != null && Arrays.asList(suppressWarningsJUnit.value()).contains(SuppressWarningsJUnit.SKIP_NOTNULL_UNIT_TEST)) {
				return;
			}
			errors.getMessages().append("--checkNullable, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", nullable: ")
					.append(column.nullable())
					.append(", db-nullable: ")
					.append(nullable).append(", change in class or on db: \nALTER TABLE ").append(tableName).append(" MODIFY (")
					.append(column.name())
					.append(" ")
					.append(column.nullable() ? "" : "NOT ")
					.append("NULL);\n");
			errors.addNullableErrors();
		}
		if (!field.isAnnotationPresent(Id.class) && !field.isAnnotationPresent(Version.class)) {
			if ((field.isAnnotationPresent(NotNull.class) == "Y".equalsIgnoreCase(nullable))) {

				//kontrolu lze vypnout pomocí anotace u fieldu
				SuppressWarningsJUnit suppressWarningsJUnit = field.getAnnotation(SuppressWarningsJUnit.class);
				if (suppressWarningsJUnit != null && Arrays.asList(suppressWarningsJUnit.value()).contains(SuppressWarningsJUnit.SKIP_NOTNULL_UNIT_TEST)) {
					return;
				}
				errors.getMessages().append("--checkNullable, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", @NotNull: ")
						.append(field.isAnnotationPresent(NotNull.class))
						.append(", db-nullable: ")
						.append(nullable).append(", change in class or on db: \nALTER TABLE ").append(tableName).append(" MODIFY (")
						.append(column.name())
						.append(" ")
						.append(column.nullable() ? "" : "NOT ")
						.append("NULL);\n");
				errors.addNullableErrors();
			}
		} else if (field.isAnnotationPresent(NotNull.class)) {
			errors.getMessages().append("--checkNullable, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", @Id - @NotNull can not be present;\n");
			errors.addNullableErrors();
		}
	}

	/**
	 * Checks optional value of field and of ManyToOne
	 */
	private void checkOptional(JoinColumn joinColumn, ManyToOne manyToOne, OneToOne oneToOne, String nullable, Field field, TableData tableData) {
		boolean optional = manyToOne != null ? manyToOne.optional() : oneToOne.optional();
		if (joinColumn != null && (joinColumn.nullable() == false || optional == false) != ("N".equalsIgnoreCase(nullable))) {
			//jedná-li se o položku pouze pro čtení, pak nekontrolujeme
			if (!(joinColumn.insertable() == false && joinColumn.updatable() == false)) {
				errors.getMessages().append("--checkOptional, Class: ").append(tableData.getClazz().getSimpleName()).append(", field: ").append(field.getName()).append(", optional: ").append(optional).append(", nullable: ").append(joinColumn.nullable()).append(", db-nullable: ").append(nullable).append(" \n")
						.append("ALTER TABLE ").append(tableData.getTableName()).append(" MODIFY (").append(joinColumn.name()).append(" NOT NULL); \n");
				errors.addOptionalErrors();
			}
		}
	}

	/**
	 * Check existing foreign constraint
	 */
	private void checkSingleConstraint(List<ForeignConstraint> constraints, String tableName, String referencedTable, Class clazz, List<Index> indexes, String columnName, String referencedColumnName, Field field) {
		ForeignConstraint existingForeignConstraint = null;
		if (referencedColumnName.isEmpty()) {
			referencedColumnName = "ID";
		}
		String fkToDrop = "";
		for (ForeignConstraint constraint : constraints) {
			if (constraint.getTableName().equals(tableName) && constraint.getColumn().equals(columnName) && constraint.getReferencedColumn().equals(referencedColumnName)
					&& constraint.getReferencedTable().equals(referencedTable)) {
				if (foreignKeyNameCheck && constraint.getName().matches(".*\\d.*")) {
					fkToDrop = "ALTER TABLE " + tableName + " DROP CONSTRAINT " + constraint.getName() + ";\n";
				} else {
					existingForeignConstraint = constraint;
				}
				break;
			}
		}

		//vazby na view nebo neznámé tabulky nekontrolujeme
		if (existingForeignConstraint == null && (tables.get(referencedTable) != null && tables.get(referencedTable).getObjectType() == ObjectType.TABLE)) {
			errors.getMessages().append("--checkForeignConstraints, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(", column: ").append(columnName).append(", missing foreign constraint on db: \n"
					+ fkToDrop
					+ "ALTER TABLE ").append(tableName).append(" ADD CONSTRAINT ").append(DbNamingConventions.createForeignConstraintName(tableName, columnName)).append(" FOREIGN KEY (").append(columnName).append(") REFERENCES ").append(referencedTable).append(" (").append(referencedColumnName).append(");\n");
			errors.addForeignErrors();
		}
		Index existingIndex = null;
		for (Index index : indexes) {
			if (index.getTableName().equals(tableName) && index.getColumn().equals(columnName)) {
				existingIndex = index;
				break;
			}
		}
		if (existingIndex == null) {
			errors.getMessages().append("--checkForeignConstraints, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(", column: ").append(columnName).append(", missing index on db: \n")
					.append("CREATE INDEX ").append(DbNamingConventions.createIndexConstraintName(tableName, columnName))
					.append(" ON ").append(tableName).append(" (").append(columnName).append(");\n");
			errors.addForeignErrors();
		}
	}

	private void checkMultiConstraint(List<ForeignConstraint> constraints, String tableName, String referencedTable, Class clazz, List<Index> indexes, JoinColumns joinColumns, Field field) {
		int existingForeignConstraintCount = 0;
		Set<String> existingForeignConstraintNames = Sets.newHashSet();
		String columnNames = "";
		String referencedColumnNames = "";
		for (JoinColumn joinColumn : joinColumns.value()) {
			String columnName = joinColumn.name();
			columnNames = columnNames + "," + columnName;
			String referencedColumnName = joinColumn.referencedColumnName();
			if (referencedColumnName.isEmpty()) {
				referencedColumnName = "ID";
			}
			referencedColumnNames = referencedColumnNames + "," + referencedColumnName;
			for (ForeignConstraint constraint : constraints) {
				if (constraint.getTableName().equals(tableName) && constraint.getColumn().equals(columnName) && constraint.getReferencedColumn().equals(referencedColumnName)
						&& constraint.getReferencedTable().equals(referencedTable)) {
					existingForeignConstraintCount++;
					existingForeignConstraintNames.add(constraint.getName());
					break;
				}
			}
		}

		//v systémových tabulkách je složený cizí klíč uložen jako více řádků se stejným jménem, každý odpovídá jedné části cizího klíče
		if (existingForeignConstraintCount != joinColumns.value().length || existingForeignConstraintNames.size() > 1) {
			errors.getMessages().append("--checkForeignConstraints, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(", columns: (").append(columnNames.substring(1)).append("), missing foreign constraint on db: \n"
					+ "ALTER TABLE ").append(tableName).append(" ADD CONSTRAINT ").append(DbNamingConventions.createForeignConstraintName(tableName, field.getName().toUpperCase())).append(" FOREIGN KEY (").append(columnNames.substring(1)).append(") REFERENCES ").append(referencedTable).append(" (").append(referencedColumnNames.substring(1)).append(");\n");
			errors.addForeignErrors();
		}

		int existingIndexesCount = 0;
		Set<String> existingIndexesNames = Sets.newHashSet();
		for (JoinColumn joinColumn : joinColumns.value()) {
			String columnName = joinColumn.name();
			for (Index index : indexes) {
				if (index.getTableName().equals(tableName) && index.getColumn().equals(columnName)) {
					existingIndexesCount++;
					existingIndexesNames.add(index.getName());
					break;
				}
			}
		}

		if (existingIndexesCount != joinColumns.value().length || existingIndexesNames.size() > 1) {
			errors.getMessages().append("--checkForeignConstraints, Class: ").append(clazz.getSimpleName()).append(", field: ").append(field.getName()).append(", type: ").append(field.getType().getSimpleName()).append(", columns: (").append(columnNames.substring(1)).append("), missing index on db: \n")
					.append("CREATE INDEX ").append(DbNamingConventions.createIndexConstraintName(tableName, field.getName().toUpperCase()))
					.append(" ON ").append(tableName).append(" (").append(columnNames.substring(1)).append(");\n");
			errors.addForeignErrors();
		}
	}

	/**
	 * Získá potřebná data o struktuře databáze z databázových systémových view
	 */
	private void initDbStructure() throws PersistenceException, SecurityException {
		String query;
		Query nativeQuery;

		if (foreignIndexCheck) {
			query = "select TABLE_NAME, COLUMN_NAME, R_TABLE_NAME, R_COLUMN_NAME, CONSTRAINT_NAME from " + dbPrefix + "ALL_FOREIGN_CONSTRAINTS_VIEW";
			nativeQuery = entityManager.createNativeQuery(query, ForeignConstraint.class);
			foreignConstraints = new ArrayList<ForeignConstraint>(nativeQuery.getResultList());

			query = "select TABLE_NAME, COLUMN_NAME, INDEX_NAME from " + dbPrefix + "ALL_INDEXES_VIEW";
			nativeQuery = entityManager.createNativeQuery(query, Index.class);
			indexes = new ArrayList<Index>(nativeQuery.getResultList());
		}

		if (triggersCheck) {
			query = "select TRIGGER_NAME, TABLE_NAME from " + dbPrefix + "ALL_TRIGGERS_VIEW";
			nativeQuery = entityManager.createNativeQuery(query, Trigger.class);
			triggers = new ArrayList<Trigger>(nativeQuery.getResultList());
		}

		if (sequencesCheck) {
			query = "select SEQUENCE_NAME from " + dbPrefix + "ALL_SEQUENCES_VIEW";
			nativeQuery = entityManager.createNativeQuery(query, Sequence.class);
			sequences = new ArrayList<Sequence>(nativeQuery.getResultList());
		}

		for (EntityType<?> entity : entityManager.getMetamodel().getEntities()) {
			Class clazz = entity.getBindableJavaType();
			if (skipClassTesting(clazz)) {
				continue;
			}

			Table table = (Table) clazz.getAnnotation(Table.class);

			TableData tableData = new TableData();
			fillTableData(tableData, clazz, table != null ? table.name() : javaNameToDbName(clazz.getSimpleName()), false);

			if (clazz.isAnnotationPresent(Audited.class)) {
				tableData = new TableData();
				tableData.setAuditTable(true);
				fillTableData(tableData, clazz, table != null ? table.name() : javaNameToDbName(clazz.getSimpleName()) + "_AUD", true);
			}
		}
	}

	private static String javaNameToDbName(String javaName) {
		return javaName.replaceAll("([a-z])([A-Z])", "$1_$2").toUpperCase();
	}

	/**
	 * Získá potřebná data o struktuře tabulky z databázových systémových view
	 */
	private void fillTableData(TableData tableData, Class clazz, String tableName, boolean auditTable) throws SecurityException {
		String query;
		Query nativeQuery;

		//inheritance
		Class subClass = clazz.getSuperclass();
		while (subClass != null && (subClass.isAnnotationPresent(MappedSuperclass.class) || subClass.isAnnotationPresent(Entity.class))) {
			if (subClass.isAnnotationPresent(Inheritance.class) && ((Inheritance) subClass.getAnnotation(Inheritance.class)).strategy() == InheritanceType.SINGLE_TABLE) {
				Table table = (Table) subClass.getAnnotation(Table.class);
				tableName = table != null ? table.name() : javaNameToDbName(clazz.getSimpleName());
			}

			subClass = subClass.getSuperclass();
		}

		tableData.setSkipClassTesting(packagesToSkip.contains(clazz.getPackage().getName()));

		query = "select OBJECT_TYPE from " + dbPrefix + "ALL_OBJECTS_VIEW where table_name = '" + tableName + "'";
		nativeQuery = entityManager.createNativeQuery(query);
		try {
			String objectType = (String) nativeQuery.getSingleResult();
			tableData.setObjectType(ObjectType.valueOf(objectType));
		} catch (NoResultException ex) {
			errors.getMessages().append("--No result for: '").append(query).append("' \n");
			errors.addOtherErrors();
		}

		query = "select SEARCH_CONDITION from " + dbPrefix + "ALL_CHECK_CONSTRAINTS_VIEW where table_name = '" + tableName + "'";
		nativeQuery = entityManager.createNativeQuery(query);
		List<String> checkConstraints = new ArrayList<String>(nativeQuery.getResultList());
		tableData.setCheckConstraints(checkConstraints);

		tableData.setGeneralization(clazz.isAnnotationPresent(PrimaryKeyJoinColumn.class));
		tableData.setTableName(tableName);
		tableData.setClazz(clazz);
		tables.put(tableName, tableData);

		List<Field> declaredFields = getAllFields(clazz);

		for (Field field : declaredFields) {
			if (Modifier.isStatic(field.getModifiers())) {
				continue;
			}
			if (auditTable && field.isAnnotationPresent(NotAudited.class)) {
				continue;
			}
			Column column = field.getAnnotation(Column.class);
			ManyToOne manyToOne = field.getAnnotation(ManyToOne.class);
			OneToOne oneToOne = field.getAnnotation(OneToOne.class);
			JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
			JoinColumns joinColumns = field.getAnnotation(JoinColumns.class);
			Size size = field.getAnnotation(Size.class);
			Max max = field.getAnnotation(Max.class);

			if (field.isAnnotationPresent(JoinColumnsOrFormulas.class) || field.isAnnotationPresent(Formula.class)) {
				//zatím ignorujeme, netestujeme dále
				continue;
			}

			if (field.isAnnotationPresent(EmbeddedId.class)) {
				tableData.setEmbeddableId(true);
				continue;
			}

			if (oneToOne != null && !oneToOne.mappedBy().isEmpty()) {
				//u této vazby není co kontrolovat, kontrolujeme z druhé strany
				continue;
			}

			if (column == null && manyToOne == null && oneToOne == null) {
				if (!field.isAnnotationPresent(Transient.class) && !Modifier.isTransient(field.getModifiers())
						&& !field.isAnnotationPresent(OneToMany.class)) {
					errors.getMessages().append("--Missing Column annotation for field '").append(field.getName()).append("' in class '").append(clazz.getSimpleName()).append("' \n");
					errors.addOtherErrors();
				}
				continue;
			}

			if (column != null && !column.table().isEmpty()) {
				//ignorujeme, netestujeme dále
				continue;
			}

			List<String> dbColumnNames = Lists.newArrayList();
			//pro join columns testujeme existenci cizího klíče a indexů najednou, proto další sloupce při této kontrole přeskakujeme
			List<String> dbColumnNamesToSkipJoinColumns = Lists.newArrayList();
			if (column != null) {
				dbColumnNames.add(column.name() != null && !column.name().isEmpty() ? column.name() : javaNameToDbName(field.getName()));
			} else if (manyToOne != null) {
				if (joinColumn == null && joinColumns == null) {
					errors.getMessages().append("--Missing JoinColumn(s) annotation for field '").append(field.getName()).append("' in class '").append(clazz.getSimpleName()).append("' \n");
					errors.addOtherErrors();
					continue;
				}
			} else if (oneToOne != null && oneToOne.mappedBy().isEmpty()) {
				if (joinColumn == null && joinColumns == null) {
					errors.getMessages().append("--Missing JoinColumn annotation for field '").append(field.getName()).append("' in class '").append(clazz.getSimpleName()).append("' \n");
					errors.addOtherErrors();
					continue;
				}
			}

			if (manyToOne != null || (oneToOne != null && oneToOne.mappedBy().isEmpty())) {
				if (joinColumn != null) {
					dbColumnNames.add(joinColumn.name() != null && !joinColumn.name().isEmpty() ? joinColumn.name() : javaNameToDbName(field.getName()) + "_ID");
				} else {
					int i = 0;
					for (JoinColumn jc : joinColumns.value()) {
						dbColumnNames.add(jc.name());
						if (i > 0) {
							dbColumnNamesToSkipJoinColumns.add(jc.name());
						}
						i++;
					}
				}
			}

			for (String dbColumnName : dbColumnNames) {
				if (dbColumnName == null || dbColumnName.isEmpty()) {
					errors.getMessages().append("--Missing name in annotation Column/JoinColumn(s) for field '").append(field.getName()).append("' in class '").append(clazz.getSimpleName()).append("' \n");
					errors.addOtherErrors();
					continue;
				}

				query = "select data_type,char_length,data_precision,data_scale,nullable,char_used from " + dbPrefix + "ALL_TAB_COLUMNS_VIEW "
						+ "where table_name='" + tableName + "' and column_name='" + dbColumnName.toUpperCase() + "'";
				nativeQuery = entityManager.createNativeQuery(query);
				Object[] result;
				try {
					result = (Object[]) nativeQuery.getSingleResult();
				} catch (Exception e) {
					errors.getMessages().append("--Column '").append(dbColumnName).append("' does not exist in table '").append(tableName).append("' (case sensitive): ").append(query).append(" \n");
					errors.addOtherErrors();
					continue;
				}

				String dataType = (String) result[0];
				BigInteger charLength = getBigInteger(result[1]);
				BigInteger dataPrecision = getBigInteger(result[2]);
				BigInteger dataScale = getBigInteger(result[3]);
				String nullable = (String) result[4];
				String charUsed = (String) result[5];

				boolean primaryKey = field.isAnnotationPresent(Id.class);

				TableColumn tableColumn = new TableColumn();
				tableColumn.setCharUsed(charUsed);
				tableColumn.setDataType(dataType);
				tableColumn.setCharLength(charLength);
				tableColumn.setDataPrecision(dataPrecision);
				tableColumn.setDataScale(dataScale);
				tableColumn.setNullable(nullable);
				tableColumn.setColumn(column);
				tableColumn.setManyToOne(manyToOne);
				tableColumn.setOneToOne(oneToOne);
				tableColumn.setField(field);
				tableColumn.setSize(size);
				tableColumn.setMax(max);
				tableColumn.setJoinColumn(joinColumn);
				tableColumn.setJoinColumns(joinColumns);
				tableColumn.setSkipJoinColumns(dbColumnNamesToSkipJoinColumns.contains(dbColumnName));
				tableData.getTableColumns().add(tableColumn);

				if (primaryKey) {
					tableData.setPrimaryColumn(tableColumn);
				}
			}
		}
	}

	private List<Field> getAllFields(Class clazz) throws SecurityException {
		List<Field> declaredFields = new ArrayList<Field>();
		Class subClass = clazz;

		while (subClass != null && subClass != Object.class) {
			declaredFields.addAll(Arrays.asList(subClass.getDeclaredFields()));
			subClass = subClass.getSuperclass();
		}
		return declaredFields;
	}

	private Field getField(Class clazz, String name) throws NoSuchFieldException, SecurityException {
		Class subClass = clazz;

		while (subClass != null && subClass != Object.class) {
			try {
				return subClass.getDeclaredField(name);
			} catch (NoSuchFieldException ex) {
				subClass = subClass.getSuperclass();
			}
		}

		throw new NoSuchFieldException();
	}

	private BigInteger getBigInteger(Object o) {
		if (o instanceof BigDecimal) {
			return ((BigDecimal) o).toBigInteger();
		}
		return (BigInteger) o;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public List<String> getPackagesToSkip() {
		return packagesToSkip;
	}

	public void setPackagesToSkip(List<String> packagesToSkip) {
		this.packagesToSkip = packagesToSkip;
	}

	private void testColumn(TableColumn tableColumn, TableData tableData) {
		if (tableColumn.getColumn() != null) {
			if (tableData.getObjectType() == ObjectType.TABLE && !tableData.isAuditTable()) {
				checkNullable(tableColumn.getColumn(), tableData.getTableName(), tableColumn.getNullable(), tableData.getClazz(), tableColumn.getField());
			}

			if ("VARCHAR2".equalsIgnoreCase(tableColumn.getDataType())) {
				if (varcharCharUsed && !"C".equals(tableColumn.getCharUsed()) && tableData.getObjectType() == ObjectType.TABLE) {
					errors.getMessages().append("--checkStringLength, Class: ").append(tableData.getClazz().getSimpleName()).append(", field: ").append(tableColumn.getField().getName()).append(", column: ").append(tableColumn.getColumn().name()).append(", used size in BYTE instead of CHAR.\n");
					errors.getMessages().append("ALTER TABLE ").append(tableData.getTableName()).append(" ADD (TMP VARCHAR2(").append(tableColumn.getColumn().length()).append(" CHAR));\n" + "UPDATE ").append(tableData.getTableName()).append(" SET TMP=").append(tableColumn.getColumn().name()).append(";\n" + "ALTER TABLE ").append(tableData.getTableName()).append(" DROP COLUMN ").append(tableColumn.getColumn().name()).append(";\n" + "ALTER TABLE ").append(tableData.getTableName()).append(" RENAME COLUMN TMP TO ").append(tableColumn.getColumn().name()).append(";\n");
					errors.addStringLengthErrors();
				}
				if (!tableColumn.getField().getType().isEnum()) {
					if (tableData.getObjectType() == ObjectType.TABLE) {
						checkStringLength(tableColumn.getField(), tableColumn.getColumn(), tableColumn.getSize(), tableColumn.getCharLength(), tableData);
					}
				} else {
					checkEnumLength(tableColumn.getField(), tableColumn.getCharLength(), tableData.getClazz(), tableData.getTableName());
				}
				if (tableData.getObjectType() == ObjectType.TABLE && !tableData.isAuditTable()) {
					checkEnumConstraints(tableData.getCheckConstraints(), tableColumn.getColumn(), tableColumn.getField(), tableData.getClazz(), tableData.getTableName(), tableColumn.getNullable());
				}
			}

			if (booleanCheck && tableColumn.getField().getType() == Boolean.class || tableColumn.getField().getType() == Boolean.TYPE) {
				if (tableData.getObjectType() == ObjectType.TABLE) {
					checkBooleanType(tableColumn.getDataType(), tableColumn.getDataPrecision(), tableColumn.getDataScale(), tableData.getClazz(), tableColumn.getField());
					//check constraint is required only when number is used
					if (!tableData.isAuditTable() && !tableColumn.getDataType().equalsIgnoreCase("BIT")) {
						checkBooleanConstraint(tableData.getCheckConstraints(), tableColumn.getColumn(), tableData.getClazz(), tableColumn.getField(), tableData.getTableName());
					}
				}
			}

			if (tableColumn.getField().getType() == Date.class) {
				checkDateType(tableColumn.getDataType(), tableData.getClazz(), tableColumn.getField());
			}

			if ("CLOB".equalsIgnoreCase(tableColumn.getDataType())) {
				checkClobType(tableColumn.getField(), tableData.getClazz(), tableColumn.getColumn());
			}

			if ("NUMBER".equalsIgnoreCase(tableColumn.getDataType())) {
				checkNumberType(tableColumn.getDataType(), tableData.getObjectType(), tableColumn.getColumn(), tableColumn.getMax(), tableColumn.getField(), tableData.getClazz(), tableColumn.getDataPrecision(), tableColumn.getDataScale(), tableData.getTableName(), tableData);
			}
		}

		if (tableColumn.getManyToOne() != null) {
			if (tableData.getObjectType() == ObjectType.TABLE && !tableData.isAuditTable()) {
				checkOptional(tableColumn.getJoinColumn(), tableColumn.getManyToOne(), null, tableColumn.getNullable(), tableColumn.getField(), tableData);

				if (foreignIndexCheck) {
					checkForeignConstraint(tables, foreignConstraints, indexes, tableColumn, tableData);
				}
			}
		}

		if (tableColumn.getOneToOne() != null && tableColumn.getOneToOne().mappedBy().isEmpty()) {
			if (tableData.getObjectType() == ObjectType.TABLE && !tableData.isAuditTable()) {
				checkOptional(tableColumn.getJoinColumn(), null, tableColumn.getOneToOne(), tableColumn.getNullable(), tableColumn.getField(), tableData);

				if (foreignIndexCheck) {
					checkForeignConstraint(tables, foreignConstraints, indexes, tableColumn, tableData);
				}
			}
		}
	}

	private void checkTriggers(TableData tableData) {
		String query;
		Query nativeQuery;
		if (/*clazz == Utilities.getBaseEntityClass(clazz) && */!tableData.getTableName().toUpperCase().endsWith("VIEW")/* && !ViewInterface.class.isAssignableFrom(clazz)*/ && !tableData.isGeneralization() && tableData.getObjectType() == ObjectType.TABLE) {

			Trigger existingTrigger = null;
			Sequence existingSequence = null;

			if (tableData.getPrimaryColumn() != null) {

				Field primaryField = tableData.getPrimaryColumn().getField();

				boolean hibernateSequence = primaryField.isAnnotationPresent(GeneratedValue.class) && ((GeneratedValue) primaryField.getAnnotation(GeneratedValue.class)).strategy() == GenerationType.AUTO;
				String sequenceName = primaryField.isAnnotationPresent(SequenceGenerator.class) && !primaryField.getAnnotation(SequenceGenerator.class).sequenceName().isEmpty() ? primaryField.getAnnotation(SequenceGenerator.class).sequenceName() : DbNamingConventions.createSequenceName(tableData.getTableName());

				if (sequencesCheck) {
					for (Sequence sequence : sequences) {
						if (hibernateSequence && "HIBERNATE_SEQUENCE".equalsIgnoreCase(sequence.getSequenceName())) {
							existingSequence = sequence;
							sequenceName = sequence.getSequenceName();
						} else if (sequenceName.equalsIgnoreCase(sequence.getSequenceName())) {
							existingSequence = sequence;
						}
					}
				}

				if (triggersCheck) {
					for (Trigger trigger : triggers) {
						if (tableData.getTableName().equalsIgnoreCase(trigger.getTableName()) && DbNamingConventions.createTriggerName(tableData.getTableName()).equalsIgnoreCase(trigger.getTriggerName())) {
							existingTrigger = trigger;
						}
					}
				}

				//kontrola sekvencí lze vypnout pomocí anotace
				SuppressWarningsJUnit suppressWarningsJUnit = tableData.getPrimaryColumn().getField().getAnnotation(SuppressWarningsJUnit.class);

				if (sequencesCheck && (suppressWarningsJUnit == null || !Arrays.asList(suppressWarningsJUnit.value()).contains(SuppressWarningsJUnit.SKIP_SEQUENCE_UNIT_TEST))) {
					if (existingSequence == null) {
						if (Number.class.isAssignableFrom(tableData.getPrimaryColumn().getField().getType())) {
							String create = "CREATE SEQUENCE \"" + sequenceName + "\";";
							errors.getMessages().append("--checkSequence, Class: ").append(tableData.getClazz().getSimpleName()).append(": \n").append(create).append("\n");
							errors.addTriggerErrors();
						}
					} else {
						try {
							query = "select max(x.id),v.last_number from " + dbPrefix + tableData.getTableName() + " x," + dbPrefix + "ALL_SEQUENCES_VIEW v where sequence_name='" + existingSequence.getSequenceName() + "' group by v.last_number";
							nativeQuery = entityManager.createNativeQuery(query);
							Object[] result = (Object[]) nativeQuery.getSingleResult();
							BigDecimal max = (BigDecimal) result[0];
							BigDecimal lastNumber = (BigDecimal) result[1];
							if (max != null && max.longValue() > lastNumber.longValue()) {
								errors.getMessages().append("--checkSequence, Class: ").append(tableData.getClazz().getSimpleName()).append(": ").append("sequence last number (").append(lastNumber.longValue()).append(") < max value (").append(max).append(")").append("\n");
								errors.addTriggerErrors();
							}
						} catch (NoResultException ex) {
							//v pořádku, nemá-li tabulka žádný záznam, pak nemůže být chyba v maximální hodnotě a sekvenci
						}
					}
				}

				//jednorázové přegenerování všech triggerů
//				if (existingTrigger != null) {
//					errors.getMessages().append("--dropTrigger, Class: ").append(tableData.getClazz().getSimpleName()).append(": \n").append("DROP TRIGGER \"" + existingTrigger.getTriggerName() + "\";").append("\n");
//					errors.addTriggerErrors();
//					existingTrigger = null;
//				}
				if (triggersCheck && existingTrigger == null) {
					String create = "CREATE OR REPLACE TRIGGER \"" + DbNamingConventions.createTriggerName(tableData.getTableName()) + "\" "
							+ "before "
							+ "  INSERT OR UPDATE ON " + tableData.getTableName() + " FOR EACH row "
							+ "begin "
							+ "  if inserting then "
							+ "    if :new.id is null then "
							+ "      select " + sequenceName + ".nextval into :new.id from DUAL; "
							+ "    end if; "
							+ "    if :new.created is null then "
							+ "      :new.created := sysdate; "
							+ "    end if; "
							+ "    if :new.creator is null then "
							+ "      :new.creator := 'SYSTEM'; "
							+ "    end if; "
							+ "  end if; "
							+ "  if updating then "
							+ "    if :new.modified is null then "
							+ "      :new.modified := sysdate; "
							+ "    end if; "
							+ "    if :new.modifier is null then "
							+ "      :new.modifier := 'SYSTEM'; "
							+ "    end if; "
							+ "  end if; "
							+ "end;\n/";
					errors.getMessages().append("--checkTrigger, Class: ").append(tableData.getClazz().getSimpleName()).append(": \n").append(create).append("\n");
					errors.addTriggerErrors();
				}
			} else {
				if (!tableData.isEmbeddableId()) {
					errors.getMessages().append("--checkSequence, Class: ").append(tableData.getClazz().getSimpleName()).append(": no primary column\n");
					errors.addTriggerErrors();
				}
			}
		}
	}

	private void checkGeneralization(TableData tableData) {
		if (foreignIndexCheck) {
			Class baseClass = tableData.getClazz();
			while (baseClass.isAnnotationPresent(PrimaryKeyJoinColumn.class)) {
				baseClass = baseClass.getSuperclass();
			}
			TableData baseTableData = null;
			for (TableData td : tables.values()) {
				if (td.getClazz() == baseClass) {
					baseTableData = td;
				}
			}
			if (baseTableData == null) {
				errors.getMessages().append("--neočekávaná chyba baseTableData " + tableData.getClazz() + ", " + baseClass);
				errors.addForeignErrors();
			}
			PrimaryKeyJoinColumn primaryKeyJoinColumn = (PrimaryKeyJoinColumn) tableData.getClazz().getAnnotation(PrimaryKeyJoinColumn.class);
			try {
				checkSingleConstraint(foreignConstraints, tableData.getTableName(), baseTableData.getTableName(), tableData.getClazz(), indexes, primaryKeyJoinColumn.name(), primaryKeyJoinColumn.referencedColumnName(), baseTableData.getPrimaryColumn().getField());
			} catch (NullPointerException e) {
				errors.getMessages().append("--neočekávaná chyba checkSingleConstraint " + tableData.getClazz() + ", " + baseClass);
				errors.addForeignErrors();
			}
		}
	}

	private boolean skipClassTesting(Class<?> clazz) {
		return clazz == null || clazz.isAnnotationPresent(Deprecated.class) || clazz.getPackage().equals(Index.class.getPackage());
	}

	private void printResult() {
		if (errors.getMessages().length() > 0) {
			errors.getMessages().append(" \nResult: \ntotalErrors=").append(errors.getNullableErrors() + errors.getStringLengthErrors() + errors.getEnumErrors()
					+ errors.getEnumLengthErrors() + errors.getClobErrors() + errors.getBooleanErrors() + errors.getDateErrors()
					+ errors.getNumberErrors() + errors.getOptionalErrors()
					+ errors.getForeignErrors() + errors.getTriggerErrors() + errors.getOtherErrors())//
					.append(" \n\nnullableErrors=").append(errors.getNullableErrors())//
					.append(" \nstringLengthErrors=").append(errors.getStringLengthErrors())//
					.append(" \nenumErrors=").append(errors.getEnumErrors())//
					.append(" \nenumLengthErrors=").append(errors.getEnumLengthErrors())//
					.append(" \nclobErrors=").append(errors.getClobErrors())//
					.append(" \nbooleanErrors=").append(errors.getBooleanErrors())//
					.append(" \ndateErrors=").append(errors.getDateErrors())//
					.append(" \nnumberErrors=").append(errors.getNumberErrors())//
					.append(" \noptionalErrors=").append(errors.getOptionalErrors())//
					.append(" \nforeignErrors=").append(errors.getForeignErrors())//
					.append(" \ntriggerErrors=").append(errors.getTriggerErrors())//
					.append(" \notherErrors=").append(errors.getOtherErrors());
		}
	}
}
