Databázové scripty (výtah z https://jira.ders.cz/browse/ZISO-1655)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

Create scripty
--------------------------------------------------------------------------------
- Slouží pro instalaci nové instance aplikace načisto. 
- Postup: spustí se create scripty a poté object scripty (viz dále).
- Pokud jsou dostupné create scripty pouze pro první verzi aplikace je třeba spustit i rozdílové change scripty (viz dále).
- Mohou obsahovat i dump s demo daty. Dump s demo daty je nutné pouštět až po komplentí instalaci db (create + [change] + object scripty).
- Cizí a primární klíče by měly začínat PK_ či FK_ a pokud možno by měly býti samopisné (když to vidim v logu, vím o co jde).

Change scripty
--------------------------------------------------------------------------------
- Slouží pro upgrade verze aplikace. 
- Postup: Zjistím, jakou mám předchozí verzi, spustím rozdílové change scripty a poté object scripty (viz dále).

Object scripty
--------------------------------------------------------------------------------
- Znovuvytvořitelné databázové objekty (triggery, balíčky, pohledy ...).
- Scripty se použijí při instalaci nové instance aplikace i povýšení verze aplikace - provádějí aktualizaci objektů stylem "drop and create".