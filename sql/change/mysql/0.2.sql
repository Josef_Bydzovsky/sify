-- sify framework update 0.2 for mysql
--------------------------------------------------------------------------------
-- WHENEVER SQLERROR EXIT FAILURE; TODO: mysql alternativa

-- role structure
ALTER TABLE core_role_composition CHANGE COLUMN source_role_id sub_role_id BIGINT(20) NOT NULL;
ALTER TABLE core_role_composition CHANGE COLUMN target_role_id superior_role_id BIGINT(20) NOT NULL;
ALTER TABLE core_role_structure CHANGE COLUMN child_id sub_role_id BIGINT(20) NOT NULL;
ALTER TABLE core_role_structure CHANGE COLUMN parent_id superior_role_id BIGINT(20) NOT NULL;
-- organization structure

ALTER TABLE core_role ADD (DESCRIPTION VARCHAR(255));

CREATE TABLE `core_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `attachment_type` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `content_guid` varchar(36) COLLATE utf8_czech_ci NOT NULL,
  `content_path` varchar(512) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(512) COLLATE utf8_czech_ci DEFAULT NULL,
  `encoding` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `filesize` bigint(20) NOT NULL,
  `mimetype` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `object_identifier` varchar(36) COLLATE utf8_czech_ci NOT NULL,
  `object_status` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `object_type` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `version_label` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `version_number` int(11) NOT NULL,
  `next_version_id` bigint(20) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fi0no2wd5u0igxvdgunf99csi` (`next_version_id`),
  KEY `FK_5j8khl29hn2hngej333wym7ui` (`parent_id`),
  CONSTRAINT `FK_5j8khl29hn2hngej333wym7ui` FOREIGN KEY (`parent_id`) REFERENCES `core_attachment` (`id`),
  CONSTRAINT `FK_fi0no2wd5u0igxvdgunf99csi` FOREIGN KEY (`next_version_id`) REFERENCES `core_attachment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE `core_workflow_entity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `object_identifier` varchar(36) COLLATE utf8_czech_ci NOT NULL,
  `object_type` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `wf_process_instance_id` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_88gyig712lyakladbyagpu3xv` (`object_identifier`,`object_type`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


CREATE TABLE `core_permission` (
  `discriminator` varchar(31) COLLATE utf8_czech_ci NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `can_delete` bit(1) NOT NULL,
  `can_read` bit(1) NOT NULL,
  `can_write` bit(1) NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `object_type` varchar(150) COLLATE utf8_czech_ci NOT NULL,
  `permission_for` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `wf_action_type` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `wf_button_key` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `wf_definition_id` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `wf_user_task` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `core_organization_id` bigint(20) DEFAULT NULL,
  `core_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ppatrfsgwbmxbtynxt6ru10tv` (`core_organization_id`),
  KEY `FK_fk8f07cyh6vr7gkvtg12vdjuy` (`core_user_id`),
  CONSTRAINT `FK_fk8f07cyh6vr7gkvtg12vdjuy` FOREIGN KEY (`core_user_id`) REFERENCES `core_user` (`id`),
  CONSTRAINT `FK_ppatrfsgwbmxbtynxt6ru10tv` FOREIGN KEY (`core_organization_id`) REFERENCES `core_organization` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE INDEX CORE_USER_ROLE_CORE_USER_ID_IX ON core_user_role (CORE_USER_ID);
CREATE INDEX CORE_USER_ROLE_CORE_ROLE_ID_IX ON core_user_role (CORE_ROLE_ID);
CREATE INDEX CORE_USER_CORE_ORGANIZATIO_IX ON core_user_role (CORE_ORGANIZATION_ID);
CREATE INDEX CORE_ROLE_SUPERIOR_ROLE_ID_IX ON core_role_structure (SUPERIOR_ROLE_ID);
CREATE INDEX CORE_ROLE_COMP_SUPER_RO_ID_IX ON core_role_composition (SUPERIOR_ROLE_ID);
CREATE INDEX CORE_ROLE_STRUC_SUB_ROLE_ID_IX ON core_role_structure (SUB_ROLE_ID);
CREATE INDEX CORE_ROLE_COMPO_SUB_ROLE_ID_IX ON core_role_composition (SUB_ROLE_ID);
CREATE INDEX CORE_PERSON_CORE_PERSON_ID_IX ON core_person_organization (CORE_PERSON_ID);
CREATE INDEX CORE_PERSO_CORE_ORGANIZATIO_IX ON core_person_organization (CORE_ORGANIZATION_ID);
CREATE INDEX CORE_PERMISSI_CORE_USER_ID_IX ON core_permission (CORE_USER_ID);
CREATE INDEX CORE_PERMISSI_CORE_ROLE_ID_IX ON core_permission (CORE_ROLE_ID);
CREATE INDEX CORE_PERM_CORE_ORGANIZATIO_IX ON core_permission (CORE_ORGANIZATION_ID);
CREATE INDEX CORE_ORGANIZATION_PARENT_ID_IX ON core_organization_structure (PARENT_ID);
CREATE INDEX CORE_ORGANIZATION_CHILD_ID_IX ON core_organization_structure (CHILD_ID);
CREATE INDEX CORE_CONTACT_CORE_PERSON_ID_IX ON core_contact (CORE_PERSON_ID);
CREATE INDEX CORE_ADDRESS_CORE_PERSON_ID_IX ON core_address (CORE_PERSON_ID);
CREATE INDEX CORE_ACL_OBJECT_OWNER_SID_IX ON core_acl_object_identity (OWNER_SID);
CREATE INDEX CORE_ACL_OBJ_PARENT_OBJECT_IX ON core_acl_object_identity (PARENT_OBJECT);
CREATE INDEX CORE_ACL_O_OBJECT_ID_CLASS_IX ON core_acl_object_identity (OBJECT_ID_CLASS);
CREATE INDEX CORE_ACL_ENTRY_sid_IX ON core_acl_entry (sid);
CREATE INDEX CORE_ACL_E_acl_object_iden_IX ON core_acl_entry (acl_object_identity);

CREATE TABLE `core_email_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `bcc` varchar(512) COLLATE utf8_czech_ci DEFAULT NULL,
  `cc` varchar(512) COLLATE utf8_czech_ci DEFAULT NULL,
  `email_from` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `encoding` varchar(25) COLLATE utf8_czech_ci NOT NULL,
  `message` longtext COLLATE utf8_czech_ci,
  `recipients` varchar(512) COLLATE utf8_czech_ci DEFAULT NULL,
  `sent` datetime DEFAULT NULL,
  `sent_log` varchar(2000) COLLATE utf8_czech_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `previous_version` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_41hyrmfbev23726tqjh55t9b0` (`parent_id`),
  KEY `FK_677mol32r5et9nbip9ql3xsjl` (`previous_version`),
  CONSTRAINT `FK_677mol32r5et9nbip9ql3xsjl` FOREIGN KEY (`previous_version`) REFERENCES `core_email_record` (`id`),
  CONSTRAINT `FK_41hyrmfbev23726tqjh55t9b0` FOREIGN KEY (`parent_id`) REFERENCES `core_email_record` (`id`)
) 

CREATE TABLE `core_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `body` longtext COLLATE utf8_czech_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

/*Example template TEST3*/
INSERT INTO `core_template` VALUES (3,NULL,NULL,'2015-08-13 15:33:04',NULL,0,'<h2 style=\"text-align: center;\"><u>Example email</u>&nbsp;</h2><h2 style=\"text-align: center;\">genereted&nbsp;<span style=\"color: inherit; line-height: 1.1;\">by Velocity&nbsp;</span></h2><h2 style=\"text-align: center;\"><span style=\"color: inherit; line-height: 1.1;\">and template with code \"TEST3\"</span></h2><div><h3 style=\"color: rgb(51, 51, 51);\"><b><br></b></h3><h3 style=\"color: rgb(51, 51, 51);\"><b>User name: $username</b></h3></div><h4><span style=\"color: rgb(119, 119, 119); font-size: 15px; line-height: 26.7856502532959px;\">This user was created: $date</span></h4><div><br></div><div><hr id=\"null\"></div><h3 style=\"text-align: left;\"><i style=\"color: inherit; line-height: 1.1;\">Variables from workflow test:</i><i style=\"color: inherit; line-height: 1.1;\">&nbsp;</i></h3><h3 style=\"text-align: left;\"><ul><li><b style=\"font-size: 12px; line-height: 1.78571;\">$var1</b><br></li><li><span style=\"font-size: 12px; line-height: 1.78571;\"><b><u>$var2</u></b></span></li></ul></h3><div></div><div><h3 style=\"text-align: center; color: rgb(51, 51, 51);\"><br></h3></div>','TEST3','test3');

---------------------------------------------------------------------------------------------------------------
-- Must be on end of the script. After is it applied no change may be add.
---------------------------------------------------------------------------------------------------------------
UPDATE core_app_setting SET value = '0.2-SNAPSHOT (row 147)', modified = now() WHERE setting_key = 'CORE_VERSION';
COMMIT;
