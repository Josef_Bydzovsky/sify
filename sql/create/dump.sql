insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'admin', 'administrator');
--password is "defaultpassword"
INSERT INTO CORE_USER (ID, CREATED, CREATOR, VERSION, PASSWORD, USERNAME) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', '$2a$10$1Jy.xKaSDe.d5eGN0AUzz.p0ASZmxLo/mvR/h0SY.T5miMc9kZg/m', 'admin');
INSERT INTO CORE_USER_ROLE (ID, CREATED, CREATOR, VERSION, CORE_USER_ID, CORE_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_user where username='admin'), (select id from core_role where name='admin'));
